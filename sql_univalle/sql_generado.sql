
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'banda'
-- 
-- ---

DROP TABLE IF EXISTS `banda`;
		
CREATE TABLE `banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `frecuencia_inicial` DECIMAL NOT NULL,
  `frecuencia_final` DECIMAL NOT NULL,
  PRIMARY KEY (`id_banda`)
);

-- ---
-- Table 'canal'
-- 
-- ---

DROP TABLE IF EXISTS `canal`;
		
CREATE TABLE `canal` (
  `id_canal` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `frecuencia_inicial` DECIMAL NOT NULL,
  `frecuencia_final` DECIMAL NOT NULL,
  `id_banda` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id_canal`)
);

-- ---
-- Table 'servicio'
-- 
-- ---

DROP TABLE IF EXISTS `servicio`;
		
CREATE TABLE `servicio` (
  `id_servicio` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NOT NULL,
  `tipo` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_servicio`)
);

-- ---
-- Table 'municipio'
-- 
-- ---

DROP TABLE IF EXISTS `municipio`;
		
CREATE TABLE `municipio` (
  `id_municipio` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `id_departamento` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_municipio`)
);

-- ---
-- Table 'departamento'
-- 
-- ---

DROP TABLE IF EXISTS `departamento`;
		
CREATE TABLE `departamento` (
  `id_departamento` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `id_region` VARCHAR(100) NOT NULL DEFAULT 'Default',
  PRIMARY KEY (`id_departamento`),
KEY (`id_departamento`)
);

-- ---
-- Table 'notas_nacionales'
-- 
-- ---

DROP TABLE IF EXISTS `notas_nacionales`;
		
CREATE TABLE `notas_nacionales` (
  `id_nota` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `descripcion` VARCHAR(6000) NOT NULL,
  PRIMARY KEY (`id_nota`)
);

-- ---
-- Table 'notas_internacionales'
-- 
-- ---

DROP TABLE IF EXISTS `notas_internacionales`;
		
CREATE TABLE `notas_internacionales` (
  `id_nota_internacional` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `descripcion` VARCHAR(6000) NOT NULL,
  PRIMARY KEY (`id_nota_internacional`)
);

-- ---
-- Table 'servicios_banda'
-- 
-- ---

DROP TABLE IF EXISTS `servicios_banda`;
		
CREATE TABLE `servicios_banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `id_servicio` INTEGER(1) NOT NULL,
  PRIMARY KEY ()
);

-- ---
-- Table 'notas_internacionales_banda'
-- 
-- ---

DROP TABLE IF EXISTS `notas_internacionales_banda`;
		
CREATE TABLE `notas_internacionales_banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `id_nota_internacional` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_banda`, `id_nota_internacional`)
);

-- ---
-- Table 'notas_banda'
-- 
-- ---

DROP TABLE IF EXISTS `notas_banda`;
		
CREATE TABLE `notas_banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `id_nota` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_nota`, `id_banda`)
);

-- ---
-- Table 'operador'
-- 
-- ---

DROP TABLE IF EXISTS `operador`;
		
CREATE TABLE `operador` (
  `id_operador` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(300) NOT NULL,
  `descripcion` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id_operador`)
);

-- ---
-- Table 'licencia_asignacion'
-- 
-- ---

DROP TABLE IF EXISTS `licencia_asignacion`;
		
CREATE TABLE `licencia_asignacion` (
  `id_licencia` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `fecha_vencimiento` DATE NOT NULL,
  `fecha_inicio` DATE NOT NULL,
  `id_canal` INTEGER(1) NOT NULL,
  `id_servicio` INTEGER(1) NOT NULL,
  `id_operador` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_licencia`)
);

-- ---
-- Table 'formula'
-- 
-- ---

DROP TABLE IF EXISTS `formula`;
		
CREATE TABLE `formula` (
  `id_formula` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `formula` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id_formula`)
);

-- ---
-- Table 'formula_banda'
-- 
-- ---

DROP TABLE IF EXISTS `formula_banda`;
		
CREATE TABLE `formula_banda` (
  `id_formula` INTEGER(1) NOT NULL,
  `id_banda` VARCHAR(50) NOT NULL,
  PRIMARY KEY ()
);

-- ---
-- Table 'rango_frecuencias'
-- 
-- ---

DROP TABLE IF EXISTS `rango_frecuencias`;
		
CREATE TABLE `rango_frecuencias` (
  `id_frecuencias` VARCHAR(20) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `frecuencia_inicial` DECIMAL NOT NULL,
  `frecuencia_final` VARCHAR NOT NULL,
  PRIMARY KEY (`id_frecuencias`)
);

-- ---
-- Table 'region'
-- 
-- ---

DROP TABLE IF EXISTS `region`;
		
CREATE TABLE `region` (
  `id_region` VARCHAR(100) NOT NULL DEFAULT 'Default',
  PRIMARY KEY (`id_region`)
);

-- ---
-- Table 'licencia_municipal'
-- 
-- ---

DROP TABLE IF EXISTS `licencia_municipal`;
		
CREATE TABLE `licencia_municipal` (
  `id_licencia` INTEGER(1) NOT NULL,
  `id_municipio` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_licencia`, `id_municipio`)
);

-- ---
-- Table 'servicios_operador'
-- 
-- ---

DROP TABLE IF EXISTS `servicios_operador`;
		
CREATE TABLE `servicios_operador` (
  `id_operador` INTEGER(1) NOT NULL,
  `id_servicio` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_operador`, `id_servicio`)
);

-- ---
-- Table 'licencia_departamental'
-- 
-- ---

DROP TABLE IF EXISTS `licencia_departamental`;
		
CREATE TABLE `licencia_departamental` (
  `id_licencia` INTEGER(1) NOT NULL,
  `id_departamento` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_licencia`, `id_departamento`)
);

-- ---
-- Table 'licencia_nacional'
-- 
-- ---

DROP TABLE IF EXISTS `licencia_nacional`;
		
CREATE TABLE `licencia_nacional` (
  `id_licencia` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_licencia`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `canal` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `municipio` ADD FOREIGN KEY (id_departamento) REFERENCES `departamento` (`id_departamento`);
ALTER TABLE `departamento` ADD FOREIGN KEY (id_region) REFERENCES `region` (`id_region`);
ALTER TABLE `servicios_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `servicios_banda` ADD FOREIGN KEY (id_servicio) REFERENCES `servicio` (`id_servicio`);
ALTER TABLE `notas_internacionales_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `notas_internacionales_banda` ADD FOREIGN KEY (id_nota_internacional) REFERENCES `notas_internacionales` (`id_nota_internacional`);
ALTER TABLE `notas_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `notas_banda` ADD FOREIGN KEY (id_nota) REFERENCES `notas_nacionales` (`id_nota`);
ALTER TABLE `licencia_asignacion` ADD FOREIGN KEY (id_canal) REFERENCES `canal` (`id_canal`);
ALTER TABLE `licencia_asignacion` ADD FOREIGN KEY (id_servicio) REFERENCES `servicio` (`id_servicio`);
ALTER TABLE `licencia_asignacion` ADD FOREIGN KEY (id_operador) REFERENCES `operador` (`id_operador`);
ALTER TABLE `formula_banda` ADD FOREIGN KEY (id_formula) REFERENCES `formula` (`id_formula`);
ALTER TABLE `formula_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `licencia_municipal` ADD FOREIGN KEY (id_licencia) REFERENCES `licencia_asignacion` (`id_licencia`);
ALTER TABLE `licencia_municipal` ADD FOREIGN KEY (id_municipio) REFERENCES `municipio` (`id_municipio`);
ALTER TABLE `servicios_operador` ADD FOREIGN KEY (id_operador) REFERENCES `operador` (`id_operador`);
ALTER TABLE `servicios_operador` ADD FOREIGN KEY (id_servicio) REFERENCES `servicio` (`id_servicio`);
ALTER TABLE `licencia_departamental` ADD FOREIGN KEY (id_licencia) REFERENCES `licencia_asignacion` (`id_licencia`);
ALTER TABLE `licencia_departamental` ADD FOREIGN KEY (id_departamento) REFERENCES `departamento` (`id_departamento`);
ALTER TABLE `licencia_nacional` ADD FOREIGN KEY (id_licencia) REFERENCES `licencia_asignacion` (`id_licencia`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `banda` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `canal` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `servicio` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `municipio` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `departamento` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `notas_nacionales` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `notas_internacionales` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `servicios_banda` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `notas_internacionales_banda` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `notas_banda` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `operador` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `licencia_asignacion` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `formula` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `formula_banda` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `rango_frecuencias` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `region` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `licencia_municipal` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `servicios_operador` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `licencia_departamental` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `licencia_nacional` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `banda` (`id_banda`,`frecuencia_inicial`,`frecuencia_final`) VALUES
-- ('','','');
-- INSERT INTO `canal` (`id_canal`,`frecuencia_inicial`,`frecuencia_final`,`id_banda`) VALUES
-- ('','','','');
-- INSERT INTO `servicio` (`id_servicio`,`nombre`,`tipo`) VALUES
-- ('','','');
-- INSERT INTO `municipio` (`id_municipio`,`nombre`,`id_departamento`) VALUES
-- ('','','');
-- INSERT INTO `departamento` (`id_departamento`,`nombre`,`id_region`) VALUES
-- ('','','');
-- INSERT INTO `notas_nacionales` (`id_nota`,`nombre`,`descripcion`) VALUES
-- ('','','');
-- INSERT INTO `notas_internacionales` (`id_nota_internacional`,`nombre`,`descripcion`) VALUES
-- ('','','');
-- INSERT INTO `servicios_banda` (`id_banda`,`id_servicio`) VALUES
-- ('','');
-- INSERT INTO `notas_internacionales_banda` (`id_banda`,`id_nota_internacional`) VALUES
-- ('','');
-- INSERT INTO `notas_banda` (`id_banda`,`id_nota`) VALUES
-- ('','');
-- INSERT INTO `operador` (`id_operador`,`nombre`,`descripcion`) VALUES
-- ('','','');
-- INSERT INTO `licencia_asignacion` (`id_licencia`,`fecha_vencimiento`,`fecha_inicio`,`id_canal`,`id_servicio`,`id_operador`) VALUES
-- ('','','','','','');
-- INSERT INTO `formula` (`id_formula`,`formula`) VALUES
-- ('','');
-- INSERT INTO `formula_banda` (`id_formula`,`id_banda`) VALUES
-- ('','');
-- INSERT INTO `rango_frecuencias` (`id_frecuencias`,`nombre`,`frecuencia_inicial`,`frecuencia_final`) VALUES
-- ('','','','');
-- INSERT INTO `region` (`id_region`) VALUES
-- ('');
-- INSERT INTO `licencia_municipal` (`id_licencia`,`id_municipio`) VALUES
-- ('','');
-- INSERT INTO `servicios_operador` (`id_operador`,`id_servicio`) VALUES
-- ('','');
-- INSERT INTO `licencia_departamental` (`id_licencia`,`id_departamento`) VALUES
-- ('','');
-- INSERT INTO `licencia_nacional` (`id_licencia`) VALUES
-- ('');
