DROP DATABASE IF EXISTS univalle;
CREATE DATABASE univalle;

-- ---
-- Table 'banda'
-- 
-- ---

DROP TABLE IF EXISTS `banda`;
		
CREATE TABLE `banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `frecuencia_inicial` DECIMAL NOT NULL,
  `frecuencia_final` DECIMAL NOT NULL,
  PRIMARY KEY (`id_banda`)
);

-- ---
-- Table 'canal'
-- 
-- ---

DROP TABLE IF EXISTS `canal`;
		
CREATE TABLE `canal` (
  `id_canal` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `frecuencia_inicial` DECIMAL NOT NULL,
  `frecuencia_final` DECIMAL NOT NULL,
  `id_banda` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id_canal`)
);

-- ---
-- Table 'servicio'
-- 
-- ---

DROP TABLE IF EXISTS `servicio`;
		
CREATE TABLE `servicio` (
  `id_servicio` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NOT NULL,
  `tipo` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_servicio`)
);

-- ---
-- Table 'municipio'
-- 
-- ---

DROP TABLE IF EXISTS `municipio`;
		
CREATE TABLE `municipio` (
  `id_municipio` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `id_departamento` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_municipio`)
);

-- ---
-- Table 'departamento'
-- 
-- ---

DROP TABLE IF EXISTS `departamento`;
		
CREATE TABLE `departamento` (
  `id_departamento` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_departamento`),
KEY (`id_departamento`)
);

-- ---
-- Table 'notas_nacionales'
-- 
-- ---

DROP TABLE IF EXISTS `notas_nacionales`;
		
CREATE TABLE `notas_nacionales` (
  `id_nota` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `descripcion` VARCHAR(6000) NOT NULL,
  PRIMARY KEY (`id_nota`)
);

-- ---
-- Table 'recomendaciones'
-- 
-- ---

DROP TABLE IF EXISTS `recomendaciones`;
		
CREATE TABLE `recomendaciones` (
  `id_recomendacion` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `descripcion` VARCHAR(6000) NOT NULL,
  PRIMARY KEY (`id_recomendacion`)
);

-- ---
-- Table 'servicios_banda'
-- 
-- ---

DROP TABLE IF EXISTS `servicios_banda`;
		
CREATE TABLE `servicios_banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `id_servicio` INTEGER(1) NOT NULL,
  PRIMARY KEY ()
);

-- ---
-- Table 'banda_recomendacion'
-- 
-- ---

DROP TABLE IF EXISTS `banda_recomendacion`;
		
CREATE TABLE `banda_recomendacion` (
  `id_banda` VARCHAR(50) NOT NULL,
  `id_recomendacion` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_banda`, `id_recomendacion`)
);

-- ---
-- Table 'banda_nota'
-- 
-- ---

DROP TABLE IF EXISTS `notas_banda`;
		
CREATE TABLE `notas_banda` (
  `id_banda` VARCHAR(50) NOT NULL,
  `id_nota` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_nota`, `id_banda`)
);

-- ---
-- Table 'operador'
-- 
-- ---

DROP TABLE IF EXISTS `operador`;
		
CREATE TABLE `operador` (
  `id_operador` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(300) NOT NULL,
  `descripcion` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id_operador`)
);

-- ---
-- Table 'licencia_asignacion'
-- 
-- ---

DROP TABLE IF EXISTS `licencia_asignacion`;
		
CREATE TABLE `licencia_asignacion` (
  `id_licencia` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `fecha_vencimiento` DATE NOT NULL,
  `fecha_inicio` DATE NOT NULL,
  `id_canal` INTEGER(1) NOT NULL,
  `id_operador` INTEGER(1) NOT NULL,
  `id_municipio` INTEGER(1) NOT NULL,
  PRIMARY KEY (`id_licencia`)
);

-- ---
-- Table 'formula'
-- 
-- ---

DROP TABLE IF EXISTS `formula`;
		
CREATE TABLE `formula` (
  `id_formula` INTEGER(1) NOT NULL AUTO_INCREMENT,
  `formula` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id_formula`)
);

-- ---
-- Table 'formula_banda'
-- 
-- ---

DROP TABLE IF EXISTS `formula_banda`;
		
CREATE TABLE `formula_banda` (
  `id_formula` INTEGER(1) NOT NULL,
  `id_banda` VARCHAR(50) NOT NULL,
  PRIMARY KEY ()
);

-- ---
-- Table 'rango_frecuencias'
-- 
-- ---

DROP TABLE IF EXISTS `rango_frecuencias`;
		
CREATE TABLE `rango_frecuencias` (
  `id_frecuencias` VARCHAR(20) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `frecuencia_inicial` DECIMAL NOT NULL,
  `frecuencia_final` VARCHAR NOT NULL,
  PRIMARY KEY (`id_frecuencias`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `canal` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `municipio` ADD FOREIGN KEY (id_departamento) REFERENCES `departamento` (`id_departamento`);
ALTER TABLE `servicios_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `servicios_banda` ADD FOREIGN KEY (id_servicio) REFERENCES `servicio` (`id_servicio`);
ALTER TABLE `banda_recomendacion` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `banda_recomendacion` ADD FOREIGN KEY (id_recomendacion) REFERENCES `recomendaciones` (`id_recomendacion`);
ALTER TABLE `notas_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);
ALTER TABLE `notas_banda` ADD FOREIGN KEY (id_nota) REFERENCES `notas_nacionales` (`id_nota`);
ALTER TABLE `licencia_asignacion` ADD FOREIGN KEY (id_canal) REFERENCES `canal` (`id_canal`);
ALTER TABLE `licencia_asignacion` ADD FOREIGN KEY (id_operador) REFERENCES `operador` (`id_operador`);
ALTER TABLE `licencia_asignacion` ADD FOREIGN KEY (id_municipio) REFERENCES `municipio` (`id_municipio`);
ALTER TABLE `formula_banda` ADD FOREIGN KEY (id_formula) REFERENCES `formula` (`id_formula`);
ALTER TABLE `formula_banda` ADD FOREIGN KEY (id_banda) REFERENCES `banda` (`id_banda`);



