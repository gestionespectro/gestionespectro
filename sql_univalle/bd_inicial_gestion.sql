--Tabla con información el rango de frecuencias en que en que se divide el espectro (VLF, LF,MF..)

DROP TABLE rango_frecuencias CASCADE;
CREATE TABLE rango_frecuencias(
id_frecuencias VARCHAR(20) NOT NULL,
nombre VARCHAR(100) NOT NULL,
frecuencia_inicial DECIMAL NOT NULL,
frecuencia_final DECIMAL NOT NULL,
longitud_onda_inicial DECIMAL NOT NULL,
longitud_onda_final DECIMAL NOT NULL
);

ALTER TABLE rango_frecuencias ADD CONSTRAINT rango_frecuencias_pk PRIMARY KEY(id_frecuencias);

--Tabla con información sobre notas internacionales 

DROP SEQUENCE nota_internacional_sq CASCADE;
CREATE SEQUENCE nota_internacional_sq
	INCREMENT 1
	MINVALUE 1
	START 1;

DROP TABLE nota_internacional CASCADE;
CREATE TABLE nota_internacional(
id_nota_internacional INTEGER DEFAULT NEXTVAL('nota_internacional_sq'),
nombre VARCHAR(20) NOT NULL,
descripcion VARCHAR(6000) NOT NULL
);

ALTER TABLE nota_internacional ADD CONSTRAINT nota_internacional_pk PRIMARY KEY(id_nota_internacional);

--Tabla con información sobre notas nacionales 

DROP SEQUENCE nota_nacional_sq CASCADE;
CREATE SEQUENCE nota_nacional_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE nota_nacional CASCADE;
CREATE TABLE nota_nacional (
id_nota_nacional INTEGER DEFAULT NEXTVAL('nota_nacional_sq'),
nombre VARCHAR(20) NOT NULL,
descripcion VARCHAR(6000) NOT NULL
);

ALTER TABLE nota_nacional ADD CONSTRAINT nota_nacional_pk PRIMARY KEY(id_nota_nacional);

--Tabla con información sobre las bandas

DROP SEQUENCE banda_sq CASCADE;
CREATE SEQUENCE banda_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE banda CASCADE;
CREATE TABLE banda (
id_banda INTEGER DEFAULT NEXTVAL('banda_sq'),
nombre VARCHAR(50) NOT NULL,
frecuencia_inicial DECIMAL NOT NULL,
frecuencia_final DECIMAL NOT NULL
);

ALTER TABLE banda ADD CONSTRAINT banda_pk PRIMARY KEY(id_banda);

--Tabla relacionada con información de las bandas y notas nacionales asociadas
DROP TABLE nota_nacional_banda CASCADE;
CREATE TABLE nota_nacional_banda (
id_banda INTEGER NOT NULL,
id_nota_nacional INTEGER NOT NULL
);

ALTER TABLE nota_nacional_banda ADD CONSTRAINT nota_nacional_banda_pk PRIMARY KEY (id_banda, id_nota_nacional);
ALTER TABLE nota_nacional_banda ADD CONSTRAINT nota_nacional_banda_fk_1 FOREIGN KEY (id_nota_nacional) REFERENCES nota_nacional(id_nota_nacional);
ALTER TABLE nota_nacional_banda ADD CONSTRAINT nota_nacional_banda_fk_2 FOREIGN KEY (id_banda) REFERENCES banda(id_banda);

--Tabla relacionada con información de las bandas y notas internacionales asociadas
DROP TABLE nota_internacional_banda CASCADE;
CREATE TABLE nota_internacional_banda (
id_banda INTEGER NOT NULL,
id_nota_internacional INTEGER NOT NULL
);

ALTER TABLE nota_internacional_banda ADD CONSTRAINT nota_internacional_banda_pk PRIMARY KEY (id_banda, id_nota_internacional);
ALTER TABLE nota_internacional_banda ADD CONSTRAINT nota_internacional_banda_fk_1 FOREIGN KEY (id_nota_internacional) REFERENCES nota_internacional(id_nota_internacional);
ALTER TABLE nota_internacional_banda ADD CONSTRAINT nota_internacional_banda_fk_2 FOREIGN KEY (id_banda) REFERENCES banda(id_banda);

--Tabla con información de los servicios del espectro.

DROP SEQUENCE servicio_sq CASCADE;
CREATE SEQUENCE servicio_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	

DROP TABLE servicio CASCADE;
CREATE TABLE servicio (
id_servicio INTEGER DEFAULT NEXTVAL('servicio_sq'),
nombre VARCHAR(200) UNIQUE NOT NULL,
tipo VARCHAR(20) NOT NULL
);

ALTER TABLE servicio ADD CONSTRAINT servicio_pk PRIMARY KEY (id_servicio);


--Tabla con la información de bandas y servicios relacionados
DROP TABLE servicio_banda CASCADE;
CREATE TABLE servicio_banda(
id_banda INTEGER NOT NULL,
id_servicio INTEGER NOT NULL
);

ALTER TABLE servicio_banda ADD CONSTRAINT servicio_banda_pk PRIMARY KEY (id_banda, id_servicio);
ALTER TABLE servicio_banda ADD CONSTRAINT servicio_banda_fk_1 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);
ALTER TABLE servicio_banda ADD CONSTRAINT servicio_banda_fk_2 FOREIGN KEY (id_banda) REFERENCES banda(id_banda);

--Tabla con la información de regiones
DROP SEQUENCE region_sq CASCADE;
CREATE SEQUENCE region_sq
	INCREMENT 1
	MINVALUE 1
	START 1;

DROP TABLE region CASCADE;
CREATE TABLE region(
id_region INTEGER DEFAULT NEXTVAL('region_sq'),
nombre VARCHAR(100) NOT NULL UNIQUE DEFAULT 'Default'
);

ALTER TABLE region ADD CONSTRAINT region_pk PRIMARY KEY (id_region);

--Tabla con la información de departamentos
DROP SEQUENCE departamento_sq CASCADE;
CREATE SEQUENCE departamento_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE departamento CASCADE;
CREATE TABLE departamento(
id_departamento INTEGER DEFAULT NEXTVAL('departamento_sq'),
nombre VARCHAR(100) NOT NULL,
id_region INTEGER NOT NULL
);

ALTER TABLE departamento ADD CONSTRAINT departamento_pk PRIMARY KEY (id_departamento);
ALTER TABLE departamento ADD CONSTRAINT departamento_fk FOREIGN KEY (id_region) REFERENCES region(id_region);

--Tabla relacionada con los municipios
DROP SEQUENCE municipio_sq CASCADE;
CREATE SEQUENCE municipio_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE municipio CASCADE;
CREATE TABLE municipio(
id_municipio INTEGER DEFAULT NEXTVAL('municipio_sq'),
nombre VARCHAR(100) NOT NULL,
id_departamento INTEGER NOT NULL
);

ALTER TABLE municipio ADD CONSTRAINT municipio_pk PRIMARY KEY (id_municipio);
ALTER TABLE municipio ADD CONSTRAINT municipio_fk FOREIGN KEY (id_departamento) REFERENCES departamento(id_departamento);

--Tabla relacionada con las formulas (definicion de canales)
DROP SEQUENCE formula_sq CASCADE;
CREATE SEQUENCE formula_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE formula CASCADE;
CREATE TABLE formula (
id_formula INTEGER DEFAULT NEXTVAL('formula_sq'),
formula VARCHAR(100) NOT NULL,
numero_canales INTEGER NOT NULL,
frecuencia_inicial DECIMAL NOT NULL,
multiplicador DECIMAL NOT NULL
);

ALTER TABLE formula ADD CONSTRAINT formula_pk PRIMARY KEY (id_formula);

--Tabla con la informacion de las formulas de los servicios (relaciones)
DROP TABLE formula_servicio CASCADE;
CREATE TABLE formula_servicio(
id_formula INTEGER NOT NULL,
id_servicio INTEGER NOT NULL
);

ALTER TABLE formula_servicio ADD CONSTRAINT formula_servicio_pk PRIMARY KEY (id_formula,id_servicio);
ALTER TABLE formula_servicio ADD CONSTRAINT formula_servicio_fk_1 FOREIGN KEY (id_formula) REFERENCES formula(id_formula);
ALTER TABLE formula_servicio ADD CONSTRAINT formula_servicio_fk_2 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);

--Tabla con la información de operadores
DROP SEQUENCE operador_sq CASCADE;
CREATE SEQUENCE operador_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE operador CASCADE;
CREATE TABLE operador (
id_operador INTEGER DEFAULT NEXTVAL('operador_sq'),
nombre VARCHAR(100) NOT NULL,
descripcion VARCHAR(2000) NOT NULL
);

ALTER TABLE operador ADD CONSTRAINT operador_pk PRIMARY KEY (id_operador);

--Tabla que relaciona los operadores con los servicios que ofrecen.
DROP TABLE servicio_operador CASCADE;
CREATE TABLE servicio_operador (
id_operador INTEGER NOT NULL,
id_servicio INTEGER NOT NULL
);

ALTER TABLE servicio_operador ADD CONSTRAINT servicio_operador_pk PRIMARY KEY(id_operador, id_servicio);
ALTER TABLE servicio_operador ADD CONSTRAINT servicio_operador_fk_1 FOREIGN KEY (id_operador) REFERENCES operador(id_operador);
ALTER TABLE servicio_operador ADD CONSTRAINT servicio_operador_fk_2 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);

--Tabla que contiene horarios de prestación de un servicio.
DROP SEQUENCE horario_sq CASCADE;
CREATE SEQUENCE horario_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE horario CASCADE;
CREATE TABLE horario (
id_horario INTEGER DEFAULT NEXTVAL('horario_sq'),
hora_inicio INTEGER NOT NULL,
minuto_inicio INTEGER NOT NULL,
hora_fin INTEGER NOT NULL,
minuto_fin INTEGER NOT NULL
);

ALTER TABLE horario ADD CONSTRAINT horario_pk PRIMARY KEY(id_horario);

--Tabla que contiene la información de las licencias
DROP SEQUENCE licencia_sq CASCADE;
CREATE SEQUENCE licencia_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE licencia CASCADE;
CREATE TABLE licencia(
id_licencia INTEGER DEFAULT NEXTVAL('licencia_sq'),
descripcion VARCHAR(2000) NOT NULL,
fecha_inicio DATE NOT NULL,
fecha_vencimiento DATE NOT NULL,
id_operador INTEGER NOT NULL
);

ALTER TABLE licencia ADD CONSTRAINT licencia_pk PRIMARY KEY(id_licencia);
ALTER TABLE licencia ADD CONSTRAINT licencia_fk FOREIGN KEY(id_operador) REFERENCES operador(id_operador);

--Tabla que relaciona licencias con servicios en un horario.
DROP TABLE servicio_licencia CASCADE;
CREATE TABLE servicio_licencia (
id_servicio INTEGER NOT NULL,
id_licencia INTEGER NOT NULL,
id_horario INTEGER NOT NULL
);

ALTER TABLE servicio_licencia ADD CONSTRAINT servicio_licencia_pk PRIMARY KEY (id_servicio, id_licencia);
ALTER TABLE servicio_licencia ADD CONSTRAINT servicio_licencia_fk_1 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);
ALTER TABLE servicio_licencia ADD CONSTRAINT servicio_licencia_fk_2 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);
ALTER TABLE servicio_licencia ADD CONSTRAINT servicio_licencia_fk_3 FOREIGN KEY (id_horario) REFERENCES horario(id_horario);

--Tabla relacionada con la información de los canales
DROP SEQUENCE canal_sq CASCADE;
CREATE SEQUENCE canal_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE canal CASCADE;
CREATE TABLE canal (
id_canal INTEGER DEFAULT NEXTVAL('canal_sq'),
no_canal INTEGER NOT NULL,
frecuencia_inicial DECIMAL NOT NULL,
frecuencia_final DECIMAL NOT NULL,
id_servicio INTEGER NOT NULL
);

ALTER TABLE canal ADD CONSTRAINT canal_pk PRIMARY KEY (id_canal);
ALTER TABLE canal ADD CONSTRAINT canal_fk FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);

--Tabla relacionada con la información de licencias y sus canales
DROP TABLE canal_licencia CASCADE;
CREATE TABLE canal_licencia (
id_canal INTEGER NOT NULL,
id_licencia INTEGER NOT NULL
);

ALTER TABLE canal_licencia ADD CONSTRAINT canal_licencia_pk PRIMARY KEY (id_canal, id_licencia);
ALTER TABLE canal_licencia ADD CONSTRAINT canal_licencia_fk_1 FOREIGN KEY (id_canal) REFERENCES canal(id_canal);
ALTER TABLE canal_licencia ADD CONSTRAINT canal_licencia_fk_2 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);

--Tabla que contine informacion de comentarios a canales.
DROP SEQUENCE comentario_sq CASCADE;
CREATE SEQUENCE comentario_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE comentario CASCADE;
CREATE TABLE comentario (
id_comentario INTEGER DEFAULT NEXTVAL('comentario_sq'),
descripcion VARCHAR(1000) NOT NULL
);

ALTER TABLE comentario ADD CONSTRAINT comentario_pk PRIMARY KEY (id_comentario);

--Tabla que contien las relaciones entre comentarios y canales
DROP TABLE comentario_canal CASCADE;
CREATE TABLE comentario_canal (
id_comentario INTEGER NOT NULL,
id_canal INTEGER NOT NULL
);

ALTER TABLE comentario_canal ADD CONSTRAINT comentario_canal_pk PRIMARY KEY (id_comentario, id_canal);
ALTER TABLE comentario_canal ADD CONSTRAINT comentario_canal_fk_1 FOREIGN KEY (id_comentario) REFERENCES comentario(id_comentario);
ALTER TABLE comentario_canal ADD CONSTRAINT comentario_canal_fk_2 FOREIGN KEY (id_canal) REFERENCES canal(id_canal);

--Tabla que conitene los eventos de las licencias
DROP SEQUENCE evento_sq CASCADE;
CREATE SEQUENCE evento_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE evento CASCADE;
CREATE TABLE evento (
id_evento INTEGER DEFAULT NEXTVAL('evento_sq'),
id_licencia INTEGER NOT NULL,
descripcion VARCHAR(1000) NOT NULL,
fecha_evento DATE NOT NULL
);

ALTER TABLE evento ADD CONSTRAINT evento_pk PRIMARY KEY(id_evento);
ALTER TABLE evento ADD CONSTRAINT evento_fk FOREIGN KEY(id_licencia) REFERENCES licencia(id_licencia);

--Tablas que contienen la informacion de las licencias a nivel, regional, nacional, departamental, municipal.
DROP TABLE licencia_regional CASCADE;
CREATE TABLE licencia_regional (
id_licencia INTEGER NOT NULL,
id_region INTEGER NOT NULL
);

ALTER TABLE licencia_regional ADD CONSTRAINT licencia_regional_pk PRIMARY KEY(id_licencia, id_region);
ALTER TABLE licencia_regional ADD CONSTRAINT licencia_regional_fk_1 FOREIGN KEY(id_licencia) REFERENCES licencia(id_licencia);
ALTER TABLE licencia_regional ADD CONSTRAINT licencia_regional_fk_2 FOREIGN KEY(id_region) REFERENCES region(id_region);

DROP TABLE licencia_nacional CASCADE;
CREATE TABLE licencia_nacional (
id_licencia INTEGER NOT NULL
);

ALTER TABLE licencia_nacional ADD CONSTRAINT licencia_nacional_pk PRIMARY KEY (id_licencia);
ALTER TABLE licencia_nacional ADD CONSTRAINT licenica_nacional_fk FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);

DROP TABLE licencia_departamental CASCADE;
CREATE TABLE licencia_departamental (
id_licencia INTEGER NOT NULL,
id_departamento INTEGER NOT NULL
);

ALTER TABLE licencia_departamental ADD CONSTRAINT licencia_departamental_pk PRIMARY KEY(id_licencia, id_departamento);
ALTER TABLE licencia_departamental ADD CONSTRAINT licencia_departamental_fk_1 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);
ALTER TABLE licencia_departamental ADD CONSTRAINT licencia_departamental_fk_2 FOREIGN KEY (id_departamento) REFERENCES departamento(id_departamento);

DROP TABLE licencia_municipal CASCADE;
CREATE TABLE licencia_municipal (
id_licencia INTEGER NOT NULL,
id_municipio INTEGER NOT NULL
);

ALTER TABLE licencia_municipal ADD CONSTRAINT licencia_municipal_pk PRIMARY KEY(id_licencia, id_municipio);
ALTER TABLE licencia_municipal ADD CONSTRAINT licencia_municipal_fk_1 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);
ALTER TABLE licencia_municipal ADD CONSTRAINT licencia_municipal_fk_2 FOREIGN KEY (id_municipio) REFERENCES municipio(id_municipio);

--Tabla que contine información de licencia con excepciones
DROP SEQUENCE excepcion_sq CASCADE;
CREATE SEQUENCE excepcion_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE excepcion CASCADE;
CREATE TABLE excepcion (
id_excepcion INTEGER DEFAULT NEXTVAL('excepcion_sq'),
id_licencia INTEGER NOT NULL
);

ALTER TABLE excepcion ADD CONSTRAINT excepcion_pk PRIMARY KEY (id_excepcion);
ALTER TABLE excepcion ADD CONSTRAINT excepcion_fk FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);

--Tabla que contiene la relación entre municipio, excepcion y canal
DROP TABLE excepcion_municipio CASCADE;
CREATE TABLE excepcion_municipio (
id_excepcion INTEGER NOT NULL,
id_municipio INTEGER NOT NULL,
id_canal INTEGER NOT NULL
);

ALTER TABLE excepcion_municipio ADD CONSTRAINT excepcion_municipio_pk PRIMARY KEY (id_excepcion, id_municipio, id_canal);
ALTER TABLE excepcion_municipio ADD CONSTRAINT excepcion_municipio_fk_1 FOREIGN KEY (id_excepcion) REFERENCES excepcion(id_excepcion);
ALTER TABLE excepcion_municipio ADD CONSTRAINT excepcion_municipio_fk_2 FOREIGN KEY (id_municipio) REFERENCES municipio(id_municipio);
ALTER TABLE excepcion_municipio ADD CONSTRAINT excepcion_municipio_fk_3 FOREIGN KEY (id_canal) REFERENCES canal(id_canal);




