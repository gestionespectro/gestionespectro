--Tabla con información del rango de frecuencias en que se divide el espectro.

DROP TABLE rango_frecuencias CASCADE;
CREATE TABLE rango_frecuencias(
id_frecuencias VARCHAR(20) NOT NULL,
nombre VARCHAR(100) NOT NULL,
frecuencia_inicial DECIMAL NOT NULL,
frecuencia_final DECIMAL NOT NULL
);

ALTER TABLE rango_frecuencias ADD CONSTRAINT rango_frecuencias_pk PRIMARY KEY(id_frecuencias);

--Tabla con información sobre notas internacionales 

DROP SEQUENCE notas_internacionales_sq CASCADE;
CREATE SEQUENCE notas_internacionales_sq
	INCREMENT 1
	MINVALUE 1
	START 1;

DROP TABLE notas_internacionales CASCADE;
CREATE TABLE notas_internacionales(
id_nota_internacional INTEGER DEFAULT NEXTVAL('notas_internacionales_sq'),
nombre VARCHAR(20) NOT NULL,
descripcion VARCHAR(6000) NOT NULL
);

ALTER TABLE notas_internacionales ADD CONSTRAINT notas_internacionales_pk PRIMARY KEY(id_nota_internacional);

--Tabla con información sobre notas nacionales 

DROP SEQUENCE notas_nacionales_sq CASCADE;
CREATE SEQUENCE notas_nacionales_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE notas_nacionales CASCADE;
CREATE TABLE notas_nacionales (
id_nota INTEGER DEFAULT NEXTVAL('notas_nacionales_sq'),
nombre VARCHAR(20) NOT NULL,
descripcion VARCHAR(6000) NOT NULL
);

ALTER TABLE notas_nacionales ADD CONSTRAINT notas_nacionales_pk PRIMARY KEY(id_nota);

--Tabla con información de formulas para las bandas que así lo tienen definido
DROP SEQUENCE formula_sq CASCADE;
CREATE SEQUENCE formula_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE formula CASCADE;
CREATE TABLE formula (
id_formula INTEGER DEFAULT NEXTVAL('formula_sq'),
formula VARCHAR(150)
);

ALTER TABLE formula ADD CONSTRAINT formula_pk PRIMARY KEY(id_formula);

--Table con la información relacionada a la banda

DROP TABLE banda CASCADE;
CREATE TABLE banda (
id_banda VARCHAR(50) NOT NULL,
freduencia_inicial DECIMAL NOT NULL,
frecuancia_final DECIMAL NOT NULL
);

ALTER TABLE banda ADD CONSTRAINT banda_pk PRIMARY KEY(id_banda);

--Tabla que realaciona las notas internacionales con las bandas
DROP TABLE notas_internacionales_banda CASCADE;
CREATE TABLE notas_internacionales_banda (
id_banda VARCHAR(50) NOT NULL,
id_nota_internacional INTEGER 
);

ALTER TABLE notas_internacionales_banda ADD CONSTRAINT notas_internacionales_banda_pk PRIMARY KEY(id_banda, id_nota_internacional);
ALTER TABLE notas_internacionales_banda ADD CONSTRAINT notas_internacionales_banda_fk FOREIGN KEY(id_nota_internacional) REFERENCES notas_internacionales(id_nota_internacional);
ALTER TABLE notas_internacionales_banda ADD CONSTRAINT banda_notas_internacionales_fk FOREIGN KEY(id_banda) REFERENCES banda(id_banda);

--Tabla que relaciona las notas nacionales con las bandas

DROP TABLE notas_nacionales_banda CASCADE;
CREATE TABLE notas_nacionales_banda (
id_banda VARCHAR(50) NOT NULL,
id_nota INTEGER 
);

ALTER TABLE notas_nacionales_banda ADD CONSTRAINT notas_nacionales_banda_pk PRIMARY KEY(id_banda, id_nota);
ALTER TABLE notas_nacionales_banda ADD CONSTRAINT notas_nacionales_banda_fk FOREIGN KEY(id_nota) REFERENCES notas_nacionales(id_nota);
ALTER TABLE notas_nacionales_banda ADD CONSTRAINT banda_notas_nacionales_fk FOREIGN KEY(id_banda) REFERENCES banda(id_banda);

--Tabla que relaciona una formula definida con una banda

DROP TABLE formula_banda CASCADE;
CREATE TABLE formula_banda (
id_formula INTEGER,
id_banda VARCHAR(50) NOT NULL
);

ALTER TABLE formula_banda ADD CONSTRAINT formula_banda_pk PRIMARY KEY(id_formula, id_banda);
ALTER TABLE formula_banda ADD CONSTRAINT formula_banda_fk FOREIGN KEY(id_formula) REFERENCES formula(id_formula);
ALTER TABLE formula_banda ADD CONSTRAINT banda_formula_fk FOREIGN KEY(id_banda) REFERENCES banda(id_banda);

--Table con la información de los servicios que se ofrecen

DROP SEQUENCE servicio_sq CASCADE;
CREATE SEQUENCE servicio_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE servicio CASCADE;
CREATE TABLE servicio (
id_servicio INTEGER DEFAULT NEXTVAL('servicio_sq'),
nombre VARCHAR(200) UNIQUE NOT NULL,
tipo VARCHAR(20) NOT NULL
);

ALTER TABLE servicio ADD CONSTRAINT servicio_pk PRIMARY KEY(id_servicio);

--Tabla que relaciona los servicios con las bandas; servicios que se ofrecen en una banda.

DROP TABLE servicios_banda CASCADE;
CREATE TABLE servicios_banda (
id_banda VARCHAR(50) NOT NULL,
id_servicio INTEGER 
);

ALTER TABLE servicios_banda ADD CONSTRAINT servicios_banda_pk PRIMARY KEY(id_banda, id_servicio);
ALTER TABLE servicios_banda ADD CONSTRAINT servicios_banda_fk FOREIGN KEY(id_servicio) REFERENCES servicio(id_servicio);
ALTER TABLE servicios_banda ADD CONSTRAINT banda_servicios_fk FOREIGN KEY(id_banda) REFERENCES banda(id_banda);

--Tabla que tiene la información referente a los canales

DROP SEQUENCE canal_sq CASCADE;
CREATE SEQUENCE canal_sq
	INCREMENT 1
	MINVALUE 1
	START 1;

DROP TABLE canal CASCADE;
CREATE TABLE canal (
id_canal INTEGER DEFAULT NEXTVAL('canal_sq'),
frecuencia_inicial DECIMAL NOT NULL,
frecuencia_final DECIMAL NOT NULL,
id_banda VARCHAR(50) NOT NULL
);

ALTER TABLE canal ADD CONSTRAINT canal_pk PRIMARY KEY(id_canal);
ALTER TABLE canal ADD CONSTRAINT banda_canal_fk FOREIGN KEY(id_banda) REFERENCES banda(id_banda);

--Tabla con la información de los operadores

DROP SEQUENCE operador_sq CASCADE;
CREATE SEQUENCE operador_sq
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE operador CASCADE;
CREATE TABLE operador (
id_operador INTEGER DEFAULT NEXTVAL('operador_sq'),
nombre VARCHAR(300) NOT NULL,
descripcion VARCHAR(500) NOT NULL
);

ALTER TABLE operador ADD CONSTRAINT operador_pk PRIMARY KEY(id_operador);

--Tabla que relaciona los operadores con servicios; que servicios ofrece un operador

DROP TABLE servicios_operador CASCADE;
CREATE TABLE servicios_operador (
id_operador INTEGER,
id_servicio INTEGER
);

ALTER TABLE servicios_operador ADD CONSTRAINT servicios_operador_pk PRIMARY KEY(id_operador, id_servicio);
ALTER TABLE servicios_operador ADD CONSTRAINT servicios_operador_fk FOREIGN KEY(id_servicio) REFERENCES servicio(id_servicio);
ALTER TABLE servicios_operador ADD CONSTRAINT operador_servicios_fk FOREIGN KEY(id_operador) REFERENCES operador(id_operador);

--Tabla que contiene los horarios posibles (con rango de una hora) en un día. Servirá para registrar horarios de uso de los servicios

DROP SEQUENCE horario_sq CASCADE;
CREATE SEQUENCE horario_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;

DROP TABLE horario CASCADE;
CREATE TABLE horario (
id_horario INTEGER DEFAULT NEXTVAL('horario_sq'),
hora_inicio INTEGER NOT NULL,
minuto_inicio INTEGER NOT NULL,
hora_fin INTEGER NOT NULL,
minuto_fin INTEGER NOT NULL
);

ALTER TABLE horario ADD CONSTRAINT horario_pk PRIMARY KEY(id_horario);

--Tabla que contine la información de las licencias otorgadas a operadores.

DROP SEQUENCE licencia_sq CASCADE;
CREATE SEQUENCE licencia_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE licencia CASCADE;
CREATE TABLE licencia (
id_licencia INTEGER DEFAULT NEXTVAL('licencia_sq'),
identificacion VARCHAR(50) NOT NULL,
fecha_vencimiento DATE NOT NULL,
fecha_inicio DATE NOT NULL,
id_operador INTEGER
);

ALTER TABLE licencia ADD CONSTRAINT licencia_pk PRIMARY KEY(id_licencia);
ALTER TABLE licencia ADD CONSTRAINT operador_licencia_fk FOREIGN KEY(id_operador) REFERENCES operador(id_operador);

--Tabla que relaciona licencia con los canales licenciados

DROP TABLE canales_licencia CASCADE;
CREATE TABLE canales_licencia (
id_canal INTEGER,
id_licencia INTEGER
);

ALTER TABLE canales_licencia ADD CONSTRAINT canales_licencia_pk PRIMARY KEY(id_canal);
ALTER TABLE canales_licencia ADD CONSTRAINT canales_licencia_fk FOREIGN KEY(id_canal) REFERENCES canal(id_canal);
ALTER TABLE canales_licencia ADD CONSTRAINT licencia_canales_fk FOREIGN KEY(id_licencia) REFERENCES licencia(id_licencia);

--Tabla que relaciona los servicios que tiene una licencia asociados; que servicios se pueden ofrecer con esa licencia.

DROP TABLE servicios_licencia CASCADE;
CREATE TABLE servicios_licencia (
id_servicio INTEGER,
id_licencia INTEGER,
id_horario INTEGER
);

ALTER TABLE servicios_licencia ADD CONSTRAINT servicios_licencia_pk PRIMARY KEY(id_servicio, id_licencia);
ALTER TABLE servicios_licencia ADD CONSTRAINT servicios_licencia_fk FOREIGN KEY(id_servicio) REFERENCES servicio(id_servicio);
ALTER TABLE servicios_licencia ADD CONSTRAINT licencia_servicios_fk FOREIGN KEY(id_licencia) REFERENCES licencia(id_licencia);
ALTER TABLE servicios_licencia ADD CONSTRAINT horario_servicios_licencia_fk FOREIGN KEY(id_horario) REFERENCES horario(id_horario);

--Tabla que tiene la información de eventos que se presentan en las licencias

DROP SEQUENCE evento_sq CASCADE;
CREATE SEQUENCE evento_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE evento CASCADE;
CREATE TABLE evento (
id_evento INTEGER DEFAULT NEXTVAL('evento_sq'),
id_licencia INTEGER,
descripcion VARCHAR(300) NOT NULL,
fecha_evento DATE NOT NULL
);

ALTER TABLE evento ADD CONSTRAINT evento_pk PRIMARY KEY(id_evento);
ALTER TABLE evento ADD CONSTRAINT licencia_evento_fk FOREIGN KEY(id_licencia) REFERENCES licencia(id_licencia);

--Tabla que contine la información de las regiones geograficas del país (definidas por los usuarios)

DROP SEQUENCE region_sq CASCADE;
CREATE SEQUENCE region_sq 
	INCREMENT 1
	MINVALUE 1
	START 1;
	
DROP TABLE region CASCADE;
CREATE TABLE region (
id_region INTEGER DEFAULT NEXTVAL('region_sq'),
nombre VARCHAR(100) NOT NULL UNIQUE DEFAULT 'Default'
);

ALTER TABLE region ADD CONSTRAINT region_pk PRIMARY KEY(id_region);

--Tabla que tiene la información de los departamentos
--QUEDA FALTANDO DEPARTMENTOS, MUNICIPIOS, LICENCIAMUNICIPAL, LICENCIADEPARTAMENTEAL, Y LICENCIA NACIOANL


