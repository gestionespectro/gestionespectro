<?php

include ('../modelo/modelBandas.php');


$frecuencia = $_POST["frecuencia"];
$escalaHz_frecuencia_Ingresada= $_POST["escalaHz_frecuencia_Ingresada"];
$servicio = $_POST["servicio"];
$notaNacional = $_POST["notaNacional"];
$notaInterNacional = $_POST["notaInterNacional"];
$idBanda = $_POST["idBanda"];
$escalaHz=$_POST["escalaHz"];

/*
  echo '<frecuencia>'.$frecuencia.'</frecuencia>';
  echo '<servicio>'.$servicio.'</servicio>';
  echo '<notaNacional>'.$notaNacional.'</notaNacional>';
  echo '<notaInterNacional>'.$notaInterNacional.'</notaInterNacional>';

 */




$modelBanda = new modelBandas ();
echo "<variables>\n";


if ($idBanda == "") {

    $result = $modelBanda->buscarBandas($frecuencia, $servicio, $notaNacional, $notaInterNacional,$escalaHz,$escalaHz_frecuencia_Ingresada);


    $rows = pg_num_rows($result);
    if ($rows == 0)
        echo "\t <respuesta> 0 </respuesta> \n";
    else {
        echo "\t <respuesta>2</respuesta> \n";


        $nameVar0 = pg_field_name($result, 0);
        $nameVar1 = pg_field_name($result, 1);
        $nameVar2 = pg_field_name($result, 2);

        while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {


            echo "\t <frecuenciaInicial>" .$line[$nameVar1] ."</frecuenciaInicial> \n";
            echo "\t <frecuenciaFInal>".$line[$nameVar2] ." </frecuenciaFInal> \n";
             echo "\t <idBanda>".$line[$nameVar0] ." </idBanda> \n";
            echo "<fila>" . "<![CDATA[ <tr class='gradeC' >  \n \t <td>" . $line[$nameVar1] . " </td> 
                \n \t <td>" . $line[$nameVar0] . " </td> 
                

            \n \t <td>

          <button type='button' onclick=\"javascript:buscarDetalles('" . $line[$nameVar0] . "')\"  title='Detalles' class ='transparente'   >
          <img src='images/iconos/vista.png'  width='20' height='20'/>
          </button>

          <button type='button' onclick=\"javascript:borrar('" . $line[$nameVar0] . "')\"  title='borrar' class ='transparente'   >
          <img src='images/iconos/borrar.png'  width='20' height='20'/>
          </button>

          </td> </tr>]]> </fila> \n";
        }
    }
}

else {

    echo "\t <respuesta> 1 </respuesta> \n";


    $result = $modelBanda->buscarBanda($idBanda);





    $nameVar0 = pg_field_name($result, 0);
    $nameVar1 = pg_field_name($result, 1);
    $nameVar2 = pg_field_name($result, 2);

    while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {


        echo "\t <id_banda>" . $line[$nameVar0] . "</id_banda> \n";
        echo "\t <frecuencia_inicial>" . $line[$nameVar1] . "</frecuencia_inicial> \n";
        echo "\t <frecuencia_final>" . $line[$nameVar2] . "</frecuencia_final> \n";
    }




    //-------------------notas nacionales------------------------
    
    
    $result2 = $modelBanda->buscarNotasBanda($idBanda,"=");


    $nameVar0 = pg_field_name($result2, 0);
    

    echo "\t <notasNacionalContenidas> \n "  ;
    while ($line = pg_fetch_array($result2, null, PGSQL_ASSOC)) {


        echo "\t <notaNacionalContenida>" . $line[$nameVar0] . "</notaNacionalContenida> \n";
        
    }
    
    echo "\t </notasNacionalContenidas> \n "  ;
    
    
    //-------------------notas no incluidas------------------------
    
    
    $result2 = $modelBanda->buscarNotasBanda($idBanda,"!=");


    $nameVar0 = pg_field_name($result2, 0);
    

    echo "\t <notasNacionalNoContenidas> \n "  ;
    while ($line = pg_fetch_array($result2, null, PGSQL_ASSOC)) {


        echo "\t <notaNacionalNoContenida>" . $line[$nameVar0] . "</notaNacionalNoContenida> \n";
        
    }
    
    echo "\t </notasNacionalNoContenidas> \n "  ;
    
    
    //--------------notas INternacionales -----------------------
    
    
    
    
    
     
    $result3 = $modelBanda->buscarNotasInternacionalesBanda($idBanda,"=");


    $nameVar0 = pg_field_name($result3, 0);
    

    echo "\t <notasInterNacionalContenidas> \n "  ;
    while ($line = pg_fetch_array($result3, null, PGSQL_ASSOC)) {


        echo "\t <notaInterNacionalContenida>" . $line[$nameVar0] . "</notaInterNacionalContenida> \n";
        
    }
    
    echo "\t </notasInterNacionalContenidas> \n "  ;
    
    
    //-------------------notas no incluidas------------------------
    
    
    $result3 = $modelBanda->buscarNotasInternacionalesBanda($idBanda,"!=");


    $nameVar0 = pg_field_name($result3, 0);
    

    echo "\t <notasInterNacionalNoContenidas> \n "  ;
    while ($line = pg_fetch_array($result3, null, PGSQL_ASSOC)) {


        echo "\t <notaInterNacionalNoContenida>" . $line[$nameVar0] . "</notaInterNacionalNoContenida> \n";
        
    }
    
    echo "\t </notasInterNacionalNoContenidas> \n "  ;
    
    
    
    
    
    //--------------SERVICIOS -----------------------
    
    
    
    
    
     
    $result4 = $modelBanda->buscarServicioBanda($idBanda,"=");


    $nameVar0 = pg_field_name($result4, 0);
    

    echo "\t <serviciosContenidos> \n "  ;
    while ($line = pg_fetch_array($result4, null, PGSQL_ASSOC)) {


        echo "\t <servicioContenido>" . $line[$nameVar0] . "</servicioContenido> \n";
        
    }
    
    echo "\t </serviciosContenidos> \n "  ;
    
    
    //-------------------SERVICIOS------------------------
    
    
    $result4 = $modelBanda->buscarServicioBanda($idBanda,"!=");


    $nameVar0 = pg_field_name($result4, 0);
    

    echo "\t <serviciosNoContenidos> \n "  ;
    while ($line = pg_fetch_array($result4, null, PGSQL_ASSOC)) {


        echo "\t <servicioNoContenido>" . $line[$nameVar0] . "</servicioNoContenido> \n";
        
    }
    
    echo "\t </serviciosNoContenidos> \n "  ;
    
    
    
    
    
    
    
    
    
    
    
    
    
}




echo "</variables>\n";
?>