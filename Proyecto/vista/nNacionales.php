﻿<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        	
        <link rel="stylesheet" href="chosen/chosen.css" />
       <!-- <script src="chosen/chosen.jquery.js" type="text/javascript"></script>-->
        <link type="text/css" href="css/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript">
            $(function(){

                // Accordion
                $("#accordion").accordion({ header: "h3" });

                // Tabs
                $('#tabs').tabs();

                // Dialog
                $('#dialog').dialog({
                    autoOpen: false,
                    width: 600,
                    buttons: {
                        "Ok": function() {
                            $(this).dialog("close");
                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                    }
                });

                // Dialog Link
                $('#dialog_link').click(function(){
                    $('#dialog').dialog('open');
                    return false;
                });

                // Datepicker
                $('#datepicker').datepicker({
                    inline: true
                });

                // Slider
                $('#slider').slider({
                    range: true,
                    values: [17, 67]
                });

                // Progressbar
                $("#progressbar").progressbar({
                    value: 20
                });

                //hover states on the static widgets
                $('#dialog_link, ul#icons li').hover(
                function() { $(this).addClass('ui-state-hover'); },
                function() { $(this).removeClass('ui-state-hover'); }
            );

            });
        </script>
        
        
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <style type="text/css" media="screen">
            @import "css/site_jui.css";
            @import "css/demo_table_jui.css";
            /*@import "jquery-ui-1.7.2.custom.css";

            /*
             * Override styles needed due to the mix of three different CSS sources! For proper examples
             * please see the themes example in the 'Examples' section of this site
            */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            .css_right { float: right; }
            #example_wrapper .fg-toolbar { font-size: 0.8em }
            #theme_links span { float: left; padding: 2px 10px; }
            #example_wrapper { -webkit-box-shadow: 2px 2px 6px #666; box-shadow: 2px 2px 6px #666; border-radius: 5px; }
            #example tbody {
                border-left: 1px solid #AAA;
                border-right: 1px solid #AAA;
            }
            #example thead th:first-child { border-left: 1px solid #AAA; }
            #example thead th:last-child { border-right: 1px solid #AAA; }
        </style>

         <!--contiene la variable para las rutas estaticas -->
       <script type="text/javascript" language="javascript" src="localhost.js"></script>



        <!-- tablas -->
        
        
        <script type="text/javascript">
            
            $(function(){
                $("#form1").submit(function(){
                    
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlNotasNacionalesBuscar.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
						
						
                         
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
						   
						   
                            var x=doc.getElementsByTagName("variables");
						   
						   
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].childNodes[0].nodeValue;
                            var info="";
						    
                            if (respuesta==0)
                            { 
                                info="La consulta no arrojo resultados"
                                document.getElementById('formularioResultado').style.display='none';
                                limpiarcampos();
                                document.getElementById('tabla').style.display='none';
                                $('#example').remove(); 
               
						
                            }
                            else if (respuesta==1){
                                limpiarcampos();
						   
                                var id=x[0].getElementsByTagName("id")[0].childNodes[0].nodeValue;
                                var nombre=x[0].getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                                var descripcion=x[0].getElementsByTagName("descripcion")[0].textContent;
						   
							
							
							
                                $("#divText").html(descripcion);
                                document.getElementById('campoEditable').value = nombre; 
                                buscar();
							
							
							 
                            }
                            else if (respuesta==2) {
                                limpiarcampos();
                                document.getElementById('tabla').style.display='block';
                
						   
                                x=doc.getElementsByTagName("fila");
						   
                                var tabla='<table cellpadding="0" cellspacing="0" border="0"  id="example" style="width:980px"><thead><tr><th>C&oacute;digo Nota Nacional</th><th>acción</th></tr></thead><tbody>';
								
						
						   
                                for (i=0;i<x.length;i++)

                                {
								
                                    tabla=tabla+"\n"+x[i].textContent;
								
                                }
                                tabla=tabla+"</tbody> </table>";
						   
                                $("#full_width").html(tabla);
						  
                                $(document).ready( function() {
                                    
                                    $('#example').dataTable( {
                                        "bJQueryUI": true,
                                        "sPaginationType": "full_numbers"
                                    } );
				
                          
                                } );
                                document.getElementById('formularioResultado').style.display='none';
							 
                            }
                            
                            $("#response").html(info);
                            $("#loading").hide();
                            
                            
                            
                        }
                        
                    })
                    return false;
                })
                
            })
			
			
            $(function(){
                $("#formularioResultado").submit(function(){
                    
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlNotasNacionales.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
						   
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
                            var x=doc.getElementsByTagName("variables");
						   
						  
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].textContent;
						  
                         
                            document.getElementById('formularioResultado').style.display='none';
                            limpiarcampos();
						  
						   
                            $("#response").html(respuesta);						  
                            $("#loading").hide();
							
                        }
                             
                            
                    }
                        
                )
                    return false;
                })
                
            })
			
			
            
            function StringtoXML(text){
                if (window.ActiveXObject){
                    var doc=new ActiveXObject('Microsoft.XMLDOM');
                    doc.async='false';
                    doc.loadXML(text);
                } else {
                    var parser=new DOMParser();
                    var doc=parser.parseFromString(text,'text/xml');
                }
                return doc;
            }
			
			
        </script>



        <link href="css/estructuraForm.css" rel="stylesheet" type="text/css" media="screen, projection" />	
        <link href='http://fonts.googleapis.com/css?family=Marmelad' rel='stylesheet' type='text/css'/>


        <script>
            function buscar(){ 
             
                document.getElementById('formularioResultado').style.display='block';
                
                document.getElementById('campoEditable').disabled = true; 
                document.getElementById('divText').contentEditable = false; 
                
                document.getElementById('botonInsertar').className='invisible';
                document.getElementById('divText').className = 'divEditable gris'; 
                
                document.getElementById('botonGuardar').className ='invisible';
                //document.getElementById('botonBorrar').className ='boton negro redondo';
                document.getElementById('botonEditar').className ='transparente';
                
                
               
                
                
            }
			
            function submitForm(nombre){ 
             
              
                document.getElementById('idBoton').value =nombre;
                document.getElementById('descripcion').value= document.getElementById('divText').innerHTML;
                document.getElementById('campoEditable').disabled = false; 
                var info= "";
                var campo=document.getElementById('campoEditable').value;
			  
				
			 
                if (campo.match(/^[1-9][0-9]*$/)==null){
                    info="  Campo código no válido <br />  "
					
                }
                if (nombre!="botonBorrar" && document.getElementById('descripcion').value==""){
			  
                    info=info+"  Campo descripción vacío <br /> "
			  
			  
                }
                if (info==""){
                    $("#error").html("");
                    $("#formularioResultado").submit();
					
			  
                } else {
                    $("#error").html(info);
			  
                }
			  
			  
               
             
                
               
                
                
            }
			
			
            
          
            
            
            
        </script>
        <script>
            function insertar(){ 
                
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
                
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('campoEditable').disabled = false; 
                document.getElementById('divText').contentEditable = true; 
                
                document.getElementById('divText').className = 'divEditable blanco'; 
                
				
                limpiarcampos();
      
                
                document.getElementById('botonEditar').className ='invisible';
                //document.getElementById('botonBorrar').className ='invisible';
                document.getElementById('botonGuardar').className ='invisible';
                document.getElementById('botonInsertar').className ='transparente';
                
               
               
               
                
            }
            function editar(){ 
                              
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('campoEditable').disabled = true; 
                document.getElementById('divText').contentEditable = true; 
                
                document.getElementById('divText').className = 'divEditable blanco'; 
                
                
                document.getElementById('botonEditar').className ='invisible';
                //document.getElementById('botonBorrar').className ='invisible';
                document.getElementById('botonGuardar').className ='transparente';
                
               
               
               
                
            }
			
            function limpiarcampos(){
                document.getElementById('campoEditable').value = ""; 
                
                $("#divText").html("");
                $("#response").html("");
                $("#error").html("");
            }
			
            function submitListarTodas(){
			
                document.getElementById('formularioResultado').style.display='none';
                
                document.getElementById('codigoNotaNacional').value="";
			
                $("#form1").submit();


            }
			
            function buscarDetalles(name){
			
                document.getElementById('tipoBuscar').value="buscarUno";
                document.getElementById('codigoNotaNacional').value=name;
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
                
                $("#form1").submit();
                document.getElementById('tipoBuscar').value="";

            }
			
            function borrar(codigo){
			
                        
                if (!confirm("¿Esta seguro que desea borrar?"))
                {
                    return;
                }        
                document.getElementById('campoEditable').value=codigo;
			
                submitForm('botonBorrar');
			
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
            }
          
        
        </script>

    </head>

    <body> 
        <header>
            Notas Nacionales
        </header>
        <div align="center">
            <div id="consulta">    
                <form method="post"  name="form1" id="form1">
                    <label>  C&oacutedigo de Nota Nacional: &nbsp &nbsp  CLM</label> 
                    <input  type="text" id="codigoNotaNacional" name="codigoNotaNacional" class="input" pattern="[1-9][0-9]*" title="Minimo 1 numero" required>

                    <input  type="text" id="tipoBuscar" name="tipoBuscar" value="" hidden="false" />
                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp

                    <button type="submit"  class ="transparente"  title="Buscar"  >
                        <img src="images/iconos/buscar.png" width="20" height="20" />
                    </button>


                    <button type="button" onclick="javascript:insertar()"  title="Agregar Nota" class ="transparente"   >
                        <img src="images/iconos/agregar.png" width="20" height="20" />
                    </button>

                    <button type="button" onclick="javascript:submitListarTodas()"  title="Listar Todas" class ="transparente"   >
                        <img src="images/iconos/verTodas.png" width="20" height="20" />
                    </button>

                    </br>   					
                </form>
            </div>
            <div id="loading" class="invisible">cargando...</div>
            <div id="error"  class="rojo" > </div>
            <div id="response"   > </div>
            <div id="tabla" style="display: none; margin:30px">
                <div id="full_width" class="full_width" >

                </div>
            </div>


            <div id="resultado">  
                <form id="formularioResultado"  method="post"   class="invisible">
                    <br />
                    <label>  C&oacutedigo : </label>  &nbsp &nbsp CLM

                    <input  type="text" id="campoEditable" name="campoCodigo"    /><br /><p></p>
                    <label>  Descripci&oacuten: </label> <br />


                    <div contentEditable="true" id ="divText"  name="divConTexto" class="divEditable blanco">
                        Acá¡ puedes escribir <br></br>

                    </div>
                    <!--a este input se le asignare el valor del divtext para que pueda ser usado con el metodo post de php  -->
                    <input  type="text" id="descripcion" name="descripcion" value="" hidden="false" />
                    <input  type="text" id="idBoton" name="nombreBoton" value="" hidden="false" />



                    <button type="button" onclick="javascript:editar()" class ="transparente"  id="botonEditar"  title="Editar" >
                        <img src="images/iconos/editar.png"  />
                    </button>

                    <button type="button" onclick="javascript:submitForm('botonInsertar')" id="botonInsertar" title="Insertar" class ="transparente"   >
                        <img src="images/iconos/agregar.png"  />
                    </button>

                    <button type="button" onclick="javascript:submitForm('botonGuardar')" id="botonGuardar"  title="Guardar" class ="transparente"   >
                        <img src="images/iconos/guardar.png"  />
                    </button>

<!--<input type="button" onclick="javascript:editar()" id="botonEditar" class="boton negro redondo" value="Editar"/>
<input type="button" onclick="javascript:submitForm('botonInsertar')" id="botonInsertar" class="boton negro redondo" value="Insertar"/>
<input type="button" onclick="javascript:submitForm('botonGuardar')" id="botonGuardar" class="boton negro redondo" value="Guardar"/>-->
<!--<input type="button" onclick="javascript:submitForm('botonBorrar')"  id="botonBorrar"class="boton negro redondo" value="Borrar"/>-->
                    <br /><p></p>

                </form>


            </div>

        </div>

    </body>
    
    
     <script>
            
            
<?php
$nombreSe = $_GET["nombreNota"];

 $sub = substr($nombreSe, 4);
if ($nombreSe != "") {

    echo "document.getElementById('codigoNotaNacional').value = '" . $sub . "' ;\n ";
    
    echo " $(document).ready(function(){ 
        $(\"#form1\").submit();}); \n ";
        
   
}
?>
    
    
   
    
    </script>
</html>