﻿<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />   

        <link rel="stylesheet" href="chosen/chosen.css" />
       <!-- <script src="chosen/chosen.jquery.js" type="text/javascript"></script>-->
        <link type="text/css" href="css/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript">
            $(function(){

                // Accordion
                $("#accordion").accordion({ header: "h3" });

                // Tabs
                $('#tabs').tabs();

                // Dialog
                $('#dialog').dialog({
                    autoOpen: false,
                    width: 600,
                    buttons: {
                        "Ok": function() {
                            $(this).dialog("close");
                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                    }
                });

                // Dialog Link
                $('#dialog_link').click(function(){
                    $('#dialog').dialog('open');
                    return false;
                });

                // Datepicker
                $('#datepicker').datepicker({
                    inline: true
                });

                // Slider
                $('#slider').slider({
                    range: true,
                    values: [17, 67]
                });

                // Progressbar
                $("#progressbar").progressbar({
                    value: 20
                });

                //hover states on the static widgets
                $('#dialog_link, ul#icons li').hover(
                function() { $(this).addClass('ui-state-hover'); },
                function() { $(this).removeClass('ui-state-hover'); }
            );

            });
        </script>


        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <style type="text/css" media="screen">
            @import "css/site_jui.css";
            @import "css/demo_table_jui.css";
            /*@import "jquery-ui-1.7.2.custom.css";

            /*
             * Override styles needed due to the mix of three different CSS sources! For proper examples
             * please see the themes example in the 'Examples' section of this site
            */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            .css_right { float: right; }
            #example_wrapper .fg-toolbar { font-size: 0.8em }
            #theme_links span { float: left; padding: 2px 10px; }
            #example_wrapper { -webkit-box-shadow: 2px 2px 6px #666; box-shadow: 2px 2px 6px #666; border-radius: 5px; }
            #example tbody {
                border-left: 1px solid #AAA;
                border-right: 1px solid #AAA;
            }
            #example thead th:first-child { border-left: 1px solid #AAA; }
            #example thead th:last-child { border-right: 1px solid #AAA; }
        </style>

        
         <!--contiene la variable para las rutas estaticas -->
        <script type="text/javascript" language="javascript" src="localhost.js"></script>


        <script type="text/javascript">
           
            //  var localhost="avispa.univalle.edu.co/site/gestionBDEspectro";
            
            var region="";
            var evento="";
           
           
        </script>


        <!-- tablas -->




        <script type="text/javascript">
            
            $(function(){
                $("#form1").submit(function(){
                    
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlRegionesBuscar.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
						
						
							
                                                        
                            
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
						   
						   
                            var x=doc.getElementsByTagName("variables");
						   
						   
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].childNodes[0].nodeValue;
                            var info="";
						    
                            if (respuesta==0)
                            { 
                                info="La consulta no arrojo resultados"
                                document.getElementById('formularioResultado').style.display='none';
                                document.getElementById('departamentos').style.display='none';
                               
                                limpiarcampos();
                                document.getElementById('tabla').style.display='none';
                                $('#example').remove();
               
						
                            }
                            else if (respuesta==1){
                                limpiarcampos();
                                $('#departamentoNuevo').empty();
						   
                                var nombre=x[0].getElementsByTagName("id")[0].childNodes[0].nodeValue;
						   			
                                document.getElementById('campoEditable').value = nombre;
                                document.getElementById('regionIdDepartamentos').value = nombre;
                                
                                
                                
                                var combo= document.getElementById('departamentoNuevo');
                                x=doc.getElementsByTagName("departamento");
                                for (i=0;i<x.length;i++)

                                {
                                    
                                    $('#departamentoNuevo').append('<option value="'+x[i].textContent+'">'+x[i].textContent+'</option>');
                                  
								
                                }
                                
                                $("#departamentoNuevo").chosen();
                                
                                    
                              
                                x=doc.getElementsByTagName("departamentoRegion");
                                for (i=0;i<x.length;i++)

                                {
                                    $("#tablaConDepartamentos").append("<tr><td>"+x[i].textContent+"</td> <td>"+"<button type='button' class='transparente'  onclick= \" javascript:borrarDepartamento('"+x[i].textContent+"') \"  title='borrar' class ='transparente'   >"+
                                        "<img src='images/iconos/minus.png'  width='20' height='20'/>"+
                                        "</button>"+"</td></tr>");
                                    
								
                                }
                                
                                
                                
                                
                                
                                
                                buscar();
							 
                            }
                            else if (respuesta==2) {
                                limpiarcampos();
                                $('#example').remove();
                                document.getElementById('tabla').style.display='block';
                
						   
                                x=doc.getElementsByTagName("fila");
						   
                                var tabla='<table cellpadding="0" cellspacing="0" border="0"  id="example" style="width:980px"><thead><tr><th>Nombre de la region </th><th>acción</th></tr></thead><tbody>';
								
						
						   
                                for (i=0;i<x.length;i++)

                                {
								
                                    tabla=tabla+"\n"+x[i].textContent;
								
                                }
                                tabla=tabla+"</tbody> </table>";
						   
                                $("#full_width").html(tabla);
						  
                             
                                $(document).ready( function() {
                                   
                                    $('#example').dataTable( {
                                        "bJQueryUI": true,
                                        "sPaginationType": "full_numbers"
                                    } );
				
                                } );
                             
                             
                                
                                
                                document.getElementById('formularioResultado').style.display='none';
                                document.getElementById('departamentos').style.display='none';
							 
                            }
                            
                            $("#response").html(info);
                            $("#loading").hide();
                            
                            
                            
                        }
                        
                    })
                    return false;
                })
                
            })
			
			
            $(function(){
                $("#formularioResultado").submit(function(){
                    
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlRegiones.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
							
						   
				
                    
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
                            var x=doc.getElementsByTagName("variables");
						   
                            document.getElementById('formularioResultado').style.display='none';
                            limpiarcampos();
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].textContent;
						  
						 
						   
						  
                            $("#response").html(respuesta);
                            						  
                            $("#loading").hide();
							
                        }
                             
                            
                    }
                        
                )
                    return false;
                })
                
            })
            
            
            $(function(){
                $("#formDepartamentos").submit(function(){
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlRegionesDepartamento.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
			
                           
                           
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
						   
						   
                            var x=doc.getElementsByTagName("variables");
                            
                            
                            if(document.getElementById('evento').value=="verRegion"){
                                
                                
                                region=x[0].getElementsByTagName("departamentoTieneRegion")[0].childNodes[0].nodeValue;
                                
                                evento.value="agregar";
                                
                                if (region=="Default"){
                                    
                                    $("#formDepartamentos").submit();
                                    
                                }
                                else {
                                    if(confirm("El departamento ya pertenece a la región "+region+" ¿desea modificarla?")){
                                        
                                         $("#formDepartamentos").submit();
                                    }
                                    else{
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                              
                               
                                
                            }
                            
                            
                            else {
                            
                            
                            
                                limpiarcamposDepartamento();
                          
                               
                                x=doc.getElementsByTagName("departamento");
                           
                            
                                $('#departamentoNuevo').empty();
                                for (i=0;i<x.length;i++)

                                {
                                    
                                
                                
                                    $('#departamentoNuevo').append('<option value="'+x[i].textContent+'">'+x[i].textContent+'</option>');
                                
								
                               				
                                }
                            
                            
                                $('#departamentoNuevo').trigger('liszt:updated');
                             
                            
                        
                                
                                x=doc.getElementsByTagName("departamentoRegion");
                                for (i=0;i<x.length;i++)

                                {
                                    $("#tablaConDepartamentos").append("<tr><td>"+x[i].textContent+"</td> <td>"+"<button type='button'  class='transparente' onclick= \" javascript:borrarDepartamento('"+x[i].textContent+"') \"  title='borrar' class ='transparente'   >"+
                                        "<img src='images/iconos/minus.png'  width='20' height='20'/>"+
                                        "</button>"+"</td></tr>");
                                    
								
                                }		   
				
                            
							
                            }
                        }       
                            
                    }
                        
                )
                    return false;
                })
                
            })
			
			
            
            function StringtoXML(text){
                if (window.ActiveXObject){
                    var doc=new ActiveXObject('Microsoft.XMLDOM');
                    doc.async='false';
                    doc.loadXML(text);
                } else {
                    var parser=new DOMParser();
                    var doc=parser.parseFromString(text,'text/xml');
                }
                return doc;
            }
			
			
        </script>




        <!-- fuente de letra con google -->
        <link href="css/estructuraForm.css" rel="stylesheet" type="text/css" media="screen, projection" />	
        <link href='http://fonts.googleapis.com/css?family=Marmelad' rel='stylesheet' type='text/css'/>


        <script>
            function buscar(){ 
             
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('departamentos').style.display='block';
                
                document.getElementById('campoEditable').disabled = true; 
                
                document.getElementById('botonInsertar').className='invisible';
               
                document.getElementById('botonGuardar').className ='invisible';
                //document.getElementById('botonBorrar').className ='boton negro redondo';
                document.getElementById('botonEditar').className ='transparente';
                 
            }
			
            function submitForm(nombre){ 
             
                document.getElementById('idBoton').value =nombre;
                document.getElementById('campoEditable').disabled = false;
               	
                
                
                var info= "";
			 
				
			  
                if (nombre != 'botonBorrar' && document.getElementById('campoEditable').value==""){
                    info="  Campo nombre vacío <br />  ";
					
                }
                /* if (nombre!="botonBorrar" && document.getElementById('descripcion').value==""){
			  
                                         info=info+"  Campo descripción vacío <br /> "
			  
			  
                           }*/
                if (info==""){
                    $("#error").html("");
                    $("#formularioResultado").submit();
					
			  
                } else {
                    $("#error").html(info);
			  
                }
		
                
            }
			
			
            
          
            
            
            
        </script>
        <script>
            function insertar(){ 
                
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
                
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('campoEditable').disabled = false;				
				
             
                limpiarcampos();
                
                document.getElementById('departamentos').style.display='none';
                
                document.getElementById('botonEditar').className ='invisible';
                //document.getElementById('botonBorrar').className ='invisible';
                document.getElementById('botonGuardar').className ='invisible';
                document.getElementById('botonInsertar').className ='transparente';
                
               
                
            }
            
            function editar(){ 			
		
                
                if(document.getElementById('campoEditable').value =='Default')
                {
                    document.getElementById('formularioResultado').style.display='none';
                    document.getElementById('departamentos').style.display='none';
                     
                    limpiarcampos();
                    $("#response").html("La region por defecto no se puede modificar");
                    $("#loading").hide();
                    document.getElementById('tabla').style.display='none';
                    $('#example').remove();
                    return;
                
                }
                
                
                document.getElementById('nombreAnt').value = document.getElementById('campoEditable').value; 
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('departamentos').style.display='none';
                document.getElementById('campoEditable').disabled = false; 
              
                document.getElementById('botonEditar').className ='invisible';
                //document.getElementById('botonBorrar').className ='invisible';
                document.getElementById('botonGuardar').className ='transparente';
                
               
                
            }
			
            function limpiarcampos(){
                
                limpiarcamposDepartamento();
                document.getElementById('campoEditable').value = ""; 
                
               
                $("#response").html("");
                $("#error").html("");
                
            }
            function limpiarcamposDepartamento(){
                
                //var combo= document.getElementById('departamentoNuevo');              
                // combo.options.length = 0;
                
                //$("#departamentoNuevo").empty();
                //$('#departamentoNuevo').find('option').remove();
                
               
                $("#tablaConDepartamentos tr").remove();
                
                
            }
			
            function submitListarTodas(){
                
			
                document.getElementById('formularioResultado').style.display='none';
                document.getElementById('departamentos').style.display='none';
                document.getElementById('nombreRegion').value="";
			
                $("#form1").submit();

            }
			
            function buscarDetalles(nombre){
		
    
                document.getElementById('campoEditable').value=nombre;	    
                document.getElementById('tipoBuscar').value=nombre;
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  						
                $("#form1").submit();
                document.getElementById('campoEditable').value="";	    
                document.getElementById('tipoBuscar').value="";
                  
                
			

            }		
			
            function borrar(nombre){
				
                                
                              
                if(confirm("¿Esta seguro de borrar la region?")) {
				
                    if(nombre=='Default')
                    {
					
                        document.getElementById('formularioResultado').style.display='none';
                        document.getElementById('departamentos').style.display='none';
                        limpiarcampos();
                        $("#response").html("La region por defecto no se puede borrar");
                        $("#loading").hide();
                        document.getElementById('tabla').style.display='none';
                        $('#example').remove(); 
				    
                    }else{
					
			
                       
                        document.getElementById('campoEditable').value=nombre;			
                        submitForm('botonBorrar');
				
                        document.getElementById('tabla').style.display='none';
                        $('#example').remove();  
                    }
                }
            }
            
            function agregarDepartamentoaRegion(){
                evento =document.getElementById('evento');
                evento.value="verRegion";
                
                
                $("#formDepartamentos").submit();
                 
                 
                
              
                /*
                document.getElementById('regionIdDepartamentos').value=document.getElementById('campoEditable').value ;	
                document.getElementById('evento').value="agregar" ;
               
                
                var value = $('#departamentoNuevo option:selected').val();
                
                
                
              
              
               
               $("#formDepartamentos").submit();
                 */
                
                  
                
            }
            
            
            function borrarDepartamento(nombreDepartamento){
               
                document.getElementById('regionIdDepartamentos').value=document.getElementById('campoEditable').value ;	
                document.getElementById('nombreDepartamento').value=nombreDepartamento ;
                document.getElementById('evento').value="borrar" ;
                
                
             
                $("#formDepartamentos").submit();
                
                if($('#departamentoNuevo').hasClass('chzn-done'))
                {    
                    $('#departamentoNuevo').removeClass('chzn-done').next().remove();
                    $('#departamentoNuevo').chosen();
                }
                
            }
        
        </script>

    </head>

    <body> 


        <header>
            Regiones 
        </header>

        <div align="center">
            <div id="consulta">    
                <form method="post"  name="form1" id="form1">



                    <label> Nombre de la region   </label> 
                    <input  type="text" id="nombreRegion" name="nombreRegion"  >
                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp

                                        <!--<input  type="image" style="border: 0;" src="images/iconos/buscar.png" alt="Submit"  width="20" height="20" title="Buscar"/>-->

                    <input  type="text" id="tipoBuscar" name="tipoBuscar" value="" hidden="false" />

                    <button type="submit"  class ="transparente"  title="Buscar"  >
                        <img src="images/iconos/buscar.png" width="20" height="20" />
                    </button>

                    <button type="button" onclick="javascript:insertar()"  title="Agregar Region" class ="transparente"   >
                        <img src="images/iconos/agregar.png" width="20" height="20" />
                    </button>

                    <button type="button" onclick="javascript:submitListarTodas()"  title="Listar Todos" class ="transparente"   >
                        <img src="images/iconos/verTodas.png" width="20" height="20" />
                    </button> <br />


                    </br>   					
                </form>
            </div>
            <div id="loading" class="invisible">cargando...</div>
            <div id="error"  class="rojo" > </div>
            <div id="response"   > </div>
            <div id="tabla" style="display: none; margin:30px">
                <div id="full_width" class="full_width" >

                </div>
            </div>

        </div>



        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Región</a></li>
                <li><a href="#tabs-2">Departamentos de la región</a></li>

            </ul>
            <div id="tabs-1">


                <center>
                    <div id="resultadoRegiones" style="width:40% ; display:block; text-align: center; background-color:whitesmoke ">  
                        <form id="formularioResultado"  method="post"   class="invisible">
                            <br />

                            <label>  Nombre : </label>  

                            <input  type="text" id="campoEditable" name="nombre"  /><br />  <p></p>
                            &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp


                            <input  type="text" id="nombreAnt" name="nombreAnt" value="" hidden="false" />
                            <input  type="text" id="idBoton" name="nombreBoton" value="" hidden="false" />

                            <br />
                            <br />
                            <br />

                            <button type="button" onclick="javascript:editar()" class ="transparente"  id="botonEditar"  title="Editar" >
                                <img src="images/iconos/editar.png"  />
                            </button>

                            <button type="button" onclick="javascript:submitForm('botonInsertar')" id="botonInsertar" title="Insertar" class ="transparente"   >
                                <img src="images/iconos/agregar.png"  />
                            </button>

                            <button type="button" onclick="javascript:submitForm('botonGuardar')" id="botonGuardar"  title="Guardar" class ="transparente"   >
                                <img src="images/iconos/guardar.png"  />
                            </button>

                            <br /><p></p>

                        </form>             
                    </div>
                </center>




            </div>

            <div id="tabs-2">
                <center>

                    <div id="departamentos" style="width:40%  ; text-align: center; background-color: whitesmoke; display: none;">


                        <center>
                            <table cellspacing="0" cellpadding="0" border="0" width="325" style="margin: 20px; " >
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="1" border="1" width="300"  class="tabla1">
                                            <tr>
                                                <th>Departamento</th>
                                                <th style="width: 40px;"></th>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="width:325px; height:100px; overflow:auto;">
                                            <table id="tablaConDepartamentos" cellspacing="0" cellpadding="1" border="1" width="300" class="tabla1" >

                                            </table>  
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </center>


                        <form method="post"  name="formDepartamentos" id="formDepartamentos" >
                            <div id="departamentosDiv" style="margin:10px;"  >
                                <input  type="text" id="regionIdDepartamentos" name="regionIdDepartamentos" value="" hidden="false" />
                                <input  type="text" id="nombreDepartamento" name="nombreDepartamento" value="" hidden="false" />
                                <input  type="text" id="evento" name="evento" value="" hidden="false" />


                                <label > Departamento: </label> 

                                <select name="departamentoNuevo"  id="departamentoNuevo" data-placeholder="Choose a Country" class="chzn-select" style="width:350px;" >


                                </select>


                                <button type="button" onclick="javascript:agregarDepartamentoaRegion()"  title="añadir departamento" class="transparente" >
                                    <img src="images/iconos/agregar.png" width="15" height="15" />
                                </button> 

                            </div>





                        </form>







                    </div>
                </center>


            </div>

        </div>







    <!--<select id="comboBox" data-placeholder="Choose a Country" class="chzn-select" style="width:350px;" tabindex="2">
        
        <option value="United States">United States</option> 
        <option value="United Kingdom">United Kingdom</option> 
        <option value="Afghanistan">Afghanistan</option> 
    
    </select>-->



<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>-->
        <script src="chosen/chosen.jquery.js" type="text/javascript"></script>


    </body>


</html>
