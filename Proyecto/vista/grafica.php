<!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>
      Google Visualization API Sample
    </title>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['imagechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        // Populate the data table.
        dataTable = google.visualization.arrayToDataTable([
          ['GOOG', 90, 100, 'hola',110, 100],
        ], true);
         
          /*['GOOG', 50, 60, 70, 80],
          ['GOOG', 20, 30, 40, 50],
          ['GOOG',  15, 45, 35, 55],
          ['GOOG', 10, 20, 30, 40]*/
        dataTable.addRows([
         ['GOOG', 70, 80,'hola', 90, 100]  
        ]);
      
        // Draw the chart.
        var chart = new google.visualization.ImageCandlestickChart(document.getElementById('visualization'));
        
         chart.draw(dataTable, {legend:'none', width:"100%", height:"100%"});
      }
      

      google.setOnLoadCallback(drawVisualization);
    </script>
  </head>
  <body style="font-family: Arial;border: 0 none;">
    <div id="visualization" style="width: 300px; height: 300px;"></div>
  </body>
</html>