﻿<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



        <link rel="stylesheet" href="chosen/chosen.css" />
       <!-- <script src="chosen/chosen.jquery.js" type="text/javascript"></script>-->
        <link type="text/css" href="css/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script type="text/javascript">
            $(function(){

                // Accordion
                $("#accordion").accordion({ header: "h3" });

                // Tabs
                $('#tabs').tabs();

                // Dialog
                $('#dialog').dialog({
                    autoOpen: false,
                    width: 600,
                    buttons: {
                        "Ok": function() {
                            $(this).dialog("close");
                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                    }
                });

                // Dialog Link
                $('#dialog_link').click(function(){
                    $('#dialog').dialog('open');
                    return false;
                });

                // Datepicker
                $('#datepicker').datepicker({
                    inline: true
                });

                // Slider
                $('#slider').slider({
                    range: true,
                    values: [17, 67]
                });

                // Progressbar
                $("#progressbar").progressbar({
                    value: 20
                });

                //hover states on the static widgets
                $('#dialog_link, ul#icons li').hover(
                function() { $(this).addClass('ui-state-hover'); },
                function() { $(this).removeClass('ui-state-hover'); }
            );

            });
        </script>


        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <style type="text/css" media="screen">
            @import "css/site_jui.css";
            @import "css/demo_table_jui.css";
            /*@import "jquery-ui-1.7.2.custom.css";

            /*
             * Override styles needed due to the mix of three different CSS sources! For proper examples
             * please see the themes example in the 'Examples' section of this site
            */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            .css_right { float: right; }
            #example_wrapper .fg-toolbar { font-size: 0.8em }
            #theme_links span { float: left; padding: 2px 10px; }
            #example_wrapper { -webkit-box-shadow: 2px 2px 6px #666; box-shadow: 2px 2px 6px #666; border-radius: 5px; }
            #example tbody {
                border-left: 1px solid #AAA;
                border-right: 1px solid #AAA;
            }
            #example thead th:first-child { border-left: 1px solid #AAA; }
            #example thead th:last-child { border-right: 1px solid #AAA; }
        </style>


        <!--contiene la variable para las rutas estaticas -->
        <script type="text/javascript" language="javascript" src="localhost.js"></script>



        <!-- tablas -->
        <!-- tablas -->
        <script type="text/javascript">
            
            $(function(){
                $("#form1").submit(function(){
                    
                   
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlServiciosBuscar.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
						
						
                        
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
						   
						   
                            var x=doc.getElementsByTagName("variables");
						   
						   
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].childNodes[0].nodeValue;
                            var info="";
						    
                            if (respuesta==0)
                            { 
                                info="La consulta no arrojo resultados"
                                document.getElementById('formularioResultado').style.display='none';
                                limpiarcampos();
                                document.getElementById('tabla').style.display='none';
                                $('#example').remove();
               
						
                            }
                            else if (respuesta==1){
                                limpiarcampos();
						   
                                var nombre=x[0].getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
                                var tipo=x[0].getElementsByTagName("tipo")[0].textContent;
                                var id=x[0].getElementsByTagName("id")[0].textContent;
						   
							
							
                                document.getElementById('idBd').value = id;							
                                document.getElementById('campoEditable').value = nombre;
                                document.getElementById('tipoServicio').value = tipo;
							
                                buscar();
							 
                            }
                            else if (respuesta==2) {
                                limpiarcampos();
                                document.getElementById('tabla').style.display='block';
                
						   
                                x=doc.getElementsByTagName("fila");
						   
                                var tabla='<table cellpadding="0" cellspacing="0" border="0"  id="example" style="width:980px"><thead><tr><th>Nombre del Servicio</th><th>acción</th></tr></thead><tbody>';
								
						
						   
                                for (i=0;i<x.length;i++)

                                {
								
                                    tabla=tabla+"\n"+x[i].textContent;
								
                                }
                                tabla=tabla+"</tbody> </table>";
						   
                                $("#full_width").html(tabla);
						  
                                $(document).ready( function() {
                                  
                                    $('#example').dataTable( {
                                        "bJQueryUI": true,
                                        "sPaginationType": "full_numbers"
                                    } );
				
                                } );
                                document.getElementById('formularioResultado').style.display='none';
							 
                            }
                            
                            $("#response").html(info);
                            $("#loading").hide();
                            
                            
                            
                        }
                        
                    })
                    return false;
                })
                
            })
			
			
            $(function(){
                $("#formularioResultado").submit(function(){
                    
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlServicios.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                        },
                        success:function(response){
							
						   
                            
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
                            var x=doc.getElementsByTagName("variables");
						   
                            document.getElementById('formularioResultado').style.display='none';
                            limpiarcampos();
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].textContent;
						  
						 
						   
						  
                            $("#response").html(respuesta);
                            						  
                            $("#loading").hide();
							
                        }
                             
                            
                    }
                        
                )
                    return false;
                })
                
            })
			
			
            
            function StringtoXML(text){
                if (window.ActiveXObject){
                    var doc=new ActiveXObject('Microsoft.XMLDOM');
                    doc.async='false';
                    doc.loadXML(text);
                } else {
                    var parser=new DOMParser();
                    var doc=parser.parseFromString(text,'text/xml');
                }
                return doc;
            }
			
			
        </script>



        <link href="css/estructuraForm.css" rel="stylesheet" type="text/css" media="screen, projection" />	
        <link href='http://fonts.googleapis.com/css?family=Marmelad' rel='stylesheet' type='text/css'/>


        <script>
            function buscar(){ 
             
                document.getElementById('formularioResultado').style.display='block';
                
                document.getElementById('campoEditable').disabled = true; 
                document.getElementById('idBd').disabled = true; 
                document.getElementById('tipoServicio').disabled=true;
                
                document.getElementById('botonInsertar').className='invisible';
               
                document.getElementById('botonGuardar').className ='invisible';
                //document.getElementById('botonBorrar').className ='boton negro redondo';
                document.getElementById('botonEditar').className ='transparente';
                
                
               
                
                
            }
			
            function submitForm(nombre){ 
             
              
                document.getElementById('idBoton').value =nombre;
                document.getElementById('campoEditable').disabled = false;
                document.getElementById('idBd').disabled = false; 			  
                var info= "";
			 
				
			  
                if (nombre != 'botonBorrar' && document.getElementById('campoEditable').value==""){
                    info="  Campo nombre vacío <br />  "
					
                }
                /* if (nombre!="botonBorrar" && document.getElementById('descripcion').value==""){
			  
                                        info=info+"  Campo descripción vacío <br /> "
			  
			  
                          }*/
                if (info==""){
                    $("#error").html("");
                    $("#formularioResultado").submit();
					
			  
                } else {
                    $("#error").html(info);
			  
                }
			  
			  
               
             
                
               
                
                
            }
			
			
            
          
            
            
            
        </script>
        <script>
            function insertar(){ 
                
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
                
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('campoEditable').disabled = false;
                document.getElementById('idBd').style.display = 'none'; 				
                document.getElementById('labeIdBd').style.display = 'none'; 				
				
             
                limpiarcampos();
      
                
                document.getElementById('botonEditar').className ='invisible';
                //document.getElementById('botonBorrar').className ='invisible';
                document.getElementById('botonGuardar').className ='invisible';
                document.getElementById('botonInsertar').className ='transparente';
                
               
                
            }
            function editar(){ 
                document.getElementById('idBd').style.display = 'inherit'; 				
                document.getElementById('labeIdBd').style.display = 'inherit'; 				
				              
                document.getElementById('formularioResultado').style.display='block';
                document.getElementById('campoEditable').disabled = false; 
                document.getElementById('idBd').disabled = true; 				
                document.getElementById('tipoServicio').disabled = false; 
              
                document.getElementById('botonEditar').className ='invisible';
                //document.getElementById('botonBorrar').className ='invisible';
                document.getElementById('botonGuardar').className ='transparente';
                
               
               
               
                
            }
			
            function limpiarcampos(){
                document.getElementById('campoEditable').value = ""; 
                
                
                $("#response").html("");
                $("#error").html("");
            }
			
            function submitListarTodas(){
               
                document.getElementById('formularioResultado').style.display='none';
                
                document.getElementById('nombreServicio').value="";
			
                $("#form1").submit();


            }
			
            function buscarDetalles(name){
			
                document.getElementById('idBd').style.display = 'inherit'; 				
                document.getElementById('labeIdBd').style.display = 'inherit'; 
			
                document.getElementById('tipoBuscar').value=name;
			
			
			
			
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
                
                $("#form1").submit();
                document.getElementById('tipoBuscar').value="";
			

            }
			
            function borrar(codigo){
			
                        
                if (!confirm("¿Esta seguro que desea borrar?"))
                {
                    return;
                }           
                document.getElementById('idBd').value=codigo;
			
                submitForm('botonBorrar');
			
                document.getElementById('tabla').style.display='none';
                $('#example').remove();  
            }
			
			
			
            function activarBuscarPorTipo(){
			
		
			
                if(document.getElementById('checkBuscarPorTipo').checked)
                {		
					
			
                    document.getElementById('tipoServicioDiv').style.display='block'; 
                }
                else 
                {
					
                    document.getElementById('tipoServicioDiv').style.display='none'; 
			
                }
			
			 
            }
			
			
          
          
        
        </script>










    </head>

    <body> 
        <header>
            Servicios 
        </header>
        <div align="center">
            <div id="consulta">    
                <form method="post"  name="form1" id="form1">



                    <label>

                        Nombre del Servicio   </label> 
                    <input  type="text" id="nombreServicio" name="nombreServicio"  >

                    <input  type="text" id="tipoBuscar" name="tipoBuscar" value="" hidden="false" />
                    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp

                                        <!--<input  type="image" style="border: 0;" src="images/iconos/buscar.png" alt="Submit"  width="20" height="20" title="Buscar"/>-->

                    <button type="submit"  class ="transparente"  title="Buscar"  >
                        <img src="images/iconos/buscar.png" width="20" height="20" />
                    </button>

                    <button type="button" onclick="javascript:insertar()"  title="Agregar Servicio" class ="transparente"   >
                        <img src="images/iconos/agregar.png" width="20" height="20" />
                    </button>

                    <button type="button" onclick="javascript:submitListarTodas()"  title="Listar Todos" class ="transparente"   >
                        <img src="images/iconos/verTodas.png" width="20" height="20" />
                    </button> <br />

                    <label> Buscar por Tipo </label> 
                    <input type="checkbox" id="checkBuscarPorTipo"  onclick="javascript:activarBuscarPorTipo()" /><br /><br />

                    <div id="tipoServicioDiv" style="display:none;" >

                        <label > Tipo de Servicio: </label> 

                        <select name="tipoServicioBuscar"   >
                            <option value=""></option>
                            <option value="primario">primario</option>
                            <option value="secundario">secundario</option>

                        </select>

                    </div>
                    </br>   					
                </form>
            </div>
            <div id="loading" class="invisible">cargando...</div>
            <div id="error"  class="rojo" > </div>
            <div id="response"   > </div>
            <div id="tabla" style="display: none; margin:30px">
                <div id="full_width" class="full_width" >

                </div>
            </div>


            <div id="resultado">  
                <form id="formularioResultado"  method="post"   class="invisible">
                    <br />

                    <label id="labeIdBd" >  Id BD : </label>  

                    <input  type="text" id="idBd" name="idBd"  /><br /><p></p>

                    <label>  Nombre : </label>  

                    <input  type="text" id="campoEditable" name="nombre"  /><br /><p></p>
                    <label> Tipo de Servicio: </label> 

                    <select name="tipoServicio"  id="tipoServicio">
                        <option value="primario">primario</option>
                        <option value="secundario">secundario</option>

                    </select>


                    <!--a este input se le asignare el valor del divtext para que pueda ser usado con el metodo post de php  -->
                    <input  type="text" id="idBoton" name="nombreBoton" value="" hidden="false" />

                    <br />
                    <br />
                    <br />

                    <button type="button" onclick="javascript:editar()" class ="transparente"  id="botonEditar"  title="Editar" >
                        <img src="images/iconos/editar.png"  />
                    </button>

                    <button type="button" onclick="javascript:submitForm('botonInsertar')" id="botonInsertar" title="Insertar" class ="transparente"   >
                        <img src="images/iconos/agregar.png"  />
                    </button>

                    <button type="button" onclick="javascript:submitForm('botonGuardar')" id="botonGuardar"  title="Guardar" class ="transparente"   >
                        <img src="images/iconos/guardar.png"  />
                    </button>

                    <br /><p></p>

                </form>


            </div>

        </div>

    </body>

    <script>
            
            
<?php
$nombreSe = $_GET["nombreServicio"];

if ($nombreSe != "") {

    echo "document.getElementById('nombreServicio').value = '" . $nombreSe . "' ;\n ";
    
    echo " $(document).ready(function(){ 
        $(\"#form1\").submit();}); \n ";
        
   
}
?>
    
    
   
    
    </script>
</html>