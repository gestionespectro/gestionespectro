﻿<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


        <link rel="stylesheet" href="chosen/chosen.css" />
        <!-- <script src="chosen/chosen.jquery.js" type="text/javascript"></script>-->
        <link type="text/css" href="css/smoothness/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>





        <script type="text/javascript">
            $(function(){

                // Accordion
                $("#accordion").accordion({ header: "h3" });

                // Tabs
                $('#tabs').tabs();
                $('#tabs-canvas').tabs();
                
                

                // Dialog
                $('#dialog').dialog({
                    autoOpen: false,
                    width: 600,
                    buttons: {
                        "Ok": function() {
                            $(this).dialog("close");
                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                    }
                });

                // Dialog Link
                $('#dialog_link').click(function(){
                    $('#dialog').dialog('open');
                    return false;
                });

                // Datepicker
                $('#datepicker').datepicker({
                    inline: true
                });

                // Slider
                $('#slider').slider({
                    range: true,
                    values: [17, 67]
                });

                // Progressbar
                $("#progressbar").progressbar({
                    value: 20
                });

                //hover states on the static widgets
                $('#dialog_link, ul#icons li').hover(
                function() { $(this).addClass('ui-state-hover'); },
                function() { $(this).removeClass('ui-state-hover'); }
            );

            });
        </script>


        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <style type="text/css" media="screen">
            @import "css/site_jui.css";
            @import "css/demo_table_jui.css";
            /*@import "jquery-ui-1.7.2.custom.css";

            /*
             * Override styles needed due to the mix of three different CSS sources! For proper examples
             * please see the themes example in the 'Examples' section of this site
            */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            .css_right { float: right; }
            #example_wrapper .fg-toolbar { font-size: 0.8em }
            #theme_links span { float: left; padding: 2px 10px; }
            #example_wrapper { -webkit-box-shadow: 2px 2px 6px #666; box-shadow: 2px 2px 6px #666; border-radius: 5px; }
            #example tbody {
                border-left: 1px solid #AAA;
                border-right: 1px solid #AAA;
            }
            #example thead th:first-child { border-left: 1px solid #AAA; }
            #example thead th:last-child { border-right: 1px solid #AAA; }
        </style>

        <!--contiene la variable para las rutas estaticas -->
        <script type="text/javascript" language="javascript" src="localhost.js"></script>



        <!-- tablas -->

        <!-- para las tablas -->

        <script>
            
            function iniciarTablaServicios(contenidas,noContenidas){
                
                var vectorContenidas=contenidas[0].getElementsByTagName("servicioContenido");
                var vectorNoContenidas=noContenidas[0].getElementsByTagName("servicioNoContenido");
                
                for (i=0;i<vectorContenidas.length;i++)

                {
								
                    $("#tablaConServicios").append("<tr><td>"+vectorContenidas[i].textContent+"</td> <td>"+"<button type='button' class='transparente'  onclick= \" javascript:borrarNota('"+vectorContenidas[i].textContent+"') \"  title='borrar' class ='transparente'   >"+
                        "<img src='images/iconos/minus.png'  width='20' height='20'/>"+
                        "</button>"+"<button type='button' class='transparente'  onclick= \" javascript:verServicio('"+vectorContenidas[i].textContent+"') \"  title='detalles' class ='transparente'   >"+
                        "<img src='images/iconos/vista.png'  width='20' height='20'/>"+
                        "</button>"+"</td>"+"</tr>");
								
                }
                   
                   
                $('#selectServicios').empty();
                for (i=0;i<vectorNoContenidas.length;i++)

                {
                    $('#selectServicios').append('<option value="'+vectorNoContenidas[i].textContent+'">'+vectorNoContenidas[i].textContent+'</option>');
                                
                      
                }
                  
                $('#selectServicios').trigger('liszt:updated');
                              
                  
                

                
                
                
            }
            
            
            
            
            function iniciarTablaNotasnacionales(contenidas,noContenidas){
                
                var vectorContenidas=contenidas[0].getElementsByTagName("notaNacionalContenida");
                var vectorNoContenidas=noContenidas[0].getElementsByTagName("notaNacionalNoContenida");
                
                for (i=0;i<vectorContenidas.length;i++)

                {
								
                    $("#tablaConNotasNacionales").append("<tr><td>"+vectorContenidas[i].textContent+"</td> <td>"+"<button type='button' class='transparente'  onclick= \" javascript:borrarNota('"+vectorContenidas[i].textContent+"') \"  title='borrar' class ='transparente'   >"+
                        "<img src='images/iconos/minus.png'  width='20' height='20'/>"+
                        "</button><button type='button' class='transparente'  onclick= \" javascript:verNotaNacional('"+vectorContenidas[i].textContent+"') \"  title='detalles' class ='transparente'   >"+
                        "<img src='images/iconos/vista.png'  width='20' height='20'/>"+
                        "</button>"+"</td></tr>");
								
                }
                   
                   
                   
                   
                $('#selectNotasNacionales').empty();
                for (i=0;i<vectorNoContenidas.length;i++)

                {
                    $('#selectNotasNacionales').append('<option value="'+vectorNoContenidas[i].textContent+'">'+vectorNoContenidas[i].textContent+'</option>');
                                
                      
                }
                  
                $('#selectNotasNacionales').trigger('liszt:updated');
                              
                  
                
            }
            
            
            
            
            
            function iniciarTablaNotasInternacionales(contenidas,noContenidas){
            
            
                var vectorContenidas=contenidas[0].getElementsByTagName("notaInterNacionalContenida");
                var vectorNoContenidas=noContenidas[0].getElementsByTagName("notaInterNacionalNoContenida");
                
                for (i=0;i<vectorContenidas.length;i++)

                {
								
                    $("#tablaConNotasInterNacionales").append("<tr><td>"+vectorContenidas[i].textContent+"</td> <td>"+"<button type='button' class='transparente'  onclick= \" javascript:borrarNota('"+vectorContenidas[i].textContent+"') \"  title='borrar' class ='transparente'   >"+
                        "<img src='images/iconos/minus.png'  width='20' height='20'/>"+
                        "</button> <button type='button' class='transparente'  onclick= \" javascript:verRecomendacion('"+vectorContenidas[i].textContent+"') \"  title='detalles' class ='transparente'   >"+
                        "<img src='images/iconos/vista.png'  width='20' height='20'/>"+
                        "</button>"+"</td></tr>");
                    
                    
                    
								
                }
                   
                   
                $('#selectNotasInterNacionales').empty();
                for (i=0;i<vectorNoContenidas.length;i++)

                {
                     
                    $('#selectNotasInterNacionales').append('<option value="'+vectorNoContenidas[i].textContent+'">'+vectorNoContenidas[i].textContent+'</option>');
                                
                      
                }
                  
                $('#selectNotasInterNacionales').trigger('liszt:updated');
            
            
            
            
            
            }
            
            $(function(){
                $("#form1").submit(function(){
                    
                    
                    
                    $.ajax({
                        type:"POST",
                        url:"http://"+localhost+"/Proyecto/controlador/controlBandas.php",
                        dataType:"html",
                        data:$(this).serialize(),
                        beforeSend:function(){
                            $("#loading").show();
                            
                            
                        },
                        success:function(response){
					
                            /*alert(response);*/
			
                            limpiarCanvas();
                            limpiarTablas();
                            document.getElementById('tab-content').className="invisible";
                            
                           
                            var x= response.toString(); 
                            var doc = StringtoXML(x);
                            var x=doc.getElementsByTagName("variables");
                           		   
						  
                            var respuesta=x[0].getElementsByTagName("respuesta")[0].textContent;
						  
			
                          
                        
                        
                            if (respuesta==1){
                               
                                var id_banda_resultado=x[0].getElementsByTagName("id_banda")[0].textContent;
                                var frecuencia_inicial_resultado=x[0].getElementsByTagName("frecuencia_inicial")[0].textContent;
                                var frecuencia_final_resultado=x[0].getElementsByTagName("frecuencia_final")[0].textContent;    
                               
                             
                               
                                document.getElementById('id_banda_formulario').value=id_banda_resultado;
                                document.getElementById('frecuencia_inicial_banda_formulario').value=frecuencia_inicial_resultado;
                                document.getElementById('frecuencia_final_banda_formulario').value=frecuencia_final_resultado;
                                
                                
                                
                                var notasNacionalContenidas=x[0].getElementsByTagName("notasNacionalContenidas");
                                var notasNacionalNoContenidas=x[0].getElementsByTagName("notasNacionalNoContenidas");
                                
                                
                                iniciarTablaNotasnacionales(notasNacionalContenidas,notasNacionalNoContenidas);
                                
                                
                                
                               
                                var notasInterNacionalContenidas=x[0].getElementsByTagName("notasInterNacionalContenidas");
                                var notasInterNacionalNoContenidas=x[0].getElementsByTagName("notasInterNacionalNoContenidas");
                                
                                
                                iniciarTablaNotasInternacionales(notasInterNacionalContenidas,notasInterNacionalNoContenidas);
                                
                                
                                
                                var serviciosContenidos=x[0].getElementsByTagName("serviciosContenidos");
                                var serviciosNoContenidos=x[0].getElementsByTagName("serviciosNoContenidos");
                                
                                
                                iniciarTablaServicios(serviciosContenidos,serviciosNoContenidos);
                                
                              
                              
                                document.getElementById('botonEditar').className='visible';
                                document.getElementById('botonGuardar').className='invisible';
                                document.getElementById('botonInsertar').className='invisible';
                                
                                document.getElementById('id_banda_formulario').disabled=true;               
                                document.getElementById('frecuencia_inicial_banda_formulario').disabled=true;
                                document.getElementById('frecuencia_final_banda_formulario').disabled=true;
                                
                                
                                document.getElementById('tabla').style.display='none';
                                document.getElementById('idBanda').value="";
                                
                                document.getElementById('tab-content').className="visible";
                                document.getElementById('tabs-canvas-div').className="invisible";
                                $("#loading").hide();
                                
                                
                                
                                
                               
                            }
                           
                           
                           
                            else {
                                /*
                            document.getElementById('formularioResultado').style.display='none';
                            limpiarcampos();
						  
						   
                            $("#response").html(respuesta);*/						  
                                $("#loading").hide();
                               
                                document.getElementById('tabla').style.display='block';
                                document.getElementById('tabs-canvas-div').className="visible";
                                
                             
                       

                
						   
                                x=doc.getElementsByTagName("fila");
						   
                                var tabla='<table cellpadding="0" cellspacing="0" border="0"  id="example" style="width:980px"><thead><tr><th>FIni Khz</th><th>FrecuenciaInicial-FrecuenciaFinal  </th><th>acción</th></tr></thead><tbody>';
								
                                var fInicialVectors=doc.getElementsByTagName("frecuenciaInicial");
                                var fFinalVectors=doc.getElementsByTagName("frecuenciaFInal");
                                var idBandaVector=doc.getElementsByTagName("idBanda");
                              
                                
                                pintarBandaKhz(fInicialVectors,fFinalVectors,idBandaVector);
                                pintarBandaMhz(fInicialVectors,fFinalVectors,idBandaVector);
                                pintarBandaGhz(fInicialVectors,fFinalVectors,idBandaVector);
						   
                                for (i=0;i<x.length;i++)

                                {
								
                                    tabla=tabla+"\n"+x[i].textContent;
								
                                }
                                tabla=tabla+"</tbody> </table>";
						   
                                $("#full_width").html(tabla);
						  
                             
                                $(document).ready( function() {
                                   
                                    $('#example').dataTable( {
                                        "bJQueryUI": true
                                        
                                    } );
				
                                } );
                        
                            }
							
                        }
                             
                            
                    }
                        
                )
                    return false;
                })
                
            })
            
            function StringtoXML(text){
                if (window.ActiveXObject){
                    var doc=new ActiveXObject('Microsoft.XMLDOM');
                    doc.async='false';
                    doc.loadXML(text);
                } else {
                    var parser=new DOMParser();
                    var doc=parser.parseFromString(text,'text/xml');
                }
                return doc;
            }
        
    
        </script>


        <!--funciones auxiliares -->
        <script>
            /*busca la informacion de una banda en especifico a traves del envio del formulario buscar con el dato id_banda modificado el cual sirve para diferenciar entre buscar
             *una banda y buscar varias bandas
             */ 
            function buscarDetalles(id_banda){
            
            
                document.getElementById('idBanda').value=id_banda;
                $("#form1").submit();
                //document.getElementById('idBanda').value="";
            
        
            }
            
           
            
            function limpiarTablas(){
                
                $("#tablaConNotasNacionales tr").remove();
                $("#tablaConNotasInterNacionales tr").remove();
                $("#tablaConServicios tr").remove();
                
                
                
            }
            
            function limpiarCanvas(){
                
                var vectorCanvas = ["canvasKhz", "canvasMhz","canvasGhz"];
                
                
                for (i=0;i<vectorCanvas.length;i++){
                    var oCanvas = document.getElementById(vectorCanvas[i]);
                    var oContext = oCanvas.getContext("2d");
                    oContext.clearRect(0, 0, oCanvas.width, oCanvas.height);
                }
                
                
            }
            
            
            
            

        </script>




        <!-- Graficas khz -->


        <script>
            function pintarBandaKhz(vectorIni,vectorFin,vectorId){
                
               
            
                var canvas = document.getElementById('canvasKhz');
                var context = canvas.getContext('2d');
                var rectWidth = 150;
                var rectHeight = 75;
                var factorDistancia=40;
                var escala=10;
                var amplitudIntervalo=20;

               
                context.fillText('Banda Khz ', 50, 50);
                context.beginPath();		
                context.moveTo(factorDistancia, canvas.height-factorDistancia);
                context.lineTo(factorDistancia,factorDistancia);
                context.stroke();
		
                context.beginPath();		
                context.moveTo(factorDistancia, canvas.height-factorDistancia);
                context.lineTo(canvas.width-factorDistancia,canvas.height-factorDistancia);
                context.stroke();
		
                context.rotate(Math.PI / 2);
          
                context.rotate(-Math.PI / 2);
               
                //textContent
               
            
               
                var anteriorPixel=factorDistancia;
                for (i=0;i<vectorIni.length;i++){
                    
                    var id=""+vectorId[i].textContent;
                    
                    if (id.indexOf("kHz")!= -1){
                        var fIniOriginal=parseInt(vectorIni[i].textContent);
                    
                   
                        var fFinOriginal=parseInt(vectorFin[i].textContent);
                    
                    
                    
                    
                        var fIni=0;
                        var fFinal=0;
                    
                   
                        if (i!=0){
                            var inicioSiguiente=parseFloat(vectorIni[i].textContent);
                            var finActual=parseFloat( vectorFin[i-1].textContent);
                   
                   
                            if ( inicioSiguiente == finActual){
                        
                        
                        
                                fIni=anteriorPixel;
                        
                        
                        
                            }
                            else {
                                
                                // var diferencia = parseInt((inicioSiguiente -finActual)/10);
                        
                                //fIni=anteriorPixel+5+diferencia;
                                
                                fIni=anteriorPixel+20;
                        
                            }
                        
                        }
                        else {
                            fIni=anteriorPixel;
                            
                        }
                    
                    
                        fFinal = fIni + (fFinOriginal-fIniOriginal)/10   +  20;
                        anteriorPixel=fFinal;
		
                
               
		
                        context.beginPath();
                        context.rect(fIni, canvas.height-factorDistancia-100, fFinal-fIni, 100);
                        context.fillStyle = '#'+Math.floor(Math.random()*16777215).toString(16);
                        context.fill();
                        context.lineWidth = 1;
                        context.strokeStyle = 'black';
                        context.stroke();
                        context.fillStyle='black';
                        context.rotate(Math.PI / 2);
		
                        var puntoMedio=parseInt((fFinal-fIni)/2);
                        // cambio de ejes puesto que se roto 90�
                        context.fillText(vectorId[i].textContent, canvas.height-factorDistancia-200 , -fIni-puntoMedio+5);
		
                        context.rotate(-Math.PI / 2);
		
                        inicio=0;
                        
                       
                    }
            
                }
            
            }
            
            
            function pintarBandaMhz(vectorIni,vectorFin,vectorId){
                
               
            
                var canvas = document.getElementById('canvasMhz');
                var context = canvas.getContext('2d');
                var rectWidth = 150;
                var rectHeight = 75;
                var factorDistancia=40;
                var escala=0.5;
                var amplitudIntervalo=20;

             
                context.fillText('Banda Mhz ', 50, 50);
                context.beginPath();		
                context.moveTo(factorDistancia, canvas.height-factorDistancia);
                context.lineTo(factorDistancia,factorDistancia);
                context.stroke();
		
                context.beginPath();		
                context.moveTo(factorDistancia, canvas.height-factorDistancia);
                context.lineTo(canvas.width-factorDistancia,canvas.height-factorDistancia);
                context.stroke();
		
                context.rotate(Math.PI / 2);
               
             
                context.rotate(-Math.PI / 2);
               
                //textContent
               
            
          
                
                var  anteriorPixel=factorDistancia;
                
                for (i=0;i<vectorIni.length;i++){
                    
                    
                    var id=""+vectorId[i].textContent;
                    
                    if (id.indexOf("MHz")!= -1){
                        var fIniOriginal=parseInt(vectorIni[i].textContent/1000);
                    
                   
                        var fFinOriginal=parseInt(vectorFin[i].textContent/1000);
                    
                    
                      
                      
                    
                        var fIni=0;
                        var fFinal=0;
                    
                   
                        if (i!=0){
                            var inicioSiguiente=parseFloat(vectorIni[i].textContent);
                            var finActual=parseFloat( vectorFin[i-1].textContent);
                   
                   
                            if ( inicioSiguiente == finActual){
                        
                        
                        
                                fIni=anteriorPixel;
                        
                        
                        
                            }
                            else {
                        
                                //fIni=anteriorPixel+5+(inicioSiguiente -finActual)/5;
                                var diferencia = parseInt((inicioSiguiente -finActual)/1000);
                                //fIni=anteriorPixel+diferencia+5;
                                 
                                fIni=anteriorPixel+20+diferencia;
                        
                            }
                        
                        }
                        
                        else {
                            fIni=anteriorPixel;
                            
                        }
                    
                    
                        var diferencia2 = parseInt((fFinOriginal -fIniOriginal)/5);
                    
                        fFinal = fIni +  diferencia2  +  20;
                        anteriorPixel=fFinal;
		
		
		
                        context.beginPath();
                        context.rect(fIni, canvas.height-factorDistancia-100, fFinal-fIni, 100);
                        context.fillStyle = '#'+Math.floor(Math.random()*16777215).toString(16);
                        context.fill();
                        context.lineWidth = 1;
                        context.strokeStyle = 'black';
                        context.stroke();
                        context.fillStyle='black';
                        context.rotate(Math.PI / 2);
		
                        var puntoMedio=parseInt((fFinal-fIni)/2);
                        // cambio de ejes puesto que se roto 90�
                        context.fillText(vectorId[i].textContent, canvas.height-factorDistancia-180 , -fIni-puntoMedio+5);
		
                        context.rotate(-Math.PI / 2);
		
                     
                    }
                
                }
            
            
            
            }
            
            
            function pintarBandaGhz(vectorIni,vectorFin,vectorId){
                
               
            
                var canvas = document.getElementById('canvasGhz');
                var context = canvas.getContext('2d');
                var rectWidth = 150;
                var rectHeight = 75;
                var factorDistancia=40;
                var escala=0.5;
                var amplitudIntervalo=20;

               
                context.fillText('Banda Ghz ', 50, 50);
                context.beginPath();		
                context.moveTo(factorDistancia, canvas.height-factorDistancia);
                context.lineTo(factorDistancia,factorDistancia);
                context.stroke();
		
                context.beginPath();		
                context.moveTo(factorDistancia, canvas.height-factorDistancia);
                context.lineTo(canvas.width-factorDistancia,canvas.height-factorDistancia);
                context.stroke();
		
                context.rotate(Math.PI / 2);
               
              
            
                context.rotate(-Math.PI / 2);
               
                //textContent
               
            
          
                
                var  anteriorPixel=factorDistancia;
                
                for (i=0;i<vectorIni.length;i++){
                    
                    
                    var id=""+vectorId[i].textContent;
                    
                    if (id.indexOf("GHz")!= -1){
                        var fIniOriginal=parseInt(vectorIni[i].textContent/1000000);
                    
                   
                        var fFinOriginal=parseInt(vectorFin[i].textContent/1000000);
                    
                    
                    
                        var fIni=0;
                        var fFinal=0;
                    
                   
                        if (i!=0){
                            var inicioSiguiente=parseFloat(vectorIni[i].textContent);
                            var finActual=parseFloat( vectorFin[i-1].textContent);
                   
                   
                            if ( inicioSiguiente == finActual){
                        
                        
                        
                                fIni=anteriorPixel;
                        
                        
                        
                            }
                            else {
                        
                                //fIni=anteriorPixel+5+(inicioSiguiente -finActual)/5;
                                var diferencia = parseInt((inicioSiguiente -finActual)/500);
                                //fIni=anteriorPixel+diferencia+5;
                                
                                fIni=anteriorPixel+20+diferencia;
                        
                            }
                        
                        }
                        
                        else {
                            fIni=anteriorPixel;
                            
                        }
                    
                    
                        var diferencia2 = parseInt((fFinOriginal -fIniOriginal)/10);
                    
                        fFinal = fIni +  diferencia2  +  20;
                        anteriorPixel=fFinal;
		
		
		
                        context.beginPath();
                        context.rect(fIni, canvas.height-factorDistancia-100, fFinal-fIni, 100);
                        context.fillStyle = '#'+Math.floor(Math.random()*16777215).toString(16);
                        context.fill();
                        context.lineWidth = 1;
                        context.strokeStyle = 'black';
                        context.stroke();
                        context.fillStyle='black';
                        context.rotate(Math.PI / 2);
		
                        var puntoMedio=parseInt((fFinal-fIni)/2);
                        // cambio de ejes puesto que se roto 90�
                        context.fillText(vectorId[i].textContent, canvas.height-factorDistancia-180 , -fIni-puntoMedio+5);
		
                        context.rotate(-Math.PI / 2);
		
                     
                    }
                
                }
            
            
            
            }
            
            
            function escalarCanvas(accion, nombre){
                
                var canvas = document.getElementById(nombre);
                var context = canvas.getContext('2d');
               
 
                // escalo el contexto (amplío)
                context.scale(1.8, 2.4);  //.scale(ancho, alto)
                // context.scale(0.75, 0.75);
               /* alert(nombre);*/
                
                
            } 
		
		
	  
        </script>





        <script>
            function editar()
            {
                
                
                document.getElementById('botonEditar').className='invisible';
                document.getElementById('botonGuardar').className='visible';
                document.getElementById('botonInsertar').className='invisible';
                
                
                
                                    
                                    
                
                document.getElementById('id_banda_formulario').disabled=false;               
                document.getElementById('frecuencia_inicial_banda_formulario').disabled=false;
                document.getElementById('frecuencia_final_banda_formulario').disabled=false;
                
                
                
                
            }
            
            function verServicioChooser(){
                verServicio(document.getElementById('selectServicios').value);
                
            }
            
            

            
            function verServicio(nombre){
                
                open("http://"+localhost+"/Proyecto/vista/servicios.php?nombreServicio="+nombre,"test");
                 
                
                
                
                
            }
            
            function verRecomendacionChooser(){
                verRecomendacion(document.getElementById('selectNotasInterNacionales').value);
                
            }
            
            function verRecomendacion(nombre){
                
                open("http://"+localhost+"/Proyecto/vista/recomendaciones.php?nombreRecomendacion="+nombre,"test");
               
            }
            
            
            function verNotaNacionalChooser(){
                verNotaNacional(document.getElementById('selectNotasNacionales').value);
                
            }
            
            function verNotaNacional(nombre){
                
                
                open("http://"+localhost+"/Proyecto/vista/nNacionales.php?nombreNota="+nombre,"test");
               
            }
            
            
             
            
        </script>





    </head>

    <link href="css/estructuraForm.css" rel="stylesheet" type="text/css" media="screen, projection" />	
    <link href='http://fonts.googleapis.com/css?family=Marmelad' rel='stylesheet' type='text/css'/>



    <body> 
        <header>
            Bandas de Frecuencias
        </header>
        <div align="center">
            <div id="consulta">    
                <form method="post"  name="form1" id="form1">
                    <label>  Frecuencia Contenida:  </label>
                    <input  type="text" id="frecuencia" name="frecuencia" class="input" pattern="[0-9]*" title="Ejemplo:1000" />
                    <select name="escalaHz_frecuencia_Ingresada"  id="escalaHz_frecuencia_Ingresada"  >

                        <option value="1">kHz</option>
                        <option value="1000">MHz</option>
                        <option value="1000000">GHz</option>                       
                    </select> <p></p>

                    <label> Servicio: </label> 
                    <select name="servicio"  id="servicio" data-placeholder="Seleccione un servicio" class="chzn-select" style="width:350px" >
                        <option value=""></option>
                        <option value="">ningun servicio</option>
                        <?php
                        include '../controlador/controlBandasChooserServicios.php';
                        ?>


                    </select> <p></p>



                    <label> Nota Nacional CLM:</label> 
                    <input  type="text" id="notaNacional" name="notaNacional" class="input"   pattern="[0-9]*"  title="Ejemplo: 6" /> <p></p>


                    <label> Nota Internacional: </label> 
                    <input  type="text" id="notaInterNacional" name="notaInterNacional" class="input" pattern="5\.[0-9]*[A-Z]*" title="Ejemplo: 5.120" />  <p></p>

                    <label> Rangos de frecuencias: </label> 
                    <select name="escalaHz"  id="escalaHz" data-placeholder="Seleccione un rango de frecuencias" >
                        <option value=""></option>
                        <option value="kHz">kHz</option>
                        <option value="MHz">MHz</option>
                        <option value="GHz">GHz</option>                       
                    </select> <p></p>


                    <input  type="text"  id="tipoBuscar" name="tipoBuscar" value="" hidden="false" />



                    <button type="submit"  class ="transparente"  title="Buscar"  >
                        <img src="images/iconos/buscar.png" width="20" height="20" >
                    </button>

                    <button type="button" onclick="javascript:insertar()"  title="Agregar Nota" class ="transparente"   >
                        <img src="images/iconos/agregar.png" width="20" height="20" >
                    </button>




                    <input  type="text" id="idBanda" name="idBanda" value="" hidden="false" />




                </form>


            </div>



            <div id="loading" class="invisible">cargando...</div>
            <div id="error"  class="rojo" > </div>
            <div id="response"   > </div>






            <div id="tabs-canvas-div" class="invisible">

                <div id="tabs-canvas" >


                    <ul>

                        <li><a href="#tabs-tabla">Tabla </a></li>
                        <li><a href="#tabs-Canvas1">Gráfica KHz</a></li>
                        <li><a href="#tabs-Canvas2">Gráfica MHz</a></li>
                        <li><a href="#tabs-Canvas3">Gráfica GHz</a></li>

                    </ul>

                    <div id="tabs-tabla">
                        <div id="tabla" style="display: none; margin:30px">
                            <div id="full_width" class="full_width" >

                            </div>
                        </div>


                    </div>

                    <div id="tabs-Canvas1">

                        <div id="divCanvaskhz"  style="overflow: scroll"> 

                            <canvas id="canvasKhz" width="7000" height="400"></canvas>
                        </div>



                    </div>
                    <div id="tabs-Canvas2">
                        <div id="divCanvasMhz"  style="overflow: scroll"> 


                            <canvas id="canvasMhz" width="8000" height="400"></canvas>
                        </div>




                    </div>
                    <div id="tabs-Canvas3">

                        <div id="divCanvasGhz"  style="overflow: scroll"> 

                            <canvas id="canvasGhz" width="8000" height="400"></canvas>
                        </div>


                    </div>



                </div>

            </div>



            <div id="tab-content" class="invisible">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Datos Básicos Banda</a></li>
                        <li><a href="#tabs-2">Notas Nacionales Banda</a></li>
                        <li><a href="#tabs-3">Notas Internacionales Banda</a></li>
                        <li><a href="#tabs-4">Servicios Banda</a></li>



                    </ul>
                    <div id="tabs-1">


                        <center>

                            <div id="resultadoBanda" style="width:40% ; display:block; text-align: center; background-color:whitesmoke ">  
                                <form id="formularioBandaResultado"  method="post"  >
                                    <br />

                                    <label>  Id Banda : </label>  

                                    <input  type="text" id="id_banda_formulario" name="id_banda_formulario"  /><br />  <p></p>




                                    <label>  Frecuencia Inicial : </label>  

                                    <input  type="number" id="frecuencia_inicial_banda_formulario" name="frecuencia_inicial_banda_formulario"  /><br />  <p></p>

                                    <label>  Frecuencia Final : </label>  

                                    <input  type="number" id="frecuencia_final_banda_formulario" name="frecuencia_final_banda_formulario"  /><br />  <p></p>


                                    <input  type="text" id="nombreAnt" name="nombreAnt" value="" hidden="false" />
                                    <input  type="text" id="idBoton" name="nombreBoton" value="" hidden="false" />

                                    <br />
                                    <br />
                                    <br />

                                    <center>

                                        <button type="button" onclick="javascript:editar()" class ="transparente"  id="botonEditar"  title="Editar" >
                                            <img src="images/iconos/editar.png"  />
                                        </button>

                                        <button type="button" onclick="javascript:submitForm('botonInsertar')" id="botonInsertar" title="Insertar" class ="transparente"   >
                                            <img src="images/iconos/agregar.png"  />
                                        </button>

                                        <button type="button" onclick="javascript:submitForm('botonGuardar')" id="botonGuardar"  title="Guardar" class ="transparente"   >
                                            <img src="images/iconos/guardar.png"  />
                                        </button>

                                    </center>

                                    <br /><p></p>

                                </form>             
                            </div>



                        </center>




                    </div>

                    <div id="tabs-2">
                        <center>
                            <div id="divNacionales" style="width:40%  ; text-align: center; background-color: whitesmoke; display: block;">


                                <center>
                                    <table cellspacing="0" cellpadding="0" border="0" width="325" style="margin: 20px; " >
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" border="1" width="300"  class="tabla1">
                                                    <tr>
                                                        <th>Nota Nacional</th>
                                                        <th style="width: 40px;"></th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="width:325px; height:100px; overflow:auto;">
                                                    <table id="tablaConNotasNacionales" cellspacing="0" cellpadding="1" border="1" width="300" class="tabla1" >

                                                    </table>  
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </center>


                                <form method="post"  name="formDepartamentos" id="formNacionales" >

                                    <label > Notas Nacionales: </label> 

                                    <select name="selectNotasNacionales"  id="selectNotasNacionales" data-placeholder="Choose a Country" class="chzn-select" style="width:200px;" >


                                    </select>


                                    <button type="button" onclick="javascript:agregarNota()"  title="añadir Nota" class="transparente" >
                                        <img src="images/iconos/agregar.png" width="15" height="15" />
                                    </button> 

                                    <button type="button" onclick="javascript:verNotaNacionalChooser()"  title="detalles" class="transparente" >
                                        <img src="images/iconos/vista.png" width="15" height="15" />
                                    </button> 





                                </form>

                            </div>

                        </center>


                    </div>

                    <div id="tabs-3">
                        <center>

                            <div id="divInterNacionales" style="width:40%  ; text-align: center; background-color: whitesmoke; display: block;">


                                <center>
                                    <table cellspacing="0" cellpadding="0" border="0" width="325" style="margin: 20px; " >
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" border="1" width="300"  class="tabla1">
                                                    <tr>
                                                        <th>Nota Internacional</th>
                                                        <th style="width: 40px;"></th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="width:325px; height:100px; overflow:auto;">
                                                    <table id="tablaConNotasInterNacionales" cellspacing="0" cellpadding="1" border="1" width="300" class="tabla1" >

                                                    </table>  
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </center>


                                <form method="post"  name="formDepartamentos" id="formInterNacionales" >

                                    <label > Notas Intenacionales: </label> 

                                    <select name="selectNotasInterNacionales"  id="selectNotasInterNacionales" data-placeholder="Choose a Country" class="chzn-select" style="width:200px;" >


                                    </select>


                                    <button type="button" onclick="javascript:agregarNota()"  title="añadir Nota" class="transparente" >
                                        <img src="images/iconos/agregar.png" width="15" height="15" />
                                    </button> 

                                    <button type="button" onclick="javascript:verRecomendacionChooser()"  title="detalles" class="transparente" >
                                        <img src="images/iconos/vista.png" width="15" height="15" />
                                    </button> 

                                </form>

                            </div>

                        </center>


                    </div>

                    <div id="tabs-4">
                        <center>

                            <div id="divServicios" style="width:40%  ; text-align: center; background-color: whitesmoke; display: block;">


                                <center>
                                    <table cellspacing="0" cellpadding="0" border="0" width="425" style="margin: 20px; " >
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" border="1" width="400"  class="tabla1">
                                                    <tr>
                                                        <th>Servicios</th>
                                                        <th style="width: 40px;"></th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="width:425px; height:100px; overflow:auto;">
                                                    <table id="tablaConServicios" cellspacing="0" cellpadding="1" border="1" width="400" class="tabla1" >

                                                    </table>  
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </center>


                                <form method="post"  name="formDepartamentos" id="formServicios" >

                                    <label >Servicios: </label> 

                                    <select name="selectServicios"  id="selectServicios" data-placeholder="Choose a Country" class="chzn-select" style="width:250px;" >


                                    </select>


                                    <button type="button" onclick="javascript:agregarNota()"  title="añadir Nota" class="transparente" >
                                        <img src="images/iconos/agregar.png" width="15" height="15" />
                                    </button> 

                                    <button type="button" onclick="javascript:verServicioChooser()"  title="detalles" class="transparente" >
                                        <img src="images/iconos/vista.png" width="15" height="15" />
                                    </button> 

                                </form>

                            </div>




                        </center>


                    </div>

                </div>

            </div>




        </div>

    </body>

    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>

    <script>
        
        $('#servicio').chosen();
        $('#selectNotasNacionales').chosen();
        $('#selectNotasInterNacionales').chosen();
        $('#selectServicios').chosen();
        
        
    
    
    
    
    </script>


</html>