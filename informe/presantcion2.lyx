#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass beamer
\begin_preamble
\usetheme{Warsaw}
% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 2
\use_esint 0
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Índice
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Este archivo es una plantilla para:
\end_layout

\begin_layout Itemize
Presentar una conferencia-coloquio
\end_layout

\begin_layout Itemize
Duración de unos 20min
\end_layout

\begin_layout Itemize
En estilo ornamental
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
 
\end_layout

\begin_layout Plain Layout
En principio, este archivo puede redistribuirse y/o modificarse bajo los
 términos de GNU Public License, versión 2.
 Sin embargo, se supone que este archivo es una plantilla para ser modificada
 según las necesidades de cada cuál.
 Por esta razón, si este archivo se utiliza como tal plantilla y no se distribuy
e específicamente como parte de otro programa/paquete, el autor otorga permiso
 extra para copiar y modificar libremente este archivo e incluso para suprimir
 esta nota de copyright.
 
\end_layout

\end_inset


\end_layout

\begin_layout Title
Base de datos y aplicativo web para la gestión del espectro
\end_layout

\begin_layout Author
María Andrea Cruz Blandón
\begin_inset Flex InstituteMark
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset

 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
and
\end_layout

\end_inset

 Luis Felipe Vargas Rojas
\begin_inset Flex InstituteMark
status collapsed

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\begin_layout Institute
\begin_inset Flex InstituteMark
status collapsed

\begin_layout Plain Layout
1
\end_layout

\end_inset

Universidad del Valle
\end_layout

\begin_layout Date
Presentación estado actual, 2013
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Si se dispone de un archivo como "institución-logo-nombrearchivo.xxx", donde
 xxx es un formato gráfico aceptable por latex o pdflatex, entonces se puede
 añadir un logotipo descomentando lo siguiente:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

%
\backslash
pgfdeclareimage[height=0.5cm]{institution-logo}{institución-logo-nombrearchivo}
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

%
\backslash
logo{
\backslash
pgfuseimage{institution-logo}}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Lo siguiente hace que se muestre un índice al inicio de cada subsección.
 Suprímelo si no lo quieres.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
AtBeginSubsection[]{
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

    
\backslash
frametitle{Agenda}   
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

    
\backslash
tableofcontents[currentsection,currentsubsection] 
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Si quieres descubrir todo en modo paso a paso, descomenta el siguiente comando:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

%
\backslash
beamerdefaultoverlayspecification{<+->}
\end_layout

\end_inset


\end_layout

\begin_layout BeginFrame
Agenda
\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Estructurar una exposición es una tarea difícil y la siguiente estructura
 podría no ser adecuada.
 Aquí hay unas reglas aplicadas a este caso: 
\end_layout

\begin_layout Itemize
Exactamente dos o tres secciones (además del sumario).
 
\end_layout

\begin_layout Itemize
Como máximo, tres subsecciones por sección.
 
\end_layout

\begin_layout Itemize
Hablar entre 30
\begin_inset space \thinspace{}
\end_inset

s y 2
\begin_inset space \thinspace{}
\end_inset

min por diapositiva.
 Así que debería haber entre 15 y 30 fotogramas para decirlo todo.
\end_layout

\begin_layout Itemize
Es probable que la audiencia sepa bastante poco sobre el tema de la conferencia,
 así pues, ¡*simplifique*!
\end_layout

\begin_layout Itemize
En una exposición de 20min, conseguir transmitir las ideas principales es
 bastante.
 Deje fuera los detalles, aunque eso signifique ser menos preciso de lo
 que podría parecer necesario.
\end_layout

\begin_layout Itemize
Si se omiten detalles vitales para una demostración/implementación, dígalo
 una vez.
 Para la audiencia será suficiente.
 
\end_layout

\end_inset


\end_layout

\begin_layout Section
Modelo
\end_layout

\begin_layout BeginFrame
Modelo final
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename diagrama_v6_final.png
	scale 25

\end_inset


\end_layout

\begin_layout Section
Base de datos
\end_layout

\begin_layout BeginFrame
\begin_inset ERT
status open

\begin_layout Plain Layout

[allowframebreaks]
\end_layout

\end_inset

Estado actual 
\end_layout

\begin_layout Itemize
Banda
\end_layout

\begin_deeper
\begin_layout Description
556 bandas
\end_layout

\begin_layout Description
156 bandas nombradas como kHz: 0 kHz - 27500 kHz
\end_layout

\begin_layout Description
236 bandas nombradas como MHz: 27,5 MHz - 10000 MHz
\end_layout

\begin_layout Description
164 bandas nombradas como GHz: 10 GHz - 1000 GHz
\end_layout

\end_deeper
\begin_layout Itemize
Nota nacional:
\end_layout

\begin_deeper
\begin_layout Description
97 notas nacionales
\end_layout

\begin_layout Description
1242 registros de relaciones entre notas nacionales y bandas
\end_layout

\end_deeper
\begin_layout Description

\end_layout

\begin_layout Itemize
Nota internacional:
\end_layout

\begin_deeper
\begin_layout Description
719 notas internacionales
\end_layout

\begin_layout Description
949 registros de relaciones entre notas internacionales y bandas
\end_layout

\end_deeper
\begin_layout Itemize
Información geográfica
\end_layout

\begin_deeper
\begin_layout Description
1122 municipios
\end_layout

\begin_layout Description
33 departamentos
\end_layout

\begin_layout Description
1 region (default)
\end_layout

\end_deeper
\begin_layout Itemize
Servicio
\end_layout

\begin_deeper
\begin_layout Description
117 servicios
\end_layout

\begin_layout Description
1405 registros de relaciones entre servicios y banda
\end_layout

\end_deeper
\begin_layout Itemize
Canal
\end_layout

\begin_deeper
\begin_layout Description
41 tablas en excel
\end_layout

\end_deeper
\begin_layout Section
Aplicativo web
\end_layout

\begin_layout BeginFrame
\begin_inset ERT
status open

\begin_layout Plain Layout

[allowframebreaks]
\end_layout

\end_inset

Estado actual
\end_layout

\begin_layout Itemize
Para la creación del sitio web se establece el uso del modelo vista controlador
\end_layout

\begin_layout Itemize
Por ahora la aplicacion cuenta con una conexión a la base de datos de postgres
 a través de los archivos ubicados en 
\shape italic
Proyecto/modelo
\shape default
 a su vez los archivos ubicados en 
\shape italic
Proyecto/controlador
\shape default
 procesan esas consultas y envían un xml que recibe los archivos ubicados
 en 
\shape italic
Proyecto/vista
\shape default
 donde se despliegan al usuario.
\end_layout

\begin_layout Itemize
Los modulos terminados son
\end_layout

\begin_deeper
\begin_layout Itemize
Gestión de notas nacionales 
\end_layout

\begin_layout Itemize
Gestión de notas Internacionales 
\end_layout

\begin_layout Itemize
Gestión de Servicios 
\end_layout

\begin_layout Itemize
Gestión de regiones 
\end_layout

\begin_layout Itemize
Consultas y vista de las propiedades de las bandas al mismo tiempo de un
 gráfico organizado por banda ya sea kHz MHz GHz donde se hace un esquemas
 del espectro en ese rango de bandas 
\end_layout

\begin_layout Itemize
Añadir propietrarios a la base de datos.
\end_layout

\end_deeper
\begin_layout Section
Trabajo futuro
\end_layout

\begin_layout BeginFrame
Trabajo futuro
\end_layout

\begin_layout Itemize
Base de datos:
\end_layout

\begin_deeper
\begin_layout Itemize
Cargar todos los datos respecto a los canales
\end_layout

\begin_layout Itemize
Conseguir la información respecto a los operadores.
\end_layout

\begin_layout Itemize
Investigación de las licencias, para mejorar el modelo.
\end_layout

\end_deeper
\begin_layout Itemize
Aplicativo web
\end_layout

\begin_deeper
\begin_layout Itemize
definir cómo lograr que las bandas se puedan modificar manteniendo la integridad
 de los datos, aunque se discutió en varias reuniones implementrlo no fue
 posible.
\end_layout

\begin_layout Itemize
buscar información para llenar la parte de propietarios, falta editar y
 consultar.
\end_layout

\begin_layout Itemize
las consultas con canales que tiene implementado Carlos Delgado.
\end_layout

\begin_layout Itemize
la gestión de licencias.
\end_layout

\end_deeper
\begin_layout Standard

\end_layout

\begin_layout EndFrame

\end_layout

\end_body
\end_document
