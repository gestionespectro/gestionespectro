\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {2}Gesti\IeC {\'o}n de Notas Nacionales e Internacionales}{1}
\contentsline {subsection}{\numberline {2.1}Consultar Notas}{2}
\contentsline {subsection}{\numberline {2.2}Detalles Notas y Borrar Notas}{3}
\contentsline {subsection}{\numberline {2.3}Editar Nota.}{4}
\contentsline {subsection}{\numberline {2.4}Agregar Nota }{5}
\contentsline {section}{\numberline {3}Gesti\IeC {\'o}n de Regiones}{5}
\contentsline {subsection}{\numberline {3.1}Agregar Regi\IeC {\'o}n}{5}
\contentsline {section}{\numberline {4}Gesti\IeC {\'o}n Servicios}{6}
\contentsline {subsection}{\numberline {4.1}Consultar Servicios}{6}
\contentsline {section}{\numberline {5}Bandas de Frecuencias}{7}
\contentsline {subsection}{\numberline {5.1}Consultar Bandas}{7}
\contentsline {subsection}{\numberline {5.2}Vista Gr\'afica}{8}
\contentsline {subsection}{\numberline {5.3}Detalles de Banda}{9}
