COPY rango_frecuencias FROM '/tmp/rango_frecuencias.csv' WITH DELIMITER ',' NULL AS '';
COPY nota_nacional(nombre,descripcion) FROM '/tmp/nota_nacional.csv' WITH DELIMITER '$' NULL AS '';
COPY nota_internacional(nombre,descripcion) FROM '/tmp/nota_internacional.csv' WITH DELIMITER '$' NULL AS '';
DROP TABLE bandatmp CASCADE;
CREATE TABLE bandatmp (
ini DECIMAL,
fin DECIMAL
);
COPY bandatmp FROM '/tmp/Bandas_kHz.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO banda(nombre, frecuencia_inicial, frecuencia_final) SELECT (ini||'-'||fin||' kHz'), ini, fin from bandatmp;
DELETE FROM bandatmp WHERE ini IS NOT NULL;
COPY bandatmp FROM '/tmp/Bandas_MHz.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO banda(nombre,frecuencia_inicial, frecuencia_final) SELECT (ini||'-'||fin||' MHz'), (ini*1000), (fin*1000) from bandatmp;
DELETE FROM bandatmp WHERE ini IS NOT NULL;
COPY bandatmp FROM '/tmp/Bandas_GHz.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO banda(nombre,frecuencia_inicial, frecuencia_final) SELECT (ini||'-'||fin||' GHz'), (ini*1000000), (fin*1000000) from bandatmp;
DROP TABLE bandatmp CASCADE;
DROP TABLE bandanota CASCADE;
CREATE TABLE bandanota (
ini DECIMAL,
fin DECIMAL,
nota VARCHAR(20)
);
COPY bandanota FROM '/tmp/Bandas_kHz_notas_nacionales.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO nota_nacional_banda(id_banda,id_nota_nacional) SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandanota.ini AND frecuencia_final=bandanota.fin) as id_banda,
(SELECT id_nota_nacional FROM nota_nacional WHERE nombre= bandanota.nota) as id_nota_nacional FROM bandanota;
DELETE FROM bandanota WHERE ini IS NOT NULL;
COPY bandanota FROM '/tmp/Bandas_MHz_notas_nacionales.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO nota_nacional_banda(id_banda,id_nota_nacional) SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandanota.ini*1000 AND frecuencia_final=bandanota.fin*1000) as id_banda,
(SELECT id_nota_nacional FROM nota_nacional WHERE nombre= bandanota.nota) as id_nota_nacional FROM bandanota;
DELETE FROM bandanota WHERE ini IS NOT NULL;
COPY bandanota FROM '/tmp/Bandas_GHz_notas_nacionales.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO nota_nacional_banda(id_banda,id_nota_nacional) SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandanota.ini*1000000 AND frecuencia_final=bandanota.fin*1000000) as id_banda,
(SELECT id_nota_nacional FROM nota_nacional WHERE nombre= bandanota.nota) as id_nota_nacional FROM bandanota;
DELETE FROM bandanota WHERE ini IS NOT NULL;
COPY bandanota FROM '/tmp/Bandas_kHz_notas_internacionales.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO nota_internacional_banda(id_banda,id_nota_internacional) SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandanota.ini AND frecuencia_final=bandanota.fin) as id_banda,
(SELECT id_nota_internacional FROM nota_internacional WHERE nombre= bandanota.nota) as id_nota_internacional FROM bandanota;
DELETE FROM bandanota WHERE ini IS NOT NULL;
COPY bandanota FROM '/tmp/Bandas_MHz_notas_internacionales.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO nota_internacional_banda(id_banda,id_nota_internacional) SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandanota.ini*1000 AND frecuencia_final=bandanota.fin*1000) as id_banda,
(SELECT id_nota_internacional FROM nota_internacional WHERE nombre= bandanota.nota) as id_nota_internacional FROM bandanota;
DELETE FROM bandanota WHERE ini IS NOT NULL;
COPY bandanota FROM '/tmp/Bandas_GHz_notas_internacionales.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO nota_internacional_banda(id_banda,id_nota_internacional) SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandanota.ini*1000000 AND frecuencia_final=bandanota.fin*1000000) as id_banda,
(SELECT id_nota_internacional FROM nota_internacional WHERE nombre= bandanota.nota) as id_nota_internacional FROM bandanota;
DROP TABLE bandanota CASCADE;
COPY servicio(nombre,tipo) FROM '/tmp/servicios.csv' WITH DELIMITER '$' NULL AS '';
DROP TABLE bandaservicio CASCADE;
CREATE TABLE bandaservicio(
ini DECIMAL,
fin DECIMAL,
servicio VARCHAR(200)
);
COPY bandaservicio FROM '/tmp/Bandas_kHz_servicios.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO servicio_banda SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandaservicio.ini AND frecuencia_final=bandaservicio.fin)as id_banda, 
(SELECT id_servicio FROM servicio WHERE nombre=bandaservicio.servicio) as id_servicio FROM bandaservicio;
DELETE FROM bandaservicio WHERE ini IS NOT NULL;
COPY bandaservicio FROM '/tmp/Bandas_MHz_servicios.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO servicio_banda SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandaservicio.ini*1000 AND frecuencia_final=bandaservicio.fin*1000)as id_banda, 
(SELECT id_servicio FROM servicio WHERE nombre=bandaservicio.servicio) as id_servicio FROM bandaservicio;
DELETE FROM bandaservicio WHERE ini IS NOT NULL;
COPY bandaservicio FROM '/tmp/Bandas_GHz_servicios.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO servicio_banda SELECT 
(SELECT id_banda FROM banda WHERE frecuencia_inicial=bandaservicio.ini*1000000 AND frecuencia_final=bandaservicio.fin*1000000)as id_banda, 
(SELECT id_servicio FROM servicio WHERE nombre=bandaservicio.servicio) as id_servicio FROM bandaservicio;
DROP TABLE bandaservicio CASCADE;
INSERT INTO region(nombre) VALUES('Default');
COPY departamento(nombre,id_region) FROM '/tmp/departamentos.csv' WITH DELIMITER '$' NULL AS '';
DROP TABLE munitmp CASCADE;
CREATE TABLE munitmp (
depa VARCHAR(100),
muni VARCHAR(100)
);
COPY munitmp FROM '/tmp/municipios.csv' WITH DELIMITER '$' NULL AS '';
INSERT INTO municipio (nombre,id_departamento) SELECT muni as nombre,
(SELECT id_departamento FROM departamento WHERE nombre=munitmp.depa) as id_departamento FROM munitmp;
DROP TABLE munitmp CASCADE;


