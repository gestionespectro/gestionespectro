Para cargar la base de datos se debe proceder así:
* por consola entrar en el directorio principal entrega_final
* Una vez ubicado ahí se debe ejecutar los diferentes scripts

Lo primero es copiar los archivos csv en el /tmp/ para cargar los datos en la base de datos.
	sudo su
	./SQL_SCRIPTS/copiaTmp
	exit

Luego debemos crear todas las tablas de la base de datos (todo el esquema)
	psql -h localhost -U proyecto -d adminespectro -f SQL_SCRIPTS/bd__inicial_gestion.sql 
	Se ingresa la contraseña de proyecto: proyecto

Luego cargamos los datos de los csv (se debe usar un usuaro root para la base de datos)
	psql -h localhost -U postgres -d adminespectro -f SQL_SCRIPTS/load_base.sql -W
	Se ingresa la contraseña de postgres (depende del equipo)
	
Para cargar la base de datos con un solo script se puede de la siguiente forma:
	psql -h localhost -U proyecto -d adminespectro < base_gestion_espectro.sql


