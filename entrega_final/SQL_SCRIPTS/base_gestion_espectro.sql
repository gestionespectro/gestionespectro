--
-- PostgreSQL database dump
--

-- Started on 2013-01-27 22:17:03 COT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 1579 (class 1259 OID 42611)
-- Dependencies: 3
-- Name: banda_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banda_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2041 (class 0 OID 0)
-- Dependencies: 1579
-- Name: banda_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('banda_sq', 556, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1580 (class 1259 OID 42613)
-- Dependencies: 1897 3
-- Name: banda; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE banda (
    id_banda integer DEFAULT nextval('banda_sq'::regclass) NOT NULL,
    nombre character varying(50) NOT NULL,
    frecuencia_inicial numeric NOT NULL,
    frecuencia_final numeric NOT NULL
);


--
-- TOC entry 1603 (class 1259 OID 42810)
-- Dependencies: 3
-- Name: canal_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE canal_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2042 (class 0 OID 0)
-- Dependencies: 1603
-- Name: canal_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('canal_sq', 1, false);


--
-- TOC entry 1604 (class 1259 OID 42812)
-- Dependencies: 1907 3
-- Name: canal; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE canal (
    id_canal integer DEFAULT nextval('canal_sq'::regclass) NOT NULL,
    no_canal integer NOT NULL,
    frecuencia_inicial numeric NOT NULL,
    frecuencia_final numeric NOT NULL,
    id_servicio integer NOT NULL
);


--
-- TOC entry 1605 (class 1259 OID 42826)
-- Dependencies: 3
-- Name: canal_licencia; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE canal_licencia (
    id_canal integer NOT NULL,
    id_licencia integer NOT NULL
);


--
-- TOC entry 1606 (class 1259 OID 42841)
-- Dependencies: 3
-- Name: comentario_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comentario_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2043 (class 0 OID 0)
-- Dependencies: 1606
-- Name: comentario_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('comentario_sq', 1, false);


--
-- TOC entry 1607 (class 1259 OID 42843)
-- Dependencies: 1908 3
-- Name: comentario; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE comentario (
    id_comentario integer DEFAULT nextval('comentario_sq'::regclass) NOT NULL,
    descripcion character varying(1000) NOT NULL
);


--
-- TOC entry 1608 (class 1259 OID 42852)
-- Dependencies: 3
-- Name: comentario_canal; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE comentario_canal (
    id_comentario integer NOT NULL,
    id_canal integer NOT NULL
);


--
-- TOC entry 1588 (class 1259 OID 42688)
-- Dependencies: 3
-- Name: departamento_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE departamento_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2044 (class 0 OID 0)
-- Dependencies: 1588
-- Name: departamento_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('departamento_sq', 33, true);


--
-- TOC entry 1589 (class 1259 OID 42690)
-- Dependencies: 1901 3
-- Name: departamento; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE departamento (
    id_departamento integer DEFAULT nextval('departamento_sq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    id_region integer NOT NULL
);


--
-- TOC entry 1609 (class 1259 OID 42867)
-- Dependencies: 3
-- Name: evento_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE evento_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2045 (class 0 OID 0)
-- Dependencies: 1609
-- Name: evento_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('evento_sq', 1, false);


--
-- TOC entry 1610 (class 1259 OID 42869)
-- Dependencies: 1909 3
-- Name: evento; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE evento (
    id_evento integer DEFAULT nextval('evento_sq'::regclass) NOT NULL,
    id_licencia integer NOT NULL,
    descripcion character varying(1000) NOT NULL,
    fecha_evento date NOT NULL
);


--
-- TOC entry 1615 (class 1259 OID 42938)
-- Dependencies: 3
-- Name: excepcion_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE excepcion_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2046 (class 0 OID 0)
-- Dependencies: 1615
-- Name: excepcion_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('excepcion_sq', 1, false);


--
-- TOC entry 1616 (class 1259 OID 42940)
-- Dependencies: 1910 3
-- Name: excepcion; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE excepcion (
    id_excepcion integer DEFAULT nextval('excepcion_sq'::regclass) NOT NULL,
    id_licencia integer NOT NULL
);


--
-- TOC entry 1617 (class 1259 OID 42951)
-- Dependencies: 3
-- Name: excepcion_municipio; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE excepcion_municipio (
    id_excepcion integer NOT NULL,
    id_municipio integer NOT NULL,
    id_canal integer NOT NULL
);


--
-- TOC entry 1592 (class 1259 OID 42714)
-- Dependencies: 3
-- Name: formula_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE formula_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2047 (class 0 OID 0)
-- Dependencies: 1592
-- Name: formula_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('formula_sq', 1, false);


--
-- TOC entry 1593 (class 1259 OID 42716)
-- Dependencies: 1903 3
-- Name: formula; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE formula (
    id_formula integer DEFAULT nextval('formula_sq'::regclass) NOT NULL,
    formula character varying(100) NOT NULL,
    numero_canales integer NOT NULL,
    frecuencia_inicial numeric NOT NULL,
    multiplicador numeric NOT NULL
);


--
-- TOC entry 1594 (class 1259 OID 42725)
-- Dependencies: 3
-- Name: formula_servicio; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE formula_servicio (
    id_formula integer NOT NULL,
    id_servicio integer NOT NULL
);


--
-- TOC entry 1573 (class 1259 OID 40031)
-- Dependencies: 3
-- Name: horarario; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE horarario (
    id_horario integer,
    hora_inicio integer NOT NULL,
    minuto_inicio integer NOT NULL,
    hora_fin integer NOT NULL,
    minuto_fin integer NOT NULL
);


--
-- TOC entry 1598 (class 1259 OID 42766)
-- Dependencies: 3
-- Name: horario_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE horario_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2048 (class 0 OID 0)
-- Dependencies: 1598
-- Name: horario_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('horario_sq', 1, false);


--
-- TOC entry 1599 (class 1259 OID 42768)
-- Dependencies: 1905 3
-- Name: horario; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE horario (
    id_horario integer DEFAULT nextval('horario_sq'::regclass) NOT NULL,
    hora_inicio integer NOT NULL,
    minuto_inicio integer NOT NULL,
    hora_fin integer NOT NULL,
    minuto_fin integer NOT NULL
);


--
-- TOC entry 1600 (class 1259 OID 42774)
-- Dependencies: 3
-- Name: licencia_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE licencia_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2049 (class 0 OID 0)
-- Dependencies: 1600
-- Name: licencia_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('licencia_sq', 1, false);


--
-- TOC entry 1601 (class 1259 OID 42776)
-- Dependencies: 1906 3
-- Name: licencia; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE licencia (
    id_licencia integer DEFAULT nextval('licencia_sq'::regclass) NOT NULL,
    descripcion character varying(2000) NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_vencimiento date NOT NULL,
    id_operador integer NOT NULL
);


--
-- TOC entry 1613 (class 1259 OID 42908)
-- Dependencies: 3
-- Name: licencia_departamental; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE licencia_departamental (
    id_licencia integer NOT NULL,
    id_departamento integer NOT NULL
);


--
-- TOC entry 1614 (class 1259 OID 42923)
-- Dependencies: 3
-- Name: licencia_municipal; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE licencia_municipal (
    id_licencia integer NOT NULL,
    id_municipio integer NOT NULL
);


--
-- TOC entry 1612 (class 1259 OID 42898)
-- Dependencies: 3
-- Name: licencia_nacional; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE licencia_nacional (
    id_licencia integer NOT NULL
);


--
-- TOC entry 1611 (class 1259 OID 42883)
-- Dependencies: 3
-- Name: licencia_regional; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE licencia_regional (
    id_licencia integer NOT NULL,
    id_region integer NOT NULL
);


--
-- TOC entry 1590 (class 1259 OID 42701)
-- Dependencies: 3
-- Name: municipio_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE municipio_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2050 (class 0 OID 0)
-- Dependencies: 1590
-- Name: municipio_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('municipio_sq', 1122, true);


--
-- TOC entry 1591 (class 1259 OID 42703)
-- Dependencies: 1902 3
-- Name: municipio; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE municipio (
    id_municipio integer DEFAULT nextval('municipio_sq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    id_departamento integer NOT NULL
);


--
-- TOC entry 1575 (class 1259 OID 42589)
-- Dependencies: 3
-- Name: nota_internacional_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE nota_internacional_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2051 (class 0 OID 0)
-- Dependencies: 1575
-- Name: nota_internacional_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('nota_internacional_sq', 719, true);


--
-- TOC entry 1576 (class 1259 OID 42591)
-- Dependencies: 1895 3
-- Name: nota_internacional; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nota_internacional (
    id_nota_internacional integer DEFAULT nextval('nota_internacional_sq'::regclass) NOT NULL,
    nombre character varying(20) NOT NULL,
    descripcion character varying(6000) NOT NULL
);


--
-- TOC entry 1582 (class 1259 OID 42637)
-- Dependencies: 3
-- Name: nota_internacional_banda; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nota_internacional_banda (
    id_banda integer NOT NULL,
    id_nota_internacional integer NOT NULL
);


--
-- TOC entry 1577 (class 1259 OID 42600)
-- Dependencies: 3
-- Name: nota_nacional_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE nota_nacional_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2052 (class 0 OID 0)
-- Dependencies: 1577
-- Name: nota_nacional_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('nota_nacional_sq', 97, true);


--
-- TOC entry 1578 (class 1259 OID 42602)
-- Dependencies: 1896 3
-- Name: nota_nacional; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nota_nacional (
    id_nota_nacional integer DEFAULT nextval('nota_nacional_sq'::regclass) NOT NULL,
    nombre character varying(20) NOT NULL,
    descripcion character varying(6000) NOT NULL
);


--
-- TOC entry 1581 (class 1259 OID 42622)
-- Dependencies: 3
-- Name: nota_nacional_banda; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE nota_nacional_banda (
    id_banda integer NOT NULL,
    id_nota_nacional integer NOT NULL
);


--
-- TOC entry 1595 (class 1259 OID 42740)
-- Dependencies: 3
-- Name: operador_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE operador_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2053 (class 0 OID 0)
-- Dependencies: 1595
-- Name: operador_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('operador_sq', 1, false);


--
-- TOC entry 1596 (class 1259 OID 42742)
-- Dependencies: 1904 3
-- Name: operador; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE operador (
    id_operador integer DEFAULT nextval('operador_sq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion character varying(2000) NOT NULL
);


--
-- TOC entry 1574 (class 1259 OID 42581)
-- Dependencies: 3
-- Name: rango_frecuencias; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE rango_frecuencias (
    id_frecuencias character varying(20) NOT NULL,
    nombre character varying(100) NOT NULL,
    frecuencia_inicial numeric NOT NULL,
    frecuencia_final numeric NOT NULL,
    longitud_onda_inicial numeric NOT NULL,
    longitud_onda_final numeric NOT NULL
);


--
-- TOC entry 1586 (class 1259 OID 42677)
-- Dependencies: 3
-- Name: region_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE region_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2054 (class 0 OID 0)
-- Dependencies: 1586
-- Name: region_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('region_sq', 1, true);


--
-- TOC entry 1587 (class 1259 OID 42679)
-- Dependencies: 1899 1900 3
-- Name: region; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE region (
    id_region integer DEFAULT nextval('region_sq'::regclass) NOT NULL,
    nombre character varying(100) DEFAULT 'Default'::character varying NOT NULL
);


--
-- TOC entry 1583 (class 1259 OID 42652)
-- Dependencies: 3
-- Name: servicio_sq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE servicio_sq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 1583
-- Name: servicio_sq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('servicio_sq', 117, true);


--
-- TOC entry 1584 (class 1259 OID 42654)
-- Dependencies: 1898 3
-- Name: servicio; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE servicio (
    id_servicio integer DEFAULT nextval('servicio_sq'::regclass) NOT NULL,
    nombre character varying(200) NOT NULL,
    tipo character varying(20) NOT NULL
);


--
-- TOC entry 1585 (class 1259 OID 42662)
-- Dependencies: 3
-- Name: servicio_banda; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE servicio_banda (
    id_banda integer NOT NULL,
    id_servicio integer NOT NULL
);


--
-- TOC entry 1602 (class 1259 OID 42790)
-- Dependencies: 3
-- Name: servicio_licencia; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE servicio_licencia (
    id_servicio integer NOT NULL,
    id_licencia integer NOT NULL,
    id_horario integer NOT NULL
);


--
-- TOC entry 1597 (class 1259 OID 42751)
-- Dependencies: 3
-- Name: servicio_operador; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE servicio_operador (
    id_operador integer NOT NULL,
    id_servicio integer NOT NULL
);


--
-- TOC entry 2010 (class 0 OID 42613)
-- Dependencies: 1580
-- Data for Name: banda; Type: TABLE DATA; Schema: public; Owner: -
--

COPY banda (id_banda, nombre, frecuencia_inicial, frecuencia_final) FROM stdin;
1	0-9 kHz	0	9
2	9-14 kHz	9	14
3	14-19.95 kHz	14	19.95
4	19.95-20.05 kHz	19.95	20.05
5	20.05-70 kHz	20.05	70
6	70-90 kHz	70	90
7	90-110 kHz	90	110
8	110-130 kHz	110	130
9	130-135.7 kHz	130	135.7
10	135.7-137.8 kHz	135.7	137.8
11	137.8-160 kHz	137.8	160
12	160-190 kHz	160	190
13	190-200 kHz	190	200
14	200-275 kHz	200	275
15	275-285 kHz	275	285
16	285-315 kHz	285	315
17	315-325 kHz	315	325
18	325-335 kHz	325	335
19	335-405 kHz	335	405
20	405-415 kHz	405	415
21	415-495 kHz	415	495
22	495-505 kHz	495	505
23	505-510 kHz	505	510
24	510-525 kHz	510	525
25	525-535 kHz	525	535
26	535-1605 kHz	535	1605
27	1605-1625 kHz	1605	1625
28	1625-1705 kHz	1625	1705
29	1705-1800 kHz	1705	1800
30	1800-1850 kHz	1800	1850
31	1850-2000 kHz	1850	2000
32	2000-2065 kHz	2000	2065
33	2065-2107 kHz	2065	2107
34	2107-2170 kHz	2107	2170
35	2170-2173.5 kHz	2170	2173.5
36	2173.5-2190.5 kHz	2173.5	2190.5
37	2190.5-2194 kHz	2190.5	2194
38	2194-2300 kHz	2194	2300
39	2300-2495 kHz	2300	2495
40	2495-2501 kHz	2495	2501
41	2501-2502 kHz	2501	2502
42	2502-2505 kHz	2502	2505
43	2505-2850 kHz	2505	2850
44	2850-3025 kHz	2850	3025
45	3025-3155 kHz	3025	3155
46	3155-3200 kHz	3155	3200
47	3200-3230 kHz	3200	3230
48	3230-3400 kHz	3230	3400
49	3400-3500 kHz	3400	3500
50	3500-3750 kHz	3500	3750
51	3750-4000 kHz	3750	4000
52	4000-4063 kHz	4000	4063
53	4063-4438 kHz	4063	4438
54	4438-4650 kHz	4438	4650
55	4650-4700 kHz	4650	4700
56	4700-4750 kHz	4700	4750
57	4750-4850 kHz	4750	4850
58	4850-4995 kHz	4850	4995
59	4995-5003 kHz	4995	5003
60	5003-5005 kHz	5003	5005
61	5005-5060 kHz	5005	5060
62	5060-5250 kHz	5060	5250
63	5250-5450 kHz	5250	5450
64	5450-5480 kHz	5450	5480
65	5480-5680 kHz	5480	5680
66	5680-5730 kHz	5680	5730
67	5730-5900 kHz	5730	5900
68	5900-5950 kHz	5900	5950
69	5950-6200 kHz	5950	6200
70	6200-6525 kHz	6200	6525
71	6525-6685 kHz	6525	6685
72	6685-6765 kHz	6685	6765
73	6765-7000 kHz	6765	7000
74	7000-7100 kHz	7000	7100
75	7100-7200 kHz	7100	7200
76	7200-7300 kHz	7200	7300
77	7300-7400 kHz	7300	7400
78	7400-7450 kHz	7400	7450
79	7450-8100 kHz	7450	8100
80	8100-8195 kHz	8100	8195
81	8195-8815 kHz	8195	8815
82	8815-8965 kHz	8815	8965
83	8965-9040 kHz	8965	9040
84	9040-9400 kHz	9040	9400
85	9400-9500 kHz	9400	9500
86	9500-9900 kHz	9500	9900
87	9900-9995 kHz	9900	9995
88	9995-10003 kHz	9995	10003
89	10003-10005 kHz	10003	10005
90	10005-10100 kHz	10005	10100
91	10100-10150 kHz	10100	10150
92	10150-11175 kHz	10150	11175
93	11175-11275 kHz	11175	11275
94	11275-11400 kHz	11275	11400
95	11400-11600 kHz	11400	11600
96	11600-11650 kHz	11600	11650
97	11650-12050 kHz	11650	12050
98	12050-12100 kHz	12050	12100
99	12100-12230 kHz	12100	12230
100	12230-13200 kHz	12230	13200
101	13200-13260 kHz	13200	13260
102	13260-13360 kHz	13260	13360
103	13360-13410 kHz	13360	13410
104	13410-13570 kHz	13410	13570
105	13570-13600 kHz	13570	13600
106	13600-13800 kHz	13600	13800
107	13800-13870 kHz	13800	13870
108	13870-14000 kHz	13870	14000
109	14000-14250 kHz	14000	14250
110	14250-14350 kHz	14250	14350
111	14350-14990 kHz	14350	14990
112	14990-15005 kHz	14990	15005
113	15005-15010 kHz	15005	15010
114	15010-15100 kHz	15010	15100
115	15100-15600 kHz	15100	15600
116	15600-15800 kHz	15600	15800
117	15800-16360 kHz	15800	16360
118	16360-17410 kHz	16360	17410
119	17410-17480 kHz	17410	17480
120	17480-17550 kHz	17480	17550
121	17550-17900 kHz	17550	17900
122	17900-17970 kHz	17900	17970
123	17970-18030 kHz	17970	18030
124	18030-18052 kHz	18030	18052
125	18052-18068 kHz	18052	18068
126	18068-18168 kHz	18068	18168
127	18168-18780 kHz	18168	18780
128	18780-18900 kHz	18780	18900
129	18900-19020 kHz	18900	19020
130	19020-19680 kHz	19020	19680
131	19680-19800 kHz	19680	19800
132	19800-19990 kHz	19800	19990
133	19990-19995 kHz	19990	19995
134	19995-20010 kHz	19995	20010
135	20010-21000 kHz	20010	21000
136	21000-21450 kHz	21000	21450
137	21450-21850 kHz	21450	21850
138	21850-21870 kHz	21850	21870
139	21870-21924 kHz	21870	21924
140	21924-22000 kHz	21924	22000
141	22000-22855 kHz	22000	22855
142	22855-23000 kHz	22855	23000
143	23000-23200 kHz	23000	23200
144	23200-23350 kHz	23200	23350
145	23350-24000 kHz	23350	24000
146	24000-24890 kHz	24000	24890
147	24890-24990 kHz	24890	24990
148	24990-25005 kHz	24990	25005
149	25005-25010 kHz	25005	25010
150	25010-25070 kHz	25010	25070
151	25070-25210 kHz	25070	25210
152	25210-25550 kHz	25210	25550
153	25550-25670 kHz	25550	25670
154	25670-26100 kHz	25670	26100
155	26100-26175 kHz	26100	26175
156	26175-27500 kHz	26175	27500
157	27.5-28 MHz	27500.0	28000
158	28-29.7 MHz	28000	29700.0
159	29.7-44 MHz	29700.0	44000
160	44-47 MHz	44000	47000
161	47-50 MHz	47000	50000
162	50-54 MHz	50000	54000
163	54-68 MHz	54000	68000
164	68-72 MHz	68000	72000
165	72-73 MHz	72000	73000
166	73-74.6 MHz	73000	74600.0
167	74.6-74.8 MHz	74600.0	74800.0
168	74.8-75.2 MHz	74800.0	75200.0
169	75.2-75.4 MHz	75200.0	75400.0
170	75.4-76 MHz	75400.0	76000
171	76-88 MHz	76000	88000
172	88-108 MHz	88000	108000
173	108-117.975 MHz	108000	117975.000
174	117.975-137 MHz	117975.000	137000
175	137-137.025 MHz	137000	137025.000
176	137.025-137.175 MHz	137025.000	137175.000
177	137.175-137.825 MHz	137175.000	137825.000
178	137.825-138 MHz	137825.000	138000
179	138-143.6 MHz	138000	143600.0
180	143.6-143.65 MHz	143600.0	143650.00
181	143.65-144 MHz	143650.00	144000
182	144-146 MHz	144000	146000
183	146-148 MHz	146000	148000
184	148-149.9 MHz	148000	149900.0
185	149.9-150.05 MHz	149900.0	150050.00
186	150.05-156.4875 MHz	150050.00	156487.5000
187	156.4875-156.5625 MHz	156487.5000	156562.5000
188	156.5625-156.7625 MHz	156562.5000	156762.5000
189	156.7625-157.45 MHz	156762.5000	157450.00
190	157.45-160.6 MHz	157450.00	160600.0
191	160.6-162.05 MHz	160600.0	162050.00
192	162.05-174 MHz	162050.00	174000
193	174-216 MHz	174000	216000
194	216-220 MHz	216000	220000
195	220-225 MHz	220000	225000
196	225-227.5 MHz	225000	227500.0
197	227.5-228.25 MHz	227500.0	228250.00
198	228.25-232.5 MHz	228250.00	232500.0
199	232.5-233.25 MHz	232500.0	233250.00
200	233.25-235 MHz	233250.00	235000
201	235-245.45 MHz	235000	245450.00
202	245.45-246.95 MHz	245450.00	246950.00
203	246.95-267 MHz	246950.00	267000
204	267-272 MHz	267000	272000
205	272-273 MHz	272000	273000
206	273-300 MHz	273000	300000
207	300-312 MHz	300000	312000
208	312-315 MHz	312000	315000
209	315-322 MHz	315000	322000
210	322-328.6 MHz	322000	328600.0
211	328.6-335.4 MHz	328600.0	335400.0
212	335.4-343 MHz	335400.0	343000
213	343-343.05 MHz	343000	343050.00
214	343.05-345.15 MHz	343050.00	345150.00
215	345.15-357.05 MHz	345150.00	357050.00
216	357.05-359.15 MHz	357050.00	359150.00
217	359.15-380.025 MHz	359150.00	380025.000
218	380.025-382 MHz	380025.000	382000
219	382-387 MHz	382000	387000
220	387-390 MHz	387000	390000
221	390-390.025 MHz	390000	390025.000
222	390.025-392 MHz	390025.000	392000
223	392-399.9 MHz	392000	399900.0
224	399.9-400.05 MHz	399900.0	400050.00
225	400.05-400.15 MHz	400050.00	400150.00
226	400.15-401 MHz	400150.00	401000
227	401-402 MHz	401000	402000
228	402-403 MHz	402000	403000
229	403-406 MHz	403000	406000
230	406-406.1 MHz	406000	406100.0
231	406.1-410 MHz	406100.0	410000
232	410-420 MHz	410000	420000
233	420-430 MHz	420000	430000
234	430-432 MHz	430000	432000
235	432-438 MHz	432000	438000
236	438-440 MHz	438000	440000
237	440-450 MHz	440000	450000
238	450-455 MHz	450000	455000
239	455-456 MHz	455000	456000
240	456-459 MHz	456000	459000
241	459-460 MHz	459000	460000
242	460-470 MHz	460000	470000
243	470-482 MHz	470000	482000
244	482-512 MHz	482000	512000
245	512-608 MHz	512000	608000
246	608-614 MHz	608000	614000
247	614-698 MHz	614000	698000
248	698-806 MHz	698000	806000
249	806-821 MHz	806000	821000
250	821-824 MHz	821000	824000
251	824-849 MHz	824000	849000
252	849-851 MHz	849000	851000
253	851-866 MHz	851000	866000
254	866-869 MHz	866000	869000
255	869-890 MHz	869000	890000
256	890-894 MHz	890000	894000
257	894-896 MHz	894000	896000
258	896-897.125 MHz	896000	897125.000
259	897.125-897.5 MHz	897125.000	897500.0
260	897.5-902 MHz	897500.0	902000
261	902-905 MHz	902000	905000
262	905-908 MHz	905000	908000
263	908-915 MHz	908000	915000
264	915-924 MHz	915000	924000
265	924-928 MHz	924000	928000
266	928-932 MHz	928000	932000
267	932-935 MHz	932000	935000
268	935-936.125 MHz	935000	936125.000
269	936.125-940 MHz	936125.000	940000
270	940-942 MHz	940000	942000
271	942-942.5 MHz	942000	942500.0
272	942.5-950 MHz	942500.0	950000
273	950-953 MHz	950000	953000
274	953-960 MHz	953000	960000
275	960-1164 MHz	960000	1164000
276	1164-1215 MHz	1164000	1215000
277	1215-1240 MHz	1215000	1240000
278	1240-1260 MHz	1240000	1260000
279	1260-1300 MHz	1260000	1300000
280	1300-1350 MHz	1300000	1350000
281	1350-1400 MHz	1350000	1400000
282	1400-1427 MHz	1400000	1427000
283	1427-1429 MHz	1427000	1429000
284	1429-1452 MHz	1429000	1452000
285	1452-1492 MHz	1452000	1492000
286	1492-1518 MHz	1492000	1518000
287	1518-1525 MHz	1518000	1525000
288	1525-1530 MHz	1525000	1530000
289	1530-1533 MHz	1530000	1533000
290	1533-1535 MHz	1533000	1535000
291	1535-1544 MHz	1535000	1544000
292	1544-1545 MHz	1544000	1545000
293	1545-1555 MHz	1545000	1555000
294	1555-1559 MHz	1555000	1559000
295	1559-1610 MHz	1559000	1610000
296	1610-1610.6 MHz	1610000	1610600.0
297	1610.6-1613.8 MHz	1610600.0	1613800.0
298	1613.8-1626.5 MHz	1613800.0	1626500.0
299	1626.5-1631.5 MHz	1626500.0	1631500.0
300	1631.5-1636.5 MHz	1631500.0	1636500.0
301	1636.5-1645.5 MHz	1636500.0	1645500.0
302	1645.5-1646.5 MHz	1645500.0	1646500.0
303	1646.5-1656.5 MHz	1646500.0	1656500.0
304	1656.5-1660 MHz	1656500.0	1660000
305	1660-1660.5 MHz	1660000	1660500.0
306	1660.5-1668 MHz	1660500.0	1668000
307	1668-1668.4 MHz	1668000	1668400.0
308	1668.4-1670 MHz	1668400.0	1670000
309	1670-1675 MHz	1670000	1675000
310	1675-1690 MHz	1675000	1690000
311	1690-1700 MHz	1690000	1700000
312	1700-1710 MHz	1700000	1710000
313	1710-1890 MHz	1710000	1890000
314	1890-1895 MHz	1890000	1895000
315	1895-1910 MHz	1895000	1910000
316	1910-1930 MHz	1910000	1930000
317	1930-1970 MHz	1930000	1970000
318	1970-1975 MHz	1970000	1975000
319	1975-1980 MHz	1975000	1980000
320	1980-1990 MHz	1980000	1990000
321	1990-2010 MHz	1990000	2010000
322	2010-2025 MHz	2010000	2025000
323	2025-2110 MHz	2025000	2110000
324	2110-2120 MHz	2110000	2120000
325	2120-2160 MHz	2120000	2160000
326	2160-2170 MHz	2160000	2170000
327	2170-2200 MHz	2170000	2200000
328	2200-2290 MHz	2200000	2290000
329	2290-2300 MHz	2290000	2300000
330	2300-2400 MHz	2300000	2400000
331	2400-2450 MHz	2400000	2450000
332	2450-2483.5 MHz	2450000	2483500.0
333	2483.5-2500 MHz	2483500.0	2500000
334	2500-2520 MHz	2500000	2520000
335	2520-2655 MHz	2520000	2655000
336	2655-2670 MHz	2655000	2670000
337	2670-2690 MHz	2670000	2690000
338	2690-2700 MHz	2690000	2700000
339	2700-2900 MHz	2700000	2900000
340	2900-3100 MHz	2900000	3100000
341	3100-3300 MHz	3100000	3300000
342	3300-3400 MHz	3300000	3400000
343	3400-3500 MHz	3400000	3500000
344	3500-3700 MHz	3500000	3700000
345	3700-4200 MHz	3700000	4200000
346	4200-4400 MHz	4200000	4400000
347	4400-4500 MHz	4400000	4500000
348	4500-4800 MHz	4500000	4800000
349	4800-4990 MHz	4800000	4990000
350	4990-5000 MHz	4990000	5000000
351	5000-5010 MHz	5000000	5010000
352	5010-5030 MHz	5010000	5030000
353	5030-5091 MHz	5030000	5091000
354	5091-5150 MHz	5091000	5150000
355	5150-5250 MHz	5150000	5250000
356	5250-5255 MHz	5250000	5255000
357	5255-5350 MHz	5255000	5350000
358	5350-5460 MHz	5350000	5460000
359	5460-5470 MHz	5460000	5470000
360	5470-5570 MHz	5470000	5570000
361	5570-5650 MHz	5570000	5650000
362	5650-5725 MHz	5650000	5725000
363	5725-5830 MHz	5725000	5830000
364	5830-5850 MHz	5830000	5850000
365	5850-5925 MHz	5850000	5925000
366	5925-6700 MHz	5925000	6700000
367	6700-7075 MHz	6700000	7075000
368	7075-7145 MHz	7075000	7145000
369	7145-7235 MHz	7145000	7235000
370	7235-7250 MHz	7235000	7250000
371	7250-7300 MHz	7250000	7300000
372	7300-7450 MHz	7300000	7450000
373	7450-7550 MHz	7450000	7550000
374	7550-7750 MHz	7550000	7750000
375	7750-7850 MHz	7750000	7850000
376	7850-7900 MHz	7850000	7900000
377	7900-8025 MHz	7900000	8025000
378	8025-8175 MHz	8025000	8175000
379	8175-8215 MHz	8175000	8215000
380	8215-8400 MHz	8215000	8400000
381	8400-8500 MHz	8400000	8500000
382	8500-8550 MHz	8500000	8550000
383	8550-8650 MHz	8550000	8650000
384	8650-8750 MHz	8650000	8750000
385	8750-8850 MHz	8750000	8850000
386	8850-9000 MHz	8850000	9000000
387	9000-9200 MHz	9000000	9200000
388	9200-9300 MHz	9200000	9300000
389	9300-9500 MHz	9300000	9500000
390	9500-9800 MHz	9500000	9800000
391	9800-9900 MHz	9800000	9900000
392	9900-10000 MHz	9900000	10000000
393	10-10.45 GHz	10000000	10450000.00
394	10.45-10.5 GHz	10450000.00	10500000.0
395	10.5-10.55 GHz	10500000.0	10550000.00
396	10.55-10.6 GHz	10550000.00	10600000.0
397	10.6-10.68 GHz	10600000.0	10680000.00
398	10.68-10.7 GHz	10680000.00	10700000.0
399	10.7-11.7 GHz	10700000.0	11700000.0
400	11.7-12.1 GHz	11700000.0	12100000.0
401	12.1-12.2 GHz	12100000.0	12200000.0
402	12.2-12.7 GHz	12200000.0	12700000.0
403	12.7-12.75 GHz	12700000.0	12750000.00
404	12.75-13.25 GHz	12750000.00	13250000.00
405	13.25-13.4 GHz	13250000.00	13400000.0
406	13.4-13.75 GHz	13400000.0	13750000.00
407	13.75-14 GHz	13750000.00	14000000
408	14-14.25 GHz	14000000	14250000.00
409	14.25-14.3 GHz	14250000.00	14300000.0
410	14.3-14.4 GHz	14300000.0	14400000.0
411	14.4-14.47 GHz	14400000.0	14470000.00
412	14.47-14.5 GHz	14470000.00	14500000.0
413	14.5-14.8 GHz	14500000.0	14800000.0
414	14.8-15.35 GHz	14800000.0	15350000.00
415	15.35-15.4 GHz	15350000.00	15400000.0
416	15.4-15.43 GHz	15400000.0	15430000.00
417	15.43-15.63 GHz	15430000.00	15630000.00
418	15.63-15.7 GHz	15630000.00	15700000.0
419	15.7-16.6 GHz	15700000.0	16600000.0
420	16.6-17.1 GHz	16600000.0	17100000.0
421	17.1-17.2 GHz	17100000.0	17200000.0
422	17.2-17.3 GHz	17200000.0	17300000.0
423	17.3-17.7 GHz	17300000.0	17700000.0
424	17.7-17.8 GHz	17700000.0	17800000.0
425	17.8-18.1 GHz	17800000.0	18100000.0
426	18.1-18.4 GHz	18100000.0	18400000.0
427	18.4-18.6 GHz	18400000.0	18600000.0
428	18.6-18.8 GHz	18600000.0	18800000.0
429	18.8-19.3 GHz	18800000.0	19300000.0
430	19.3-19.7 GHz	19300000.0	19700000.0
431	19.7-20.1 GHz	19700000.0	20100000.0
432	20.1-20.2 GHz	20100000.0	20200000.0
433	20.2-21.2 GHz	20200000.0	21200000.0
434	21.2-21.4 GHz	21200000.0	21400000.0
435	21.4-22 GHz	21400000.0	22000000
436	22-22.21 GHz	22000000	22210000.00
437	22.21-22.5 GHz	22210000.00	22500000.0
438	22.5-22.55 GHz	22500000.0	22550000.00
439	22.55-23.55 GHz	22550000.00	23550000.00
440	23.55-23.6 GHz	23550000.00	23600000.0
441	23.6-24 GHz	23600000.0	24000000
442	24-24.05 GHz	24000000	24050000.00
443	24.05-24.25 GHz	24050000.00	24250000.00
444	24.25-24.45 GHz	24250000.00	24450000.00
445	24.45-24.65 GHz	24450000.00	24650000.00
446	24.65-24.75 GHz	24650000.00	24750000.00
447	24.75-25.25 GHz	24750000.00	25250000.00
448	25.25-25.5 GHz	25250000.00	25500000.0
449	25.5-27 GHz	25500000.0	27000000
450	27-27.5 GHz	27000000	27500000.0
451	27.5-28.5 GHz	27500000.0	28500000.0
452	28.5-29.1 GHz	28500000.0	29100000.0
453	29.1-29.5 GHz	29100000.0	29500000.0
454	29.5-29.9 GHz	29500000.0	29900000.0
455	29.9-30 GHz	29900000.0	30000000
456	30-31 GHz	30000000	31000000
457	31-31.3 GHz	31000000	31300000.0
458	31.3-31.5 GHz	31300000.0	31500000.0
459	31.5-31.8 GHz	31500000.0	31800000.0
460	31.8-32 GHz	31800000.0	32000000
461	32-32.3 GHz	32000000	32300000.0
462	32.3-33 GHz	32300000.0	33000000
463	33-33.4 GHz	33000000	33400000.0
464	33.4-34.2 GHz	33400000.0	34200000.0
465	34.2-34.7 GHz	34200000.0	34700000.0
466	34.7-35.2 GHz	34700000.0	35200000.0
467	35.2-35.5 GHz	35200000.0	35500000.0
468	35.5-36 GHz	35500000.0	36000000
469	36-37 GHz	36000000	37000000
470	37-37.5 GHz	37000000	37500000.0
471	37.5-37.55 GHz	37500000.0	37550000.00
472	37.55-38 GHz	37550000.00	38000000
473	38-39.5 GHz	38000000	39500000.0
474	39.5-40 GHz	39500000.0	40000000
475	40-40.5 GHz	40000000	40500000.0
476	40.5-41 GHz	40500000.0	41000000
477	41-42.5 GHz	41000000	42500000.0
478	42.5-43.5 GHz	42500000.0	43500000.0
479	43.5-47 GHz	43500000.0	47000000
480	47-47.2 GHz	47000000	47200000.0
481	47.2-47.5 GHz	47200000.0	47500000.0
482	47.5-47.9 GHz	47500000.0	47900000.0
483	47.9-48.2 GHz	47900000.0	48200000.0
484	48.2-50.2 GHz	48200000.0	50200000.0
485	50.2-50.4 GHz	50200000.0	50400000.0
486	50.4-51.4 GHz	50400000.0	51400000.0
487	51.4-52.6 GHz	51400000.0	52600000.0
488	52.6-54.25 GHz	52600000.0	54250000.00
489	54.25-55.78 GHz	54250000.00	55780000.00
490	55.78-56.9 GHz	55780000.00	56900000.0
491	56.9-57 GHz	56900000.0	57000000
492	57-58.2 GHz	57000000	58200000.0
493	58.2-59 GHz	58200000.0	59000000
494	59-59.3 GHz	59000000	59300000.0
495	59.3-64 GHz	59300000.0	64000000
496	64-65 GHz	64000000	65000000
497	65-66 GHz	65000000	66000000
498	66-71 GHz	66000000	71000000
499	71-74 GHz	71000000	74000000
500	74-76 GHz	74000000	76000000
501	76-77.5 GHz	76000000	77500000.0
502	77.5-78 GHz	77500000.0	78000000
503	78-79 GHz	78000000	79000000
504	79-81 GHz	79000000	81000000
505	81-84 GHz	81000000	84000000
506	84-86 GHz	84000000	86000000
507	86-92 GHz	86000000	92000000
508	92-94 GHz	92000000	94000000
509	94-94.1 GHz	94000000	94100000.0
510	94.1-95 GHz	94100000.0	95000000
511	95-100 GHz	95000000	100000000
512	100-102 GHz	100000000	102000000
513	102-105 GHz	102000000	105000000
514	105-109.5 GHz	105000000	109500000.0
515	109.5-111.8 GHz	109500000.0	111800000.0
516	111.8-114.25 GHz	111800000.0	114250000.00
517	114.25-116 GHz	114250000.00	116000000
518	116-119.98 GHz	116000000	119980000.00
519	119.98-120.02 GHz	119980000.00	120020000.00
520	120.02-122.25 GHz	120020000.00	122250000.00
521	122.25-123 GHz	122250000.00	123000000
522	123-126 GHz	123000000	126000000
523	126-130 GHz	126000000	130000000
524	130-134 GHz	130000000	134000000
525	134-136 GHz	134000000	136000000
526	136-141 GHz	136000000	141000000
527	141-148.5 GHz	141000000	148500000.0
528	148.5-151.5 GHz	148500000.0	151500000.0
529	151.5-155.5 GHz	151500000.0	155500000.0
530	155.5-158.5 GHz	155500000.0	158500000.0
531	158.5-164 GHz	158500000.0	164000000
532	164-167 GHz	164000000	167000000
533	167-168 GHz	167000000	168000000
534	168-170 GHz	168000000	170000000
535	170-174.5 GHz	170000000	174500000.0
536	174.5-174.8 GHz	174500000.0	174800000.0
537	174.8-182 GHz	174800000.0	182000000
538	182-185 GHz	182000000	185000000
539	185-190 GHz	185000000	190000000
540	190-191.8 GHz	190000000	191800000.0
541	191.8-200 GHz	191800000.0	200000000
542	200-209 GHz	200000000	209000000
543	209-217 GHz	209000000	217000000
544	217-226 GHz	217000000	226000000
545	226-231.5 GHz	226000000	231500000.0
546	231.5-232 GHz	231500000.0	232000000
547	232-235 GHz	232000000	235000000
548	235-238 GHz	235000000	238000000
549	238-240 GHz	238000000	240000000
550	240-241 GHz	240000000	241000000
551	241-248 GHz	241000000	248000000
552	248-250 GHz	248000000	250000000
553	250-252 GHz	250000000	252000000
554	252-265 GHz	252000000	265000000
555	265-275 GHz	265000000	275000000
556	275-1000 GHz	275000000	1000000000
\.


--
-- TOC entry 2025 (class 0 OID 42812)
-- Dependencies: 1604
-- Data for Name: canal; Type: TABLE DATA; Schema: public; Owner: -
--

COPY canal (id_canal, no_canal, frecuencia_inicial, frecuencia_final, id_servicio) FROM stdin;
\.


--
-- TOC entry 2026 (class 0 OID 42826)
-- Dependencies: 1605
-- Data for Name: canal_licencia; Type: TABLE DATA; Schema: public; Owner: -
--

COPY canal_licencia (id_canal, id_licencia) FROM stdin;
\.


--
-- TOC entry 2027 (class 0 OID 42843)
-- Dependencies: 1607
-- Data for Name: comentario; Type: TABLE DATA; Schema: public; Owner: -
--

COPY comentario (id_comentario, descripcion) FROM stdin;
\.


--
-- TOC entry 2028 (class 0 OID 42852)
-- Dependencies: 1608
-- Data for Name: comentario_canal; Type: TABLE DATA; Schema: public; Owner: -
--

COPY comentario_canal (id_comentario, id_canal) FROM stdin;
\.


--
-- TOC entry 2016 (class 0 OID 42690)
-- Dependencies: 1589
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: -
--

COPY departamento (id_departamento, nombre, id_region) FROM stdin;
1	AMAZONAS	1
2	ANTIOQUIA	1
3	ARAUCA	1
4	ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA	1
5	ATLÁNTICO	1
6	BOGOTÁ, D.C.	1
7	BOLÍVAR	1
8	BOYACÁ	1
9	CALDAS	1
10	CAQUETÁ	1
11	CASANARE	1
12	CAUCA	1
13	CESAR	1
14	CHOCÓ	1
15	CÓRDOBA	1
16	CUNDINAMARCA	1
17	GUAINÍA	1
18	GUAVIARE	1
19	HUILA	1
20	LA GUAJIRA	1
21	MAGDALENA	1
22	META	1
23	NARIÑO	1
24	NORTE DE SANTANDER	1
25	PUTUMAYO	1
26	QUINDIO	1
27	RISARALDA	1
28	SANTANDER	1
29	SUCRE	1
30	TOLIMA	1
31	VALLE DEL CAUCA	1
32	VAUPÉS	1
33	VICHADA	1
\.


--
-- TOC entry 2029 (class 0 OID 42869)
-- Dependencies: 1610
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: -
--

COPY evento (id_evento, id_licencia, descripcion, fecha_evento) FROM stdin;
\.


--
-- TOC entry 2034 (class 0 OID 42940)
-- Dependencies: 1616
-- Data for Name: excepcion; Type: TABLE DATA; Schema: public; Owner: -
--

COPY excepcion (id_excepcion, id_licencia) FROM stdin;
\.


--
-- TOC entry 2035 (class 0 OID 42951)
-- Dependencies: 1617
-- Data for Name: excepcion_municipio; Type: TABLE DATA; Schema: public; Owner: -
--

COPY excepcion_municipio (id_excepcion, id_municipio, id_canal) FROM stdin;
\.


--
-- TOC entry 2018 (class 0 OID 42716)
-- Dependencies: 1593
-- Data for Name: formula; Type: TABLE DATA; Schema: public; Owner: -
--

COPY formula (id_formula, formula, numero_canales, frecuencia_inicial, multiplicador) FROM stdin;
\.


--
-- TOC entry 2019 (class 0 OID 42725)
-- Dependencies: 1594
-- Data for Name: formula_servicio; Type: TABLE DATA; Schema: public; Owner: -
--

COPY formula_servicio (id_formula, id_servicio) FROM stdin;
\.


--
-- TOC entry 2006 (class 0 OID 40031)
-- Dependencies: 1573
-- Data for Name: horarario; Type: TABLE DATA; Schema: public; Owner: -
--

COPY horarario (id_horario, hora_inicio, minuto_inicio, hora_fin, minuto_fin) FROM stdin;
\.


--
-- TOC entry 2022 (class 0 OID 42768)
-- Dependencies: 1599
-- Data for Name: horario; Type: TABLE DATA; Schema: public; Owner: -
--

COPY horario (id_horario, hora_inicio, minuto_inicio, hora_fin, minuto_fin) FROM stdin;
\.


--
-- TOC entry 2023 (class 0 OID 42776)
-- Dependencies: 1601
-- Data for Name: licencia; Type: TABLE DATA; Schema: public; Owner: -
--

COPY licencia (id_licencia, descripcion, fecha_inicio, fecha_vencimiento, id_operador) FROM stdin;
\.


--
-- TOC entry 2032 (class 0 OID 42908)
-- Dependencies: 1613
-- Data for Name: licencia_departamental; Type: TABLE DATA; Schema: public; Owner: -
--

COPY licencia_departamental (id_licencia, id_departamento) FROM stdin;
\.


--
-- TOC entry 2033 (class 0 OID 42923)
-- Dependencies: 1614
-- Data for Name: licencia_municipal; Type: TABLE DATA; Schema: public; Owner: -
--

COPY licencia_municipal (id_licencia, id_municipio) FROM stdin;
\.


--
-- TOC entry 2031 (class 0 OID 42898)
-- Dependencies: 1612
-- Data for Name: licencia_nacional; Type: TABLE DATA; Schema: public; Owner: -
--

COPY licencia_nacional (id_licencia) FROM stdin;
\.


--
-- TOC entry 2030 (class 0 OID 42883)
-- Dependencies: 1611
-- Data for Name: licencia_regional; Type: TABLE DATA; Schema: public; Owner: -
--

COPY licencia_regional (id_licencia, id_region) FROM stdin;
\.


--
-- TOC entry 2017 (class 0 OID 42703)
-- Dependencies: 1591
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: -
--

COPY municipio (id_municipio, nombre, id_departamento) FROM stdin;
1	LETICIA	1
2	EL ENCANTO	1
3	LA CHORRERA	1
4	LA PEDRERA	1
5	LA VICTORIA	1
6	MIRITI - PARANÁ	1
7	PUERTO ALEGRÍA	1
8	PUERTO ARICA	1
9	PUERTO NARIÑO	1
10	PUERTO SANTANDER	1
11	TARAPACÁ	1
12	MEDELLÍN	2
13	ABEJORRAL	2
14	ABRIAQUÍ	2
15	ALEJANDRÍA	2
16	AMAGÁ	2
17	AMALFI	2
18	ANDES	2
19	ANGELÓPOLIS	2
20	ANGOSTURA	2
21	ANORÍ	2
22	SANTAFÉ DE ANTIOQUIA	2
23	ANZA	2
24	APARTADÓ	2
25	ARBOLETES	2
26	ARGELIA	2
27	ARMENIA	2
28	BARBOSA	2
29	BELMIRA	2
30	BELLO	2
31	BETANIA	2
32	BETULIA	2
33	CIUDAD BOLÍVAR	2
34	BRICEÑO	2
35	BURITICÁ	2
36	CÁCERES	2
37	CAICEDO	2
38	CALDAS	2
39	CAMPAMENTO	2
40	CAÑASGORDAS	2
41	CARACOLÍ	2
42	CARAMANTA	2
43	CAREPA	2
44	EL CARMEN DE VIBORAL	2
45	CAROLINA	2
46	CAUCASIA	2
47	CHIGORODÓ	2
48	CISNEROS	2
49	COCORNÁ	2
50	CONCEPCIÓN	2
51	CONCORDIA	2
52	COPACABANA	2
53	DABEIBA	2
54	DONMATÍAS	2
55	EBÉJICO	2
56	EL BAGRE	2
57	ENTRERRIOS	2
58	ENVIGADO	2
59	FREDONIA	2
60	FRONTINO	2
61	GIRALDO	2
62	GIRARDOTA	2
63	GÓMEZ PLATA	2
64	GRANADA	2
65	GUADALUPE	2
66	GUARNE	2
67	GUATAPE	2
68	HELICONIA	2
69	HISPANIA	2
70	ITAGUI	2
71	ITUANGO	2
72	JARDÍN	2
73	JERICÓ	2
74	LA CEJA	2
75	LA ESTRELLA	2
76	LA PINTADA	2
77	LA UNIÓN	2
78	LIBORINA	2
79	MACEO	2
80	MARINILLA	2
81	MONTEBELLO	2
82	MURINDÓ	2
83	MUTATÁ	2
84	NARIÑO	2
85	NECOCLÍ	2
86	NECHÍ	2
87	OLAYA	2
88	PEÑOL	2
89	PEQUE	2
90	PUEBLORRICO	2
91	PUERTO BERRÍO	2
92	PUERTO NARE	2
93	PUERTO TRIUNFO	2
94	REMEDIOS	2
95	RETIRO	2
96	RIONEGRO	2
97	SABANALARGA	2
98	SABANETA	2
99	SALGAR	2
100	SAN ANDRÉS DE CUERQUÍA	2
101	SAN CARLOS	2
102	SAN FRANCISCO	2
103	SAN JERÓNIMO	2
104	SAN JOSÉ DE LA MONTAÑA	2
105	SAN JUAN DE URABÁ	2
106	SAN LUIS	2
107	SAN PEDRO DE LOS MILAGROS	2
108	SAN PEDRO DE URABA	2
109	SAN RAFAEL	2
110	SAN ROQUE	2
111	SAN VICENTE	2
112	SANTA BÁRBARA	2
113	SANTA ROSA DE OSOS	2
114	SANTO DOMINGO	2
115	EL SANTUARIO	2
116	SEGOVIA	2
117	SONSON	2
118	SOPETRÁN	2
119	TÁMESIS	2
120	TARAZÁ	2
121	TARSO	2
122	TITIRIBÍ	2
123	TOLEDO	2
124	TURBO	2
125	URAMITA	2
126	URRAO	2
127	VALDIVIA	2
128	VALPARAÍSO	2
129	VEGACHÍ	2
130	VENECIA	2
131	VIGÍA DEL FUERTE	2
132	YALÍ	2
133	YARUMAL	2
134	YOLOMBÓ	2
135	YONDÓ	2
136	ZARAGOZA	2
137	ARAUCA	3
138	ARAUQUITA	3
139	CRAVO NORTE	3
140	FORTUL	3
141	PUERTO RONDÓN	3
142	SARAVENA	3
143	TAME	3
144	SAN ANDRÉS	4
145	PROVIDENCIA	4
146	BARRANQUILLA	5
147	BARANOA	5
148	CAMPO DE LA CRUZ	5
149	CANDELARIA	5
150	GALAPA	5
151	JUAN DE ACOSTA	5
152	LURUACO	5
153	MALAMBO	5
154	MANATÍ	5
155	PALMAR DE VARELA	5
156	PIOJÓ	5
157	POLONUEVO	5
158	PONEDERA	5
159	PUERTO COLOMBIA	5
160	REPELÓN	5
161	SABANAGRANDE	5
162	SABANALARGA	5
163	SANTA LUCÍA	5
164	SANTO TOMÁS	5
165	SOLEDAD	5
166	SUAN	5
167	TUBARÁ	5
168	USIACURÍ	5
169	BOGOTÁ, D.C.	6
170	CARTAGENA	7
171	ACHÍ	7
172	ALTOS DEL ROSARIO	7
173	ARENAL	7
174	ARJONA	7
175	ARROYOHONDO	7
176	BARRANCO DE LOBA	7
177	CALAMAR	7
178	CANTAGALLO	7
179	CICUCO	7
180	CÓRDOBA	7
181	CLEMENCIA	7
182	EL CARMEN DE BOLÍVAR	7
183	EL GUAMO	7
184	EL PEÑÓN	7
185	HATILLO DE LOBA	7
186	MAGANGUÉ	7
187	MAHATES	7
188	MARGARITA	7
189	MARÍA LA BAJA	7
190	MONTECRISTO	7
191	MOMPÓS	7
192	MORALES	7
193	NOROSÍ	7
194	PINILLOS	7
195	REGIDOR	7
196	RÍO VIEJO	7
197	SAN CRISTÓBAL	7
198	SAN ESTANISLAO	7
199	SAN FERNANDO	7
200	SAN JACINTO	7
201	SAN JACINTO DEL CAUCA	7
202	SAN JUAN NEPOMUCENO	7
203	SAN MARTÍN DE LOBA	7
204	SAN PABLO	7
205	SANTA CATALINA	7
206	SANTA ROSA	7
207	SANTA ROSA DEL SUR	7
208	SIMITÍ	7
209	SOPLAVIENTO	7
210	TALAIGUA NUEVO	7
211	TIQUISIO	7
212	TURBACO	7
213	TURBANÁ	7
214	VILLANUEVA	7
215	ZAMBRANO	7
216	TUNJA	8
217	ALMEIDA	8
218	AQUITANIA	8
219	ARCABUCO	8
220	BELÉN	8
221	BERBEO	8
222	BETÉITIVA	8
223	BOAVITA	8
224	BOYACÁ	8
225	BRICEÑO	8
226	BUENAVISTA	8
227	BUSBANZÁ	8
228	CALDAS	8
229	CAMPOHERMOSO	8
230	CERINZA	8
231	CHINAVITA	8
232	CHIQUINQUIRÁ	8
233	CHISCAS	8
234	CHITA	8
235	CHITARAQUE	8
236	CHIVATÁ	8
237	CIÉNEGA	8
238	CÓMBITA	8
239	COPER	8
240	CORRALES	8
241	COVARACHÍA	8
242	CUBARÁ	8
243	CUCAITA	8
244	CUÍTIVA	8
245	CHÍQUIZA	8
246	CHIVOR	8
247	DUITAMA	8
248	EL COCUY	8
249	EL ESPINO	8
250	FIRAVITOBA	8
251	FLORESTA	8
252	GACHANTIVÁ	8
253	GAMEZA	8
254	GARAGOA	8
255	GUACAMAYAS	8
256	GUATEQUE	8
257	GUAYATÁ	8
258	GÃœICÁN	8
259	IZA	8
260	JENESANO	8
261	JERICÓ	8
262	LABRANZAGRANDE	8
263	LA CAPILLA	8
264	LA VICTORIA	8
265	LA UVITA	8
266	VILLA DE LEYVA	8
267	MACANAL	8
268	MARIPÍ	8
269	MIRAFLORES	8
270	MONGUA	8
271	MONGUÍ	8
272	MONIQUIRÁ	8
273	MOTAVITA	8
274	MUZO	8
275	NOBSA	8
276	NUEVO COLÓN	8
277	OICATÁ	8
278	OTANCHE	8
279	PACHAVITA	8
280	PÁEZ	8
281	PAIPA	8
282	PAJARITO	8
283	PANQUEBA	8
284	PAUNA	8
285	PAYA	8
286	PAZ DE RÍO	8
287	PESCA	8
288	PISBA	8
289	PUERTO BOYACÁ	8
290	QUÍPAMA	8
291	RAMIRIQUÍ	8
292	RÁQUIRA	8
293	RONDÓN	8
294	SABOYÁ	8
295	SÁCHICA	8
296	SAMACÁ	8
297	SAN EDUARDO	8
298	SAN JOSÉ DE PARE	8
299	SAN LUIS DE GACENO	8
300	SAN MATEO	8
301	SAN MIGUEL DE SEMA	8
302	SAN PABLO DE BORBUR	8
303	SANTANA	8
304	SANTA MARÍA	8
305	SANTA ROSA DE VITERBO	8
306	SANTA SOFÍA	8
307	SATIVANORTE	8
308	SATIVASUR	8
309	SIACHOQUE	8
310	SOATÁ	8
311	SOCOTÁ	8
312	SOCHA	8
313	SOGAMOSO	8
314	SOMONDOCO	8
315	SORA	8
316	SOTAQUIRÁ	8
317	SORACÁ	8
318	SUSACÓN	8
319	SUTAMARCHÁN	8
320	SUTATENZA	8
321	TASCO	8
322	TENZA	8
323	TIBANÁ	8
324	TIBASOSA	8
325	TINJACÁ	8
326	TIPACOQUE	8
327	TOCA	8
328	TOGÃœÍ	8
329	TÓPAGA	8
330	TOTA	8
331	TUNUNGUÁ	8
332	TURMEQUÉ	8
333	TUTA	8
334	TUTAZÁ	8
335	UMBITA	8
336	VENTAQUEMADA	8
337	VIRACACHÁ	8
338	ZETAQUIRA	8
339	MANIZALES	9
340	AGUADAS	9
341	ANSERMA	9
342	ARANZAZU	9
343	BELALCÁZAR	9
344	CHINCHINÁ	9
345	FILADELFIA	9
346	LA DORADA	9
347	LA MERCED	9
348	MANZANARES	9
349	MARMATO	9
350	MARQUETALIA	9
351	MARULANDA	9
352	NEIRA	9
353	NORCASIA	9
354	PÁCORA	9
355	PALESTINA	9
356	PENSILVANIA	9
357	RIOSUCIO	9
358	RISARALDA	9
359	SALAMINA	9
360	SAMANÁ	9
361	SAN JOSÉ	9
362	SUPÍA	9
363	VICTORIA	9
364	VILLAMARÍA	9
365	VITERBO	9
366	FLORENCIA	10
367	ALBANIA	10
368	BELÉN DE LOS ANDAQUÍES	10
369	CARTAGENA DEL CHAIRÁ	10
370	CURILLO	10
371	EL DONCELLO	10
372	EL PAUJIL	10
373	LA MONTAÑITA	10
374	MILÁN	10
375	MORELIA	10
376	PUERTO RICO	10
377	SAN JOSÉ DEL FRAGUA	10
378	SAN VICENTE DEL CAGUÁN	10
379	SOLANO	10
380	SOLITA	10
381	VALPARAÍSO	10
382	YOPAL	11
383	AGUAZUL	11
384	CHAMEZA	11
385	HATO COROZAL	11
386	LA SALINA	11
387	MANÍ	11
388	MONTERREY	11
389	NUNCHÍA	11
390	OROCUÉ	11
391	PAZ DE ARIPORO	11
392	PORE	11
393	RECETOR	11
394	SABANALARGA	11
395	SÁCAMA	11
396	SAN LUIS DE PALENQUE	11
397	TÁMARA	11
398	TAURAMENA	11
399	TRINIDAD	11
400	VILLANUEVA	11
401	POPAYÁN	12
402	ALMAGUER	12
403	ARGELIA	12
404	BALBOA	12
405	BOLÍVAR	12
406	BUENOS AIRES	12
407	CAJIBÍO	12
408	CALDONO	12
409	CALOTO	12
410	CORINTO	12
411	EL TAMBO	12
412	FLORENCIA	12
413	GUACHENÉ	12
414	GUAPI	12
415	INZÁ	12
416	JAMBALÓ	12
417	LA SIERRA	12
418	LA VEGA	12
419	LÓPEZ	12
420	MERCADERES	12
421	MIRANDA	12
422	MORALES	12
423	PADILLA	12
424	PAEZ	12
425	PATÍA	12
426	PIAMONTE	12
427	PIENDAMÓ	12
428	PUERTO TEJADA	12
429	PURACÉ	12
430	ROSAS	12
431	SAN SEBASTIÁN	12
432	SANTANDER DE QUILICHAO	12
433	SANTA ROSA	12
434	SILVIA	12
435	SOTARA	12
436	SUÁREZ	12
437	SUCRE	12
438	TIMBÍO	12
439	TIMBIQUÍ	12
440	TORIBIO	12
441	TOTORÓ	12
442	VILLA RICA	12
443	VALLEDUPAR	13
444	AGUACHICA	13
445	AGUSTÍN CODAZZI	13
446	ASTREA	13
447	BECERRIL	13
448	BOSCONIA	13
449	CHIMICHAGUA	13
450	CHIRIGUANÁ	13
451	CURUMANÍ	13
452	EL COPEY	13
453	EL PASO	13
454	GAMARRA	13
455	GONZÁLEZ	13
456	LA GLORIA	13
457	LA JAGUA DE IBIRICO	13
458	MANAURE	13
459	PAILITAS	13
460	PELAYA	13
461	PUEBLO BELLO	13
462	RÍO DE ORO	13
463	LA PAZ	13
464	SAN ALBERTO	13
465	SAN DIEGO	13
466	SAN MARTÍN	13
467	TAMALAMEQUE	13
468	QUIBDÓ	14
469	ACANDÍ	14
470	ALTO BAUDÓ	14
471	ATRATO	14
472	BAGADÓ	14
473	BAHÍA SOLANO	14
474	BAJO BAUDÓ	14
475	BOJAYA	14
476	EL CANTÓN DEL SAN PABLO	14
477	CARMEN DEL DARIEN	14
478	CÉRTEGUI	14
479	CONDOTO	14
480	EL CARMEN DE ATRATO	14
481	EL LITORAL DEL SAN JUAN	14
482	ISTMINA	14
483	JURADÓ	14
484	LLORÓ	14
485	MEDIO ATRATO	14
486	MEDIO BAUDÓ	14
487	MEDIO SAN JUAN	14
488	NÓVITA	14
489	NUQUÍ	14
490	RÍO IRÓ	14
491	RÍO QUITO	14
492	RIOSUCIO	14
493	SAN JOSÉ DEL PALMAR	14
494	SIPÍ	14
495	TADÓ	14
496	UNGUÍA	14
497	UNIÓN PANAMERICANA	14
498	MONTERÍA	15
499	AYAPEL	15
500	BUENAVISTA	15
501	CANALETE	15
502	CERETÉ	15
503	CHIMÁ	15
504	CHINÚ	15
505	CIÉNAGA DE ORO	15
506	COTORRA	15
507	LA APARTADA	15
508	LORICA	15
509	LOS CÓRDOBAS	15
510	MOMIL	15
511	MONTELÍBANO	15
512	MOÑITOS	15
513	PLANETA RICA	15
514	PUEBLO NUEVO	15
515	PUERTO ESCONDIDO	15
516	PUERTO LIBERTADOR	15
517	PURÍSIMA	15
518	SAHAGÚN	15
519	SAN ANDRÉS SOTAVENTO	15
520	SAN ANTERO	15
521	SAN BERNARDO DEL VIENTO	15
522	SAN CARLOS	15
523	SAN JOSÉ DE URÉ	15
524	SAN PELAYO	15
525	TIERRALTA	15
526	TUCHÍN	15
527	VALENCIA	15
528	AGUA DE DIOS	16
529	ALBÁN	16
530	ANAPOIMA	16
531	ANOLAIMA	16
532	ARBELÁEZ	16
533	BELTRÁN	16
534	BITUIMA	16
535	BOJACÁ	16
536	CABRERA	16
537	CACHIPAY	16
538	CAJICÁ	16
539	CAPARRAPÍ	16
540	CAQUEZA	16
541	CARMEN DE CARUPA	16
542	CHAGUANÍ	16
543	CHÍA	16
544	CHIPAQUE	16
545	CHOACHÍ	16
546	CHOCONTÁ	16
547	COGUA	16
548	COTA	16
549	CUCUNUBÁ	16
550	EL COLEGIO	16
551	EL PEÑÓN	16
552	EL ROSAL	16
553	FACATATIVÁ	16
554	FOMEQUE	16
555	FOSCA	16
556	FUNZA	16
557	FÚQUENE	16
558	FUSAGASUGÁ	16
559	GACHALA	16
560	GACHANCIPÁ	16
561	GACHETÁ	16
562	GAMA	16
563	GIRARDOT	16
564	GRANADA	16
565	GUACHETÁ	16
566	GUADUAS	16
567	GUASCA	16
568	GUATAQUÍ	16
569	GUATAVITA	16
570	GUAYABAL DE SIQUIMA	16
571	GUAYABETAL	16
572	GUTIÉRREZ	16
573	JERUSALÉN	16
574	JUNÍN	16
575	LA CALERA	16
576	LA MESA	16
577	LA PALMA	16
578	LA PEÑA	16
579	LA VEGA	16
580	LENGUAZAQUE	16
581	MACHETA	16
582	MADRID	16
583	MANTA	16
584	MEDINA	16
585	MOSQUERA	16
586	NARIÑO	16
587	NEMOCÓN	16
588	NILO	16
589	NIMAIMA	16
590	NOCAIMA	16
591	VENECIA	16
592	PACHO	16
593	PAIME	16
594	PANDI	16
595	PARATEBUENO	16
596	PASCA	16
597	PUERTO SALGAR	16
598	PULÍ	16
599	QUEBRADANEGRA	16
600	QUETAME	16
601	QUIPILE	16
602	APULO	16
603	RICAURTE	16
604	SAN ANTONIO DEL TEQUENDAMA	16
605	SAN BERNARDO	16
606	SAN CAYETANO	16
607	SAN FRANCISCO	16
608	SAN JUAN DE RÍO SECO	16
609	SASAIMA	16
610	SESQUILÉ	16
611	SIBATÉ	16
612	SILVANIA	16
613	SIMIJACA	16
614	SOACHA	16
615	SOPÓ	16
616	SUBACHOQUE	16
617	SUESCA	16
618	SUPATÁ	16
619	SUSA	16
620	SUTATAUSA	16
621	TABIO	16
622	TAUSA	16
623	TENA	16
624	TENJO	16
625	TIBACUY	16
626	TIBIRITA	16
627	TOCAIMA	16
628	TOCANCIPÁ	16
629	TOPAIPÍ	16
630	UBALÁ	16
631	UBAQUE	16
632	VILLA DE SAN DIEGO DE UBATE	16
633	UNE	16
634	ÚTICA	16
635	VERGARA	16
636	VIANÍ	16
637	VILLAGÓMEZ	16
638	VILLAPINZÓN	16
639	VILLETA	16
640	VIOTÁ	16
641	YACOPÍ	16
642	ZIPACÓN	16
643	ZIPAQUIRÁ	16
644	INÍRIDA	17
645	BARRANCO MINAS	17
646	MAPIRIPANA	17
647	SAN FELIPE	17
648	PUERTO COLOMBIA	17
649	LA GUADALUPE	17
650	CACAHUAL	17
651	PANA PANA	17
652	MORICHAL	17
653	SAN JOSÉ DEL GUAVIARE	18
654	CALAMAR	18
655	EL RETORNO	18
656	MIRAFLORES	18
657	NEIVA	19
658	ACEVEDO	19
659	AGRADO	19
660	AIPE	19
661	ALGECIRAS	19
662	ALTAMIRA	19
663	BARAYA	19
664	CAMPOALEGRE	19
665	COLOMBIA	19
666	ELÍAS	19
667	GARZÓN	19
668	GIGANTE	19
669	GUADALUPE	19
670	HOBO	19
671	IQUIRA	19
672	ISNOS	19
673	LA ARGENTINA	19
674	LA PLATA	19
675	NÁTAGA	19
676	OPORAPA	19
677	PAICOL	19
678	PALERMO	19
679	PALESTINA	19
680	PITAL	19
681	PITALITO	19
682	RIVERA	19
683	SALADOBLANCO	19
684	SAN AGUSTÍN	19
685	SANTA MARÍA	19
686	SUAZA	19
687	TARQUI	19
688	TESALIA	19
689	TELLO	19
690	TERUEL	19
691	TIMANÁ	19
692	VILLAVIEJA	19
693	YAGUARÁ	19
694	RIOHACHA	20
695	ALBANIA	20
696	BARRANCAS	20
697	DIBULLA	20
698	DISTRACCIÓN	20
699	EL MOLINO	20
700	FONSECA	20
701	HATONUEVO	20
702	LA JAGUA DEL PILAR	20
703	MAICAO	20
704	MANAURE	20
705	SAN JUAN DEL CESAR	20
706	URIBIA	20
707	URUMITA	20
708	VILLANUEVA	20
709	SANTA MARTA	21
710	ALGARROBO	21
711	ARACATACA	21
712	ARIGUANÍ	21
713	CERRO SAN ANTONIO	21
714	CHIBOLO	21
715	CIÉNAGA	21
716	CONCORDIA	21
717	EL BANCO	21
718	EL PIÑON	21
719	EL RETÉN	21
720	FUNDACIÓN	21
721	GUAMAL	21
722	NUEVA GRANADA	21
723	PEDRAZA	21
724	PIJIÑO DEL CARMEN	21
725	PIVIJAY	21
726	PLATO	21
727	PUEBLOVIEJO	21
728	REMOLINO	21
729	SABANAS DE SAN ANGEL	21
730	SALAMINA	21
731	SAN SEBASTIÁN DE BUENAVISTA	21
732	SAN ZENÓN	21
733	SANTA ANA	21
734	SANTA BÁRBARA DE PINTO	21
735	SITIONUEVO	21
736	TENERIFE	21
737	ZAPAYÁN	21
738	ZONA BANANERA	21
739	VILLAVICENCIO	22
740	ACACÍAS	22
741	BARRANCA DE UPÍA	22
742	CABUYARO	22
743	CASTILLA LA NUEVA	22
744	CUBARRAL	22
745	CUMARAL	22
746	EL CALVARIO	22
747	EL CASTILLO	22
748	EL DORADO	22
749	FUENTE DE ORO	22
750	GRANADA	22
751	GUAMAL	22
752	MAPIRIPÁN	22
753	MESETAS	22
754	LA MACARENA	22
755	URIBE	22
756	LEJANÍAS	22
757	PUERTO CONCORDIA	22
758	PUERTO GAITÁN	22
759	PUERTO LÓPEZ	22
760	PUERTO LLERAS	22
761	PUERTO RICO	22
762	RESTREPO	22
763	SAN CARLOS DE GUAROA	22
764	SAN JUAN DE ARAMA	22
765	SAN JUANITO	22
766	SAN MARTÍN	22
767	VISTAHERMOSA	22
768	PASTO	23
769	ALBÁN	23
770	ALDANA	23
771	ANCUYÁ	23
772	ARBOLEDA	23
773	BARBACOAS	23
774	BELÉN	23
775	BUESACO	23
776	COLÓN	23
777	CONSACA	23
778	CONTADERO	23
779	CÓRDOBA	23
780	CUASPUD	23
781	CUMBAL	23
782	CUMBITARA	23
783	CHACHAGÃœÍ	23
784	EL CHARCO	23
785	EL PEÑOL	23
786	EL ROSARIO	23
787	EL TABLÓN DE GÓMEZ	23
788	EL TAMBO	23
789	FUNES	23
790	GUACHUCAL	23
791	GUAITARILLA	23
792	GUALMATÁN	23
793	ILES	23
794	IMUÉS	23
795	IPIALES	23
796	LA CRUZ	23
797	LA FLORIDA	23
798	LA LLANADA	23
799	LA TOLA	23
800	LA UNIÓN	23
801	LEIVA	23
802	LINARES	23
803	LOS ANDES	23
804	MAGÃœI	23
805	MALLAMA	23
806	MOSQUERA	23
807	NARIÑO	23
808	OLAYA HERRERA	23
809	OSPINA	23
810	FRANCISCO PIZARRO	23
811	POLICARPA	23
812	POTOSÍ	23
813	PROVIDENCIA	23
814	PUERRES	23
815	PUPIALES	23
816	RICAURTE	23
817	ROBERTO PAYÁN	23
818	SAMANIEGO	23
819	SANDONÁ	23
820	SAN BERNARDO	23
821	SAN LORENZO	23
822	SAN PABLO	23
823	SAN PEDRO DE CARTAGO	23
824	SANTA BÁRBARA	23
825	SANTACRUZ	23
826	SAPUYES	23
827	TAMINANGO	23
828	TANGUA	23
829	SAN ANDRES DE TUMACO	23
830	TÚQUERRES	23
831	YACUANQUER	23
832	CÚCUTA	24
833	ABREGO	24
834	ARBOLEDAS	24
835	BOCHALEMA	24
836	BUCARASICA	24
837	CÁCOTA	24
838	CACHIRÁ	24
839	CHINÁCOTA	24
840	CHITAGÁ	24
841	CONVENCIÓN	24
842	CUCUTILLA	24
843	DURANIA	24
844	EL CARMEN	24
845	EL TARRA	24
846	EL ZULIA	24
847	GRAMALOTE	24
848	HACARÍ	24
849	HERRÁN	24
850	LABATECA	24
851	LA ESPERANZA	24
852	LA PLAYA	24
853	LOS PATIOS	24
854	LOURDES	24
855	MUTISCUA	24
856	OCAÑA	24
857	PAMPLONA	24
858	PAMPLONITA	24
859	PUERTO SANTANDER	24
860	RAGONVALIA	24
861	SALAZAR	24
862	SAN CALIXTO	24
863	SAN CAYETANO	24
864	SANTIAGO	24
865	SARDINATA	24
866	SILOS	24
867	TEORAMA	24
868	TIBÚ	24
869	TOLEDO	24
870	VILLA CARO	24
871	VILLA DEL ROSARIO	24
872	MOCOA	25
873	COLÓN	25
874	ORITO	25
875	PUERTO ASÍS	25
876	PUERTO CAICEDO	25
877	PUERTO GUZMÁN	25
878	PUERTO LEGUÍZAMO	25
879	SIBUNDOY	25
880	SAN FRANCISCO	25
881	SAN MIGUEL	25
882	SANTIAGO	25
883	VALLE DEL GUAMUEZ	25
884	VILLAGARZÓN	25
885	ARMENIA	26
886	BUENAVISTA	26
887	CALARCA	26
888	CIRCASIA	26
889	CÓRDOBA	26
890	FILANDIA	26
891	GÉNOVA	26
892	LA TEBAIDA	26
893	MONTENEGRO	26
894	PIJAO	26
895	QUIMBAYA	26
896	SALENTO	26
897	PEREIRA	27
898	APÍA	27
899	BALBOA	27
900	BELÉN DE UMBRÍA	27
901	DOSQUEBRADAS	27
902	GUÁTICA	27
903	LA CELIA	27
904	LA VIRGINIA	27
905	MARSELLA	27
906	MISTRATÓ	27
907	PUEBLO RICO	27
908	QUINCHÍA	27
909	SANTA ROSA DE CABAL	27
910	SANTUARIO	27
911	BUCARAMANGA	28
912	AGUADA	28
913	ALBANIA	28
914	ARATOCA	28
915	BARBOSA	28
916	BARICHARA	28
917	BARRANCABERMEJA	28
918	BETULIA	28
919	BOLÍVAR	28
920	CABRERA	28
921	CALIFORNIA	28
922	CAPITANEJO	28
923	CARCASÍ	28
924	CEPITÁ	28
925	CERRITO	28
926	CHARALÁ	28
927	CHARTA	28
928	CHIMA	28
929	CHIPATÁ	28
930	CIMITARRA	28
931	CONCEPCIÓN	28
932	CONFINES	28
933	CONTRATACIÓN	28
934	COROMORO	28
935	CURITÍ	28
936	EL CARMEN DE CHUCURÍ	28
937	EL GUACAMAYO	28
938	EL PEÑÓN	28
939	EL PLAYÓN	28
940	ENCINO	28
941	ENCISO	28
942	FLORIÁN	28
943	FLORIDABLANCA	28
944	GALÁN	28
945	GAMBITA	28
946	GIRÓN	28
947	GUACA	28
948	GUADALUPE	28
949	GUAPOTÁ	28
950	GUAVATÁ	28
951	GÃœEPSA	28
952	HATO	28
953	JESÚS MARÍA	28
954	JORDÁN	28
955	LA BELLEZA	28
956	LANDÁZURI	28
957	LA PAZ	28
958	LEBRIJA	28
959	LOS SANTOS	28
960	MACARAVITA	28
961	MÁLAGA	28
962	MATANZA	28
963	MOGOTES	28
964	MOLAGAVITA	28
965	OCAMONTE	28
966	OIBA	28
967	ONZAGA	28
968	PALMAR	28
969	PALMAS DEL SOCORRO	28
970	PÁRAMO	28
971	PIEDECUESTA	28
972	PINCHOTE	28
973	PUENTE NACIONAL	28
974	PUERTO PARRA	28
975	PUERTO WILCHES	28
976	RIONEGRO	28
977	SABANA DE TORRES	28
978	SAN ANDRÉS	28
979	SAN BENITO	28
980	SAN GIL	28
981	SAN JOAQUÍN	28
982	SAN JOSÉ DE MIRANDA	28
983	SAN MIGUEL	28
984	SAN VICENTE DE CHUCURÍ	28
985	SANTA BÁRBARA	28
986	SANTA HELENA DEL OPÓN	28
987	SIMACOTA	28
988	SOCORRO	28
989	SUAITA	28
990	SUCRE	28
991	SURATÁ	28
992	TONA	28
993	VALLE DE SAN JOSÉ	28
994	VÉLEZ	28
995	VETAS	28
996	VILLANUEVA	28
997	ZAPATOCA	28
998	SINCELEJO	29
999	BUENAVISTA	29
1000	CAIMITO	29
1001	COLOSO	29
1002	COROZAL	29
1003	COVEÑAS	29
1004	CHALÁN	29
1005	EL ROBLE	29
1006	GALERAS	29
1007	GUARANDA	29
1008	LA UNIÓN	29
1009	LOS PALMITOS	29
1010	MAJAGUAL	29
1011	MORROA	29
1012	OVEJAS	29
1013	PALMITO	29
1014	SAMPUÉS	29
1015	SAN BENITO ABAD	29
1016	SAN JUAN DE BETULIA	29
1017	SAN MARCOS	29
1018	SAN ONOFRE	29
1019	SAN PEDRO	29
1020	SAN LUIS DE SINCÉ	29
1021	SUCRE	29
1022	SANTIAGO DE TOLÚ	29
1023	TOLÚ VIEJO	29
1024	IBAGUÉ	30
1025	ALPUJARRA	30
1026	ALVARADO	30
1027	AMBALEMA	30
1028	ANZOÁTEGUI	30
1029	ARMERO	30
1030	ATACO	30
1031	CAJAMARCA	30
1032	CARMEN DE APICALÁ	30
1033	CASABIANCA	30
1034	CHAPARRAL	30
1035	COELLO	30
1036	COYAIMA	30
1037	CUNDAY	30
1038	DOLORES	30
1039	ESPINAL	30
1040	FALAN	30
1041	FLANDES	30
1042	FRESNO	30
1043	GUAMO	30
1044	HERVEO	30
1045	HONDA	30
1046	ICONONZO	30
1047	LÉRIDA	30
1048	LÍBANO	30
1049	SAN SEBASTIÁN DE MARIQUITA	30
1050	MELGAR	30
1051	MURILLO	30
1052	NATAGAIMA	30
1053	ORTEGA	30
1054	PALOCABILDO	30
1055	PIEDRAS	30
1056	PLANADAS	30
1057	PRADO	30
1058	PURIFICACIÓN	30
1059	RIOBLANCO	30
1060	RONCESVALLES	30
1061	ROVIRA	30
1062	SALDAÑA	30
1063	SAN ANTONIO	30
1064	SAN LUIS	30
1065	SANTA ISABEL	30
1066	SUÁREZ	30
1067	VALLE DE SAN JUAN	30
1068	VENADILLO	30
1069	VILLAHERMOSA	30
1070	VILLARRICA	30
1071	CALI	31
1072	ALCALÁ	31
1073	ANDALUCÍA	31
1074	ANSERMANUEVO	31
1075	ARGELIA	31
1076	BOLÍVAR	31
1077	BUENAVENTURA	31
1078	GUADALAJARA DE BUGA	31
1079	BUGALAGRANDE	31
1080	CAICEDONIA	31
1081	CALIMA	31
1082	CANDELARIA	31
1083	CARTAGO	31
1084	DAGUA	31
1085	EL ÁGUILA	31
1086	EL CAIRO	31
1087	EL CERRITO	31
1088	EL DOVIO	31
1089	FLORIDA	31
1090	GINEBRA	31
1091	GUACARÍ	31
1092	JAMUNDÍ	31
1093	LA CUMBRE	31
1094	LA UNIÓN	31
1095	LA VICTORIA	31
1096	OBANDO	31
1097	PALMIRA	31
1098	PRADERA	31
1099	RESTREPO	31
1100	RIOFRÍO	31
1101	ROLDANILLO	31
1102	SAN PEDRO	31
1103	SEVILLA	31
1104	TORO	31
1105	TRUJILLO	31
1106	TULUÁ	31
1107	ULLOA	31
1108	VERSALLES	31
1109	VIJES	31
1110	YOTOCO	31
1111	YUMBO	31
1112	ZARZAL	31
1113	MITÚ	32
1114	CARURU	32
1115	PACOA	32
1116	TARAIRA	32
1117	PAPUNAUA	32
1118	YAVARATÉ	32
1119	PUERTO CARREÑO	33
1120	LA PRIMAVERA	33
1121	SANTA ROSALÍA	33
1122	CUMARIBO	33
\.


--
-- TOC entry 2008 (class 0 OID 42591)
-- Dependencies: 1576
-- Data for Name: nota_internacional; Type: TABLE DATA; Schema: public; Owner: -
--

COPY nota_internacional (id_nota_internacional, nombre, descripcion) FROM stdin;
1	5.53	Las administraciones que autoricen el empleo de frecuencias inferiores a 9 kHz deberán asegurarse de que no se producen interferencias perjudiciales a los servicios a los que se han atribuido las bandas de frecuencias superiores a 9 kHz.
2	5.54	Se insta  a  las  administraciones que efectúen investigaciones científicas empleando frecuencias inferiores a 9 kHz a que lo comuniquen a las otras administraciones interesadas, a fin   de   que   pueda   proporcionarse   a esas investigaciones toda la protección posible contra la interferencia perjudicial.
3	5.55	Atribución adicional: en Armenia, Azerbaiyán, Federación de Rusia, Georgia, Kirguistán, Tayikistán y Turkmenistán, la banda 14-17 kHz está también atribuida, a título primario, al servicio de radionavegación.
4	5.56	Las estaciones de los servicios a los que se han atribuido las bandas 14-19,95 kHz y 20,05-70 kHz, y además en la Región 1 las bandas 72-84 kHz y 86-90 kHz, podrán transmitir frecuencias patrón y señales horarias. Tales estaciones quedarán protegidas contra interferencias perjudiciales. En Armenia, Azerbaiyán, Belarús, Bulgaria, Federación de Rusia, Georgia, Kazajstán, Mongolia, Kirguistán, Eslovaquia, Tayikistán y  Turkmenistán, se  utilizarán las frecuencias de 25 kHz y 50 kHz para los mismos fines y en las mismas condiciones.
5	5.57	La utilización de las bandas 14 - 19,95 kHz, 20,05 - 70 kHz y 70 - 90 kHz (72 - 84 kHz y 86 - 90 kHz en la Región 1) por el servicio móvil marítimo está limitada a las estaciones costeras radiotelegráficas (A1A y F1B solamente). Excepcionalmente, está autorizado el empleo de las clases de emisión J2B o J7B, a condición de que no se rebase la anchura de banda necesaria utilizada normalmente para emisiones de clase A1A o F1B en las bandas de que se trata.
6	5.58	Atribución adicional: en Armenia, Azerbaiyán, Bulgaria, Georgia, Kazakstán, Kirguistán, Federación de Rusia, Tayikistán y Turkmenistán, la banda 67-70 kHz está también atribuida, a título primario, al servicio de radionavegación. (CMR-00)
7	5.59	Categoría de servicio diferente: en Bangladesh y Pakistán, la atribución de las bandas 70-72 kHz y 84-86 kHz a los servicios fijo y móvil marítimo es a título primario (véase el número 5.33). (CMR-00)
8	5.60	En las bandas 70 - 90 kHz (70 - 86 kHz en la Región 1) y 110 - 130 kHz (112 - 130 kHz en la Región 1), podrán utilizarse sistemas de radionavegación por impulsos siempre y cuando no causen interferencia perjudicial a otros servicios a que están atribuidas esas bandas.
9	5.61	En la Región 2, las estaciones del servicio de radionavegación marítima en las bandas 70 - 90 kHz y 110 - 130 kHz podrán establecerse y funcionar, a reserva de obtener el acuerdo indicado en el número 9.21 de las administraciones cuyos servicios explotados con arreglo al cuadro puedan verse afectados. No obstante, las estaciones de los servicios fijo, móvil marítimo y de radiolocalización no deben causar interferencia perjudicial a las estaciones del servicio de radionavegación marítima que se establezcan como consecuencia de tales acuerdos.
10	5.62	Se insta a las administraciones que explotan estaciones del servicio de radionavegación en la banda 90 - 110 kHz a que coordinen las características técnicas y de explotación de modo que se evite interferencia perjudicial a los servicios proporcionados por estas estaciones.
11	5.63	(SUP-CMR-97).
12	5.64	Las emisiones de las clases A1A o F1B, A2C, A3C, F1C o F3C son las únicas autorizadas para las estaciones del servicio fijo en las bandas atribuidas a este servicio entre 90 kHz y 160 kHz (148,5 kHz en la Región 1) y para las estaciones del servicio móvil marítimo en las bandas atribuidas a este servicio entre 110 kHz y 160 kHz (148,5 kHz en la Región 1). Excepcionalmente, las estaciones del servicio móvil marítimo podrán también utilizar las clases de emisión J2B o J7B en las bandas entre 110 kHz y 160 kHz (148,5 kHz en la Región 1).
13	5.65	Categoría de servicio diferente: en Bangladesh, la atribución de las bandas 112-117,6 kHz y 126-129 kHz a los servicios fijo y móvil marítimo es a título primario (véase el número 5.33). (CMR-00)
14	5.66	Categoría de servicio diferente: en Alemania, la atribución de la banda 115 - 117,6 kHz a los servicios fijo y móvil marítimo es a título primario (véase el número 5.33) y al servicio de radionavegación a título secundario (véase el número 5.32).
15	5.67	Atribución adicional: en Mongolia, Kirguistán y Turkmenistán, la banda 130-148,5 kHz está también atribuida, a título secundario, al servicio de radionavegación. En el interior de estos países, y entre ellos, el citado servicio funciona sobre la base de igualdad de derechos. (CMR-07)
16	5.67A	Las estaciones del servicio de aficionados que utilicen frecuencias en la banda 135,7- 137,8 kHz no superarán la potencia radiada máxima de 1 W (p.i.r.e.) ni causarán interferencia perjudicial a las estaciones del servicio de radionavegación de los países indicados en el número 5.67. (CMR-07)
17	5.67B	La utilización de la banda 135,7-137,8 kHz en Argelia, Egipto, Irán (República Islámica del), Iraq, Jamahiriya Árabe Libia, Líbano, República Árabe Siria, Sudán y Túnez se limita a los servicios fijo y móvil marítimo. El servicio de aficionados no deberá utilizarse en la banda 135,7-137,8 kHz en los países citados y los países que autoricen tal utilización deberán tener en cuenta dicha restricción. (CMR-07)
18	5.68	Atribución sustitutiva: en Angola, Burundi, Congo, Malawi, Rep. Dem. del Congo, Rwanda y Sudafricana (Rep.), la banda 160-200 kHz está atribuida, a título primario, al servicio fijo. (CMR-03)
19	5.69	Atribución adicional: en Somalia, la banda 200 - 255 kHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica.
20	5.70	Atribución sustitutiva: en Angola, Botswana, Burundi, Centroafricana (Rep.), Congo (Rep. del), Etiopía, Kenya, Lesotho, Madagascar, Malawi, Mozambique, Namibia, Nigeria, Omán, Rep. Dem. del Congo, Rwanda, Sudafricana (Rep.), Swazilandia, Tanzanía, Chad, Zambia y Zimbabwe, la banda 200-283,5 kHz está atribuida, a título primario, al servicio de radionavegación aeronáutica. (CMR-07)
21	5.71	Atribución sustitutiva: en Túnez, la banda 255 - 283,5 kHz está atribuida, a título primario, al servicio de radiodifusión.
22	5.72	Las estaciones noruegas del servicio fijo situadas en las zonas septentrionales (al norte de 60° N) sujetas a las perturbaciones debidas a las auroras, quedan autorizadas para continuar su funcionamiento empleando cuatro frecuencias de las bandas 283,5 - 490 kHz y 510 - 526,5 kHz.
23	5.73	La banda 285 - 325 kHz (283,5 - 325 kHz en la Región 1), atribuida al servicio de radionavegación marítima, puede utilizarse para transmitir información suplementaria útil a la navegación utilizando técnicas de banda estrecha, a condición de no causar interferencia perjudicial a las estaciones de radiofaro que funcionen en el servicio de radionavegación. (CMR-97)
24	5.74	Atribución adicional: en la Región 1, la banda de frecuencias 285,3 - 285,7 kHz está atribuida también al servicio de radionavegación marítima (distinto de los radiofaros) a título primario.
25	5.75	Categoría de servicio diferente: en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Moldova, Kirguistán, Tayikistán, Turkmenistán y Ucrania, y en la zona rumana del Mar Negro, la atribución de la banda 315-325 kHz al servicio de radionavegación marítima es a título primario con la siguiente condición: en la zona del Mar Báltico, la asignación de frecuencia en esta banda a las nuevas estaciones de radionavegación marítima o aeronáutica se hará previa consulta entre las administraciones interesadas. (CMR-97)
26	5.76	La frecuencia 410 kHz está designada para radiogoniometría en el servicio de radionavegación marítima. Los demás servicios de radionavegación a los que se ha atribuido la banda 405 - 415 kHz no deberán causar interferencia perjudicial a la radiogoniometría en la banda 406,5 - 413,5 kHz.
27	5.77	Categoría de servicio diferente: en Australia, China, comunidades francesas de Ultramar de la Región 3, India, Irán (República Islámica del), Japón, Pakistán, Papua Nueva Guinea y Sri Lanka la atribución de la banda 415-495 kHz al servicio de radionavegación aeronáutica, es a título primario. Las administraciones de estos países adoptarán todas las medidas prácticas necesarias para asegurar que las estaciones de radionavegación aeronáutica que funcionan en la banda 435-495 kHz no causen interferencia a las estaciones costeras en la recepción de las estaciones de barco que transmitan en frecuencias designadas con carácter mundial para estas estaciones (véase el número 52.39). (CMR-07)
28	5.78	Categoría de servicio diferente: en Cuba, en Estados Unidos y en México la banda 415 - 435 kHz está atribuida a título primario al servicio de radionavegación aeronáutica.
29	5.79	El uso de las bandas 415 - 495 kHz y 505 - 526,5 kHz (505 - 510 kHz en la Región 2) por el servicio móvil marítimo está limitado a la radiotelegrafía.
30	5.79A	Se recomienda firmemente a las administraciones que, cuando establezcan estaciones costeras del servicio NAVTEX en las frecuencias 490 kHz, 518 kHz y 4 209,5 kHz, coordinen las características de explotación de conformidad con los procedimientos de la Organización Marítima Internacional (OMI) (véase la Resolución 339 (Rev.CMR-07)). (CMR-07)
31	5.80	En la Región 2, la utilización de la banda 435 - 495 kHz por el servicio de radionavegación aeronáutica está limitada a los radiofaros no direccionales que no utilicen transmisiones vocales.
32	5.81	SUP - CMR 2000.
33	5.82	En el servicio móvil marítimo, la frecuencia 490 kHz deberá utilizarse exclusivamente para la transmisión por las estaciones costeras de avisos a los navegantes, boletines meteorológicos e información urgente con destino a los barcos, por medio de telegrafía de impresión directa de banda estrecha. Las condiciones para la utilización de la frecuencia 490 kHz se prescriben en los Artículos 31 y 52. Se ruega a las administraciones que, al utilizar la banda 415-495 kHz para el servicio de radionavegación aeronáutica, se aseguren de que no se cause interferencia perjudicial a la frecuencia 490 kHz. (CMR-07)
34	5.82A	La utilización de la banda 495-505 kHz queda limitada a la radiotelegrafía. (CMR-07)
35	5.82B	Las administraciones que autoricen el uso de frecuencias en la banda 495-505 kHz por los servicios distintos del móvil marítimo deberán garantizar que no se causa interferencia perjudicial al servicio móvil marítimo en esa banda ni a los servicios con atribuciones en las bandas adyacentes, observando en particular las condiciones de utilización de las frecuencias de 490 kHz y 518 kHz, tal como se enuncian en los Artículos 31 y 52. (CMR-07)
36	5.83	SUP - CMR-2007.
37	5.84	Las condiciones de utilización de la frecuencia de 518 kHz por el servicio móvil marítimo están descritas en los Artículos 31 y 52. (CMR-07)
38	5.85	No utilizado.
39	5.86	En la Región 2, en la banda 525 - 535 kHz, la potencia de la portadora de las estaciones de radiodifusión no deberá exceder de 1 kW durante el día y de 250 vatios durante la noche.
40	5.87	Atribución adicional: en Angola, Botswana, Lesotho, Malawi, Mozambique, Namibia, Sudafricana (Rep.), Swazilandia y Zimbabwe, la banda 526,5-535 kHz está también atribuida, a título secundario, al servicio móvil. (CMR-03)
41	5.87A	Atribución adicional: en Uzbekistán, la banda 526,5 – 1606,5 kHz está también atribuida, a título primario, al servicio de radionavegación. Esta utilización está sujeta al acuerdo obtenido en virtud del número 9.21 con las administraciones pertinentes y está limitada a las radiobalizas en tierra que se encuentren en servicio el 27 de octubre de 1997, hasta el final de su vida útil. (CMR-97)
42	5.88	Atribución adicional: en China, la banda 526,5 - 535 kHz está también atribuida, a título secundario, al servicio de radionavegación aeronáutica.
43	5.89	En la Región 2, la utilización de la banda 1 605 - 1 705 kHz por las estaciones del servicio de radiodifusión está sujeta al plan establecido por la Conferencia Administrativa Regional de Radiocomunicaciones (Río de Janeiro, 1988). El examen de las asignaciones de frecuencia a estaciones de los servicios fijo y móvil en la banda 1 625 - 1 705 kHz, tendrá en cuenta las adjudicaciones que aparecen en el Plan establecido por la Conferencia Administrativa Regional de Radiocomunicaciones (Río de Janeiro, 1988).
44	5.90	En la banda 1 605 - 1 705 kHz, cuando una estación del servicio de radiodifusión de la Región 2 resulte afectada, la zona de servicio de las estaciones del servicio móvil marítimo en la Región 1 se limitará a la determinada por la propagación de la onda de superficie.
45	5.91	Atribución adicional: en Filipinas y Sri Lanka, la banda 1 606,5 - 1 705 kHz está también atribuida, a título secundario, al servicio de radiodifusión. (CMR-97)
46	5.92	Algunos países de la Región 1 utilizan sistemas de radiodeterminación en las bandas 1 606,5 - 1 625 kHz, 1 635 - 1 800 kHz, 1 850 - 2 160 kHz, 2 194 - 2 300 kHz, 2 502 - 2 850 kHz y 3 500 - 3 800 kHz, a reserva de obtener el acuerdo indicado en el número 9.21. La potencia media radiada por estas estaciones no superará los 50 W.
47	5.93	Atribución adicional: en Angola, Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Hungría, Kazajstán, Letonia, Lituania, Moldova, Mongolia, Nigeria, Uzbekistán, Polonia, Kirguistán, Eslovaquia, Rep. Checa, Tayikistán, Chad, Turkmenistán y Ucrania, las bandas 1 625-1 635 kHz, 1 800-1 810 kHz y 2 160-2 170 kHz están también atribuidas, a título primario, a los servicios fijo y móvil terrestre, a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-07)
48	5.94	No utilizado
49	5.95	No utilizado
702	5.560	La banda 78 - 79 GHz puede ser utilizada, a título primario, por los radares situados en estaciones espaciales del servicio de exploración de la Tierra por satélite y del Servicio de investigación espacial.
50	5.96	En Alemania, Armenia, Austria, Azerbaiyán, Belarús, Dinamarca, Estonia, Finlandia, Georgia, Hungría, Irlanda, Islandia, Israel, Kazajstán, Letonia, Liechtenstein, Lituania, Malta, Moldova, Noruega, Uzbekistán, Polonia, Kirguistán, Eslovaquia, Rep. Checa, Reino Unido, Federación de Rusia, Suecia, Suiza, Tayikistán, Turkmenistán y Ucrania, las administraciones podrán atribuir hasta 200 kHz al servicio de aficionados en las bandas 1 715-1 800 kHz y 1 850-2 000 kHz. Sin embargo, al proceder a tales atribuciones en estas bandas, las administraciones, después de consultar con las de los países vecinos, deberán tomar las medidas eventualmente necesarias para evitar que su servicio de aficionados cause interferencias perjudiciales a los servicios fijo y móvil de los demás países. La potencia media de toda estación de aficionado no podrá ser superior a 10 W. (CMR-03)
51	5.97	En la Región 3, la frecuencia de trabajo del sistema Loran es 1 850 kHz o bien 1 950 kHz; las bandas ocupadas son, respectivamente, 1 825 - 1 875 kHz y 1 925 - 1 975 kHz. Los demás servicios a los que está atribuida la banda 1 800 - 2 000 kHz pueden emplear cualquier frecuencia de esta banda, a condición de que no causen interferencia perjudicial al sistema Loran que funcione en la frecuencia de 1 850 kHz o en la de 1 950 kHz.
52	5.98	Atribución sustitutiva: en Angola, Armenia, Azerbaiyán, Belarús, Bélgica, Camerún, Congo (Rep. del), Dinamarca, Egipto, Eritrea, España, Etiopía, Federación de Rusia, Georgia, Grecia, Italia, Kazajstán, Líbano, Lituania, Moldova, República Árabe Siria, Kirguistán, Somalia, Tayikistán, Túnez, Turkmenistán, Turquía y Ucrania, la banda 1 810-1 830 kHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
53	5.99	Atribución adicional: en Arabia Saudita, Austria, Iraq, Jamahiriya Árabe Libia, Uzbekistán, Eslovaquia, Rumania, Serbia, Eslovenia, Chad y Togo, la banda 1 810-1 830 kHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
54	5.100	En la Región 1, no deberá concederse autorización al servicio de aficionados para utilizar la banda 1 810 - 1 830 kHz en los países situados total o parcialmente al norte del paralelo 40° N, sin consulta previa con los países indicados en los números 5.98 y 5.99, a fin de determinar las medidas necesarias que deben tomarse para evitar las interferencias perjudiciales entre las estaciones de aficionado y las estaciones de los demás servicios que funcionen de acuerdo con los números 5.98 y 5.99.
55	5.101	Atribución sustitutiva: en Burundi y Lesotho, la banda 1 810 - 1 850 kHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico.
56	5.102	Atribución sustitutiva: en Bolivia, Chile, México, Paraguay, Perú y Uruguay, la banda 1 850-2 000 kHz está atribuida, a título primario, a los servicios fijo, móvil, salvo móvil aeronáutico, de radiolocalización y de radionavegación. (CMR-07)
57	5.103	En la Región 1, al hacer asignaciones a las estaciones de los servicios fijo y móvil en las bandas 1 850 - 2 045 kHz, 2 194 - 2 498 kHz, 2 502 - 2 625 kHz y 2 650 - 2 850 kHz, las administraciones deberán tener en cuenta las necesidades particulares del servicio móvil marítimo.
58	5.104	En la Región 1, la utilización de la banda 2 025 - 2 045 kHz por el servicio de ayudas a la meteorología está limitada a las estaciones de boyas oceanográficas.
59	5.105	En la Región 2, exceptuada Groenlandia, las estaciones costeras y las estaciones de barco que utilicen la radiotelefonía, en la banda 2 065 - 2 107 kHz, sólo podrán efectuar emisiones de clase J3E, sin que la potencia en la cresta de la envolvente rebase el valor de 1 kW. Conviene que estas estaciones utilicen preferentemente las siguientes frecuencias portadoras: 2 065,0 kHz, 2 079,0 kHz, 2 082,5 kHz, 2 086,0 kHz, 2 093,0 kHz, 2 096,5 kHz, 2 100,0 kHz y 2 103,5 kHz. En Argentina y Uruguay también se utilizan para este fin las frecuencias portadoras de 2 068,5 kHz y de 2 075,5 kHz, quedando para el uso previsto en el número 52.165 las frecuencias comprendidas en la banda 2 072 - 2 075,5 kHz.
60	5.106	A reserva de no causar interferencia perjudicial al servicio móvil marítimo, las frecuencias comprendidas entre 2 065 kHz y 2 107 kHz podrán utilizarse en las Regiones 2 y 3 por las estaciones del servicio Fijo, que comuniquen únicamente dentro de las fronteras nacionales, y cuya potencia media no exceda de 50 vatios. Cuando se haga la notificación de las frecuencias, se llamará la atención de la oficina sobre estas disposiciones.
61	5.107	Atribución adicional: en Arabia Saudita, Eritrea, Etiopía, Iraq, Lesotho, Jamahiriya Árabe Libia, Somalia y Swazilandia, la banda 2 160-2 170 kHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico (R). Las estaciones de estos servicios no podrán utilizar una potencia media que exceda de 50 W. (CMR-03)
62	5.108	La frecuencia portadora de 2 182 kHz es una frecuencia internacional de socorro y de llamada para radiotelefonía. En los Artículos 31 y 52 se fijan las condiciones para el empleo de la banda 2 173,5-2 190,5 kHz. (CMR-07)
63	5.109	Las frecuencias de 2 187,5 kHz, 4 207,5 kHz, 6 312 kHz, 8 414,5 kHz, 12 577 kHz y 16 804,5 kHz son frecuencias internacionales de socorro para la llamada selectiva digital. Las condiciones de utilización de estas frecuencias están descritas en el artículo 31.
64	5.110	Las frecuencias de 2 174,5 kHz, 4 177,5 kHz, 6 268 kHz, 8 376,5 kHz, 12 520 kHz y 16 695 kHz son frecuencias internacionales de socorro para telegrafía de impresión directa de banda estrecha. Las condiciones de utilización de estas frecuencias están descritas en el artículo 31.
65	5.111	Las frecuencias portadoras de 2 182 kHz, 3 023 kHz, 5 680 kHz y 8 364 kHz, y las frecuencias de 121,5 MHz, 156,525 MHz, 156,8 MHz y 243 MHz pueden además utilizarse de conformidad con los procedimientos en vigor para los servicios de radiocomunicación terrenales, en operaciones de búsqueda y salvamento de vehículos espaciales tripulados. Las condiciones de utilización de estas frecuencias se fijan en el Artículo 31. También pueden utilizarse las frecuencias de 10 003 kHz, 14 993 kHz y 19 993 kHz, aunque en este caso las emisiones deben estar limitadas a una banda de ± 3 kHz en torno a dichas frecuencias. (CMR- 07)
66	5.112	Atribución sustitutiva: en Dinamarca, Malta, Serbia y Sri Lanka, la banda 2 194-2 300 kHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
67	5.113	Para las condiciones de utilización de las bandas 2 300 - 2 495 kHz (2 498 kHz en la Región 1), 3 200 - 3 400 kHz, 4 750 - 4 995 kHz y 5 005 - 5 060 kHz por el servicio de radiodifusión, véanse los número5.16 a 5.20, 5.21 y 23.3 a 23.10.
68	5.114	Atribución sustitutiva: en Dinamarca, Iraq, Malta y Serbia, la banda 2 502-2 625 kHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
641	5.532	La utilización de la banda 22,21 - 22,5 GHz por los servicios de exploración de la Tierra por satélite (pasivo) y de investigación espacial (pasivo) no debe imponer limitaciones a los servicios fijo y móvil, salvo móvil aeronáutico.
69	5.115	Las frecuencias portadoras (frecuencias de referencia) de 3 023 kHz y de 5 680 kHz pueden también ser utilizadas en las condiciones especificadas en el Artículo 31 por las estaciones del servicio móvil marítimo que participen en operaciones coordinadas de búsqueda y salvamento. (CMR-07)
70	5.116	Se ruega encarecidamente a las administraciones que autoricen la utilización de la banda 3 155 - 3 195 kHz para proporcionar un canal común mundial destinado a los sistemas de comunicación inalámbrica de baja potencia para personas de audición deficiente. Las administraciones podrán asignar canales adicionales a estos dispositivos en las bandas comprendidas entre 3 155 kHz y 3 400 kHz para atender necesidades locales. Conviene tener en cuenta que las frecuencias en la gama de 3 000 kHz a 4 000 kHz son adecuadas para los dispositivos de comunicación para personas de audición deficiente concebidos para funcionar a corta distancia dentro del campo de inducción.
71	5.117	Atribución sustitutiva: en Côte d'Ivoire, Dinamarca, Egipto, Liberia, Malta, Serbia, Sri Lanka y Togo, la banda 3 155-3 200 kHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
72	5.118	Atribución adicional: en Estados Unidos, México, Perú y Uruguay, la banda 3 230-3 400 kHz está también atribuida, a título secundario, al servicio de radiolocalización. (CMR-03)
73	5.119	Atribución adicional: en Honduras, México y Perú, la banda 3 500-3 750 kHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-07)
74	5.120	SUP - CMR 2000.
75	5.121	No utilizado.
76	5.122	Atribución sustitutiva: en Bolivia, Chile, Ecuador, Paraguay, Perú y Uruguay, la banda 3 750-4 000 kHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
77	5.123	Atribución adicional: en Botswana, Lesotho, Malawi, Mozambique, Namibia, República Sudafricana, Swazilandia, Zambia y Zimbabwe, la banda 3 900 - 3 950 kHz está también atribuida, a título primario, al servicio de radiodifusión, a reserva de obtener el acuerdo indicado en el número 9.21.
78	5.124	SUP – CMR 2000.
79	5.125	Atribución adicional: en Groenlandia, la banda 3 950 - 4 000 kHz está también atribuida, a título primario, al servicio de radiodifusión. La potencia de las estaciones de radiodifusión que funcionen en esta banda no deberá rebasar el valor necesario para asegurar un servicio nacional, y en ningún caso podrá sobrepasar los 5 kW.
80	5.126	En la Región 3, las estaciones de los servicios a los que se atribuye la banda 3 995 - 4 005 kHz podrán transmitir frecuencias patrón y señales horarias.
81	5.127	El uso de la banda 4 000 - 4 063 kHz, por el servicio móvil marítimo, está limitado a las estaciones de barco que funcionan en radiotelefonía (véanse el número 52.220 y el apéndice 17).
82	5.128	Excepcionalmente, y a condición de no causar interferencia perjudicial al servicio móvil marítimo, las estaciones del servicio fijo podrán utilizar las frecuencias comprendidas en las bandas 4 063-4 123 kHz y 4 130-4 438 kHz con una potencia media inferior a 50 W sólo para la comunicación dentro del país en el que estén situadas. Además, las estaciones del servicio fijo cuya potencia media no rebase el valor de 1 kW podrán funcionar en Afganistán, Argentina, Armenia, Azerbaiyán, Belarús, Botswana, Burkina Faso, Centroafricana (Rep.), China, Federación de Rusia, Georgia, India, Kazajstán, Malí, Níger, Kirguistán, Tayikistán, Chad, Turkmenistán y Ucrania, en las bandas 4 063-4 123 kHz, 4 130-4 133 kHz y 4 408-4 438 kHz, siempre y cuando estén situadas a 600 km como mínimo de la costa y a condición de no causar interferencia perjudicial al servicio móvil marítimo. (CMR-07)
83	5.129	SUP – CMR 2007.
84	5.130	Las condiciones de utilización de las frecuencias portadoras de 4 125 kHz y 6 215 kHz están descritas en los Artículos 31 y 52. (CMR-07)
85	5.131	La frecuencia 4 209,5 kHz se utilizará exclusivamente para la transmisión por las estaciones costeras de avisos a los navegantes, boletines meteorológicos e información urgente con destino a los barcos mediante técnicas de impresión directa de banda estrecha. (CMR-97)
86	5.132	Las frecuencias 4 210 kHz, 6 314 kHz, 8 416,5 kHz, 12 579 kHz, 16 806,5 kHz, 19 680,5 kHz, 22 376 kHz y 26 100,5 kHz son las frecuencias internacionales de transmisión de información relativa a la seguridad marítima (MSI) (véanse el apéndice 17).
87	5.133	Categoría de servicio diferente: en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Kazajstán, Letonia, Lituania, Uzbekistán, Kirguistán, Tayikistán, Turkmenistán y Ucrania, la atribución de la banda 5 130-5 250 kHz al servicio móvil, salvo móvil aeronáutico, es a título primario (véase el número 5.33). (CMR-07)
88	5.134	La utilización de las banda5 900-5 950 kHz, 7 300-7 350 kHz, 9 400-9 500 kHz, 11 600- 11 650 kHz, 12 050-12 100 kHz, 13 570-13 600 kHz, 13 800-13 870 kHz, 15 600-15 800 kHz, 17 480-17 550 kHz y 18 900-19 020 kHz por el servicio de radiodifusión estará sujeta a la aplicación del procedimiento del Artículo 12. Se alienta a las administraciones a que utilicen estas bandas a fin de facilitar la introducción de las emisiones uladas digitalmente, según lo dispuesto en la Resolución 517 (Rev.CMR-07). (CMR-07)
89	5.135	SUP-CMR-97.
90	5.136	Atribución adicional: A condición de no causar interferencia perjudicial al servicio de radiodifusión, y sólo para la comunicación dentro del país en que se encuentren, las frecuencias comprendidas en la banda 5 900-5 950 kHz podrán ser utilizadas por estaciones de los siguientes servicios: servicio fijo (en las tres Regiones), servicio móvil terrestre (en la Región 1), y servicio móvil salvo móvil aeronáutico (R) (en la Región 2 y la Región 3) Cuando utilicen frecuencias para estos servicios, se insta a las administraciones a utilizar la mínima potencia necesaria y a tener en cuenta la utilización estacional de frecuencias por el servicio de radiodifusión publicada de conformidad con el Reglamento de Radiocomunicaciones. (CMR- 07)
91	5.137	Excepcionalmente, y a condición de no causar interferencia perjudicial al servicio móvil marítimo, las bandas 6 200 - 6 213,5 kHz y 6 220,5 - 6 525 kHz podrán ser utilizadas por estaciones del servicio fijo que comuniquen únicamente dentro de las fronteras nacionales y cuya potencia media no rebase el valor de 50 vatios. Cuando se haga la notificación de las frecuencias, se llamará la atención de la oficina sobre estas disposiciones.
129	5.162A	Atribución adicional: en Alemania, Austria, Bélgica, Bosnia y Herzegovina, China, Vaticano, Dinamarca, España, Estonia, Federación de Rusia, Finlandia, Francia, Irlanda, Islandia, Italia, Letonia, la ex República Yugoslava de Macedonia, Liechtenstein, Lituania, Luxemburgo, Mónaco, Montenegro, Noruega, Países Bajos, Polonia, Portugal, Eslovaquia, Rep. Checa, Reino Unido, Serbia, Eslovenia, Suecia y Suiza, la banda 46-68 MHz también está atribuida al servicio de radiolocalización a título secundario. Dicha utilización se limita a las operaciones de radares de perfil del viento, de conformidad con la Resolución 217 (CMR-97). (CMR-07)
92	5.138	Las bandas: 6 765 - 6 795 kHz (frecuencia central 6 780 kHz), 433,05 - 434,79 MHz(frecuencia central 433,92 MHz) en la Región 1, excepto en los países mencionados en el número 5.280, 61 - 61,5 GHz (frecuencia central 61,25 GHz), 122 - 123 GHz (frecuencia central 122,5 GHz), y 244 - 246 GHz (frecuencia central 245 GHz) están designadas para aplicaciones industriales, científicas y médicas (ICM). La utilización de estas bandas para las aplicaciones ICM está sujeta a una autorización especial concedida por la administración interesada de acuerdo con las otras administraciones cuyos servicios de radiocomunicación puedan resultar afectados. Al aplicar esta disposición, las administraciones tendrán debidamente en cuenta las últimas recomendaciones UIT-R pertinentes.
93	5.138A	Hasta el 29 de marzo de 2009, la banda 6 765 7 000 kHz está atribuida al servicio fijo a título primario y al servicio móvil terrestre a título secundario. Después de esa fecha, esta banda está atribuida a los servicios fijo y móvil, salvo móvil aeronáutico (R), a título primario. (CMR- 03)
94	5.139	Categoría de servicio diferente: hasta el 29 de marzo de 2009, en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Kazajstán, Letonia, Lituania, Mongolia, Uzbekistán, Kirguistán, Tayikistán, Turkmenistán y Ucrania, la atribución de la banda 6 765-7 000 kHz al servicio móvil terrestre es a título primario (véase el número 5.33). (CMR-07)
95	5.140	Atribución adicional: en Angola, Iraq,  Kenya, Rwanda, Somalia y  Togo, la  banda 7 000-7 050 kHz está también atribuida, a título primario, al servicio fijo. (CMR-03)
96	5.141	Atribución susti tutiva: en Egipto, Eritrea, Etiopía, Guinea, Libia y Madagascar, la banda 7 000 - 7 050 kHz está atribuida, a título primario, al servicio fijo. (CMR-97)
97	5.141A	Atribución adicional: En Uzbekistán y Kirguistán, las bandas 7 000 7 100 kHz y 7 100 7 200 kHz  están también atribuidas, a título secundario, a los servicios fijo y móvil terrestre. (CMR-03)
98	5.141B	Atribución adicional: a partir del 29 de marzo de 2009, en Argelia, Arabia Saudita, Australia, Bahrein, Botswana, Brunei Darussalam, China, Comoras, Corea (Rep. de), Diego García, Djibouti, Egipto, Emiratos Árabes Unidos, Eritrea, Indonesia, Irán (República Islámica del), Jamahiriya Árabe Libia, Japón, Jordania, Kuwait, Marruecos, Mauritania, Nueva Zelandia, Omán, Papua Nueva Guinea, Qatar, República Árabe Siria, Singapur, Sudán, Túnez, Viet Nam y Yemen, la banda 7 100-7 200 kHz también estará atribuida a título primario a los servicios fijo y móvil salvo móvil aeronáutico (R). (CMR-03)
99	5.141C	En las Regiones 1 y 3, la banda 7 100-7 200 kHz está atribuida, a título primario, al servicio de radiodifusión hasta el 29 de marzo de 2009. (CMR-03)
100	5.142	Hasta el 29 de marzo de 2009, la utilización de la banda 7 100-7 300 kHz por el servicio de aficionados en la Región 2 no deberá imponer limitaciones al servicio de radiodifusión destinado a utilizarse dentro de la Región 1 y de la Región 3. Después del 29 de marzo de 2009, la utilización de la banda 7 200-7 300 kHz en la Región 2 por el servicio de radioaficionados no deberá imponer limitaciones al servicio de radiodifusión destinado a utilizarse en la Región 1 y en la Región 3. (CMR-03)
101	5.143	Atribución adicional: Las estaciones del servicio fijo y el servicio móvil terrestre podrán utilizar las frecuencias comprendidas en la banda 7 300-7 350 kHz sólo para la comunicación dentro del país en que están situadas, a condición de que no se cause interferencia perjudicial al servicio de radiodifusión. Cuando utilicen frecuencias para estos servicios, se  insta  a  las administraciones a utilizar la mínima potencia necesaria y a tener en cuenta la utilización estacional de frecuencias por el servicio de radiodifusión publicada de conformidad con el Reglamento de Radiocomunicaciones. (CMR-07)
102	5.143A	En la Región 3, la banda 7 350-7 450 kHz está atribuida, hasta el 29 de marzo de 2009, al servicio fijo a título primario y al servicio móvil terrestre a título secundario. Después del 29 de marzo de 2009, las  frecuencias de esta banda podrán ser utilizadas por estaciones de los servicios antes mencionados, para comunicar únicamente dentro de las fronteras del país en el cual estén situadas, a condición de que no se cause interferencia perjudicial al servicio de radiodifusión. Cuando utilicen frecuencias para estos servicios, se insta a las administraciones a utilizar la mínima potencia necesaria y a tener en cuenta la utilización estacional de frecuencias por el servicio de radio¬difusión publicada de conformidad con el Reglamento de Radiocomunicaciones. (CMR-03)
103	5.143B	En la Región 1, la banda 7 350-7 450 kHz está atribuida hasta el 29 de marzo de 2009 al servicio fijo a título primario y al servicio móvil terrestre a título secundario. A partir del 29 de marzo de 2009, a condición de que no  se cause interferencia perjudicial al  servicio de radiodifusión y de no utilizar una potencia radiada total superior a 24 dBW, las estaciones de los servicios fijo y móvil terrestre podrán utilizar frecuencias en la banda 7 350-7 450 kHz para comunicar únicamente dentro de las fronteras del país en el cual estén situadas. (CMR-03)
104	5.143C	Atribución adicional: a partir del 29 de marzo de 2009, las bandas 7 350-7 400 kHz y 7 400-7 450 kHz estarán también atribuidas, a título primario, al servicio fijo en Argelia, Arabia Saudita, Bahrein, Comoras, Djibouti, Egipto, Emiratos Árabes Unidos, Irán (República Islámica del), Jamahiriya Árabe Libia, Jordania, Kuwait, Marruecos, Mauritania, Omán, Qatar, República Árabe Siria, Sudán, Túnez y Yemen. (CMR-03)
105	5.143D	En la Región 2, la banda 7 350-7 400 kHz está atribuida, hasta el 29 de marzo de 2009, al servicio fijo a título primario y al servicio móvil terrestre a título secundario. Después del 29 de marzo de 2009, las  frecuencias de esta banda podrán ser utilizadas por estaciones de los servicios antes mencionados, para comunicar únicamente dentro de las fronteras del país en el cual estén situadas, a condición de que no se cause interferencia perjudicial al servicio de radiodifusión. Cuando utilicen frecuencias para estos servicios, se insta a las administraciones a utilizar la mínima potencia necesaria y a tener en cuenta la utilización estacional de frecuencias por el  servicio de radiodifusión publicada de conformidad con el Reglamento de Radiocomunicaciones. (CMR-03)
106	5.143E	Hasta el 29 de marzo de 2009, la banda 7 450-8 100 kHz está atribuida al servicio fijo a título primario y al servicio móvil terrestre a título secundario. (CMR-03)
107	5.144	En la Región 3, las estaciones de los servicios a los que está atribuida la banda 7 995 - 8 005 kHz pueden transmitir frecuencias patrón y señales horarias.
108	5.145	Las condiciones de utilización de las frecuencias portadoras 8 291 kHz, 12 290 kHz y 16 420 kHz están descritas en los Artículos 31 y 52. (CMR-07)
130	5.163	Atribución adicional: en Armenia, Belarús, Federación de Rusia, Georgia, Hungría, Kazajstán, Letonia, Lituania, Moldova, Uzbekistán, Kirguistán, Eslovaquia, Rep. Checa, Tayikistán, Turkmenistán y Ucrania, las bandas 47-48,5 MHz y 56,5-58 MHz están también atribuidas, a título secundario, a los servicios fijo y móvil terrestre. (CMR-07)
109	5.146	Atribución adicional: Las estaciones del servicio fijo y el servicio móvil terrestre podrán utilizar las frecuencias comprendidas en las bandas 9 400-9 500 kHz, 11 600-11 650 kHz, 12 050-12 100 kHz, 15 600-15 800 kHz, 17 480-17 550 kHz y 18 900-19 020 kHz sólo para la comunicación dentro del  país  en que están situadas, a  condición de  que no se  cause interferencia perjudicial al  servicio  de  radiodifusión. Cuando utilicen  frecuencias para el servicio fijo, se insta a las administraciones a utilizar la mínima potencia necesaria y a tener en cuenta la utilización estacional de frecuencias por el servicio de radiodifusión publicada de conformidad con el Reglamento de Radiocomunicaciones. (CMR-07)
110	5.147	A condición de  no causar  interferencia perjudicial  al  servicio de  radiodifusión, las frecuencias de  las  bandas 9 775  -  9 900 kHz, 11 650 - 11 700 kHz y  11 975 - 12 050 kHz podrán ser utilizadas por estaciones del servicio fijo que comuniquen únicamente dentro de las fronteras nacionales, no rebasando cada estación una potencia radiada total de 24 dBW.
111	5.148	SUP-CMR-97.
112	5.149	Se insta  a  las  administraciones a  que,  al  hacer  asignaciones a  estaciones de  otros servicios a los que están atribuidas las bandas: 13 360-13 410 kHz, 25 550-25 670 kHz, 37,5-38,25 MHz, 73-74,6 MHz en las Regiones 1 y 3, 150,05-153 MHz en la Región 1, 322-328,6 MHz, 406,1-410 MHz, 608-614 MHz en las Regiones 1 y 3, 1 330-1 400 MHz, 1 610,6-1 613,8 MHz, 1 660-1 670 MHz, 1 718,8-1 722,2 MHz, 2 655-2 690 MHz, 3 260-3 267 MHz, 3 332-3 339 MHz, 3 345,8-3 352,5 MHz, 4 825-4 835 MHz, 4 950-4 990 MHz, 4 990-5 000 MHz, 6 650-6 675,2 MHz, 10,6-10,68 GHz, 14,47-14,5 GHz, 22,01-22,21 GHz, 22,21-22,5 GHz, 22,81-22,86 GHz, 23,07-23,12 GHz, 31,2-31,3 GHz, 31,5-31,8 GHz en las Regiones 1 y 3, 36,43-36,5 GHz, 42,5-43,5 GHz, 42,77-42,87 GHz, 43,07-43,17 GHz, 43,37-43,47 GHz, 48,94-49,04 GHz, 76-86 GHz, 92-94 GHz, 94,1-100 GHz, 102-109,5 GHz, 111,8-114,25 GHz, 128,33-128,59 GHz, 129,23-129,49 GHz, 130-134 GHz, 136-148,5 GHz, 151,5-158,5 GHz, 168,59-168,93 GHz, 171,11-171,45 GHz, 172,31-172,65 GHz, 173,52-173,85 GHz, 195,75-196,15 GHz, 209-226 GHz, 241-250 GHz, 252-275 GHz tomen  todas las medidas prácticamente posibles para proteger el servicio de radioastronomía contra las interferencias perjudiciales. Las emisiones desde estaciones a bordo de vehículos espaciales o aeronaves pueden constituir fuentes de interferencia particularmente graves para el servicio de radioastronomía (véanse los números 4.5 y 4.6 y el artículo 29). (CMR-07)
113	5.150	Las bandas: 13 553 - 13 567 kHz (frecuencia central 13 560 kHz), 26 957 - 27 283 kHz (frecuencia central 27 120 kHz), 40,66 - 40,70 MHz (frecuencia central 40,68 MHz), 902 - 928 MHz en la Región 2 (frecuencia central 915 MHz), 2 400 - 2 500 MHz (frecuencia central 2 450 MHz), 5 725 - 5 875 MHz (frecuencia central 5 800 MHz) y 24 - 24,25 GHz (frecuencia central 24,125 GHz) están designadas para aplicaciones industriales, científicas y médicas (ICM). Los servicios de radiocomunicación que funcionan en estas bandas deben aceptar la interferencia perjudicial resultante de estas aplicaciones. Los equipos ICM que funcionen en estas bandas estarán sujetos a las disposiciones del número 15.13.
114	5.151	Atribución adicional:  Las estaciones del servicio fijo y el servicio móvil terrestre, salvo móvil aeronáutico (R), podrán utilizar las frecuencias comprendidas en las bandas 13 570-13 600 kHz y 13 800-13 870 kHz sólo para la comunicación dentro del país en que están situadas, a condición de que no se cause interferencia perjudicial al servicio de radiodifusión. Cuando utilicen frecuencias para estos servicios, se insta a las administraciones a utilizar la mínima potencia necesaria y a tener en cuenta la utilización estacional de frecuencias por el servicio de radiodifusión publicada de conformidad con el Reglamento de Radiocomunicaciones.  (CMR- 07)
115	5.152	Atribución adicional: en Armenia, Azerbaiyán, China, Côte d'Ivoire, Georgia, Irán (Rep. Islámica del), Kazajstán, Uzbekistán, Kirguistán, Federación de Rusia, Tayikistán, Turkmenistán y Ucrania, la banda 14 250-14 350 kHz está también atribuida, a título primario, al servicio fijo. La potencia radiada por las estaciones del servicio fijo no deberá exceder de 24 dBW. (CMR-03)
116	5.153	En la Región 3, las estaciones de los servicios a los que está atribuida la banda 15 995 - 16 005 kHz pueden transmitir frecuencias patrón y señales horarias.
117	5.154	Atribución adicional: en Armenia, Azerbaiyán, Georgia, Kazajstán, Kirguistán, Federación de Rusia, Tayikistán, Turkmenistán y Ucrania, la banda 18 068-18 168 kHz está también atribuida, a título primario, al servicio fijo para utilización dentro de sus fronteras respectivas con una potencia máxima en la cresta de la envolvente de 1 kW. (CMR-03)
118	5.155	Atribución adicional: en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Kazajstán, Moldova, Mongolia, Uzbekistán, Kirguistán, Eslovaquia, Tayikistán, Turkmenistán y Ucrania, la banda 2 1 850-21 870 kHz está atribuida también, a título primario, al servicio móvil aeronáutico (R). (CMR-07)
119	5.155A	En Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Kazajstán, Moldova, Mongolia, Uzbekistán, Kirguistán, Eslovaquia, Tayikistán, Turkmenistán y Ucrania, la utilización de la banda 21 850-21 870 kHz por el servicio fijo está limitada al suministro de servicios relacionados con la seguridad de los vuelos de aeronave. (CMR-07)
120	5.155B	La banda 21 870  -  21 924 kHz es utilizada por el servicio fijo para el suministro de servicios relacionados con la seguridad de los vuelos de aeronave.
121	5.156	Atribución adicional:  en Nigeria, la banda 22 720 - 23 200 Hz está también atribuida, a título primario, al servicio de ayudas a la meteorología (radiosondas).
122	5.156A	La utilización de la banda 23 200  -  23 350 kHz por el servicio fijo está limitada al suministro de servicios relacionados con la seguridad de los vuelos de aeronave.
123	5.157	La utilización de la banda 23 350 - 24 000 kHz por el servicio móvil marítimo está limitada a la radiotelegrafía entre barcos.
124	5.158	No utilizados.
125	5.159	No utilizados.
126	5.160	Atribución adicional: en Botswana, Burundi, Lesotho, Malawi, Rep. Dem. del Congo, Rwanda y Swazilandia, la banda 41-44 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica. (CMR-00)
127	5.161	Atribución adicional: en la República Islámica del Irán y en Japón, la banda 41 - 44 MHz está también atribuida, a título secundario, al servicio de radiolocalización.
128	5.162	Atribución adicional: en Australia y Nueva Zelandia la banda 44-47 MHz está también atribuida, a título primario, al servicio de radiodifusión.
155	5.187	Atribución sustitutiva: en Albania, la banda 81 - 87,5 MHz está atribuida, a título primario, al servicio de radiodifusión y se utiliza de conformidad con las decisiones contenidas en las Actas Finales de la Conferencia Regional Especial (Ginebra, 1960).
642	5.533	El servicio entre satélites no reclamará protección contra la interferencia perjudicial procedente de estaciones de equipos de detección de superficie de aeropuertos del servicio de radionavegación.
131	5.164	Atribución adicional: en Albania, Alemania, Austria, Bélgica, Bosnia y Herzegovina, Botswana, Bulgaria, Côte d'Ivoire, Dinamarca, España, Estonia, Finlandia, Francia, Gabón, Grecia, Irlanda, Israel, Italia, Jamahiriya Árabe Libia, Jordania, Líbano, Liechtenstein, Luxemburgo, Madagascar, Malí, Malta, Marruecos, Mauritania, Mónaco, Montenegro, Nigeria, Noruega, Países Bajos, Polonia, República Árabe Siria, Rumania, Reino Unido, Serbia, Eslovenia, Suecia, Suiza, Swazilandia, Chad, Togo, Túnez y Turquía, la banda 47-68 MHz, en Sudafricana (Rep.) la banda 47-50 MHz, en la Rep. Checa la banda 66-68 MHz y en Letonia y Lituania la banda de 48,5-56,5 MHz, están también atribuidas, a título primario, al servicio móvil terrestre. Sin embargo, las estaciones del servicio móvil terrestre de los países mencionados para cada una de las bandas que figuran en la presente nota no deben causar interferencia perjudicial a las estaciones de radiodifusión existentes o en proyecto de países distintos de los mencionados en esta nota para cada una de estas bandas, ni reclamar protección frente a ellas. (CMR-07)
132	5.165	Atribución adicional: en Angola, Camerún, Congo, Madagascar, Mozambique, Somalia, Sudán, Tanzanía y Chad, la banda 47 - 68 MHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico.
133	5.166	Atr ibución sustitutiva: en Nueva Zelandia, la banda 50 - 51 MHz está atribuida, a título primario, a los servicios fijo, móvil y de radiodifusión; la banda 53 - 54 MHz está atribuida, a título primario, a los servicios fijo y móvil.
134	5.167	Atribución sustitutiva: en Bangladesh, Brunei Darussalam, India, Irán (República Islámica del), Pakistán, Singapur y Tailandia, la banda 50-54 MHz está atribuida, a título primario, a los servicios fijo, móvil y de radiodifusión. (CMR-07)
135	5.167A	Atribución adicional: en Indonesia, la banda 50-54 MHz también está atribuida a los servicios fijo, móvil y de radiodifusión a título primario. (CMR-07)
136	5.168	Atribución adicional: en Australia, China y República Popular Democrática de Corea, la banda 50 - 54 MHz está también atribuida, a título primario, al servicio de radiodifusión.
137	5.169	Atribución sustitutiva: en Botswana, Burundi, Lesotho, Malawi, Namibia, Rwanda, República Sudafricana, Swazilandia, Zaire, Zambia y Zimbabwe, la banda 50 - 54 MHz está atribuida, a título primario, al servicio de aficionados.
138	5.170	Atribución adicional: en Nueva Zelandia, la banda 51 - 53 MHz está también atribuida, a título primario, a los servicios fijo y móvil.
139	5.171	Atribución adicional: en Botswana, Burundi, Lesotho, Malawi, Malí, Namibia, Rwanda, República Sudafricana, Swazilandia, Zaire y Zimbabwe, la banda 54 - 68 MHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico.
140	5.172	Categoría de servicio diferente: en los Departamentos franceses de Ultramar de la Región 2, en Guyana, Jamaica y México, la atribución de la banda 54 - 68 MHz a los servicios fijo y móvil es a título primario (véase el número 5.33).
141	5.173	Categoría de servicio diferente: en los Departamentos franceses de Ultramar de la Región 2, en Guyana, Jamaica y México, la atribución de la banda 68 - 72 MHz a los servicios fijo y móvil es a título primario (véase el número 5.33).
142	5.174	SUP – CMR 2007.
143	5.175	Atribución sustitutiva: en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Kazajstán, Moldova, Uzbekistán, Kirguistán, Tayikistán, Turkmenistán y en Ucrania, las bandas 68-73 MHz y 76-87,5 MHz están atribuidas, a título primario, al servicio de radiodifusión. En Letonia y Lituania, las bandas 68-73 MHz y 76-87,5 MHz están atribuidas a título primario a los servicios de radiodifusión y móvil, salvo móvil aeronáutico. Los servicios a los que están atribuidas estas bandas en otros países, y el servicio de radiodifusión en estos países, están sujetos a acuerdos entre los países vecinos interesados. (CMR-07)
144	5.176	Atribución adicional: en Australia, China, Corea (Rep. de), Filipinas, Rep. Pop. Dem. de Corea y Samoa la banda 68-74 MHz está también atribuida, a título primario, al servicio de radiodifusión. (CMR-07)
145	5.177	Atribución adicional: en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Kazajstán, Uzbekistán, Kirguistán, Tayikistán, Turkmenistán y Ucrania, la banda 73-74 MHz está también atribuida, a título primario, al servicio de radiodifusión, a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-07)
146	5.178	Atribución adicional: en Colombia, Costa Rica, Cuba, El Salvador, Guatemala, Guyana, Honduras y Nicaragua, la banda 73 - 74,6 MHz está también atribuida, a título secundario, a los servicios fijo y móvil.
147	5.179	Atribución adicional: en Armenia, Azerbaiyán, Belarús, China, Federación de Rusia, Georgia, Kazajstán, Lituania, Mongolia, Kirguistán, Eslovaquia, Tayikistán, Turkmenistán y Ucrania, las bandas 74,6-74,8 MHz y 75,2-75,4 MHz están también atribuidas, a título primario, al servicio de radionavegación aeronáutica, únicamente para transmisores instalados en tierra. (CMR-07)
148	5.180	La frecuencia de 75 MHz se asigna a las radiobalizas. Las administraciones deberán abstenerse de asignar frecuencias próximas a los límites de la banda de guarda a las estaciones de otros servicios que, por su potencia o su posición geográfica, puedan causar interferencias perjudiciales a las radiobalizas aeronáuticas o imponerles otras limitaciones. Debe hacerse todo lo posible para seguir mejorando las características de los receptores a bordo de aeronaves y limitar la potencia de las estaciones que transmitan en frecuencias próximas a los límites de 74,8 MHz y 75,2 MHz.
149	5.181	Atribución adicional: en Egipto, Israel, y República Árabe Siria, la banda 74,8-75,2 MHz está también atribuida al servicio móvil a título secundario, a reserva de obtener el acuerdo indicado en el número 9.21. A fin de garantizar que no se produzca interferencia perjudicial a las estaciones del servicio de radionavegación aeronáutica, no se introducirán las estaciones del servicio móvil en la banda hasta que ya no la necesite para el servicio de radionavegación aeronáutica ninguna administración que pueda ser identificada en aplicación del procedimiento invocado en el número 9.21. (CMR-03)
150	5.182	Atribución adicional: en Samoa Occidental, la banda 75,4 - 87 MHz está también atribuida, a título primario, al servicio de radiodifusión.
151	5.183	Atribución adicional: en China, República de Corea, Japón, Filipinas y República Popular Democrática de Corea, la banda 76 - 87 MHz está también atribuida, a título primario, al servicio de radiodifusión.
152	5.184	SUP – CMR 2007.
153	5.185	Categoría de servicio diferente: en Estados Unidos, en los Departamentos franceses de Ultramar de la Región 2, en Guyana, Jamaica, México y Paraguay, la atribución de la banda 76 - 88 MHz a los servicios fijo y móvil es a título primario (véase el número 5.33).
154	5.186	SUP-CMR-07.
643	5.534	SUP - CMR 2003.
156	5.188	Atribución adicional: en Australia, la banda 85 - 87 MHz está también atribuida, a título primario, al servicio de radiodifusión. La introducción del servicio de radiodifusión en Australia está sujeta a acuerdos especiales entre las administraciones interesadas.
157	5.189	No utilizado.
158	5.190	Atribución adicional: en Mónaco, la banda 87,5 - 88 MHz está también atribuida, a título primario, al servicio móvil terrestre, a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-97)
159	5.191	No utilizado.
160	5.192	Atribución adicional: en China y República de Corea, la banda 100 - 108 MHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-97)
161	5.193	No utilizado.
162	5.194	Atribución adicional: en Azerbaiyán, Kirguistán, Somalia y Turkmenistán, la banda 104- 108 MHz está también atribuida al servicio móvil, salvo móvil aeronáutico (R), a título secundario. (CMR-07)
163	5.195	No utilizados.
164	5.196	No utilizados.
165	5.197	Atribución adicional: en Pakistán y República Árabe Siria, la banda 108-111,975 MHz está también atribuida al servicio móvil a título secundario, a reserva de obtener el acuerdo indicado en el número 9.21. A fin de garantizar que no se produzca interferencia perjudicial a las estaciones del servicio de radionavegación aeronáutica, no se introducirán las estaciones del servicio móvil en la banda hasta que ya no la necesite para el servicio de radionavegación aeronáutica ninguna administración que pueda ser identificada en aplicación del procedimiento invocado en el número 9.21. (CMR-07)
166	5.197A	Atribución adicional: la banda 108-117,975 MHz también se atribuye a título primario al servicio móvil aeronáutico (R) exclusivamente para los sistemas que funcionan en conformidad con las normas aeronáuticas internacionales reconocidas. Dicha utilización ha de ser conforme con la Resolución 413 (Rev.CMR-07). La utilización de la banda 108-112 MHz por el servicio móvil aeronáutico (R) se limitará a los sistemas compuestos por transmisores en tierra y los correspondientes receptores para la transmisión de información de navegación para la navegación aérea en conformidad con las normas aeronáuticas internacionales reconocidas. (CMR-07)
167	5.198	SUP – CMR 2007.
168	5.199	SUP – CMR 2007.
169	5.200	En la banda 117,975-137 MHz, la frecuencia de 121,5 MHz es la frecuencia aeronáutica de emergencia y, de necesitarse, la frecuencia de 123,1 MHz es la frecuencia aeronáutica auxiliar de la de 121,5 MHz. Las estaciones móviles del servicio móvil marítimo podrán comunicar en estas frecuencias, en las condiciones que se fijan en el Artículo 31, para fines de socorro y seguridad, con las estaciones del servicio móvil aeronáutico. (CMR-07)
170	5.201	Atribución adicional: en Angola, Armenia, Azerbaiyán, Belarús, Bulgaria, Estonia, Georgia, Hungría, República Islámica del Irán, Iraq, Japón, Kazakstán, Letonia, Moldova, Mongolia, Mozambique, Uzbekistán, Papua Nueva Guinea, Polonia, Kirguistán, Eslovaquia, República Checa, Rumania, Rusia, Tayikistán, Turkmenistán y Ucrania la banda 132 - 136 MHz está también atribuida, a título primario, al servicio móvil aeronáutico (OR). Al asignar frecuencias a las estaciones del servicio móvil aeronáutico (OR), la administración deberá tener en cuenta las frecuencias asignadas a las estaciones del servicio móvil aeronáutico (R). (CMR-97)
171	5.202	Atribución adicional: en Arabia Saudita, Armenia, Azerbaiyán, Belarús, Bulgaria, Emiratos Árabes Unidos, Georgia, República Islámica del Irán, Jordania, Letonia, Moldova, Omán, Uzbekistán, Polonia, Siria, Kirguistán, Eslovaquia, República Checa, Rumania, Federación de Rusia, Tayikistán, Turkmenistán, y Ucrania, la banda 136-137 MHz está atribuida también a título primario al servicio móvil aeronáutico (OR). Al asignar frecuencias a las estaciones del servicio móvil aeronáutico (OR), la administración deberá tener en cuenta las frecuencias asignadas a las estaciones del servicio móvil aeronáutico (R). (CMR-00)
172	5.203	SUP – CMR 2007.
173	5.203A	SUP – CMR 2007.
174	5.203B	SUP – CMR 2007.
175	5.204	Categoría de servicio diferente: en Afganistán, Arabia Saudita, Bahrein, Bangladesh, Brunei Darussalam, China, Cuba, Emiratos Árabes Unidos, India, Indonesia, Irán (República Islámica del), Iraq, Kuwait, Montenegro, Omán, Pakistán, Filipinas, Qatar, Serbia, Singapur, Tailandia y Yemen, la atribución de la banda 137-138 MHz a los servicios fijo y móvil, salvo móvil aeronáutico (R), es a título primario (véase el número 5.33). (CMR-07)
176	5.205	Categoría de servicio diferente: en Israel y Jordania, la atribución de la banda 137 - 138 MHz a los servicios fijo y móvil, salvo móvil aeronáutico es a título primario (véase el número 5.33).
177	5.206	Categoría de servicio diferente: en Armenia, Azerbaiyán, Belarús, Bulgaria, Egipto, Finlandia, Francia, Georgia, Grecia, Kazakstán, Líbano, Moldova, Mongolia, Uzbekistán, Polonia, Kirguistán, Siria, Eslovaquia, República Checa, Rumania, Federación de Rusia, Tayikistán, Turkmenistán y Ucrania, la atribución de la banda 137-138 MHz al servicio móvil aeronáutico (OR) es a título primario (véase el número 5.33). (CMR-00)
178	5.207	Atribución adicional: en Australia, la banda 137 - 144 MHz está también atribuida, a título primario, al servicio de radiodifusión hasta que sea posible acomodar dicho servicio en las atribuciones regionales a la radiodifusión.
179	5.208	La utilización de la banda 137 - 138 MHz por el servicio móvil por satélite está sujeta a la coordinación a tenor del número 9.11A. (CMR-97)
180	5.208A	Al efectuar las asignaciones a las estaciones espaciales del servicio móvil por satélite en las bandas 137-138 MHz, 387-390 MHz y 400,15-401 MHz, las administraciones adoptarán todas las medidas posibles para proteger el servicio de radioastronomía en las bandas 150,05- 153 MHz, 322-328,6 MHz, 406,1-410 MHz y 608-614 MHz de la interferencia perjudicial producida por las emisiones no deseadas. Los niveles umbral de interferencia perjudicial para el servicio de radioastronomía se indican en la Recomendación UIT-R pertinente. (CMR-07)
181	5.208B	En las bandas: 137-138 MHz, 387-390 MHz, 400,15-401 MHz, 1 452-1 492 MHz, 1 525-1 610 MHz, 1 613,8-1 626,5 MHz, 2 655-2 690 MHz, 21,4-22 GHz, se aplica la Resolución 739 (Rev.CMR-07). (CMR-07) Esta disposición fue numerada anteriormente como número 5.208B. Se renumeró para mantener el orden secuencial. SUP-CMR-07.
182	5.209	La utilización de las bandas 137 - 138 MHz, 148 - 150,05 MHz, 399,9 - 400,05 MHz, 400,15 - 401 MHz, 454 - 456 MHz y 459 - 460 MHz por el servicio móvil por satélite está limitada a los sistemas de satélites no geoestacionarios. (CMR-97)
183	5.210	Atribución adicional: en Italia, Rep. Checa y Reino Unido, las bandas 138-143,6 MHz y 143,65-144 MHz están también atribuidas, a título secundario, al servicio de investigación espacial (espacio-Tierra). (CMR-07)
184	5.211	Atribución adicional: en Alemania, Arabia Saudita, Austria, Bahrein, Bélgica, Dinamarca, Emiratos Árabes Unidos, España, Finlandia, Grecia, Irlanda, Israel, Kenya, Kuwait, la ex República Yugoslava de Macedonia, Líbano, Liechtenstein, Luxemburgo, Malí, Malta, Montenegro, Noruega, Países Bajos, Qatar, Reino Unido, Serbia, Eslovenia, Somalia, Suecia, Suiza, Tanzanía, Túnez y Turquía, la banda 138-144 MHz está también atribuida, a título primario, a los servicios móvil marítimo y móvil terrestre. (CMR-07)
185	5.212	Atribución sustitutiva: en Angola, Botswana, Burundi, Camerún, Centroafricana (Rep.), Congo (Rep. del), Gabón, Gambia, Ghana, Guinea, Iraq, Jamahiriya Árabe Libia, Jordania, Lesotho, Liberia, Malawi, Mozambique, Namibia, Omán, Uganda, República Árabe Siria, Rep. Dem. del Congo, Rwanda, Sierra Leona, Sudafricana (Rep.), Swazilandia, Chad, Togo, Zambia y Zimbabwe, la banda 138-144 MHz está atribuida, a título primario, a los servicios fijo y móvil. (CMR-07)
186	5.213	Atribución adicional: en China, la banda 138 - 144 MHz está también atribuida, a título primario, al servicio de radiolocalización.
187	5.214	Atribución adicional: en Eritrea, Etiopía, Kenya, la ex República Yugoslava de Macedonia, Malta, Montenegro, Serbia, Somalia, Sudán y Tanzanía, la banda 138-144 MHz está también atribuida, a título primario, al servicio fijo. (CMR-07)
188	5.215	No utilizado.
189	5.216	Atribución adicional : en China, la banda 144 - 146 MHz está también atribuida, a título secundario, al servicio móvil aeronáutico (OR).
190	5.217	Atribución sustitutiva : en Afganistán, Bangladesh, Cuba, Guyana e India, la banda 146 - 148 MHz está atribuida, a título primario, a los servicios fijo y móvil.
191	5.218	Atribución adicional : la banda 148 - 149,9 MHz está también atribuida al servicio de operaciones espaciales (Tierra-espacio) a título primario, a reserva de obtener el acuerdo indicado en el número 9.21. La anchura de banda de toda emisión no deberá ser superior a ± 25 kHz.
192	5.219	La utilización de la banda 148 - 149,9 MHz por el servicio móvil por satélite está sujeta a la coordinación a tenor de la Resolución 46 (Rev. CMR-97)/del número 9.11A. El servicio móvil por satélite no limitará el desarrollo y utilización de los servicios fijo, móvil y de operaciones espaciales en la banda 148 - 149,9 MHz.
193	5.220	La utilización de las bandas 149,9 - 150,05 MHz y 399,9 - 400,05 MHz por el servicio móvil por satélite está sujeta a la coordinación a tenor de la Resolución 46 (Rev.CMR-97)/del número 9.11A. El servicio móvil por satélite no limitará el desarrollo y utilización del servicio de radionavegación por satélite en las bandas 149,9 - 150,05 MHz y 399,9 - 400,05 MHz. (CMR-97)
194	5.221	Las estaciones del servicio móvil por satélite en la banda 148-149,9 MHz no causarán interferencia perjudicial a las estaciones de los servicios fijos o móviles explotadas de conformidad con el Cuadro de atribución de bandas de frecuencias, situadas en los siguientes países, ni solicitarán protección frente a ellas: Albania, Argelia, Alemania, Arabia Saudita, Australia, Austria, Bahrein, Bangladesh, Barbados, Belarús, Bélgica, Benin, Bosnia y Herzegovina, Botswana, Brunei Darussalam, Bulgaria, Camerún, China, Chipre, Congo (Rep. del), Corea (Rep. de), Côte d'Ivoire, Croacia, Cuba, Dinamarca, Egipto, Emiratos Árabes Unidos, Eritrea, España, Estonia, Etiopía, Federación de Rusia, Finlandia, Francia, Gabón, Ghana, Grecia, Guinea, Guinea-Bissau, Hungría, India, Irán (República Islámica del), Irlanda, Islandia, Israel, Italia, Jamahiriya Áraba Libia, Jamaica, Japón, Jordania, Kazajstán, Kenya, Kuwait, la ex República Yugoslava de Macedonia, Lesotho, Letonia, Líbano, Liechtenstein, Lituania, Luxemburgo, Malasia, Malí, Malta, Mauritania, Moldova, Mongolia, Montenegro, Mozambique, Namibia, Noruega, Nueva Zelandia, Omán, Uganda, Uzbekistán, Pakistán, Panamá, Papua Nueva Guinea, Paraguay, Países Bajos, Filipinas, Polonia, Portugal, Qatar, República Árabe Siria, Kirguistán, Rep. Pop. Dem. de Corea, Eslovaquia, Rumania, Reino Unido, Senegal, Serbia, Sierra Leona, Singapur, Eslovenia, Sri Lanka, Sudafricana (Rep.), Suecia, Suiza, Swazilandia, Tanzanía, Chad, Tailandia, Togo, Tonga, Trinidad y Tabago, Túnez, Turquía, Ucrania, Viet Nam, Yemen, Zambia y Zimbabwe. (CMR-07)
195	5.222	Las emisiones del servicio de radionavegación por satélite en las bandas 149,9 - 150,05 MHz y 399,9 - 400,05 MHz pueden además ser utilizadas por las estaciones terrenas receptoras del servicio de investigación espacial.
196	5.223	Reconociendo que la utilización de la banda 149,9 - 150,05 MHz por los servicios fijo y móvil puede causar interferencia perjudicial al servicio de radionavegación por satélite, se insta a las administraciones a no autorizar estos usos en aplicación del número 4.4.
197	5.224	SUP-CMR-97.
198	5.224A	La utilización de las bandas 149,9 – 150,05 MHz y 399,9 – 400,05 MHz por el servicio móvil por satélite (Tierra-espacio) está limitada al servicio móvil terrestre por satélite(Tierra- espacio) hasta el 1 de enero de 2015. (CMR-97)
199	5.224B	La atribución de las bandas 149,9 – 150,05 MHz y 399,9 – 400,05 MHz al servicio de radionavegación por satélite será efectiva hasta el 1 de enero de 2015. (CMR-97)
200	5.225	Atribución adicional: en Australia y en India, la banda 150,05 - 153 MHz está también atribuida, a título primario, al servicio de radioastronomía.
201	5.226	La frecuencia de 156,8 MHz es la frecuencia internacional de socorro, seguridad y llamada del servicio móvil marítimo radiotelefónico en ondas métricas. Las condiciones de utilización de esta frecuencia y de la banda 156,7625-156,8375 MHz se especifican en el Artículo 31 y en el Apéndice 18. La frecuencia de 156,525 MHz es la frecuencia internacional de socorro, seguridad y llamada del servicio móvil marítimo radiotelefónico en ondas métricas con llamada selectiva digital (LLSD). Las condiciones de utilización de esta frecuencia y de la banda 156,4875-156,5625 MHz se especifican en los Artículos 31 y 52 y en el Apéndice 18. En las bandas 156-156,4875 MHz, 156,5625-156,7625 MHz, 156,8375-157,45 MHz, 160,6- 160,975 MHz y 161,475-162,05 MHz, las administraciones darán prioridad al servicio móvil marítimo únicamente en aquellas frecuencias de estas bandas que se hayan asignado a las estaciones de dicho servicio (véanse los Artículos 31 y 52 y el Apéndice 18). Se procurará evitar la utilización de frecuencias comprendidas en estas bandas por los otros servicios a los que asimismo estén atribuidas, en aquellas zonas en que su empleo pueda causar interferencias perjudiciales a las radiocomunicaciones del servicio móvil marítimo en ondas métricas. Sin embargo, las frecuencias de 156,8 MHz y 156,525 MHz y las bandas de frecuencias en las cuales está autorizado el servicio móvil marítimo pueden utilizarse para las radiocomunicaciones en vías interiores de navegación, a reserva de acuerdos entre las administraciones interesadas y aquellas cuyos servicios, a los que la banda está atribuida, pudieran resultar afectados, teniendo en cuenta la utilización corriente de las frecuencias y los acuerdos existentes. (CMR-07)
202	5.227	Atribución adicional: las bandas 156,4875-156,5125 MHz y 156,5375-156,5625 MHz también están atribuidas a los servicios fijo y móvil terrestre a título primario. La utilización de estas bandas por los servicios fijo y móvil terrestre no causará interferencia perjudicial al servicio móvil marítimo en ondas métricas, ni reclamarán protección contra el mismo. (CMR- 07)
203	5.227A	Atribución adicional: las bandas 161,9625-161,9875 MHz y 162,0125-162,0375 MHz también están atribuidas al servicio móvil por satélite (Tierra-espacio) a título secundario para la recepción de emisiones del sistema de identificación automática (AIS), de estaciones que funcionen en el servicio móvil marítimo (véase el Apéndice 18). (CMR-07)
204	5.228	No utilizado.
205	5.229	Atribución sustitutiva: en Marruecos, la banda 162 - 174 MHz está atribuida, a título primario, al servicio de radiodifusión. Esta utilización estará sujeta al acuerdo con las administraciones cuyos servicios explotados o que se explotarán de conformidad con el presente cuadro puedan resultar afectados. Las estaciones existentes el 1 de enero de 1981 con sus características técnicas en esa fecha no serán afectadas por este acuerdo.
206	5.230	Atribución adicional: en China, la banda 163 - 167 MHz está también atribuida, a título primario, al servicio de operaciones espaciales (espacio-Tierra), a reserva de obtener el acuerdo indicado en el número 9.21.
207	5.231	Atribución adicional: en Afganistán, China y Pakistán, la banda 167 - 174 MHz está también atribuida, a título primario, al servicio de radiodifusión. La utilización de esta banda por el servicio de radiodifusión estará sujeta al acuerdo con los países vecinos de la Región 3 cuyos servicios puedan ser afectados.
208	5.232	Atribución adicional: en Japón, la banda 170 - 174 MHz está también atribuida, a título primario, al servicio de radiodifusión.
209	5.233	Atribución adicional: en China, la banda 174 - 184 MHz está también atribuida, a título primario, a los servicios de investigación espacial (espacio-Tierra) y de operaciones espaciales (espacio-Tierra), a reserva de obtener el acuerdo indicado en el número 9.21. Estos servicios no causarán interferencia perjudicial a las estaciones de radiodifusión existentes o previstas ni reclamarán protección frente a ellas.
210	5.234	Categoría de servicio diferente: en México, la atribución de la banda 174 - 216 MHz a los servicios fijo y móvil se hace a título primario (véase el número 5.33).
211	5.235	Atribución adicional: en Alemania, Austria, Bélgica, Dinamarca, España, Finlandia, Francia, Israel, Italia, Liechtenstein, Malta, Mónaco, Noruega, Países Bajos, Reino Unido, Suecia y Suiza, la banda 174 - 223 MHz está también atribuida, a título primario, al servicio móvil terrestre. Sin embargo, las estaciones del servicio móvil terrestre no deben causar interferencia perjudicial a las estaciones de radiodifusión existentes o previstas de los países no mencionados en la presente nota, ni solicitar protección frente a dichas estaciones.
212	5.236	No utilizado.
213	5.237	Atribución adicional: en Congo (Rep. del), Eritrea, Etiopía, Gambia, Guinea, Jamahiriya Árabe Libia, Malawi, Malí, Sierra Leona, Somalia y Chad, la banda 174-223 MHz está también atribuida, a título secundario, a los servicios fijo y móvil. (CMR-07)
214	5.238	Atribución adicional: en Bangladesh, India, Pakistán y Filipinas la banda 200 - 216 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica.
215	5.239	No utilizado.
216	5.240	Atribución adicional: en China e India la banda 216 - 223 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica y, a título secundario, al servicio de radiolocalización.
217	5.241	En la Región 2, no podrán autorizarse nuevas estaciones del servicio de radiolocalización en la banda 2 16 - 225 MHz. Las estaciones autorizadas antes del 1 de enero de 1990 podrán continuar funcionando a título secundario.
218	5.242	Atribución adicional: en Canadá, la banda 216 - 220 MHz está también atribuida, a título primario, al servicio móvil terrestre.
219	5.243	Atribución adicional: en Somalia, la banda 216 - 225 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica, a reserva de no causar interferencia perjudicial a las estaciones de radiodifusión existentes o previstas en otros países.
220	5.244	SUP-CMR-97.
221	5.245	Atribución adicional: en Japón, la banda 222 - 223 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica y, a título secundario, al servicio de radiolocalización.
222	5.246	Atribución sustitutiva : en España, Francia, Israel y Mónaco, la banda 223 - 230 MHz está atribuida a título primario a los servicios móvil terrestre y de radiodifusión (véase el número 5.33) teniendo en cuenta que al preparar los planes de frecuencias, el servicio de radiodifusión tendrá prioridad en la elección de frecuencias; también está atribuida a título secundario a los servicios fijo y móvil, salvo móvil terrestre. Sin embargo, las estaciones del servicio móvil terrestre no deben causar interferencia perjudicial a las estaciones de radiodifusión existentes o previstas en Marruecos y Argelia, ni solicitar protección frente a dichas estaciones.
223	5.247	Atribución adicional: en Arabia Saudita, Bahrein, Emiratos Árabes Unidos, Jordania, Omán, Qatar y Siria la banda 223 - 235 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica.
224	5.248	No utilizados.
225	5.249	No utilizados.
226	5.250	Atribución adicional: en China, la banda 225 - 235 MHz está también atribuida, a título secundario, al servicio de radioastronomía.
227	5.251	Atribución adicional: en Nigeria, la banda 230 - 235 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica, a reserva de obtener el acuerdo indicado en el número 9.21.
228	5.252	Atribución sustitutiva: en Botswana, Lesotho, Malawi, Mozambique, Namibia, República Sudafricana, Swazilandia, Zambia y Zimbawe, las bandas 230 - 238 MHz y 246 - 254 MHz están atribuidas, a título primario, al servicio de radiodifusión, a reserva de obtener el acuerdo indicado en el número 9.21.
229	5.253	No utilizado.
230	5.254	Las bandas 235-322 MHz y 335,4-399,9 MHz pueden utilizarse por el servicio móvil por satélite, a reserva de obtener el acuerdo indicado en el número 9.21, y a condición de que las estaciones de este servicio no produzcan interferencia perjudicial a las de otros servicios explotados o que se prevea explotar de conformidad con el Cuadro de atribución de frecuencias, salvo la atribución adicional al que se hace referencia el número 5.256A. (CMR-03)
231	5.255	Las bandas 312 - 315 MHz (Tierra-espacio) y 387 - 390 MHz (espacio-Tierra) del servicio móvil por satélite podrán también ser utilizadas por los sistemas de satélites no geoestacionarios. Esta utilización está sujeta a la coordinación a tenor del número 9.11A.
232	5.256	La frecuencia de 243 MHz se utilizará en esta banda por las estaciones o dispositivos de salvamento, así como por los equipos destinados a operaciones de salvamento. (CMR-07)
233	5.256A	Atribución adicional: en China, Federación de Rusia, Kazajstán y Ucrania, la banda 258-261 MHz está también atribuida a título primario al servicio de investigación espacial (Tierra espacio) y al servicio de operaciones espaciales (Tierra espacio). Las estaciones del servicio de investigación espacial (Tierra espacio) y del servicio de operaciones espaciales (Tierra espacio) no deben ocasionar interferencia perjudicial a los sistemas del servicio móvil y del servicio móvil por satélite que funcionen en esta banda, ni reclamar protección frente a ellos o limitar su utilización y desarrollo. Las estaciones del servicio de investigación espacial (Tierra espacio) y del servicio de operaciones espaciales (Tierra espacio) no limitarán el futuro desarrollo de sistemas del servicio fijo de otros países. (CMR 03)
234	5.257	La banda 267 - 272 MHz puede ser utilizada por cada administración, a título primario, en su propio país, para telemedida espacial, a reserva de obtener el acuerdo indicado en el número 9.21.
235	5.258	La utilización de la banda 328,6 - 335,4 MHz por el servicio de radionavegación aeronáutica está limitada a los sistemas de aterrizaje con instrumentos (radioalineación de descenso).
236	5.259	Atribución adicional: en Egipto, Israel y República Árabe Siria, la banda 328,6-335,4 MHz está también atribuida al servicio móvil a título secundario, a reserva de obtener el acuerdo indicado en el número 9.21. A fin de garantizar que no se produzca interferencia perjudicial a las estaciones del servicio de radionavegación aeronáutica, no se introducirán las estaciones del servicio móvil en la banda hasta que ya no la necesite para el servicio de radionavegación aeronáutica ninguna administración que pueda ser identificada en aplicación del procedimiento invocado en el número 9.21. (CMR-07)
237	5.260	Reconociendo que la utilización de la banda 399,9 - 400,05 MHz por los servicios fijo y móvil puede causar interferencia perjudicial al servicio de radionavegación por satélite, se insta a las administraciones a no autorizar estos usos en aplicación del número 4.4.
238	5.261	Las emisiones deben restringirse a una banda de ±25 kHz respecto de la frecuencia patrón 400,1 MHz.
239	5.262	Atribución adicional: en Arabia Saudita, Armenia, Azerbaiyán, Bahrein, Belarús, Botswana, Colombia, Costa Rica, Cuba, Egipto, Emiratos Árabes Unidos, Ecuador, Federación de Rusia, Georgia, Hungría, Irán (República Islámica del), Iraq, Israel, Jordania, Kazajstán, Kuwait, Liberia, Malasia, Moldova, Uzbekistán, Pakistán, Filipinas, Qatar, República Árabe Siria, Kirguistán, Rumania, Singapur, Somalia, Tayikistán, Turkmenistán y Ucrania, la banda 400,05-401 MHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR- 07)
240	5.263	La banda 400,15 - 401 MHz está también atribuida al servicio de investigación espacial en sentido espacio-espacio para las comunicaciones con vehículos espaciales tripulados. En esta aplicación el servicio de investigación espacial no se considerará un servicio de seguridad.
241	5.264	La utilización de la banda 400,15 - 401 MHz por el servicio móvil por satélite está sujeta a la coordinación a tenor del número 9.11A. El límite de densidad de flujo de potencia indicado en el Anexo 1 del Apéndice 5 se aplicará hasta su revisión por una conferencia mundial de radiocomunicaciones competente.
242	5.265	No utilizado.
243	5.266	El uso de la banda 406-406,1 MHz por el servicio móvil por satélite está limitado a las estaciones de radiobalizas de localización de siniestros por satélite de poca potencia (véase también el Artículo 31). (CMR-07)
244	5.267	Se prohíbe cualquier emisión que pueda causar interferencia perjudicial a las utilizaciones autorizadas de la banda 406 - 406,1 MHz.
245	5.268	La utilización de la banda 410 - 420 MHz por el servicio de investigación espacial está limitada a las comunicaciones en un radio de 5 km a partir de un vehículo espacial tripulado en órbita. La densidad de flujo de potencia sobre la superficie de la Tierra producida por emisiones 2 de actividades fuera del vehículo espacial no excederán de – 153 dB(W/m ) para 0°≤Φ≤5°, - 2 2 153 + 0,077 (Φ-5) dB(W/m ) para 5°≤Φ≤70°y – 148 dB(W/m ) para 70°≤Φ≤90°, siendo Φ el ángulo de incidencia de la onda de radiofrecuencia y 4 kHz la anchura de banda de referencia. El número 4.10 no se aplica a las actividades fuera del vehículo espacial. En esta banda de frecuencias el servicio de investigación espacial (espacio-espacio) no reclamará protección contra estaciones de los servicios fijo y móvil, ni limitará su utilización ni su desarrollo. (CMR-97)
246	5.269	Categoría de servicio diferente: en Australia, Estados Unidos, India, Japón y Reino Unido, la atribución de las bandas 420 - 430 MHz y 440 - 450 MHz al servicio de radiolocalización es a título primario (véase el número 5.33).
247	5.270	Atribución adicional: en Australia, Estados Unidos, Jamaica y Filipinas, las bandas 420 - 430 MHz y 440 - 450 MHz están también atribuidas, a título secundario, al servicio de aficionados.
248	5.271	Atribución adicional: en Belarús, China, India, Kirguistán y Turkmenistán, la banda 420-460 MHz está también atribuida, a título secundario, al servicio de radionavegación aeronáutica (radioaltímetros). (CMR-07)
249	5.272	Categoría de servicio diferente: en Francia, la atribución de la banda 430 - 434 MHz al servicio de aficionados es a título secundario (véase el número 5.32 ).
250	5.273	Categoría de servicio diferente: en Jamahiriya Árabe Libia, la atribución de las bandas 430-432 MHz y 438-440 MHz al servicio de radiolocalización es a título secundario (véase el número 5.32). (CMR-03)
251	5.274	Atribución sustitutiva: en Dinamarca, Noruega y Suecia, las bandas 430 - 432 MHz y 438 - 440 MHz están atribuidas, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico.
252	5.275	Atribución adicional: en Croacia, Estonia, Finlandia, Jamahiriya Árabe Libia, la ex República Yugoslava de Macedonia, Montenegro, Serbia y Eslovenia, las bandas 430-432 MHz y 438-440 MHz están también atribuidas, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
272	5.288	En las aguas territoriales de Estados Unidos y Filipinas, las estaciones de comunicaciones a bordo utilizarán de preferencia las frecuencias de 457,525 MHz, 457,550 MHz, 457,575 MHz y 457,600 MHz. Estas frecuencias están asociadas por pares respectivamente con las frecuencias de 467,750 MHz, 467,775 MHz, 467,800 MHz y 467,825 MHz. Las características de los equipos utilizados deberán satisfacer lo dispuesto en la Recomendación UIT-R M.1174-2. (CMR-03)
253	5.276	Atribución adicional: en Afganistán, Argelia, Arabia Saudita, Bahrein, Bangladesh, Brunei Darussalam, Burkina Faso, Burundi, Egipto, Emiratos Árabes Unidos, Ecuador, Eritrea, Etiopía, Grecia, Guinea, India, Indonesia, Irán (República Islámica del), Iraq, Israel, Italia, Jamahiriya Árabe Libia, Jordania, Kenya, Kuwait, Líbano, Malasia, Malta, Nigeria, Omán, Pakistán, Filipinas, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Singapur, Somalia, Suiza, Tanzanía, Tailandia, Togo, Turquía y Yemen, la banda 430-440 MHz está también atribuida, a título primario, al servicio fijo y las bandas 430-435 MHz y 438-440 MHz están también atribuidas, a título primario, al servicio móvil, salvo móvil aeronáutico. (CMR-07)
254	5.277	Atribución adicional: en Angola, Armenia, Azerbaiyán, Belarús, Camerún, Congo (Rep. del), Djibouti, Federación de Rusia, Georgia, Hungría, Israel, Kazajstán, Malí, Moldova, Mongolia, Uzbekistán, Polonia, Kirguistán, Eslovaquia, Rumania, Rwanda, Tayikistán, Chad, Turkmenistán y Ucrania, la banda 430-440 MHz está también atribuida, a título primario, al servicio fijo. (CMR-07)
255	5.278	Categoría de servicio diferente: en Argentina, Colombia, Costa Rica, Cuba, Guyana, Honduras, Panamá y Venezuela, la atribución de la banda 430 - 440 MHz al servicio de aficionados es a título primario (véase el número 5.33).
256	5.279	Atribución adicional: en México las bandas 430 - 435 MHz y 438 - 440 MHz están también atribuidas, a título primario, al servicio móvil terrestre, a reserva de obtener el acuerdo indicado en el número 9.21.
257	5.279A	La utilización de esta banda por sensores del servicio de exploración de la Tierra por satélite (activo) será conforme con la Recomendación UIT-R RS.1260-1. Además, el servicio de exploración de la Tierra por satélite (activo) en la banda 432-438 MHz no causará interferencia perjudicial al servicio de radionavegación aeronáutica en China. Las disposiciones de esta nota no derogan de ningún modo la obligación del servicio de exploración de la Tierra por satélite (activo) de funcionar en calidad de servicio secundario de conformidad con los números 5.29 y 5.30. (CMR 03)
258	5.280	En Alemania, Austria, Bosnia y Herzegovina, Croacia, la ex República Yugoslava de Macedonia, Liechtenstein, Montenegro, Portugal, Serbia, Eslovenia y Suiza, la banda 433,05- 434,79 MHz (frecuencia central 433,92 MHz) está designada para aplicaciones industriales, científicas y médicas (ICM). Los servicios de radiocomunicación de estos países que funcionan en esta banda deben aceptar la interferencia perjudicial resultante de estas aplicaciones. Los equipos ICM que funcionen en esta banda estarán sujetos a las disposiciones del número 15.13. (CMR 07)
259	5.281	Atribución adicional: en los Departamentos franceses de Ultramar de la Región 2, y en India, la banda 433,75 - 434,25 MHz está también atribuida, a título primario, al servicio de operaciones espaciales (Tierra-espacio). En Francia y en Brasil esta banda se encuentra atribuida, a título secundario, al mismo servicio.
260	5.282	El servicio de aficionados por satélite podrá explotarse en las bandas 435 - 438 MHz, 1 260 - 1 270 MHz, 2 400 - 2 450 MHz, 3 400 - 3 410 MHz (en las Regiones 2 y 3 solamente), y 5 650 - 5 670 MHz, siempre que no cause interferencia perjudicial a otros servicios explotados de conformidad con el cuadro (véase el número 5.43). Las administraciones que autoricen tal utilización se asegurarán que toda interferencia perjudicial causada por emisiones de una estación del servicio de aficionados por satélite sea inmediatamente eliminada, en cumplimiento de lo dispuesto en el número 25.11. La utilización de las bandas 1 260 - 1 270 MHz y 5 650 - 5 670 MHz por el servicio de aficionados por satélite se limitará al sentido Tierra-espacio.
261	5.283	Atribución adicional: en Austria, la banda 438 - 440 MHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico.
262	5.284	Atribución adicional: en Canadá, la banda 440 - 450 MHz está también atribuida, a título secundario, al servicio de aficionados.
263	5.285	Categoría de servicio diferente: en Canadá, la atribución de la banda 440 - 450 MHz al servicio de radiolocalización es a título primario (véase el número 5.33).
264	5.286	La banda 449,75 - 450,25 MHz puede utilizarse por el servicio de operaciones espaciales (Tierra-espacio) y el servicio de investigación espacial (Tierra-espacio), a reserva de obtener el acuerdo indicado en el número 9.21.
265	5.286A	La utilización de las bandas 454 - 456 MHz y 459 - 460 MHz por el servicio móvil por satélite está sujeta a la coordinación a tenor del número 9.11A. (CMR-97)
266	5.286AA	La banda 450-470 MHz se ha identificado para su utilización por las administraciones que deseen introducir las Telecomunicaciones Móviles Internacionales (IMT). Véase la Resolución 224 (Rev.CMR 07). Dicha identificación no excluye el uso de esta banda por ninguna aplicación de los servicios a los cuales está atribuida y no implica prioridad alguna en el Reglamento de Radiocomunicaciones. (CMR-07)
267	5.286B	La utilización de las bandas 454 – 455 MHz en los países enumerados en 5.286D, 455 - 456 MHz y 459 - 460 MHz en la Región 2, y 454 – 456 MHz y 459 – 460 MHz en los países enumerados en 5.286E, por las estaciones del servicio móvil por satélite no causarán interferencia perjudicial a las estaciones de los servicios fijo y móvil ni permitirá reclamar protección con respecto a dichas estaciones que funcionan de acuerdo con el cuadro de atribución de bandas de frecuencias. (CMR-97)
268	5.286C	La utilización de las bandas 454 – 455 MHz en los países enumerados en 5.286D, 455 - 456 MHz y 459 - 460 MHz en la Región 2, y 454 – 456 MHz y 459 – 460 MHz en los países enumerados en 5.286E, por las estaciones del servicio móvil por satélite no restringirá el desarrollo y utilización de los servicios fijo y móvil que funcionan de acuerdo con el cuadro de atribución de bandas de frecuencias. (CMR-97)
269	5.286D	Atribución adicional: Canadá, Estados Unidos y Panamá, la banda 454-455 MHz está también atribuida al servicio móvil por satélite (Tierra-espacio) a título primario. (CMR-07)
270	5.286E	Atribución adicional: en Cabo Verde, Nepal y Nigeria las bandas 454-456 MHz y 459- 460 MHz están también atribuidas al servicio móvil por satélite (Tierra-espacio) a título primario. (CMR-07)
271	5.287	En el servicio móvil marítimo, las frecuencias de 457,525 MHz, 457,550 MHz, 457,575 MHz, 467,525 MHz, 467,550 MHz y 467,575 MHz pueden ser utilizadas por las estaciones de comunicaciones a bordo. Cuando sea necesario, pueden introducirse para las comunicaciones a bordo los equipos diseñados para una separación de canales de 12,5 kHz que empleen también las frecuencias adicionales de 457,5375 MHz, 457,5625 MHz, 467,5375 MHz y 467,5625 MHz. Su empleo en aguas territoriales puede estar sometido a reglamentación nacional de la administración interesada. Las características de los equipos utilizados deberán satisfacer lo dispuesto en la Recomendación UIT-R M.1174-2. (CMR-07)
273	5.289	Las bandas 460 - 470 MHz y 1 690 - 1 710 MHz pueden también ser utilizadas para las aplicaciones del servicio de exploración de la Tierra por satélite distintas de las del servicio de meteorología por satélite, para las transmisiones espacio-Tierra, a reserva de no causar interferencia perjudicial a las estaciones que funcionan de conformidad con el cuadro.
274	5.290	Categoría de servicio diferente: en Afganistán, Azerbaiyán, Belarús, China, Federación de Rusia, Japón, Mongolia, Kirguistán, Eslovaquia, Tayikistán, Turkmenistán y Ucrania, la atribución de la banda 460-470 MHz al servicio de meteorología por satélite (espacio-Tierra) es a título primario (véase el número 5.33), a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-07)
275	5.291	Atribución adicional: en China, la banda 470 - 485 MHz está también atribuida, a título primario, a los servicios de investigación espacial (espacio-Tierra) y de operaciones espaciales (espacio-Tierra) a reserva de obtener el acuerdo indicado en el número 9.21 y de no causar interferencia perjudicial a las estaciones de radiodifusión existentes o previstas.
276	5.291A	Atribución adicional: en Alemania, Austria, Dinamarca, Estonia, Finlandia, Liechtenstein, Noruega, Países Bajos, República Checa y Suiza, la banda 470 - 494 MHz también está atribuida al servicio de radiolocalización a título secundario. Dicha utilización se limita a las operaciones de radares de perfil de viento, de conformidad con la Resolución 217 (CMR-97). (CMR-97)
277	5.292	Categoría de servicio diferente: en México la atribución de la banda 470-512 MHz a los servicios fijo y móvil y, en Argentina, Uruguay y Venezuela, al servicio móvil es a título primario (véase el número 5.33), a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-07)
278	5.293	Categoría de servicio diferente: en Canadá, Chile, Colombia, Cuba, Estados Unidos, Guyana, Honduras, Jamaica, México, Panamá y Perú, la atribución de las bandas 470-512 MHz y 614-806 MHz al servicio fijo es a título primario (véase el número 5.33), a reserva de obtener el acuerdo indicado en el número 9.21. En Canadá, Chile, Colombia, Cuba, Estados Unidos, Guyana, Honduras, Jamaica, México, Panamá y Perú, la atribución de las bandas 470-512 MHz y 614-698 MHz al servicio móvil es a título primario (véase el número 5.33), sujeto al acuerdo obtenido con arreglo al número 9.21. En Argentina y Ecuador, la banda 470-512 MHz está atribuida a título primario a los servicios fijo y móvil (véase el número 5.33), sujeto a la obtención de un acuerdo con arreglo al número 9.21. (CMR-07)
279	5.294	Atribución adicional: en Arabia Saudita, Burundi, Camerún, Côte d'Ivoire, Egipto, Etiopía, Israel, Jamahiriya Árabe Libia, Kenya, Malawi, República Árabe Siria, Sudán, Chad y Yemen, la banda 470-582 MHz está también atribuida, a título secundario, al servicio fijo.
280	5.295	No utilizado.
281	5.296	Atribución adicional: en Alemania, Arabia Saudita, Austria, Bélgica, Côte d'Ivoire, Dinamarca, Egipto, España, Finlandia, Francia, Irlanda, Israel, Italia, Jamahiriya Árabe Libia, Jordania, Lituania, Malta, Marruecos, Mónaco, Noruega, Omán, Países Bajos, Portugal, República Árabe Siria, Reino Unido, Suecia, Suiza, Swazilandia y Túnez, la banda 470-790 MHz está también atribuida, a título secundario, al servicio móvil terrestre para aplicaciones auxiliares de radiodifusión. Las estaciones del servicio móvil terrestre de los países enumerados en la presente nota no causarán interferencia perjudicial a las estaciones existentes o previstas que operen con arreglo a lo dispuesto en el Cuadro en países distintos de los indicados en la presente nota. (CMR-07)
282	5.297	Atribución adicional: en Canadá, Costa Rica, Cuba, El Salvador, Estados Unidos, Guatemala, Guyana, Honduras, Jamaica y México, la banda 512-608 MHz está también atribuida, a título primario, a los servicios fijo y móvil, a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-07)
283	5.298	Atribución adicional: en India, la banda 549,75 - 550,25 MHz está también atribuida, a título secundario, al servicio de operaciones espaciales (espacio-Tierra).
284	5.299	No utilizado.
285	5.300	Atribución adicional: en Arabia Saudita, Egipto, Israel, Jamahiriya Árabe Libia, Jordania, Omán, República Árabe Siria y Sudán, la banda 582-790 MHz está también atribuida, a título secundario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
286	5.301	No utilizado.
287	5.302	Atribución adicional: en el Reino Unido, la banda 590 - 598 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica. Todas las nuevas asignaciones a las estaciones del servicio de radionavegación aeronáutica, incluidas las transferidas desde bandas adyacentes, estarán sujetas a coordinación con las Administraciones de los siguientes países: Alemania, Bélgica, Dinamarca, España, Francia, Irlanda, Luxemburgo, Marruecos, Noruega y Países Bajos.
288	5.303	No utilizado.
289	5.304	Atribución adicional: en la Zona Africana de radiodifusión (véanse los número5.10 a 5.13), la banda 606 - 614 MHz está también atribuida, a título primario, al servicio de radioastronomía.
290	5.305	Atribución adicional: en China, la banda 606 - 614 MHz está también atribuida, a título primario, al servicio de radioastronomía.
291	5.306	Atribución adicional : en la Región 1, salvo en la Zona Africana de radiodifusión (véanse los número5.10 a 5.13), y en la Región 3, la banda 608 - 614 MHz está también atribuida, a título secundario, al servicio de radioastronomía.
292	5.307	Atribución adicional: en India la banda 608 - 614 MHz está también atribuida, a título primario, al servicio de radioastronomía.
293	5.308	No utilizado.
294	5.309	Categoría de servicio diferente: en Costa Rica, El Salvador y Honduras, la atribución de la banda 614 - 806 MHz al servicio Fijo es a título primario (véase el número 5.33), a reserva de obtener el acuerdo indicado en el número 9.21.
295	5.310	SUP-CMR-97.
296	5.311	SUP – CMR 2007.
297	5.311A	Para la banda de frecuencias 620-790 MHz, véase asimismo la Resolución 549 (CMR- 07). (CMR-07)
298	5.312	Atribución adicional: en Armenia, Azerbaiyán, Belarús, Bulgaria, Georgia, Hungría, Kazajstán, Moldova, Mongolia, Uzbekistán, Polonia, Kirguistán, Eslovaquia, Rep. Checa, Rumania, Federación de Rusia, Tayikistán, Turkmenistán y Ucrania, la banda 645-862 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica. (CMR-03)
299	5.313	SUP-CMR-97.
332	5.335A	En la banda 1 260-1 300 MHz los sensores activos a bordo de vehículos espaciales de los servicios de exploración de la Tierra por satélite y de investigación espacial no deberán causar interferencias perjudiciales ni imponer limitaciones al funcionamiento o al desarrollo del servicio de radiolocalización y otros servicios que cuentan con atribuciones a título primario, mediante notas, ni reclamarán protección con relación a los mismos. (CMR 2000)
333	5.336	No utilizado.
300	5.313A	En Bangladesh, China, Corea (Rep. de), India, Japón, Nueva Zelandia, Papua Nueva Guinea, Filipinas y Singapur, la banda 698-790 MHz, o partes de ella, se ha identificado para su utilización por las administraciones que deseen aplicar Telecomunicaciones Móviles Internacionales (IMT). Esta identificación no impide la utilización de esta banda por cualquier aplicación de otros servicios a los que está atribuida ni establece prioridad alguna en el Reglamento de Radiocomunicaciones. En China, el uso de las IMT en esta banda no comenzará hasta 2015. (CMR 07)
301	5.313B	Categoría de servicio diferente: en Brasil, la atribución de la banda 698 - 806 MHz al servicio móvil es a título secundario (véase el número 5.32). (CMR 07)
302	5.314	Atribución adicional: en Austria, Italia, Moldova, Uzbekistán, Kirguistán, el Reino Unido y Swazilandia, la banda 790-862 MHz está también atribuida, a título secundario, al servicio móvil terrestre. (CMR 07)
303	5.315	Atribución sustitutiva: en Grecia, Italia y Túnez, la banda 790-838 MHz está atribuida, a título primario, al servicio de radiodifusión. (CMR 07)
304	5.316	Atribución adicional: en Alemania, Arabia Saudita, Bosnia y Herzegovina, Burkina Faso, Camerún, Côte d'Ivoire, Croacia, Dinamarca, Egipto, Finlandia, Grecia, Israel, Jamahiriya Árabe Libia, Jordania, Kenya, la ex República Yugoslava de Macedonia, Liechtenstein, Malí, Mónaco, Montenegro, Noruega, Países Bajos, Portugal, Reino Unido, República Árabe Siria, Serbia, Suecia y Suiza, la banda 790-830 MHz, y en estos mismos países y en España, Francia, Gabón y Malta, la banda 830-862 MHz, están también atribuidas, a título primario, al servicio móvil, salvo móvil aeronáutico. Sin embargo, las estaciones del servicio móvil de los países mencionados para cada una de las bandas que figuran en la presente nota no deben causar interferencia perjudicial a las estaciones de los servicios que funcionan de conformidad con el Cuadro en países distintos de los mencionados para cada una de estas bandas en esta nota, ni reclamar protección frente a ellas. Esta atribución es efectiva hasta el 16 de junio de 2015. (CMR 07)
305	5.316A	Atribución adicional: en España, Francia, Gabón y Malta, la banda 790-830 MHz, en Angola, Bahrein, Benin, Botswana, Congo (República del), Departamentos y colectividades franceses de Ultramar de la Región 1, Gambia, Ghana, Guinea, Kuwait, Lesotho, Líbano, Malawi, Malí, Marruecos, Mauritania, Mozambique, Namibia, Níger, Omán, Uganda, Polonia, Qatar, Rwanda, Senegal, Sudán, Sudafricana (Rep.), Swazilandia, Tanzanía, Chad, Togo, Yemen, Zambia y Zimbabwe la banda 790-862 MHz, en Georgia la banda 806 862 MHz y en Lituania la banda 830-862 MHz, están también atribuidas al servicio móvil, salvo el móvil aeronáutico, a título primario sujeto al acuerdo por las administraciones obtenido con arreglo al número 9.21 y al Acuerdo GE06, según el caso, incluidas las administraciones mencionadas en el número 5.312, cuando corresponda. Sin embargo, las estaciones del servicio móvil de los países mencionados en relación con cada una de las bandas referidas en esta nota no deberán causar interferencia inaceptable a las estaciones de los servicios que funcionan de conformidad con el Cuadro en países distintos de los mencionados en relación con la banda, ni reclamar protección contra las mismas. Las asignaciones de frecuencias al servicio móvil dentro de esta atribución en Lituania y Polonia no se utilizarán antes de haber obtenido el acuerdo de la Federación de Rusia y de Belarús. Esta atribución es efectiva hasta el 16 de junio de 2015. (CMR 07)
306	5.316B	En la Región 1, la atribución al servicio móvil, salvo móvil aeronáutico, a título primario en la banda de frecuencias 790-862 MHz entrará en vigor a partir del 17 de junio de 2015 y estará sujeta a la obtención del acuerdo obtenido con arreglo al número 9.21 con respecto al servicio de navegación aeronáutica en países mencionados en el número 5.312. En los países signatarios del Acuerdo GE06, la utilización de estaciones del servicio móvil también está sujeta a la aplicación satisfactoria de los procedimientos de dicho Acuerdo. Deberán aplicarse las Resoluciones 224 (Rev.CMR 07) y 749 (CMR 07). (CMR 07)
307	5.317	Atribución adicional: en la Región 2 (excepto Brasil y Estados Unidos), la banda 806 - 890 MHz está también atribuida, a título primario, al servicio móvil por satélite, a reserva de obtener el acuerdo indicado en el número 9.21. Este servicio está destinado para su utilización dentro de las fronteras nacionales.
308	5.317A	Las partes de la banda 698-960 MHz en la Región 2 y de la banda 790-960 MHz en las Regiones 1 y 3 atribuidas al servicio móvil a título primario se han identificado para su utilización por las administraciones que deseen introducir las Telecomunicaciones Móviles Internacionales (IMT) (Véase las Resoluciones 224 (Rev.CMR07)) y 749 (CMR-07). La identificación de estas bandas no excluye que se utilicen para otras aplicaciones de los servicios a los que están atribuidas y no implica prioridad alguna en el Reglamento de Radiocomunicaciones. (CMR 07)
309	5.318	Atribución adicional: en Canadá, Estados Unidos y México, las bandas 849 - 851 MHz y 894 - 896 MHz están además atribuidas al servicio móvil aeronáutico a título primario para la correspondencia pública con aeronaves. La utilización de la banda 849 - 851 MHz se limita a las transmisiones desde estaciones aeronáuticas y la utilización de la banda 894 - 896 MHz se limita a las transmisiones desde estaciones de aeronave.
310	5.319	Atribución adicional: en Belarús, Rusia y Ucrania, las bandas 806 - 840 MHz (Tierra- espacio) y 856 - 890 MHz (espacio-Tierra) están también atribuidas al servicio móvil por satélite, salvo móvil aeronáutico (R) por satélite. La utilización de estas bandas por este servicio no causará interferencia perjudicial a los servicios de otros países que funcionen conforme al cuadro de atribución de bandas de frecuencias ni implica la exigencia de protección frente a ellos, y está sujeta a acuerdos especiales entre las administraciones interesadas.
311	5.320	Atribución adicional: en la Región 3, las bandas 806 - 890 MHz y 942 - 960 MHz están también atribuidas, a título primario, al servicio móvil por satélite, salvo móvil aeronáutico por satélite (R), a reserva de obtener el acuerdo indicado en el número 9.21. La explotación de este servicio está limitada al interior de las fronteras nacionales. En la búsqueda de dicho acuerdo, se dará protección adecuada a los servicios explotados de conformidad con el presente cuadro para asegurar que no se causa interferencia perjudicial a los mismos.
312	5.321	SUP – CMR 2007.
313	5.322	En la Región 1, en la banda 862-960 MHz, las estaciones del servicio de radiodifusión serán explotadas solamente en la Zona Africana de radiodifusión (véanse los número5.10 a 5.13), con exclusión de Argelia, Egipto, España, Libia, Marruecos, Namibia, Nigeria, República Sudafricana, Tanzanía, Zimbabwe y Zambia, a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-00)
334	5.337	El empleo de las bandas 1 300 - 1 350 MHz, 2 700 - 2 900 MHz y 9 000 - 9 200 MHz por el servicio de radionavegación aeronáutica está limitado a los radares terrestres y a los respondedores aeroportados asociados que emitan sólo en frecuencias de estas bandas y, únicamente, cuando sean accionados por los radares que funcionen en la misma banda.
314	5.323	Atribución adicional : en Armenia, Azerbaiyán, Belarús, Bulgaria, Federación de Rusia, Hungría, Kazajstán, Moldova, Uzbekistán, Polonia, Kirguistán, Rumania, Tayikistán, Turkmenistán y Ucrania, la banda 862-960 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica. Esta utilización está sujeta al acuerdo obtenido en virtud del número 9.21 con las administraciones pertinentes y está limitada a las radiobalizas en tierra que se encuentren en servicio el 27 de octubre de 1997, hasta el final de su vida útil. (CMR-07)
315	5.324	No utilizado.
316	5.325	Categoría de servicio diferente: en Estados Unidos, la atribución de la banda 890 - 942 MHz al servicio de radiolocalización es a título primario, a reserva de obtener el acuerdo indicado en el número 9.21 (véase el número 5.33).
317	5.325A	Categoría de servicio diferente: en Cuba, la banda 902-915 MHz está atribuida a título primario al servicio móvil terrestre. (CMR-00)
318	5.326	Categoría de servicio diferente: en Chile, la atribución de la banda 903 - 905 MHz al servicio móvil, salvo móvil aeronáutico, es a título primario, a reserva de obtener el acuerdo indicado en el número 9.21.
319	5.327	Categoría de servicio diferente: en Australia, la atribución de la banda 915 - 928 MHz al servicio de radiolocalización es a título primario (véase el número 5.33).
320	5.327A	La utilización de la banda 960-1 164 MHz por el servicio móvil aeronáutico (R) se limita a los sistemas que funcionan en conformidad con las normas aeronáuticas internacionales reconocidas. Dicha utilización deberá ser conforme con la Resolución 417 (CMR-07). (CMR-07)
321	5.328	La utilización de la banda 960-1 215 MHz por el servicio de radionavegación aeronáutica se reserva en todo el mundo para la explotación y el desarrollo de equipos electrónicos de ayudas a la navegación aérea instalados a bordo de aeronaves y de las instalaciones con base en tierra directamente asociadas. (CMR-00)
322	5.328A	Las estaciones del servicio de radionavegación por satélite en la banda 1 164-1 215 MHz funcionarán de conformidad con las disposiciones de la Resolución 609 (Rev.CMR-07) y no reclamarán protección con relación a las estaciones del servicio de radionavegación aeronáutica en la banda 960-1 215 MHz. No se aplican las disposiciones del número 5.43A. Se aplicarán las disposiciones del número 21.18. (CMR-07)
323	5.328B	La utilización de las bandas 1 164-1 300 MHz, 1 559-1 610 MHz y 5 010-5 030 MHz por los sistemas y redes del servicio de radionavegación por satélite sobre los cuales la Oficina de Radiocomunicaciones haya recibido la información de coordinación o notificación completa, según el caso, después del 1 de enero de 2005 está sujeta a las disposiciones de los números 9.12, 9.12A y 9.13. Se aplicará igualmente la Resolución 610 (CMR-03).Ahora bien, en el caso de las redes y sistemas del servicio de radionavegación por satélite (espacio-espacio), esta Resolución sólo se aplicará a las estaciones espaciales transmisoras. De conformidad con el número 5.329A, para los sistemas y redes del servicio de radionavegación por satélite (espacio- espacio) en las bandas 1 215-1 300 MHz y 1 559-1 610 MHz, las disposiciones de los números 9.7, 9.12, 9.12A y 9.13 sólo se aplicarán con respecto a los otros sistemas y redes del servicio de radionavegación por satélite (espacio-espacio). (CMR-07)
324	5.329	La utilización por el servicio de radionavegación por satélite de la banda 1 215-1 300 MHz estará sujeta a la condición de no causar interferencias perjudiciales al servicio de radionavegación, autorizado en el número 5.331 ni reclamar protección con respecto a los mismos. Además, la utilización del servicio de radionavegación por satélite en la banda 1 215-1 300 MHz estará sujeta a la condición de no causar interferencia perjudicial al servicio de radiolocalización. No se aplica el número 5.43 en relación con el servicio de radiolocalización. Se aplicará la Resolución 608 (CMR-03). (CMR-03)
325	5.329A	La utilización de sistemas del servicio de radionavegación por satélite (espacio-espacio) que funcionan en las bandas 1 215-1 300 MHz y 1 559-1 610 MHz no está prevista para aplicaciones de los servicios de seguridad, y no deberá imponer limitaciones adicionales a los sistemas del servicio de radionavegación por satélite (espacio-Tierra) o a otros servicios que funcionen con arreglo al Cuadro de atribución de bandas de frecuencias. (CMR-07)
326	5.330	Atribución adicional: en Angola, Arabia Saudita, Bahrein, Bangladesh, Camerún, China, Emiratos Árabes Unidos, Eritrea, Etiopía, Guyana, India, Indonesia, Irán (Rep. Islámica del), Iraq, Israel, Japón, Jordania, Kuwait, Líbano, Jamahiriya Árabe Libia, Mozambique, Nepal, Pakistán, Filipinas, Qatar, República Árabe Siria, Somalia, Sudán, Chad, Togo y Yemen, la banda 1 215-1 300 MHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-03)
327	5.331	Atribución adicional: en Argelia, Alemania, Arabia Saudita, Australia, Austria, Bahrein, Belarús, Bélgica, Benin, Bosnia y Herzegovina, Brasil, Burkina Faso, Burundi, Camerún, China, Corea (Rep. de), Croacia, Dinamarca, Egipto, Emiratos Árabes Unidos, Estonia, Federación de Rusia, Finlandia, Francia, Ghana, Grecia, Guinea, Guinea Ecuatorial, Hungría, India, Indonesia, Irán (República Islámica del), Iraq, Irlanda, Israel, Jordania, Kenya, Kuwait, la ex República Yugoslava de Macedonia, Lesotho, Letonia, Líbano, Liechtenstein, Lituania, Luxemburgo, Madagascar, Malí, Mauritania, Montenegro, Nigeria, Noruega, Omán, Países Bajos, Polonia, Portugal, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Eslovaquia, Reino Unido, Serbia, Eslovenia, Somalia, Sudán, Sri Lanka, Sudafricana (Rep.), Suecia, Suiza, Tailandia, Togo, Turquía, Venezuela y Viet Nam, la banda 1 215-1 300 MHz está también atribuida, a título primario, al servicio de radionavegación. En Canadá y Estados Unidos, la banda 1 240-1 300 MHz está también atribuida al servicio de radionavegación, y la utilización del servicio de radionavegación está limitada al servicio de radionavegación aeronáutica. (CMR-07)
328	5.332	En la banda 1 215-1 260 MHz los sensores activos a bordo de vehículos espaciales de los servicios de exploración de la Tierra por satélite y de investigación espacial no causarán interferencia perjudicial o impondrán limitaciones al funcionamiento o al desarrollo del servicio de radiolocalización, el servicio de radionavegación por satélite y otros servicios que cuentan con atribuciones a título primario, ni reclamarán protección contra éstos. (CMR-00)
329	5.333	SUP-CMR-97
330	5.334	Atribución adicional: en Canadá y en Estados Unidos, la banda 1 350-1 370 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica. (CMR-03)
331	5.335	En Canadá y Estados Unidos, en la banda 1 240 - 1 300 MHz, los sensores activos a bordo de vehículos espaciales de los servicios de exploración de la Tierra por satélite y de investigación espacial no causarán interferencia o impondrán limitaciones a la explotación o al desarrollo del servicio de radionavegación aeronáutica ni reclamarán protección contra el. (CMR-97)
335	5.337A	El empleo de la banda 1 300-1 350 MHz por las estaciones terrenas del servicio de radionavegación por satélite y las estaciones del servicio de radiolocalización no deberá ocasionar interferencias perjudiciales ni limitar el funcionamiento y desarrollo del servicio de radionavegación aeronáutica. (CMR-00)
336	5.338	En Mongolia, Kirguistán, Eslovaquia, Rep. Checa y Turkmenistán, las instalaciones existentes del servicio de radionavegación pueden continuar funcionando en la banda 1 350-1 400 MHz. (CMR-07)
337	5.338A	En las bandas 1 350-1 400 MHz, 1 427-1 452 MHz, 22,55 23,55 GHz, 30-31,3 GHz, 49,7-50,2 GHz, 50,4 50,9 GHz y 51,4 52,6 GHz, se aplica la Resolución 750 (CMR-07). (CMR 07)
338	5.339	Las bandas 1 370 - 1 400 MHz, 2 640 - 2 655 MHz, 4 950 - 4 990 MHz y 15,20 - 15,35 GHz están también atribuidas, a título secundario, a los servicios de investigación espacial (pasivo) y de exploración de la Tierra por satélite (pasivo).
339	5.339A	SUP-CMR-07
340	5.340	Se prohiben todas las emisiones en las siguientes bandas: 1 400-1 427 MHz, 2 690-2 700 MHz, excepto las indicadas en el número 5.422, 10,68-10,7 GHz, excepto las indicadas en el número 5.483, 15,35-15,4 GHz, excepto las indicadas en el número 5.511, 23,6-24 GHz, 31,3-31,5 GHz, 31,5-31,8 GHz, en la Región 2, 48,94-49,04 GHz, por estaciones a bordo de aeronaves, 50,2-50,4 GHz, 52,6- 54,25 GHz, 86-92 GHz, 100-102 GHz, 109,5-111,8 GHz, 114,25-116 GHz, 148,5-151,5 GHz, 164-167 GHz, 182-185 GHz, 190-191,8 GHz, 200-209 GHz, 226-231,5 GHz, 250-252 GHz.
341	5.341	En las bandas 1 400 - 1 727 MHz, 101 - 120 GHz y 197 - 220 GHz, ciertos países realizan operaciones de investigación pasiva en el marco de un programa de búsqueda de emisiones intencionales de origen extraterrestre.
342	5.342	Atribución adicional: en Armenia, Azerbaiyán, Belarús, Bulgaria, Uzbekistán, Kirguistán, Federación de Rusia y Ucrania, la banda 1 429-1 535 MHz está atribuida también a título primario al servicio móvil aeronáutico, exclusivamente a fines de telemedida aeronáutica dentro del territorio nacional. Desde el 1 de abril de 2007 la utilización de la banda 1 452-1 492 MHz estará sujeta a un acuerdo entre las administraciones implicadas. (CMR-00)
343	5.343	En la Región 2, la utilización de la banda 1 435 - 1 535 MHz por el servicio móvil aeronáutico para la telemedida aeronáutica tiene prioridad sobre otros usos por el servicio móvil.
344	5.344	Atribución sustitutiva: en Estados Unidos, la banda 1 452 - 1 525 MHz está atribuida a los servicios fijo y móvil a título primario (véase también el número 5.343).
345	5.345	La utilización de la banda 1 452 - 1 492 MHz por el servicio de radiodifusión por satélite y por el servicio de radiodifusión está limitada a la radiodifusión Sonora digital y sujeta a las disposiciones de la Resolución 528 (CAMR-92).
346	5.346	No utilizado.
347	5.347	SUP – CMR 2007.
348	5.348	La utilización de la banda 1 518-1 525 MHz por el servicio móvil por satélite está sujeta a la coordinación a tenor del número 9.11A. Las estaciones del servicio móvil por satélite en la banda de 1 518-1 525 MHz no pueden reclamar protección contra las estaciones del servicio fijo. No se aplica el número 5.43A. (CMR-03)
349	5.348A	En la banda 1 518-1 525 MHz, los umbrales de coordinación en términos de niveles de densidad de flujo de potencia en la superficie de la Tierra en aplicación del número 9.11A para las estaciones espaciales del servicio móvil por satélite (espacio-Tierra) con respecto al servicio móvil terrestre utilizado para radiocomunicaciones móviles especializadas o juntamente con redes de telecomunicaciones públicas conmutadas (RTPC) explotadas dentro del territorio de Japón serán de – 150 dB(W/m^2) en cualquier banda de 4 kHz para todos los ángulos de llegada, en lugar de los umbrales indicados en el Cuadro 5-2 del Apéndice 5. En la banda 1 518-1 525 MHz las estaciones del servicio móvil por satélite no reclamarán protección contra las estaciones del servicio móvil en el territorio de Japón. No se aplica el número 5.43A. (CMR- 03)
350	5.348B	En la banda 1 518-1 525 MHz, las estaciones del servicio móvil por satélite no reclamarán protección contra las estaciones de telemedida móvil aeronáutica del servicio móvil en el territorio de Estados Unidos (véanse los números 5.343 y 5.344) y de los países a los que se refiere el número 5.342. No se aplica el número 5.43A. (CMR 03)
351	5.348C	SUP-CMR-07
352	5.349	Categoría de servicio diferente: en Arabia Saudita, Azerbaiyán, Bahrein, Camerún, Egipto, Francia, Irán (República Islámica del), Iraq, Israel, Kazajstán, Kuwait, la ex República Yugoslava de Macedonia, Líbano, Marruecos, Qatar, República Árabe Siria, Kirguistán, Turkmenistán y Yemen, la atribución de la banda 1 525-1 530 MHz, al servicio móvil, salvo móvil aeronáutico, es a título primario (véase el número 5.33). (CMR-07)
353	5.350	Atribución adicional: en Azerbaiyán, Kirguistán y Turkmenistán, la banda 1 525-1 530 MHz está, también atribuida, a título primario, al servicio móvil aeronáutico. (CMR-00)
354	5.351	Las bandas 1 525 - 1 544 MHz, 1 545 - 1 559 MHz, 1 626,5 - 1 645,5 MHz y 1 646,5 - 1 660,5 MHz no se utilizarán para enlaces de conexión de ningún servicio. No obstante, en circunstancias excepcionales, una administración podrá autorizar a una estación terrena situada en un punto fijo determinado de cualquiera de los servicios móviles por satélite a comunicar a través de estaciones espaciales que utilicen estas bandas.
355	5.351A	En lo que respecta a la utilización de las bandas 1 518-1 544 MHz, 1 545-1 559 MHz, 1 610-1 626,5 MHz, 1 626,5-1 645,5 MHz, 1 646,5-1 660,5 MHz, 1 668-1 675 MHz, 1 980-2 010 MHz, 2 170-2 200 MHz, 2 483,5-2 500 MHz, 2 500-2 520 MHz y 2 670-2 690 MHz por el servicio móvil por satélite, véanse las Resoluciones 212 (Rev.CMR-07) y 225 (Rev.CMR-07). (CMR-07)
356	5.352	SUP-CMR-97.
357	5.352A	En la banda 1 525 - 1 530 MHz, las estaciones del servicio móvil por satélite, con excepción de las estaciones del servicio móvil marítimo por satélite no causarán interferencias perjudiciales ni podrán reclamar protección contra estaciones del servicio fijo en Francia y los territorios franceses de ultramar en la Región 3, Argelia, Arabia Saudita, Egipto, Guinea, la India, Israel, Italia, Jordania, Kuwait, Malí, Malta, Marruecos, Mauritania, Nigeria, Omán, Paquistán, Filipinas, Qatar, Siria, Tanzania, Viet Nam y Yemen, notificadas antes del 1 de abril de 1998. (CMR-97)
358	5.353	SUP-CMR-97.
398	5.380A	En la banda 1 670-1 675 MHz, las estaciones del servicio móvil por satélite no causarán interferencia perjudicial a las actuales estaciones terrenas del servicio de meteorología por satélite notificadas antes del 1 de enero de 2004 ni limitarán su desarrollo. Toda nueva asignación a dichas estaciones terrenas en esta banda también habrá de estar protegida contra la interferencia perjudicial causada por las estaciones del servicio móvil por satélite. (CMR-07)
359	5.353A	Cuando se aplican los procedimientos de la sección II del artículo 9 al servicio móvil por satélite en las bandas 1 530-1 544 MHz y 1 626,5-1 645,5 MHz, deberán satisfacerse en primer lugar las necesidades de espectro para comunicaciones de socorro, emergencia y seguridad del Sistema Mundial de Socorro y Seguridad Marítimos (SMSSM). Las comunicaciones de socorro, emergencia y seguridad del servicio móvil marítimo por satélite tendrán acceso prioritario y disponibilidad inmediata frente a todas las demás comunicaciones móviles por satélite en la misma red. Los sistemas móviles por satélite no causarán interferencias inaceptables ni podrán reclamar protección contra las comunicaciones de socorro, emergencia y seguridad del SMSSM. Se tendrá en cuenta la prioridad de las comunicaciones relacionadas con la seguridad en los demás servicios móviles por satélite. (Se aplicarán las disposiciones de la Resolución 222 (CMR-2000).) (CMR-00)
360	5.354	La utilización de las bandas 1 525 - 1 559 MHz y 1 626,5 - 1 660,5 MHz por los servicios móviles por satélite está sujeta a la coordinación a tenor del número 9.11A.
361	5.355	Atribución adicional: en Bahrein, Bangladesh, Congo, Egipto, Eritrea, Iraq, Israel, Kuwait, Líbano, Malta, Qatar, República Árabe Siria, Somalia, Sudán, Chad, Togo y Yemen, las bandas 1 540-1 559 MHz, 1 610-1 645,5 MHz y 1 646,5-1 660 MHz están también atribuidas, a título secundario, al servicio fijo. (CMR-03)
362	5.356	El empleo de la banda 1 544 - 1 545 MHz por el servicio móvil por satélite (espacio- Tierra) está limitado a las comunicaciones de socorro y seguridad (véase el artículo 31).
363	5.357	En la banda 1 545 - 1 555 MHz las transmisiones directas del servicio móvil aeronáutico (R), desde estaciones aeronáuticas terrenales a estaciones de aeronave, o entre estaciones de aeronave, están también autorizadas cuando esas transmisiones están destinadas a aumentar o a completar los enlaces establecidos entre estaciones de satélite y estaciones de aeronave.
364	5.357A	Al aplicar los procedimientos de la sección II del artículo 9 al servicio móvil por satélite en las bandas 1 545-1 555 MHz y 1 646,5-1 656,5 MHz, deberán satisfacerse en primer lugar las necesidades de espectro del servicio móvil aeronáutico por satélite (R) para la transmisión de mensajes con prioridad 1 a 6 con arreglo al artículo 44. Las comunicaciones del servicio móvil aeronáutico por satélite (R) con prioridad 1 a 6 con arreglo al artículo 44 tendrán acceso prioritario y disponibilidad inmediata, de ser necesario mediante precedencia, sobre todas las demás comunicaciones móviles por satélite en la misma red. Los sistemas móviles por satélite no causarán interferencias inaceptables ni podrán reclamar protección contra las comunicaciones del servicio móvil aeronáutico por satélite (R) con prioridad 1 a 6 con arreglo al artículo 44. Se tendrá en cuenta la prioridad de las comunicaciones relacionadas con la seguridad en los demás servicios móviles por satélite. (Se aplicarán las disposiciones de la Resolución 222 (CMR2000).) (CMR-00)
365	5.358	SUP-CMR-97.
366	5.359	Atribución adicional: en Alemania, Arabia Saudita, Armenia, Austria, Azerbaiyán, Belarús, Benin, Bulgaria, Camerún, España, Federación de Rusia, Francia, Gabón, Georgia, Grecia, Guinea, Guinea-Bissau, Jamahiriya Árabe Libia, Jordania, Kazajstán, Kuwait, Líbano, Lituania, Mauritania, Moldova, Uganda, Uzbekistán, Pakistán, Polonia, República Árabe Siria, Kirguistán, Rep. Pop. Dem. de Corea, Rumania, Swazilandia, Tayikistán, Tanzanía, Túnez, Turkmenistán y Ucrania, las bandas 1 550-1 559 MHz, 1 610-1 645,5 MHz y 1 646,5-1 660 MHz están también atribuidas, a título primario, al servicio fijo. Se insta a las administraciones a que hagan todos los esfuerzos posibles para evitar la implantación de nuevas estaciones del servicio fijo en esas bandas. (CMR-07)
367	5.360	SUP-CMR-97.
368	5.361	SUP-CMR-97.
369	5.362	SUP-CMR-97.
370	5.362A	En Estados Unidos, en las bandas 1 555-1 559 MHz y 1 656,5-1 660,5 MHz, el servicio móvil aeronáutico por satélite (R) tendrá acceso prioritario y disponibilidad inmediata, de ser necesario mediante precedencia, sobre las demás comunicaciones móviles por satélite en la misma red. Los sistemas móviles por satélite no causarán interferencias inaceptables ni podrán reclamar protección contra las comunicaciones del servicio móvil aeronáutico por satélite (R) con prioridad 1 a 6 con arreglo al artículo 44. Se tendrá en cuenta la prioridad de las comunicaciones relacionadas con la seguridad en los demás servicios móviles por satélite. (CMR-97)
371	5.362B	Atribución adicional: la banda 1 559-1 610 MHz también está atribuida al servicio fijo a título primario hasta el 1 de enero de 2010 en Argelia, Arabia Saudita, Camerún, Jamahiriya Árabe Libia, Jordania, Malí, Mauritania, República Árabe Siria y Túnez. Después de esta fecha, el servicio fijo puede continuar funcionando a título secundario hasta el 1 de enero de 2015, fecha a partir de la cual esta atribución dejará de ser válida. La banda 1 559-1 610 MHz está atribuida asimismo al servicio fijo en Argelia, Alemania, Armenia, Azerbaiyán, Belarús, Benin, Bulgaria, España, Federación de Rusia, Francia, Gabón, Georgia, Guinea, Guinea-Bissau, Kazajstán, Lituania, Moldova, Nigeria, Uganda, Uzbekistán, Pakistán, Polonia, Kirguistán, Rep. Dem. Pop. de Corea, Rumania, Senegal, Swazilandia, Tayikistán, Tanzanía, Turkmenistán y Ucrania a título secundario hasta el 1 de enero de 20 15, fecha a partir de la cual esta atribución dejará de ser válida. Se insta a las administraciones a que tomen todas las medidas a su alcance para proteger el servicio de radionavegación por satélite y el servicio de radionavegación aeronáutica, y a que no autoricen nuevas asignaciones de frecuencia a los sistemas del servicio fijo en esta banda. (CMR-07)
372	5.362C	Atribución adicional: en Congo (Rep. del), Egipto, Eritrea, Iraq, Israel, Jordania, Malta, Qatar, República Árabe Siria, Somalia, Sudán, Chad, Togo y Yemen, la banda 1 559-1 610 MHz está también atribuida, a título secundario, al servicio fijo, hasta el 1 de enero de 2015, fecha después de la cual la atribución dejará de ser válida. Se insta a las administraciones a que tomen todas las medidas a su alcance para proteger el servicio de radionavegación por satélite, y a que no autoricen nuevas asignaciones de frecuencia a los sistemas del servicio fijo en esta banda. (CMR-07)
373	5.363	SUP – CMR 2007.
399	5.381	Atribución adicional: en Afganistán, Costa Rica, Cuba, India, Irán (Rep. Islámica del) y Pakistán, la banda 1 690-1 700 MHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-03)
421	5.393	Atribución adicional: en Canadá, Estados Unidos, India y México, la banda 2 310-2 360 MHz está también atribuida a título primario al servicio de radiodifusión por satélite (sonora) y al servicio de radiodifusión sonora terrenal complementario. Su utilización está limitada a la radiodifusión sonora digital y sujeta a las disposiciones de la Resolución 528 (Rev.CMR-03) con excepción del resuelve 3 en lo que respecta a la limitación impuesta a los sistemas del servicio de radiodifusión por satélite en los 25 MHz superiores. (CMR-07)
374	5.364	La utilización de la banda 1 610 - 1 626,5 MHz por el servicio móvil por satélite (Tierra- espacio) y por el servicio de radiodeterminación por satélite (Tierra-espacio) está sujeta a la coordinación a tenor del número 9.11A. Una estación terrena móvil que funcione en cualquiera de estos servicios en esta banda no dará una densidad máxima de p.i.r.e. mayor de – 15 dB(W/4 kHz) en el tramo de la banda utilizado por los sistemas que funcionan conforme a las disposiciones del número 5.366 (al cual se aplica el número 4.10), a menos que acuerden otra cosa las administraciones afectadas. En el tramo de la banda no utilizado por dichos sistemas la densidad de p.i.r.e. media no excederá de -3 dB(W/4 kHz). Las estaciones del servicio móvil por satélite no solicitarán protección frente a las estaciones del servicio de radionavegación aeronáutica, las estaciones que funcionen de conformidad con las disposiciones del número 5.366 y las estaciones del servicio fijo que funcionen con arreglo a las disposiciones del número 5.359. Las administraciones responsables de la coordinación de las redes móviles por satélite harán lo posible para garantizar la protección de las estaciones que funcionen de conformidad con lo dispuesto en el número 5.366.
375	5.365	La utilización de la banda 1 613,8 - 1 626,5 MHz por el servicio móvil por satélite (espacio-Tierra) está sujeta a la coordinación a tenor de la Resolución 46 (Rev.CMR-95)/del número 9.11A.
376	5.366	La banda 1 610 - 1 626,5 MHz se reserva, en todo el mundo, para el uso y el desarrollo de equipos electrónicos de ayuda a la navegación aérea instaladas a bordo de aeronaves, así como de las instalaciones con base en tierra o a bordo de satélites directamente asociadas a dichos equipos. Este uso de satélites está sujeto a la obtención del acuerdo indicado en el número 9.21.
377	5.367	Atribución adicional: las bandas 1 610 - 1 626,5 MHz y 5 000 - 5 150 MHz están también atribuidas, a título primario, al servicio móvil aeronáutico por satélite (R), a reserva de obtener el acuerdo indicado en el número 9.21.
378	5.368	En lo que respecta al servicio de radiodeterminación por satélite y al servicio móvil por satélite, las disposiciones del número 4.10 no se aplican a la banda de frecuencias 1 610 - 1 626,5 MHz, salvo al servicio de radionavegación aeronáutica por satélite.
379	5.369	Categoría de servicio diferente: en Angola, Australia, Burundi, China, Eritrea, Etiopía, India, Irán (Rep. Islámica del), Israel, Líbano, Liberia, Jamahiriya Árabe Libia, Madagascar, Malí, Pakistán, Papua Nueva Guinea, Rep. Dem. del Congo, República Árabe Siria, Sudán, Swazilandia, Togo y Zambia, la atribución de la banda 1 610-1 626,5 MHz al servicio de radiodeterminación por satélite (Tierra-espacio) es a título primario (véase el número 5.33), a reserva de obtener el acuerdo indicado en el número 9.21 en relación con otros países no incluidos en esta disposición. (CMR-03)
380	5.370	Categoría de servicio diferente: en Venezuela, la atribución al servicio de radiodeterminación por satélite en la banda 1 610 - 1 626,5 MHz (Tierra-espacio) es a título secundario.
381	5.371	Atribución adicional : en la Región 1, las bandas 1 610 - 1 626,5 MHz (Tierra-espacio) y 2 483,5 - 2 500 MHz (espacio-Tierra) están también atribuidas, a título secundario, al servicio de radiodeterminación por satélite, a reserva de obtener el acuerdo indicado en el número 9.21.
382	5.372	Las estaciones del servicio de radiodeterminación por satélite y del servicio móvil por satélite no causarán interferencia perjudicial a las estaciones del servicio de radioastronomía que utilicen la banda 1 610,6 - 1 613,8 MHz. (Se aplica el número 29.13.)
383	5.373	No utilizado.
384	5.373A	SUP-CMR-97.
385	5.374	Las estaciones terrenas móviles del servicio móvil por satélite que funcionan en las bandas 1 631,5 - 1 634,5 MHz y 1 656,5 - 1 660 MHz no causarán interferencia perjudicial a las estaciones del servicio fijo que funcionen en los países mencionados en el número 5.359. (CMR-97)
386	5.375	El empleo de la banda 1 645,5 - 1 646,5 MHz por el servicio móvil por satélite (Tierra- espacio) y para enlaces entre satélites está limitado a las comunicaciones de socorro y seguridad (véase el artículo 31).
387	5.376	En la banda 1 646,5 - 1 656,5 MHz, las transmisiones directas de estaciones de aeronave del servicio móvil aeronáutico (R) a estaciones aeronáuticas terrenales, o entre estaciones de aeronave, están también autorizadas si esas transmisiones están destinadas a aumentar o a completar los enlaces establecidos entre estaciones de aeronave y estaciones de satélite.
388	5.376A	Las estaciones terrenas móviles que funcionan en la banda 1 660,0 – 1 660,5 MHz no causarán interferencia perjudicial a las estaciones que funcionan en el servicio de radioastronomía. (CMR-97)
389	5.377	SUP - CMR 2003.
390	5.378	No utilizado.
391	5.379	Atribución adicional: en Bangladesh, India, Indonesia, Nigeria y Pakistán, la banda 1 660,5 - 1 668,4 MHz está también atribuida, a título secundario, al servicio de ayudas a la meteorología.
392	5.379A	Se encarece a las administraciones que en la banda 1 660,5 - 1 668,4 MHz aseguren toda la protección posible a la futura investigación de radioastronomía, en particular eliminando tan pronto como sea posible las emisiones aire-tierra del servicio de ayudas a la meteorología en la banda 1 664,4 - 1 668,4 MHz.
393	5.379B	La utilización de la banda 1 668-1 675 MHz por el servicio móvil por satélite está sujeta a coordinación a tenor del número 9.11A. En la banda 1 668-1 668,4 MHz, será de aplicación la Resolución 904 (CMR-07). (CMR-07)
394	5.379C	A fin de proteger el servicio de radioastronomía en la banda 1 668-1 670 MHz, las estaciones terrenas de una red del servicio móvil por satélite que funcionen en esta banda no rebasarán los valores de la densidad de flujo de potencia combinada de – 181 dB(W/m^2) en 10 MHz y – 194 dB(W/m^2) en todo tramo de 20 kHz en cualquier estación de radioastronomía inscrita en el Registro Internacional de Frecuencias, durante más del 2% del tiempo en periodos de integración de 2 000 s. (CMR-03).
395	5.379D	Para la compartición de la banda 1 668,4-1 675 MHz entre el servicio móvil por satélite y los servicios fijo y móvil, se aplicará la Resolución 744 (Rev.CMR-07). (CMR-07)
396	5.379E	En la banda 1 668,4-1 675 MHz, las estaciones del servicio móvil por satélite no causarán interferencia perjudicial a las estaciones del servicio de ayudas a la meteorología de China, Irán (República Islámica del), Japón y Uzbekistán. En la banda 1 668,4-1 675 MHz, se insta a las administraciones a no implementar nuevos sistemas del servicio de ayudas a la meteorología y se les alienta a transferir las actuales operaciones del servicio de ayudas a la meteorología a otras bandas, tan pronto como sea posible. (CMR-03)
397	5.380	SUP – CMR 2007.
559	5.479	La banda 9 975 - 10 025 MHz está también atribuida, a título secundario, al servicio de meteorología por satélite para ser utilizada por los radares meteorológicos.
400	5.382	Categoría de servicio diferente: en Arabia Saudita, Armenia, Azerbaiyán, Bahrein, Belarús, Congo (Rep. del), Egipto, Emiratos Árabes Unidos, Eritrea, Etiopía, Federación de Rusia, Guinea, Iraq, Israel, Jordania, Kazajstán, Kuwait, la ex República Yugoslava de Macedonia, Líbano, Mauritania, Moldova, Mongolia, Omán, Uzbekistán, Polonia, Qatar, República Árabe Siria, Kirguistán, Serbia, Somalia, Tayikistán, Tanzanía, Turkmenistán, Ucrania y Yemen, en la banda 1 690-1 700 MHz, la atribución al servicio fijo y al servicio móvil, salvo móvil aeronáutico, es a título primario (véase el número 5.33), y en la Rep. Dem. de Corea, la atribución de la banda 1 690-1 700 MHz al servicio fijo es a título primario (véase el número 5.33) y al servicio móvil, salvo móvil aeronáutico, a título secundario. (CMR-07)
401	5.383	No utilizado.
402	5.384	Atribución adicional: en India, Indonesia, Japón, la banda 1 700 - 1 710 MHz está también atribuida, a título primario, al servicio de investigación espacial (espacio-Tierra). (CMR-97)
403	5.384A	Las bandas 1 710-1 885 MHz, 2 300-2 400 MHz y 2 500-2 690 MHz, o partes de esas bandas, se han identificado para su utilización por las administraciones que deseen introducir las Telecomunicaciones Móviles Internacionales (IMT) de conformidad con la Resolución 223 (Rev.CMR-07). Dicha identificación no excluye su uso por ninguna aplicación de los servicios a los cuales están atribuidas y no implica prioridad alguna en el Reglamento de Radiocomunicaciones. (CMR-07)
404	5.385	Atribución adicional: la banda 1 718,8-1 722,2 MHz, está también atribuida, a título secundario, al servicio de radioastronomía para la observación de rayas espectrales. (CMR-00)
405	5.386	Atribución adicional: la banda 1 750-1 850 MHz está también atribuida, a título primario, al servicio de operaciones espaciales (Tierra-espacio) y al servicio de investigación espacial (Tierra-espacio) en la Región 2, en Australia, Guam, India, Indonesia y Japón, a reserva de obtener el acuerdo indicado en el número 9.21, con atención particular a los sistemas de dispersión troposférica. (CMR-03)
406	5.387	Atribución adicional: en Belarús, Georgia, Kazajstán, Mongolia, Kirguistán, Eslovaquia, Rumania, Tayikistán y Turkmenistán, la banda 1 770-1 790 MHz está también atribuida, a título primario, al servicio de meteorología por satélite, a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-07)
407	5.388	Las bandas 1 885-2 025 MHz y 2 110-2 200 MHz están destinadas a su utilización, a nivel mundial, por las administraciones que desean introducir las telecomunicaciones móviles internacionales-2000 (IMT-2000). Dicha utilización no excluye el uso de estas bandas por otros servicios a los que están atribuidas. Las bandas de frecuencias deberían ponerse a disposición de las IMT-2000 de acuerdo con lo dispuesto en la Resolución 212 (Rev.CMR-97). Véase también la Resolución 223 (CMR-2000).) (CMR-00)
408	5.388A	En las Regiones 1 y 3, las bandas 1 885-1 980 MHz, 2 010-2 025 MHz y 2 110-2 170 MHz, y en la Región 2, las bandas 1 885-1 980 MHz y 2 110-2 160 MHz, pueden ser utilizadas por las estaciones situadas en plataformas a gran altitud como estaciones de base para la prestación de los servicios de las Telecomunicaciones Móviles Internacionales-2000 (IMT-2000), de acuerdo con la Resolución 221 (Rev.CMR-03). Su utilización por las aplicaciones IMT-2000 que empleen estaciones situadas en plataformas a gran altitud como estaciones de base no impide el uso de estas bandas a ninguna estación de los servicios con atribuciones en las mismas ni establece prioridad alguna en el Reglamento de Radiocomunicaciones. (CMR-03)
409	5.388B	Para proteger los servicios fijo y móvil, incluidas las estaciones móviles IMT-2000, en los territorios de Argelia, Arabia Saudita (Reino de), Bahrein (Reino de), Benin, Burkina Faso, Camerún, China, Comoras, Côte d'Ivoire, Cuba, Djibouti, Egipto, Emiratos Árabes Unidos, Eritrea, Etiopía, Gabón, Gana, India, Irán (República Islámica de), Israel, Jordania, Kenya, Kuwait, Jamahiriya Árabe Libia, Malí, Marruecos, Mauritania, Nigeria, Omán, Uganda, Qatar, República Árabe Siria, Sudán, Tanzanía, Chad, Túnez, Yemen, Zambia y Zimbawe contra interferencia en el mismo canal, una HAPS que funcione como estación de base IMT-2000 en los países vecinos, en las bandas a las que se refiere el número 5.388A, no rebasará la densidad de flujo de potencia (dfp) en el mismo canal de – 127 dB (W/(m2 · MHz)) en la superficie de la Tierra más allá de las fronteras del país salvo que la administración afectada otorgue su acuerdo explícito en el momento de la notificación de la estación en plataforma a gran altitud. (CMR- 03)
410	5.389	No utilizado.
411	5.389A	La utilización de las bandas 1 980-2 010 MHz y 2 170-2 200 MHz por el servicio móvil por satélite está sujeta a la coordinación con arreglo al número 9.11A y a las disposiciones de la Resolución 716 (Rev.CMR-2000). (CMR-07)
412	5.389B	La utilización de la banda 1 980 - 1 990 MHz por el servicio móvil por satélite no causará interferencia perjudicial ni limitará el desarrollo de los servicios fijo y móvil en Argentina, Brasil, Canadá, Chile, Ecuador, Estados Unidos, Honduras, Jamaica, México, Perú, Suriname, Trinidad y Tobago, Uruguay y Venezuela.
413	5.389C	La utilización de las bandas 2 010-2 025 MHz y 2 160-2 170 MHz en la Región 2 por el servicio móvil por satélite está sujeta a la coordinación con arreglo al número 9.11A y a las disposiciones de la Resolución 716 (Rev.CMR-2000). (CMR-07)
414	5.389D	SUP - CMR 2003.
415	5.389E	La utilización de las bandas 2 010 - 2 025 MHz y 2 160 - 2 170 MHz por el servicio móvil por satélite en la Región 2 no causará interferencia perjudicial a o limitará el desarrollo de los servicios fijo y móvil de las Regiones 1 y 3.
416	5.389F	En Argelia, Benin, Cabo Verde, Egipto, Irán (República Islámica del), Malí, Siria y Túnez la utilización de las bandas 1 980-2 010 MHz y 2 170-2 200 MHz por el servicio móvil por satélite no debe causar interferencia perjudicial a los servicios fijos y móviles, o impedir el desarrollo de estos servicios antes del 1 de enero de 2005, ni solicitar protección con respecto a estos servicios. (CMR-00)
417	5.390	SUP – CMR 2007.
418	5.391	Al hacer asignaciones al servicio móvil en las bandas 2 025 - 2 110 MHz y 2 200 - 2 290 MHz, las administraciones no introducirán sistemas móviles de alta densidad como los descritos en la Recomendación UIT-R SA.1154 y tendrán en cuenta esta recomendación para la introducción de cualquier otro tipo de sistema móvil. (CMR-97)
419	5.392	Se insta a las administraciones a tomar todas las medidas viables para garantizar que las transmisiones espacio-espacio entre dos o más satélites no geoestacionarios de los servicios de investigación espacial, operaciones espaciales y exploración de la Tierra por satélite en las bandas 2 025 - 2 110 MHz y 2 200 - 2 290 MHz, no imponen ninguna restricción a las transmisiones Tierra-espacio, espacio-Tierra y otras transmisiones espacio-espacio de esos servicios y en esas bandas, entre satélites geoestacionarios y no geoestacionarios.
420	5.392A	SUP – CMR 2007.
422	5.394	En Estados Unidos, el uso de la banda 2 300-2 390 MHz por el servicio móvil aeronáutico para la telemedida tiene prioridad sobre otros usos por los servicios móviles. En Canadá, el uso de la banda 2 360-2 400 MHz por el servicio móvil aeronáutico para la telemedida tiene prioridad sobre otros usos por los servicios móviles. (CMR-07)
423	5.395	En Francia y Turquía, la utilización de la banda 2 310-2 360 MHz por el servicio móvil aeronáutico para telemedida tiene prioridad sobre las demás utilizaciones del servicio móvil. (CMR-03)
424	5.396	Las estaciones espaciales del servicio de radiodifusión por satélite en la banda 2 310 - 2 360 MHz, explotadas de conformidad con el número 5.393, que puedan afectar a los servicios a los que esta banda está atribuida en otros países, se coordinarán y notificarán de conformidad con la resolución 33. Las estaciones del servicio complementario de radiodifusión terrenal estarán sujetas a coordinación bilateral con los países vecinos antes de su puesta en servicio.
425	5.397	Categoría de servicio diferente: en Francia, la banda 2 450 - 2 500 MHz está atribuida a título primario al servicio de radiolocalización (véase el número 5.33). Este uso está sujeto a acuerdo con las administraciones que tengan servicios explotados o que se explotarán de conformidad con el presente cuadro y que puedan resultar afectados.
426	5.398	Con respecto al servicio de radiodeterminación por satélite, las disposiciones del número 4.10 no se aplican en la banda 2 483,5 - 2 500 MHz.
427	5.399	En la Región 1, en países distintos de los enunciados en el número 5.400, las estaciones del servicio de radiodeterminación por satélite no deberán causar interferencia perjudicial ni pedir protección contra estaciones del servicio de radiolocalización.
428	5.400	Categoría de servicio diferente: en Angola, Australia, Bangladesh, Burundi, China, Eritrea, Etiopía, India, Irán (Rep. Islámica del), Líbano, Liberia, Jamahiriya Árabe Libia, Madagascar, Malí, Pakistán, Papua Nueva Guinea, Rep. Dem. del Congo, República Árabe Siria, Sudán, Swazilandia, Togo y Zambia, la atribución de la banda 2 483,5-2 500 MHz al servicio de radiodeterminación por satélite (espacio-Tierra) es a título primario (véase el número 5.33), a reserva de obtener el acuerdo indicado en el número 9.21 en relación con otros países no incluidos en esta disposición. (CMR-03)
429	5.401	No utilizado.
430	5.402	La utilización de la banda 2 483,5 - 2 500 MHz por el servicio móvil por satélite y el servicio de radiodeterminación por satélite está sujeta a la coordinación a tenor del número 9.11A. Se insta a las administraciones a que tomen todas las medidas necesarias para evitar la interferencia perjudicial al servicio de radioastronomía procedente de las emisiones en la banda 2 483,5 - 2 500 MHz, especialmente la interferencia provocada por la radiación del segundo armónico que caería en la banda 4 990 - 5 000 MHz atribuida al servicio de radioastronomía a escala mundial.
431	5.403	A reserva de obtener el acuerdo indicado en el número 9.21, la banda 2 520-2 535 MHz puede ser utilizada también por el servicio móvil por satélite (espacio-Tierra), salvo móvil aeronáutico por satélite, estando su explotación limitada al interior de las fronteras nacionales. En este caso se aplicarán las disposiciones del número 9.11A. (CMR-07)
432	5.404	Atribución adicional: en India y en la República Islámica del Irán, la banda 2 500 - 2 516,5 MHz puede también utilizarse por el servicio de radiodeterminación por satélite (espacio-Tierra) para la explotación dentro de las fronteras nacionales, a reserva de obtener el acuerdo indicado en el número 9.21.
433	5.405	Atribución adicional: en Francia, la banda 2 500 - 2 550 MHz está también atribuida a título primario al servicio de radiolocalización. Esta utilización está sujeta a acuerdo con las administraciones que tengan servicios explotados o que se explotarán de conformidad con el presente cuadro de atribución de bandas de frecuencias y que puedan resultar afectados.
434	5.406	No utilizado.
435	5.407	En la banda 2 500 - 2 520 MHz, la densidad de flujo de potencia en la superficie de la Tierra de las estaciones espaciales que operan en el servicio móvil por satélite (espacio-Tierra) no rebasará el valor de – 152 dB (W/m^2/4 kHz) en Argentina, a menos que las administraciones interesadas acuerden otra cosa.
436	5.408	SUP - CMR 2000.
437	5.409	SUP – CMR 2007.
438	5.410	La banda 2 500-2 690 MHz puede utilizarse por sistemas de dispersión troposférica en la Región 1, a reserva de obtener el acuerdo indicado en el número 9.21. Las administraciones harán todo lo posible por evitar la introducción de nuevos sistemas de dispersión troposférica en esta banda. Al planificar nuevos radioenlaces de dispersión troposférica en esta banda, se adoptarán todas las medidas posibles para evitar dirigir las antenas de dichos enlaces hacia la órbita de satélites geoestacionarios. (CMR-07)
439	5.411	SUP – CMR 2007.
440	5.412	Atribución sustitutiva: en Azerbaiyán, Kirguistán y Turkmenistán, la banda 2 500- 2 690 MHz está atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
441	5.413	Al proyectar sistemas del servicio de radiodifusión por satélite, funcionando en las bandas situadas entre 2 500 MHz y 2 690 MHz, se insta a las administraciones a que tomen todas las medidas necesarias para proteger el servicio de radioastronomía en la banda 2 690 - 2 700 MHz.
442	5.414	La atribución de la banda 2 500-2 520 MHz al servicio móvil por satélite (espacio- Tierra) está sujeta a la coordinación con arreglo al número 9.11A. (CMR-07)
443	5.414A	En Japón e India, la utilización de las bandas 2 500-2 520 MHz y 2 520-2 535 MHz, de conformidad con el el número 5.403, por una red de satélites del servicio móvil por satélite (espacio-Tierra) se limita exclusivamente al interior de las fronteras nacionales y está sujeta a la aplicación del número 9.11A. Se utilizarán los siguientes valores de dfp como umbral de coordinación de acuerdo con el número 9.11A, sean cuales sean las condiciones y métodos de ulación, en una zona de 1 000 km alrededor del territorio de la administración notificante de la red del servicio móvil por satélite: −136 dB(W/(m2 · MHz)) para 0° ≤ θ ≤ 5° −136 + 0,55 (θ − 5) dB(W/(m2 · MHz)) para 5° < θ ≤ 25° – 125 dB(W/(m2 · MHz)) para 25° < θ ≤ 90° siendo θ el ángulo de llegada de la onda incidente por encima del plano horizontal, en grados. Fuera de esta zona, será de aplicación el Cuadro 21-4 del Artículo 21. Además, se aplicarán a los sistemas cuya información de notificación completa haya recibido la Oficina de Radiocomunicaciones antes del 14 de noviembre de 2007 inclusive, y que se hayan puesto en servicio antes de esa misma fecha, los umbrales de coordinación del Cuadro 5-2 del Anexo 1 al Apéndice 5 del Reglamento de Radiocomunicaciones (edición de 2004), junto con las disposiciones aplicables de los Artículos 9 y 11 asociadas al número 9.11A. (CMR-07)
444	5.415	La utilización de la banda 2 500-2 690 MHz en la Región 2 y de las bandas 2 500-2 535 MHz y 2 655-2 690 MHz en la Región 3 por el servicio fijo por satélite está limitada a los sistemas nacionales y regionales, a reserva de obtener el acuerdo indicado en el número 9.21, teniendo particularmente en cuenta el servicio de radiodifusión por satélite en la Región 1. (CMR-07)
445	5.415A	Atribución adicional: en India y Japón, con sujeción al acuerdo obtenido con arreglo al número 9.21, la banda 2 515-2 535 MHz también puede ser utilizada por el servicio móvil aeronáutico por satélite (espacio-Tierra) para operaciones circunscritas a sus fronteras nacionales. (CMR-00)
446	5.416	La utilización de la banda 2 520-2 670 MHz por el servicio de radiodifusión por satélite está limitada a los sistemas nacionales y regionales para la recepción comunitaria, a reserva de obtener el acuerdo indicado en el número 9.21. Las administraciones aplicarán las disposiciones del número 9.19 en esta banda en sus negociaciones bilaterales o multilaterales. (CMR-07)
447	5.417	SUP - CMR 2000.
448	5.417A	Al aplicar la disposición del número 5.418, en Corea (República de) y Japón, el resuelve 3 de la Resolución 528 (Rev.CMR-03) se hace menos estricto para que el servicio de radiodifusión por satélite (sonora) y el servicio de radiodifusión terrenal complementario puedan funcionar adicionalmente, a título primario, en la banda 2 605-2 630 MHz. Esta utilización está limitada a los sistemas destinados a asegurar una cobertura nacional. Una administración citada en esta disposición no tendrá simultáneamente dos atribuciones de frecuencias superpuestas, una con arreglo a esta disposición y la otra con arreglo a las disposiciones del número 5.416. No se aplican las disposiciones del número 5.416 y el Cuadro 21-4 del Artículo 21. La utilización de los sistemas de satélites no geoestacionarios en el servicio de radiodifusión por satélite (sonora) en la banda 2 605-2 630 MHz está sujeta a las disposiciones de la Resolución 539 (Rev. CMR-03). La densidad de flujo de potencia en la superficie de la Tierra producida por emisiones procedentes de una estación espacial geoestacionaria del servicio de radiodifusión por satélite (sonora) que funciona en la banda 2 605-2 630 MHz, de la cual se haya recibido la información de coordinación del Artículo 4 completa, o información de notificación, después del 4 de julio de 2003, para todas las condiciones y todos los métodos de modulación, no rebasará los siguientes límites: −130 dB(W/(m2 ·MHz)) para 0o ≤θ≤ 5 o −130 + 0,4 (θ−5) dB(W/(m2 ·MHz)) para 5 o < θ≤25 o −122 dB(W/(m2 ·MHz)) para 25 o < θ≤90 o siendo θel ángulo de llegada de la onda incidente por encima del plano horizontal, en grados. Estos límites pueden rebasarse en el territorio de cualquier país cuya administración así lo acepte. En el caso de las redes del SRS (sonora) de Corea (Rep. de), como excepción a los límites indicados, el valor de dfp de – 122 dB(W/(m2 • MHz)) se utilizará como umbral de coordinación con arreglo al número 9.11 en una superficie de 1 000 km alrededor del territorio de la administración notificante del servicio de radiodifusión por satélite, para ángulos de llegada superiores a 35º. (CMR-03)
449	5.417B	En Corea (Rep. de) y Japón, la utilización de la banda 2 605-2 630 MHz por los sistemas de satélites no geoestacionarios del servicio de radiodifusión por satélite (sonora), en cumplimiento del número 5.417A, para los cuales se haya recibido la información de coordinación o de notificación completa del Apéndice 4, después del 4 de julio de 2003, está sujeta a la aplicación de las disposiciones del número 9.12A, con respecto a las redes de satélites geoestacionarios para las cuales se considere que se ha recibido la información de coordinación del Apéndice 4 completa, o información de notificación, después del 4 de julio de 2003, y no se aplica el número 22.2. El número 22.2 seguirá aplicándose con respecto a las redes de satélites geoestacionarios para las cuales se considere que se ha recibido la información de coordinación del Apéndice 4 completa, o información de notificación, antes del 5 de julio de 2003. (CMR-03)
450	5.417C	La utilización de la banda 2 605-2 630 MHz por los sistemas de satélites no geoestacionarios del servicio de radiodifusión por satélite (sonora), en cumplimiento del número 5.417A, para los cuales se haya recibido la información de coordinación o de notificación completa del Apéndice 4, después del 4 de julio de 2003, está sujeta a la aplicación de las disposiciones del número 9.12. (CMR-03)
451	5.417D	La utilización de la banda 2 605-2 630 MHz por las redes de satélites geoestacionarios para las cuales se haya recibido la información de coordinación o de notificación completa del Apéndice 4, después del 4 de julio de 2003, está sujeta a la aplicación de las disposiciones del número 9.13 con respecto a los sistemas de satélites no geoestacionarios del servicio de radiodifusión por satélite (sonora), en cumplimiento del número 5.417A, y no se aplica el número 22.2. (CMR-03)
452	5.418	Atribución adicional: en Corea (Rep. de), India, Japón, Pakistán y Tailandia, la banda 2 535-2 655 MHz está también atribuida, a título primario, al servicio de radiodifusión por satélite (sonora) y al servicio de radiodifusión terrenal complementario. Esta utilización está limitada a la radiodifusión sonora digital y sujeta a las disposiciones de la Resolución 528 (Rev.CMR-03). Las disposiciones del número 5.416 y del Cuadro 21-4 del Artículo 21, no se aplican a esta atribución adicional. La utilización de sistemas de satélites no geoestacionarios en el servicio de radiodifusión por satélite (sonora) está sujeta a las disposiciones de la Resolución 539 (Rev.CMR-03). Los sistemas del servicio de radiodifusión por satélite (sonora) con satélites geoestacionarios para los cuales se haya recibido la información de coordinación completa del Apéndice 4 después del 1 de junio de 2005 se limitan a sistemas destinados a asegurar una cobertura nacional. La densidad de flujo de potencia en la superficie de la Tierra producida por emisiones procedentes de una estación espacial del servicio de radiodifusión por satélite (sonora) con satélites geoestacionarios que funciona en la banda 2 630-2 655 MHz, y para la cual se haya recibido la información completa de coordinación del Apéndice 4 después del 1 de junio de 2005, no rebasará los siguientes límites, sean cuales sean las condiciones y los métodos de ulación: −130 dB(W/(m2 · MHz)) para 0° ≤ θ ≤ 5° −130 + 0,4 (θ − 5) dB(W/(m2 · MHz)) para 5° < θ ≤ 25° – 122 dB(W/(m2 · MHz)) para 25° < θ ≤ 90° siendo θ el ángulo de llegada de la onda incidente por encima del plano horizontal, en grados. Estos límites pueden rebasarse en el territorio de cualquier país cuya administración así lo acepte. Como excepción a los límites indicados, el valor de densidad de flujo de potencia de – 122 dB(W/(m2 · MHz)) se utilizará como umbral de coordinación con arreglo al número 9.11 en una zona de 1 500 km alrededor del territorio de la administración que notifica el sistema del servicio de radiodifusión por satélite (sonora). Además, una administración enumerada en esta disposición no tendrá simultáneamente dos asignaciones de frecuencia superpuestas, una con arreglo a esta disposición y la otra con arreglo a las disposiciones del número 5.416 para los sistemas sobre los que se haya recibido información de coordinación completa del Apéndice 4 después del 1 de junio 2005. (CMR-07)
453	5.418A	La utilización de la banda 2 630-2 655 MHz por los sistemas de satélites no geoestacionarios del servicio de radiodifusión por satélite (sonora) en determinados países de la Región 3, enumerados en el número 5.418, de los que se haya recibido la información de coordinación del Apéndice 4 completa, o información de notificación, después del 2 de junio de 2000, está sujeta a la aplicación de las disposiciones del número 9.12A respecto a las redes de satélites geoestacionarios para las cuales se considere que se ha recibido la información de coordinación o de notificación completa a la que se refiere el Apéndice 4, después del 2 de junio de 2000, en cuyo caso no se aplica el número 22.2. El número 22.2 continuará aplicándose respecto a las redes de satélites geoestacionarios para las cuales se considere que se ha recibido la información de coordinación del Apéndice 4 completa, o información de notificación, antes del 3 de junio de 2000. (CMR-03)
454	5.418B	La utilización de la banda de 2 630-2 655 MHz por sistemas de satélite no geoestacionarios del servicio de radiodifusión por satélite (sonora) en cumplimiento del número 5.418, de los quese haya recibido la información de coordinación o de notificación completa del Apéndice 4 después del 2 de junio de 2000, está sujeta a la aplicación de las disposiciones del número 9.12. (CMR-03)
455	5.418C	La utilización de la banda 2 630-2 655 MHz por redes de satélite geoestacionarios de los que se haya recibido la información de coordinación o de notificación completa del Apéndice 4 después del 2 de junio de 2000, está sujeta a la aplicación de las disposiciones del número 9.13 respecto a los sistemas de satélite no geoestacionarios que funcionan en el servicio de radiodifusión por satélite (sonora), en cumplimiento del número 5.418, y no se aplica el número 22.2. (CMR-03)
456	5.419	Al introducir sistemas del servicio móvil por satélite en la banda 2 670-2 690 MHz, las administraciones tomarán todas las medidas necesarias para proteger los sistemas de satélites que funcionen en esta banda antes del 3 de marzo de 1992. La coordinación de los sistemas del servicio móvil por satélite en esta banda está sujeta a la aplicación de las disposiciones del número 9.11A. (CMR-07)
457	5.420	La banda 2 655 - 2 670 MHz (hasta el 1 de enero de 2005 la banda 2 655 - 2 690 MHz) puede también utilizarse en el servicio móvil por satélite (Tierra-espacio), salvo móvil aeronáutico por satélite, para explotación limitada al interior de las fronteras nacionales, a reserva de obtener el acuerdo indicado en el número 9.21. La coordinación está sujeta a la aplicación de las disposiciones del número 9.11A. (CMR-07)
458	5.420A	SUP – CMR 2007.
459	5.421	SUP - CMR 2003.
460	5.422	Atribución adicional: en Arabia Saudita, Armenia, Azerbaiyán, Bahrein, Belarús, Brunei Darussalam, Congo (Rep. del), Côte d'Ivoire, Cuba, Egipto, Emiratos Árabes Unidos, Eritrea, Etiopía, Gabón, Georgia, Guinea, Guinea-Bissau, Irán (República Islámica del), Iraq, Israel, Jordania, Kuwait, Líbano, Mauritania, Moldova, Mongolia, Montenegro, Nigeria, Omán, Pakistán, Filipinas, Qatar, República Árabe Siria, Kirguistán, Rep. Dem. del Congo, Rumania, Somalia, Tayikistán, Túnez, Turkmenistán, Ucrania y Yemen, la banda 2 690-2 700 MHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. Su utilización está limitada a los equipos que estuvieran en funcionamiento el 1 de enero de 1985. (CMR-07)
461	5.423	Los radares instalados en tierra, que funcionen en la banda 2 700 - 2 900 MHz para las necesidades de la meteorología, están autorizados a funcionar sobre una base de igualdad con las estaciones del servicio de radionavegación aeronáutica.
462	5.424	Atribución adicional: en Canadá, la banda 2 850 - 2 900 MHz está también atribuida, a título primario, al servicio de radionavegación marítima, para que la utilicen los radares instalados en la costa.
463	5.424A	En la banda 2 900-3 100 MHz, las estaciones del servicio de radiolocalización no causarán interferencia perjudicial a los sistemas de radar que operan en el servicio de radionavegación ni reclamarán protección respecto a ellos. (CMR-03)
464	5.425	En la banda 2 900 - 3 100 MHz, el uso del sistema interrogador-transpondedor a bordo de barcos (SIT-shipborne interrogator-transponder) se limitará a la sub-banda 2 930 - 2 950 MHz.
465	5.426	La utilización de la banda 2 900 - 3 100 MHz por el servicio de radionavegación aeronáutica se limita a los radares instalados en tierra.
466	5.427	En las bandas 2 900 - 3 100 MHz y 9 300 - 9 500 MHz, la respuesta procedente de transpondedores de radar no podrá confundirse con la de balizas-radar (racons) y no causará interferencia a radares de barco o aeronáuticos del servicio de radionavegación, teniendo en cuenta sin embargo, la disposición del número 4.9.
467	5.428	Atribución adicional: en Azerbaiyán, Mongolia, Kirguistán, Rumania y Turkmenistán, la banda 3 100-3 300 MHz está también atribuida, a título primario, al servicio de radionavegación. (CMR-07)
468	5.429	Atribución adicional: en Arabia Saudita, Bahrein, Bangladesh, Brunei Darussalam, China, Congo (Rep. del), Corea (Rep. de), Côte d'Ivoire, Emiratos Árabes Unidos, India, Indonesia, Irán (República Islámica del), Iraq, Israel, Jamahiriya Árabe Libia, Japón, Jordania, Kenya, Kuwait, Líbano, Malasia, Omán, Uganda, Pakistán, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea y Yemen, la banda 3 300-3 400 MHz está también atribuida, a título primario, a los servicios fijo y móvil. Los países ribereños del Mediterráneo no pueden pretender protección de sus servicios fijo y móvil por parte del servicio de radiolocalización. (CMR-07)
469	5.430	Atribución adicional: en Azerbaiyán, Mongolia, Kirguistán, Rumania y Turkmenistán, la banda 3 300-3 400 MHz está también atribuida, a título primario, al servicio de radionavegación. (CMR-07)
470	5.430A	Categoría de servicio diferente: en Albania, Argelia, Alemania, Andorra, Arabia Saudita, Austria, Azerbaiyán, Bahrein, Bélgica, Bosnia y Herzegovina, Botswana, Bulgaria, Burkina Faso, Camerún, Chipre, Vaticano, Côte d'Ivoire, Croacia, Dinamarca, Egipto, España, Estonia, Finlandia, Francia, Gabón, Georgia, Grecia, Hungría, India, Irlanda, Islandia, Israel, Italia, Jordania, Kuwait, Lesotho, Letonia, Macedonia, Liechtenstein, Lituania, Malawi, Malta, Marruecos, Mauritania, Moldova, Mónaco, Montenegro, Mozambique, Namibia, Níger, Noruega, Omán, Países Bajos, Polonia, Portugal, Qatar, Siria, Congo, Eslovaquia, República Checa, Rumanía, Reino Unido, San Marino, Senegal, Serbia, Sierra Leona, Singapur, Eslovenia, Sudafricana (República), Suecia, Suiza, Swazilandia, Chad, Túnez, Turquía, Ucrania, Zambia y Zimbabwe, la banda 3 400-3 600 MHz está atribuida al servicio móvil, salvo móvil aeronáutico, a título primario, a reserva de obtener el acuerdo con otras administraciones de conformidad con el número 9.21, y está identificada para las Telecomunicaciones Móviles Internacionales (IMT). Esta identificación no impide la utilización de esta banda por cualquier aplicación de los servicios a los que está atribuida, ni establece prioridad alguna en el Reglamento de Radiocomunicaciones. En la etapa de coordinación también se aplican las disposiciones de los números 9.17 y 9.18. Antes de que una administración ponga en servicio una estación (de base o móvil) del servicio móvil en esta banda, deberá garantizar que la densidad de flujo de potencia (dfp) producida a 3 m sobre el suelo no supera el valor de – 154,5 dBW/(m2 ⋅4 kHz) durante más del 20% del tiempo en la frontera del territorio de cualquier otra administración. Este límite puede rebasarse en el territorio de cualquier país cuya administración así lo acepte. Para asegurar que se satisface el límite de dfp en la frontera del territorio de cualquier otra administración, deberán realizarse los cálculos y verificaciones correspondientes, teniendo en cuenta la información pertinente, con el acuerdo mutuo de ambas administraciones (administración responsable de la estación terrenal y administración responsable de la estación terrena), y con la asistencia de la Oficina si así se solicita. En caso de desacuerdo, el cálculo y la verificación de la dfp los realizará la Oficina, teniendo en cuenta la información antes indicada. Las estaciones del servicio móvil en la banda 3 400-3 600 MHz no reclamarán contra las estaciones espaciales más protección que la que figura en el Cuadro 21-4 del Reglamento de Radiocomunicaciones (edición de 2004). Esta atribución entrará en vigor el 17 de noviembre de 2010. (CMR-07)
471	5.431	Atribución adicional: en Alemania, Israel y Reino Unido, la banda 3 400-3 475 MHz está también atribuida, a título secundario, al servicio de aficionados. (CMR-03)
472	5.431A	Categoría de servicio diferente: en Argentina, Brasil, Chile, Costa Rica, Cuba República Dominicana, El Salvador, Guatemala, México, Paraguay, Suriname, Uruguay, Venezuela y Departamentos y Territorios de Ultramar de Francia en la Región 2, la banda 3 400-3 500 MHz está atribuida al servicio móvil, salvo móvil aeronáutico, a título primario, a reserva de obtener el acuerdo con otras administraciones de conformidad con el número 9.21. Las estaciones del servicio móvil en la banda 3 400-3 500 MHz no reclamarán contra las estaciones espaciales más protección que la que figura en el Cuadro 21-4 del Reglamento de Radiocomunicaciones (Edición de 2004). (CMR-07)
473	5.432	Categoría de servicio diferente: en la República de Corea, Japón y Pakistán, la atribución de la banda 3 400-3 500 MHz al servicio móvil, salvo móvil aeronáutico, es a título primario (véase el número 5.33). (CMR-00)
618	5.517	En la Región 2 el servicio fijo por satélite (espacio-Tierra) en la banda 17,7-17,8 GHz no deberá causar interferencia perjudicial ni reclamar protección contra las asignaciones del servicio de radiodifusión por satélite que funciona de conformidad con el Reglamento de Radiocomunicaciones. (CMR-07)
474	5.432A	En, Corea (República de), Japón y Pakistán, la banda 3 400-3 500 MHz está identificada para las Telecomunicaciones Móviles Internacionales (IMT). Esta identificación no impide la utilización de esta banda por cualquier aplicación de los servicios a los que está atribuida, ni establece prioridad alguna en el Reglamento de Radiocomunicaciones. En la etapa de coordinación también se aplican las disposiciones de los números 9.17 y 9.18. Antes de que una administración ponga en servicio una estación (base o móvil) del servicio móvil en esta banda, deberá garantizar que la densidad de flujo de potencia (dfp) producida a 3 m sobre el suelo no supera el valor de – 154,5 dBW/(m2 ⋅4 kHz) durante más del 20% del tiempo en la frontera del territorio de cualquier otra administración. Este límite puede rebasarse en el territorio de cualquier país cuya administración así lo acepte. Para garantizar que se satisface el límite de dfp en la frontera del territorio de cualquier otra administración, deben realizarse los cálculos y verificaciones correspondientes, teniendo en cuenta toda la información pertinente, con el mutuo acuerdo de ambas administraciones (administración responsable de la estación terrenal y administración responsable de la estación terrena), y con la asistencia de la Oficina si así se solicita. En caso de desacuerdo, el cálculo y la verificación de la dfp los realizará la Oficina teniendo en cuenta la información antes indicada. Las estaciones del servicio móvil en la banda 3 400-3 500 MHz no reclamarán contra las estaciones espaciales más protección que la que figura en el Cuadro 21-4 del Reglamento de Radiocomunicaciones (Edición de 2004). (CMR- 07)
475	5.432B	Categoría de servicio diferente: en Bangladesh, China, India, Irán (República Islámica del), Nueva Zelandia, Singapur y Colectividades francesas de Ultramar de la Región 3, la banda 3 400-3 500 MHz está atribuida al servicio móvil, salvo móvil aeronáutico, a título primario, a reserva de obtener el acuerdo con otras administraciones de conformidad con el número 9.21, y está identificada para las Telecomunicaciones Móviles Internacionales (IMT). Esta identificación no impide la utilización de esta banda por cualquier aplicación de los servicios a los que está atribuida, ni establece prioridad alguna en el Reglamento de Radiocomunicaciones. En la etapa de coordinación también se aplican las disposiciones de los números 9.17 y 9.18. Antes de que una administración ponga en servicio una estación (de base o móvil) del servicio móvil en esta banda, deberá garantizar que la densidad de flujo de potencia (dfp) producida a 3 m sobre el suelo no supera el valor de −154,5 dB(W/(m2 ⋅4 kHz)) durante más del 20% del tiempo en la frontera del territorio de cualquier otra administración. Este límite puede rebasarse en el territorio de cualquier país cuya administración así lo acepte. Para asegurar que se satisface el límite de dfp en la frontera del territorio de cualquier otra administración, deberán realizarse los cálculos y verificaciones correspondientes, teniendo en cuenta la información pertinente, con el acuerdo mutuo de ambas administraciones (administración responsable de la estación terrenal y administración responsable de la estación terrena), y con la asistencia de la Oficina si así se solicita. En caso de desacuerdo, el cálculo y la verificación de la dfp los realizará la Oficina, teniendo en cuenta la información antes indicada. Las estaciones del servicio móvil en la banda 3 400-3 500 MHz no reclamarán contra las estaciones espaciales más protección que la que figura en el Cuadro 21-4 del Reglamento de Radiocomunicaciones (Edición de 2004). Esta atribución entrará en vigor el 17 de noviembre de 2010. (CMR-07)
476	5.433	En las Regiones 2 y 3, la banda 3 400 - 3 600 MHz se atribuye al servicio de radiolocalización a título primario. Sin embargo, se insta a todas las administraciones que explotan sistemas de radiolocalización en esta banda a que cesen de hacerlo antes de 1985; a partir de este momento, las administraciones deberán tomar todas las medidas prácticamente posibles para proteger el servicio fijo por satélite, sin imponerse a este último servicio condiciones en materia de coordinación.
477	5.433A	En Bangladesh, China, Corea (Rep. de), India, Irán (República Islámica del), Japón, Nueva Zelandia, Pakistán, y Colectividades francesas de Ultramar de la Región 3, la banda 3 500-3 600 MHz está identificada para las Telecomunicaciones Móviles Internacionales (IMT). Esta identificación no impide la utilización de esta banda por cualquier aplicación de los servicios a los que está atribuida ni establece prioridad alguna en el Reglamento de Radiocomunicaciones. En la etapa de coordinación también se aplican las disposiciones de los números 9.17 y 9.18. Antes de que una administración ponga en servicio una estación (de base o móvil) del servicio móvil en esta banda, deberá garantizar que la densidad de flujo de potencia (dfp) producida a 3 m sobre el suelo no supera el valor de – 154,5 dB(W/(m2 ⋅4 kHz)) durante más del 20% del tiempo en la frontera del territorio de cualquier otra administración. Este límite puede rebasarse en el territorio de cualquier país cuya administración así lo acepte. Para garantizar que se satisface el límite de dfp en la frontera del territorio de cualquier otra administración, deben realizarse los cálculos y verificaciones correspondientes, teniendo en cuenta toda la información pertinente, con el acuerdo mutuo de ambas administraciones (administración responsable de la estación terrenal y administración responsable de la estación terrena), y con la asistencia de la Oficina si así se solicita. En caso de desacuerdo, el cálculo y la verificación de la dfp los realizará la Oficina teniendo en cuenta la información antes indicada. Las estaciones del servicio móvil en la banda 3 500-3 600 MHz no reclamarán contra las estaciones espaciales más protección que la que figura en el Cuadro 21-4 del Reglamento de Radiocomunicaciones (Edición de 2004). (CMR-07)
478	5.434	SUP-CMR-97.
479	5.435	En Japón, el servicio de radiolocalización se excluye de la banda 3 620 - 3 700 MHz.
480	5.436	No utilizado.
481	5.437	SUP - CMR 2000.
482	5.438	La utilización de la banda 4 200 - 4 400 MHz por el servicio de radionavegación aeronáutica se reserva exclusivamente a los radioaltímetros instalados a bordo de aeronaves y a los respondedores asociados instalados en tierra. Sin embargo, puede autorizarse en esta banda, a título secundario, la detección pasiva en los servicios de exploración de la Tierra por satélite y de investigación espacial (los radioaltímetros no proporcionarán protección alguna).
483	5.439	Atribución adicional: en la República Islámica del Irán y Libia, la banda 4 200-4 400 MHz está también atribuida, a título secundario, al servicio fijo. (CMR-00)
484	5.440	El servicio de frecuencias patrón y señales horarias por satélite puede ser autorizado a utilizar la frecuencia de 4 202 MHz para las emisiones de espacio-Tierra y la frecuencia de 6 427 MHz para las emisiones Tierra-espacio. Tales emisiones deberán estar contenidas dentro de los límites de ± 2 MHz de dichas frecuencias, a reserva de obtener el acuerdo indicado en el número 9.21.
485	5.440A	En la Región 2 (salvo Brasil, Cuba, Departamentos y colectividades franceses de Ultramar, Guatemala, Paraguay, Uruguay y Venezuela) y en Australia, la banda 4 400-4 940 MHz puede utilizarse para la telemedida móvil aeronáutica para pruebas en vuelo con estaciones de aeronaves (véase el número 1.83). Esta utilización ha de ser conforme a la Resolución 416 (CMR-07) y no podrá causar interferencia perjudicial a los servicios fijo y fijo por satélite ni reclamar protección contra los mismos. Dicha utilización no impide que estas bandas sean utilizadas por otras aplicaciones del servicio móvil o por otros servicios a los que estas bandas se han atribuido a título primario con igualdad de derechos y no establece ninguna prioridad en el Reglamento de Radiocomunicaciones. (CMR-07)
619	5.518	SUP – CMR 2007.
486	5.441	La utilización de las bandas 4 500-4 800 MHz (espacio-Tierra) y 6 725-7 025 MHz (Tierra-espacio) por el servicio fijo por satélite se ajustará a las disposiciones del apéndice 30B. La utilización de las bandas 10,7-10,95 GHz (espacio-Tierra), 11,2-11,45 GHz (espacio-Tierra) y 12,75-13,25 GHz (Tierra-espacio) por los sistemas de satélites geoestacionarios del servicio fijo por satélite se ajustará a las disposiciones del apéndice 30B. La utilización de las bandas 10,7-10,95 GHz (espacio-Tierra), 11,2-11,45 GHz (espacio-Tierra) y 12,75-13,25 GHz (Tierra- espacio) por un sistema de satélites no geoestacionarios del servicio fijo por satélite se ajustará a lo dispuesto en el número 9.12 para la coordinación con otros sistemas de satélites no geoestacionarios del servicio fijo por satélite. Los sistemas de satélites no geoestacionarios del servicio fijo por satélite no reclamarán protección con relación a las redes de satélites geoestacionarios del servicio fijo por satélite que funcionen de conformidad con el Reglamento de Radiocomunicaciones, sea cual sea la fecha en que la oficina reciba la información completa de coordinación o de notificación, según el caso, de los sistemas del SFS no OSG y la información completa de coordinación o de notificación, según el caso, de las redes OSG. El número 5.43 no se aplica. Los sistemas de satélites no geoestacionarios del SFS se explotarán en las bandas precitadas de forma que cualquier interferencia inaceptable que pueda producirse durante su explotación se elimine rápidamente. (CMR-00)
487	5.442	En las bandas 4 825-4 835 MHz y 4 950-4 990 MHz, la atribución al servicio móvil está limitada al servicio móvil, salvo móvil aeronáutico. En la Región 2 (salvo Brasil, Cuba, Guatemala, Paraguay, Uruguay y Venezuela) y en Australia, la banda 4 825-4 835 MHz también está atribuida al servicio móvil aeronáutico, exclusivamente con miras a la telemedida móvil aeronáutica (TMA) para pruebas en vuelo por estaciones de aeronaves. Esta utilización ha de ser conforme a la Resolución 416 (CMR-07) y no se deberá causar interferencia perjudicial a los servicios fijos. (CMR-07)
488	5.443	Categoría de servicio diferente: en Argentina, Australia y Canadá, la atribución de las bandas 4 825 - 4 835 MHz y 4 950 - 4 990 MHz al servicio de radioastronomía es a título primario (véase el número 5.33).
489	5.443A	SUP-CMR-03
490	5.443B	Para no causar interferencia al sistema de aterrizaje por microondas que funciona por encima de 5 030 MHz, la densidad de flujo de potencia combinada producida en la superficie de la Tierra en la banda 5 030-5 150 MHz por todas las estaciones espaciales de cualquier sistema de radionavegación por satélite (espacio-Tierra) que funciona en la banda 5 010-5 030 MHz no debe rebasar el nivel de – 124,5 dB(W/m^2) en una anchura de banda de 150 kHz. Para no causar interferencia perjudicial al servicio de radioastronomía en la banda 4 990-5 000 MHz, los sistemas del servicio de radionavegación por satélite que funcionan en la banda 5 010-5 030 MHz deberán cumplir los límites aplicables a la banda 4 990-5 000 MHz, definidos en la Resolución 741 (CMR-03). (CMR-03)
491	5.444	La banda 5 030-5 150 MHz se utilizará para el sistema internacional normalizado (sistema de aterrizaje por microondas) para la aproximación y el aterrizaje de precisión. En la banda 5 030-5 091 MHz se dará prioridad a las necesidades de este sistema sobre otras utilizaciones de esta banda. Para la utilización de la banda 5 091-5 150 MHz se aplicará el número 5.444A y la Resolución 114 (Rev.CMR-03). (CMR-07)
492	5.444A	Atribución adicional: la banda 5 091-5 150 MHz también está atribuida al servicio fijo por satélite (Tierra-espacio) a título primario. La atribución está limitada a los enlaces de conexión de los sistemas de satélites no geoestacionarios del servicio móvil y está sujeta a la coordinación prevista en el número 9.11A. En la banda 5 091-5 150 MHz, se aplican también las siguientes condiciones: – antes del 1 de enero de 2018, la utilización de la banda 5 091-5 150 MHz por los enlaces de conexión de los sistemas de satélites no geoestacionarios del servicio móvil por satélite se llevará a cabo de acuerdo con la Resolución 114 (Rev.CMR-03); – después del 1 de enero de 2012, no se efectuarán nuevas asignaciones a estaciones terrenas que provean enlaces de conexión para sistemas de satélites no geoestacionarios del servicio móvil por satélite; – después del 1 de enero de 2018 el servicio fijo por satélite pasará a tener categoría secundaria respecto del servicio de radionavegación aeronáutica. (CMR-07)
493	5.444B	La utilización de la banda 5 091-5 150 MHz por el servicio móvil aeronáutico estará limitada a: – los sistemas que funcionan en el servicio móvil aeronáutico (R) y de conformidad con las normas aeronáuticas internacionales, exclusivamente para aplicaciones de superficie en los aeropuertos. Dicha utilización se realizará de conformidad con la Resolución 748 (CMR-07); – las transmisiones de telemedida aeronáutica desde estaciones de aeronave (véase el número 1.83), de conformidad con la Resolución 418 (CMR-07); – las transmisiones de seguridad aeronáutica. Dicha utilización se realizará de conformidad con la Resolución 419 (CMR-07). (CMR-07)
494	5.445	No utilizado.
495	5.446	Atribución adicional: en los países mencionados en los números 5.369 y 5.400, la banda 5 150 - 5 216 MHz está también atribuida, a título primario, al servicio de radiodeterminación por satélite (espacio-Tierra), a reserva de obtener el acuerdo indicado en el número 9.21. En la Región 2, esta banda está también atribuida, a título primario, al servicio de radiodeterminación por satélite (espacio-Tierra). En las Regiones 1 y 3, excepto en los países mencionados en los número5.369 y 5.400, esta banda está también atribuida, a título secundario, al servicio de radiodeterminación por satélite (espacio-Tierra). El uso de esta banda por el servicio de radiodeterminación por satélite está limitado a los enlaces de conexión del servicio de radiodeterminación por satélite que funciona en las bandas 1 610 - 1 626,5 MHz y/ó 2 483,5 - 2 500 MHz. La densidad de flujo de potencia total en la superficie de la Tierra no podrá 2 exceder en ningún caso de – 159 dB(W/m ) en cualquier ancho de banda de 4 kHz para todos los ángulos de llegada.
496	5.446A	La utilización de las bandas 5 150-5 350 MHz y 5 470-5 725 MHz por las estaciones del servicio móvil, salvo el servicio móvil aeronáutico, y se ajustará a lo dispuesto en la Resolución 229 (CMR-03). (CMR-07)
497	5.446B	En la banda 5 150-5 250 MHz, las estaciones del servicio móvil no reclamarán protección contra las estaciones terrenas del servicio fijo por satélite. No se aplican las disposiciones del número 5.43A al servicio móvil con respecto a las estaciones terrenas del servicio fijo por satélite. (CMR-03)
498	5.446C	Atribución adicional: en la Región 1 (salvo en Argelia, Arabia Saudita, Bahrein, Egipto, Emiratos Árabes Unidos, Jordania, Kuwait, Líbano, Marruecos, Omán, Qatar, República Árabe Siria, Sudán y Túnez) y en Brasil, la banda 5 150-5 250 MHz también está atribuida a título primario al servicio móvil aeronáutico, exclusivamente para las transmisiones de telemedida aeronáutica desde estaciones de aeronave (véase el número 1.83), de conformidad con la Resolución 418 (CMR-07). Dichas estaciones no reclamarán protección contra otras estaciones que funcionen de conformidad con el Artículo 5. No se aplica el número 5.43A. (CMR-07)
499	5.447	Atribución adicional: en Côte d'Ivoire, Israel, Líbano, Pakistán, República Árabe Siria y Túnez, la banda 5 150-5 250 MHz está también atribuida, a título primario, al servicio móvil, a reserva de obtener el acuerdo indicado en el número 9.21. En este caso no se aplican las disposiciones de la Resolución 229 (CMR-03). (CMR-07)
500	5.447A	La atribución al servicio Fijo por satélite (Tierra-espacio) está limitada a los enlaces de conexión de los sistemas de satélites no geoestacionarios del servicio móvil por satélite y está sujeta a la coordinación a tenor del número 9.11A.
501	5.447B	Atribución adicional : la banda 5 150 - 5 216 MHz está también atribuida a título primario al servicio Fijo por satélite (espacio-Tierra). Esta atribución está limitada a los enlaces de conexión de los sistemas de satélites no geoestacionarios del servicio móvil por satélite y está sujeta a la coordinación a tenor de la Resolución 46 (Rev.CMR-95)/del número 9.11A. La densidad de flujo de potencia en la superficie de la Tierra producida por las estaciones espaciales del servicio Fijo por satélite que funcionen en el sentido espacio-Tierra en la banda 5 150 - 5 216 MHz no deberá rebasar en ningún caso el valor de -164 dB(W/m^2) en cualquier banda de 4 kHz para todos los ángulos de llegada.
502	5.447C	Las administraciones responsables de las redes del servicio Fijo por satélite en la banda 5 150 - 5 250 MHz que funcionen con arreglo a los números 5.447A y 5.447B coordinarán en igualdad de condiciones, sujetas a la coordinación a tenor del número 9.11A, con las administraciones responsables de las redes de satélites no geoestacionarios que funcionen con arreglo al número 5.446 y puestas en funcionamiento antes del 17 de noviembre de 1995. Las redes de satélites que funcionen con arreglo al número 5.446 puestas en funcionamiento después del 17 de noviembre de 1995 no causarán interferencia perjudicial a las estaciones del servicio Fijo por satélite que funcionen con arreglo a los número5.447A y 5.447B ni reclamarán protección contra la misma.
503	5.447D	La atribución de la banda 5 250 –5 255 MHz al servicio de investigación espacial a título primario está limitada a los sensores activos a bordo de vehículos espaciales. Otra utilización de la banda por el servicio de investigación espacial es a título secundario. (CMR-97)
504	5.447E	Atribución adicional: la banda 5 250-5 350 MHz está también atribuida a título primario al servicio fijo en los siguientes países de la Región 3: Australia, Corea (Rep. de), India, Indonesia, Irán (República Islámica del), Japón, Malasia, Papua Nueva Guinea, Filipinas, Rep. Pop. Dem. de Corea, Sri Lanka, Tailandia y Viet Nam. Se incluye la utilización de esta banda por el servicio fijo para la implementación de los sistemas fijos de acceso inalámbrico y deberá ser conforme con la Recomendación UIT-R F.1613. Además, el servicio fijo no reclamará protección contra el servicio de radiodeterminación, el servicio de exploración de la Tierra por satélite (activo) y el servicio de investigación espacial (activo), aunque las disposiciones del número 5.43A no se aplican al servicio fijo con respecto al servicio de exploración de la Tierra por satélite (activo) y al servicio de investigación espacial (activo). Tras la implementación de los sistemas fijos de acceso inalámbrico del servicio fijo con protección de los sistemas de radiodeterminación existentes, las futuras aplicaciones del servicio de radiodeterminación no impondrán restricciones más estrictas a los sistemas fijos de acceso inalámbrico del servicio fijo. (CMR-07)
505	5.447F	En la banda 5 250-5 350 MHz, las estaciones del servicio móvil no reclamarán protección contra los servicios de radiolocalización, de exploración de la Tierra por satélite (activo) y de investigación espacial (activo). Estos servicios no impondrán al servicio móvil, basándose en las características del sistema y en los criterios de interferencia, criterios de protección más estrictos que los previstos en las Recomendaciones UIT-R M.1638 y UIT-R SA.1632. (CMR-03)
506	5.448	Atribución adicional: en Azerbaiyán, Jamahiriya Árabe Libia, Mongolia, Kirguistán, Eslovaquia, Rumania y Turkmenistán, la banda 5 250-5 350 MHz está también atribuida, a título primario, al servicio de radionavegación. (CMR-03)
507	5.448A	Los servicios de exploración de la Tierra por satélite (activo) y de investigación espacial (activo) en la banda de frecuencia5 250-5 350 MHz no reclamarán protección contra el servicio de radiolocalización. No se aplica el número 5.43A. (CMR-03)
508	5.448B	El servicio de exploración de la Tierra por satélite (activo) y el servicio de investigación espacial (activo) que funcionan en la banda de frecuencia5 350-5 570 MHz no ocasionarán interferencia perjudicial al servicio de radionavegación aeronáutica en la banda 5 350-5 460 MHz, ni al servicio de radionavegación en la banda 5 460-5 470 MHz ni al servicio de radionavegación marítima en la banda 5 470-5 570 MHz. (CMR-03)
509	5.448C	El servicio de investigación espacial (activo) que funciona en la banda 5 350-5 460 MHz no debe ocasionar interferencia perjudicial a otros servicios a los cuales esta banda se encuentra atribuida ni tampoco reclamar protección contra esos servicios. (CMR-03)
510	5.448D	En la banda de frecuencias 5 350-5 470 MHz, las estaciones del servicio de radiolocalización no causarán interferencia perjudicial a los sistemas de radares del servicio de radionavegación aeronáutica que funcionen de conformidad con el número 5.449, ni reclamarán protección contra ellos. (CMR-03)
511	5.449	La utilización de la banda 5 350 - 5 470 MHz por el servicio de radionavegación aeronáutica se limita a los radares aeroportados y a las radiobalizas de a bordo asociadas.
512	5.450	Atribución adicional: en Austria, Azerbaiyán, Irán (Rep. Islámica del), Mongolia, Kirguistán, Rumania, Turkmenistán y Ucrania, la banda 5 470-5 650 MHz está también atribuida, a título primario, al servicio de radionavegación aeronáutica. (CMR-03)
513	5.450A	En la banda 5 470 - 5 725 MHz, las estaciones del servicio móvil no reclamarán protección contra los servicios de radiodeterminación. Los servicios de radiodeterminación no impondrán al servicio móvil, basándose en las características del sistema y en los criterios de interferencia, criterios de protección más estrictos que los previstos en la Recomendación UIT R M.1638. (CMR 03)
514	5.450B	En la banda de frecuencias 5 470 - 5 650 MHz, las estaciones del servicio de radiolocalización, excepto los radares en tierra utilizados con fines meteorológicos en la banda 5 600-5 650 MHz, no causarán interferencia perjudicial a los sistemas de radares del servicio de radionavegación marítima, ni reclamarán protección contra ellos. (CMR 03)
515	5.451	Atribución adicional: en el Reino Unido, la banda 5 470 - 5 850 MHz está también atribuida, a título secundario al servicio móvil terrestre. En la banda 5 725 - 5 850 MHz son aplicables los límites de potencia indicados en los números 21.2, 21.3, 21.4 y 21.5.
516	5.452	Los radares instalados en tierra, que funcionan en la banda 5 600 - 5 650 MHz para las necesidades de la meteorología, están autorizados a funcionar sobre una base de igualdad con las estaciones del servicio de radionavegación marítima.
517	5.453	Atribución adicional: en Arabia Saudita, Bahrein, Bangladesh, Brunei Darussalam, Camerún, China, Congo, Côte d'Ivoire, Kenya, Corea (Rep. de), Egipto, Emiratos Árabes Unidos, Gabón, Guinea, Guinea Ecuatorial, India, Indonesia, Irán (Rep. Islámica del), Iraq, Israel, Japón, Jordania, Kuwait, Líbano, Jamahiriya Árabe Libia, Madagascar, Malasia, Nigeria, Omán, Pakistán, Filipinas, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Singapur, Sri Lanka, Swazilandia, Tanzanía, Togo, Chad, Tailandia, Viet Nam y Yemen, la banda 5 650- 5 850 MHz está también atribuida, a título primario, a los servicios fijo y móvil. En este caso no se aplican las disposiciones de la Resolución 229 (CMR-03). (CMR-03)
518	5.454	Categoría de servicio diferente: en Azerbaiyán, Federación de Rusia, Georgia, Mongolia, Kirguistán, Tayikistán y Turkmenistán, la atribución de la banda 5 670-5 725 MHz al servicio de investigación espacial es a título primario (véase el número 5.33). (CMR-07)
519	5.455	Atribución adicional: en Armenia, Azerbaiyán, Belarús, Cuba, Federación de Rusia, Georgia, Hungría, Kazajstán, Moldova, Mongolia, Uzbekistán, Kirguistán, Tayikistán, Turkmenistán y Ucrania, la banda 5 670-5 850 MHz está también atribuida, a título primario, al servicio fijo. (CMR-07)
520	5.456	Atribución adicional: en Camerún, la banda 5 755-5 850 MHz está también atribuida, a título primario, al servicio fijo. (CMR-03)
521	5.457	No utilizado.
522	5.457A	En las bandas 5 925-6 425 MHz y 14-14,5 GHz, las estaciones terrenas situadas a bordo de barcos pueden comunicar con las estaciones espaciales del servicio fijo por satélite. Esta utilización deberá ser conforme con la Resolución 902 (CMR-03). (CMR-03)
523	5.457B	En las bandas 5 925-6 425 MHz y 14-14,5 GHz, las estaciones terrenas situadas a bordo de barcos pueden funcionar con las características y en las condiciones que figuran en la Resolución 902 (CMR-03) en Argelia, Arabia Saudita, Bahrein, Comoras, Djibuti, Egipto, Emiratos Árabes Unidos, Jamahiriya Árabe Libia, Jordania, Kuwait, Marruecos, Mauritania, Omán, Qatar, República Árabe Siria, Sudán, Túnez y Yemen, así como en el servicio móvil marítimo por satélite a título secundario; tal utilización se efectuará de conformidad con la Resolución 902 (CMR-03). (CMR-03)
524	5.457C	En la Región 2 (salvo Brasil, Cuba, Departamentos y colectividades franceses de Ultramar, Guatemala, Paraguay, Uruguay y Venezuela), la banda 5 925-6 700 MHz puede utilizarse para la telemedida móvil aeronáutica para pruebas en vuelo por estaciones de aeronaves (véase el número 1.83). Esta utilización ha de ser conforme a la Resolución 416 (CMR-07) y no se deberá causar interferencia perjudicial a los servicios fijo y fijo por satélite ni reclamar protección contra los mismos. Dicha utilización no impide que estas bandas sean utilizadas por otras aplicaciones del servicio móvil o por otros servicios a los que se han atribuido estas bandas a título primario con igualdad de derechos y no establece ninguna prioridad en el Reglamento de Radiocomunicaciones. (CMR-07)
525	5.458	En la banda 6 425 - 7 075 MHz, se llevan a cabo mediciones con sensores pasivos de microondas por encima de los océanos. En la banda 7 075 - 7 250 MHz, se realizan mediciones con sensores pasivos de microondas. Conviene que las administraciones tengan en cuenta las necesidades de los servicios de exploración de la Tierra por satélite (pasivo) y de investigación espacial (pasivo) en la planificación de la utilización futura de las bandas 6 425 - 7 025 MHz y 7 075 - 7 250 MHz.
526	5.458A	Al hacer asignaciones en la banda 6 700 - 7 075 MHz a estaciones espaciales del servicio Fijo por satélite, se insta a las administraciones a que adopten todas las medidas posibles para proteger las observaciones de las rayas espectrales del servicio de radioastronomía en la banda 6 650 - 6 675,2 MHz contra la interferencia perjudicial procedente de emisiones no deseadas.
527	5.458B	La atribución espacio-Tierra al servicio Fijo por satélite en la banda 6 700 - 7 075 MHz está limitada a enlaces de conexión para sistemas de satélites no geoestacionarios del servicio móvil por satélite y está sujeta a la coordinación a tenor de la Resolución 46 (Rev.CMR-95)/del número 9.11A. La utilización de la banda 6 700 - 7 075 MHz (espacio-Tierra) para enlaces de conexión de sistemas de satélites no geoestacionarios del servicio móvil por satélite no está sujeta al número 22.2.
528	5.458C	Las administraciones que sometan asignaciones en la banda 7 025 - 7 075 MHz (Tierra- espacio) para sistemas de satélite del SFS con satélites geoestacionarios (OSG) después del 17 de noviembre de 1995 consultarán, sobre la base de las Recomendaciones UIT-R pertinentes, a las administraciones que han notificado y puesto en servicio sistemas de satélite no geoestacionarios en esta banda de frecuencias antes del 18 de noviembre de 1995 a petición de estas últimas administraciones. Esta consulta se hará con miras a facilitar las operaciones compartidas de los sistemas del SFS/OSG y no-OSG en esta banda.
529	5.459	Atribución adicional: en la Federación de Rusia, las bandas de frecuencias 7 100- 7 155 MHz y 7 190-7 235 MHz están también atribuidas, a título primario, al servicio de operaciones espaciales (Tierra-espacio) a reserva de obtener el acuerdo indicado en el número 9.21. (CMR-97).
530	5.460	La utilización de la banda 7 145-7 190 MHz por el servicio de investigación espacial (Tierra-espacio) está limitada al espacio lejano; no se efectuará ninguna emisión destinada al espacio lejano en la banda 7 190-7 235 MHz. Los satélites geoestacionarios del servicio de investigación espacial que funcionan en la banda 7 190-7 235 MHz no reclamarán protección respecto de los sistemas actuales y futuros de los servicios fijo y móvil y no se aplicará el número 5.43A. (CMR-03)
531	5.461	Atribución adicional: las bandas 7 250 - 7 375 MHz (espacio-Tierra) y 7 900 - 8 025 MHz (Tierra-espacio) están también atribuidas, a título primario, al servicio móvil por satélite a reserva de obtener el acuerdo indicado en el número 9.21.
532	5.461A	La utilización de las bandas de frecuencias 7 450 – 7 550 MHz por el servicio de meteorología por satélite (espacio-Tierra) queda circunscrita a los sistemas de satélites geoestacionarios. Los sistemas de meteorología por satélites no geoestacionarios notificados antes del 30 de noviembre de 1997 en dicha banda pueden continuar funcionando a título primario hasta el final de su vida útil. (CMR-97)
533	5.461B	La utilización de las bandas de frecuencias 7 750 – 7 850 MHz por el servicio de meteorología por satélite (espacio-Tierra) está limitada a los sistemas de satélites no geoestacionarios. (CMR-97)
534	5.462	SUP-CMR-97.
560	5.480	Atribución adicional: en Argentina, Brasil, Chile, Costa Rica, Cuba, El Salvador, Ecuador, Guatemala, Honduras, México, Paraguay, Antillas Neerlandesas, Perú y Uruguay la banda 10-10,45 GHz está también atribuida, a título primario a los servicios fijo y móvil. En Venezuela, está también atribuida al servicio fijo a título primario. (CMR-07)
639	5.530	En las Regiones 1 y 3 la utilización de la banda 21,4-22 GHz por el servicio de radiodifusión por satélite está sujeta a las disposiciones de la Resolución 525 (Rev.CMR-07). (CMR-07)
535	5.462A	En las Regiones 1 y 3 (salvo en Japón), en la banda 8 025 – 8 400 MHz, el servicio de explotación de la Tierra por satélite que utiliza satélites geoestacionarios no deberá producir una densidad de flujo de potencia superior a los siguientes valores provisionales para un ángulo de llegada (θ), sin el consentimiento de la administración afectada. -174 dB (W/m^2) en una banda de 4 KHz para 0^°≤θ<5^° -174 + 0,5 (θ- 5) dB(W/m^2) en una banda de 4 KHz para 5^°≤θ<25^° -164 dB(W/m^2) en una banda de 4 KHz para 25^°≤θ≤90^° Estos valores son motivo de estudio según la Resolución 124 (CMR-97). (CMR-97)
536	5.463	No se permite a las estaciones de aeronave transmitir en la banda 8 025 - 8 400 MHz. (CMR-97)
537	5.464	SUP - CMR 1997.
538	5.465	En el servicio de investigación espacial, la utilización de la banda 8 400 - 8 450 MHz está limitada al espacio lejano.
539	5.466	Categoría de servicio diferente: en Israel, Singapur y Sri Lanka, la atribución de la banda 8 400-8 500 MHz, al servicio de investigación espacial es a título secundario (véase el número 5.32). (CMR-03)
540	5.467	SUP - CMR 2003.
541	5.468	Atribución adicional: en Arabia Saudita, Bahrein, Bangladesh, Brunei Darussalam, Burundi, Camerún, China, Congo, Costa Rica, Egipto, Emiratos Árabes Unidos, Gabón, Guyana, Indonesia, Irán (Rep. Islámica del), Iraq, Jamaica, Jordania, Kenya, Kuwait, Líbano, Jamahiriya Árabe Libia, Malasia, Malí, Marruecos, Mauritania, Nepal, Nigeria, Omán, Pakistán, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Senegal, Singapur, Somalia, Swazilandia, Tanzanía, Chad, Togo, Túnez y Yemen, la banda 8 500-8 750 MHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-03)
542	5.469	Atribución adicional: en Armenia, Azerbaiyán, Belarús, Georgia, Hungría, Lituania, Moldova, Mongolia, Uzbekistán, Polonia, Kirguistán, Rep. Checa, Rumania, Federación de Rusia, Tayikistán, Turkmenistán y Ucrania, la banda 8 500-8 750 MHz está también atribuida, a título primario, a los servicios móvil terrestre y de radionavegación. (CMR-03)
543	5.469A	En la banda 8 550-8 650 MHz, las estaciones del servicio de exploración de la Tierra por satélite (activo) y del servicio de investigación espacial (activo) no causarán interferencia perjudicial a las estaciones de los servicios de radiolocalización ni limitarán su utilización o desarrollo. (CMR-97)
544	5.470	La utilización de la banda 8 750 - 8 850 MHz por el servicio de radionavegación aeronáutica se limita a las ayudas a la navegación a bordo de aeronaves que utilizan el efecto Doppler con una frecuencia central de 8 800 MHz.
545	5.471	Atribución adicional: en Argelia, Alemania, Arabia Saudita, Bahrein, Bélgica, China, Egipto, Emiratos Árabes Unidos, Francia, Grecia, Indonesia, Irán (República Islámica del), Jamahiriya Árabe Libia, Países Bajos, Qatar y Sudán, las bandas 8 825-8 850 MHz y 9 000-9 200 MHz están también atribuidas, a título primario, al servicio de radionavegación marítima sólo para los radares costeros. (CMR-07)
546	5.472	En las bandas 8 850 - 9 000 MHz y 9 200 - 9 225 MHz, el servicio de radionavegación marítima está limitado a los radares costeros.
547	5.473	Atribución adicional: en Armenia, Austria, Azerbaiyán, Belarús, Cuba, Federación de Rusia, Georgia, Hungría, Mongolia, Uzbekistán, Polonia, Kirguistán, Rumania, Tayikistán, Turkmenistán y Ucrania, las bandas 8 850-9 000 MHz y 9 200-9 300 MHz están también atribuidas, a título primario, al servicio de radionavegación. (CMR-07)
548	5.473A	En la banda 9 000-9 200 MHz las estaciones del servicio de radiolocalización no causarán interferencia perjudicial a los sistemas del servicio de radionavegación aeronáutica que figuran en el número 5.337, ni a los sistemas de radar del servicio de radionavegación marítima que funcionen en esta banda a título primario en los países enumerados en el número 5.471, ni reclamarán protección contra dichos sistemas. (CMR-07)
549	5.474	En la banda 9 200 - 9 500 MHz pueden utilizarse transpondedores de búsqueda y salvamento (SART), teniendo debidamente en cuenta la correspondiente Recomendación UIT- R (véase también el artículo 31).
550	5.475	La utilización de la banda 9 300-9 500 MHz, por el servicio de radionavegación aeronáutica se limita a los radares meteorológicos de aeronaves y a los radares instalados en tierra. Además, se permiten las balizas de radar del servicio de radionavegación aeronáutica instaladas en tierra en la banda 9 300-9 320 MHz, a condición de que no causen interferencia perjudicial al servicio de radionavegación marítima. (CMR-07)
551	5.475A	La utilización de la banda 9 300-9 500 MHz por el servicio de exploración de la Tierra por satélite (activo) y el servicio de investigación espacial (activo) se limita a los sistemas que requieren una anchura de banda superior a 300 MHz, la cual no puede acomodarse íntegramente en la banda 9 500-9 800 MHz. (CMR-07)
552	5.475B	En la banda 9 300-9 500 MHz las estaciones del servicio de radiolocalización no causarán interferencia perjudicial a los radares del servicio de radionavegación que funcionan de conformidad con el Reglamento de Radiocomunicaciones, ni reclamarán protección contra los mismos. Los radares en tierra utilizados con fines meteorológicos tendrán prioridad sobre cualquier otro uso de radiolocalización. (CMR-07)
553	5.476	SUP – CMR 2007.
554	5.476A	En la banda 9 300-9 800 MHz, las estaciones del servicio de exploración de la Tierra por satélite (activo) y del servicio de investigación espacial (activo) no causarán interferencia perjudicial a estaciones de los servicios de radionavegación y de radiolocalización ni reclamarán protección contra las mismas. (CMR-07)
555	5.477	Categoría de servicio diferente: en Argelia, Arabia Saudita, Bahrein, Bangladesh, Brunei Darussalam, Camerún, Egipto, Emiratos Árabes Unidos, Eritrea, Etiopía, Guyana, India, Indonesia, Irán (República Islámica del), Iraq, Jamaica, Japón, Jordania, Kuwait, Líbano, Liberia, Malasia, Nigeria, Omán, Pakistán, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Singapur, Somalia, Sudán, Trinidad y Tabago y Yemen, la atribución de la banda 9 800- 10 000 MHz al servicio fijo es a título primario (véase el número 5.33). (CMR-07)
556	5.478	Atribución adicional: en Azerbaiyán, Mongolia, Kirguistán, Rumania, Turkmenistán y Ucrania, la banda 9 800-10 000 MHz está también atribuida, a título primario, al servicio de radionavegación. (CMR-07)
557	5.478A	La utilización de la banda 9 800-9 900 MHz por el servicio de exploración de la Tierra por satélite (activo) y el servicio de investigación espacial (activo) se limita a sistemas que requieren una anchura de banda mayor que 500 MHz, la cual no puede acomodarse íntegramente en la banda 9 300-9 800 MHz. (CMR-07)
558	5.478B	En la banda 9 800-9 900 MHz las estaciones del servicio de exploración de la Tierra por satélite (activo) y el servicio de investigación espacial (activo) no causarán interferencia perjudicial a las estaciones del servicio fijo, a las que esta banda está atribuida a título secundario, ni reclamarán protección contra las mismas. (CMR-07)
561	5.481	Atribución adicional: en Alemania, Angola, Brasil, China, Costa Rica, Côte d'Ivoire, El Salvador, Ecuador, España, Guatemala, Hungría, Japón, Kenya, Marruecos, Nigeria, Omán, Uzbekistán, Paraguay, Perú, Rep. Pop. Dem. de Corea, Rumania, Tanzanía, Tailandia y Uruguay, la banda 10,45-10,5 GHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-07)
562	5.482	En la banda 10,6-10,68 GHz, la potencia suministrada a la antena de las estaciones de los servicios fijo y móvil, salvo móvil aeronáutico, no será superior a −3 dBW. Este límite puede rebasarse siempre y cuando se obtenga el acuerdo indicado en el número 9.21. Sin embargo, esta restricción impuesta a los servicios fijo y móvil, salvo móvil aeronáutico, no es aplicable en los países siguientes: Argelia, Arabia Saudita, Armenia, Azerbaiyán, Bahrein, Bangladesh, Belarús, Egipto, Emiratos Árabes Unidos, Georgia, India, Indonesia, Irán (República Islámica del), Iraq, Jamahiriya Árabe Libia, , Jordania, Kazajstán, Kuwait, Líbano, Marruecos, Mauritania, Moldova, Nigeria, Omán, Uzbekistán, Pakistán, Filipinas, Qatar, Singapur, República Árabe Siria, Túnez, Kirguistán, Tayikistán, Turkmenistán y Viet Nam. (CMR-07)
563	5.482A	Para la compartición de la banda 10,6-10,68 GHz entre el servicio de exploración de la Tierra por satélite (pasivo) y los servicios fijo y móvil, salvo móvil aeronáutico, se aplica la Resolución 751 (CMR-07). (CMR-07)
564	5.483	Atribución adicional: en Arabia Saudita, Armenia, Azerbaiyán, Bahrein, Belarús, China, Colombia, Corea (Rep. de), Costa Rica, Egipto, Emiratos Árabes Unidos, Georgia, Irán (República Islámica del), Iraq, Israel, Jordania, Kazajstán, Kuwait, Líbano, Mongolia, Qatar, Kirguistán, Rep. Pop. Dem. de Corea, Rumania, Tayikistán, Turkmenistán y Yemen, la banda 10,68-10,7 GHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. Este uso está limitado a los equipos que estuvieran en funcionamiento el 1 de enero de 1985. (CMR-07)
565	5.484	En la Región 1, la utilización de la banda 10,7 - 11,7 GHz por el servicio Fijo por satélite (Tierra-espacio) está limitada a los enlaces de conexión para el servicio de radiodifusión por satélite.
566	5.484A	La utilización de las bandas 10,95-11,2 GHz (espacio-Tierra), 11,45-11,7 GHz (espacio- Tierra), 11,7-12,2 GHz (espacio-Tierra) en la Región 2, 12,2-12,75 GHz (espacio-Tierra) en la Región 3, 12,5-12,75 GHz (espacio-Tierra) en la Región 1, 13,75-14,5 GHz (Tierra-espacio), 17,8-18,6 GHz (espacio-Tierra), 19,7-20,2 GHz (espacio-Tierra), 27,5-28,6 GHz (Tierra- espacio) y 29,5-30 GHz (Tierra-espacio) por un sistema de satélites no geoestacionarios del servicio fijo por satélite está sujeta a la aplicación de las disposiciones del número 9.12 para la coordinación con otros sistemas de satélites no geoestacionarios del servicio fijo por satélite. Los sistemas de satélites no geoestacionarios del servicio fijo por satélite no reclamarán protección con relación a las redes de satélitesgeoestacionarios del servicio fijo por satélite que funcionen de conformidad con el Reglamento de Radiocomunicaciones, sea cual sea la fecha en que la Oficina reciba la información completa de coordinación o de notificación, según proceda, de los sistemas del SFS no OSG y la información completa de coordinación o de notificación, según proceda, de las redes OSG. El número 5.43 no se aplica. Los sistemas de satélites no geoestacionarios del SFS se explotarán en las bandas precitadas de forma que cualquier interferencia inaceptable que pueda producirse durante su explotación se elimine rápidamente. (CMR-00)
567	5.485	En la Región 2, en la banda 11,7 - 12,2 GHz, los transpondedores de estaciones espaciales del servicio Fijo por satélite pueden ser utilizados adicionalmente para transmisiones del servicio de radiodifusión por satélite, a condición de que dichas transmisiones no tengan una p.i.r.e. máxima superior a 53 dBW por canal de televisión y no causen una mayor interferencia ni requieran mayor protección contra la interferencia que las asignaciones de frecuencia coordinadas del servicio Fijo por satélite. Con respecto a los servicios espaciales, esta banda será utilizada principalmente por el servicio Fijo por satélite.
568	5.486	Categoría de servicio diferente: en México y Estados Unidos, la atribución de la banda 11,7 - 12,1 GHz al servicio Fijo es a título secundario (véase el número 5.32).
569	5.487	En la banda 11,7-12,5 GHz, en las Regiones 1 y 3, los servicios fijo, fijo por satélite, móvil, salvo móvil aeronáutico, y de radiodifusión, según sus respectivas atribuciones, no causarán interferencias perjudiciales a las estaciones de radiodifusión por satélite que funcionen de acuerdo con el Plan para las Regiones 1 y 3 del Apéndice 30, ni reclamarán protección con relación a las mismas. (CMR-03)
570	5.487A	Atribución adicional : en la Región 1 la banda 11,7-12,5 GHz, en la Región 2 la banda 12,2-12,7 GHz y en la Región 3 la banda 11,7-12,2 GHz están también atribuidas, al servicio fijo por satélite (espacio-Tierra) a título primario y su utilización está limitada a los sistemas de satélites no geoestacionarios y sujeta a lo dispuesto en el número 9.12 para la coordinación con otros sistemas de satélites no geoestacionarios del servicio fijo por satélite. Los sistemas de satélites no geoestacionarios del servicio fijo por satélite no reclamarán protección con relación a las redes de satélites geoestacionarios del servicio de radiodifusión por satélite que funcionen de conformidad con el Reglamento de Radiocomunicaciones, sea cual sea la fecha en que la Oficina reciba la información completa de coordinación o de notificación, según proceda, de los sistemas de satélites no geoestacionarios del servicio fijo por satélite y la información completa de coordinación o de notificación, según proceda, de las redes de satélites geoestacionarios. El número 5.43A no se aplica. Los sistemas de satélites no geoestacionarios del servicio fijo por satélite se explotarán en las bandas precitadas de forma que cualquier interferencia inaceptable que pueda producirse durante su explotación se elimine rápidamente. (CMR-03)
571	5.488	La utilización de la banda 11,7-12,2 GHz por redes de satélites geoestacionarios del servicio fijo por satélite en la Región 2 está sujeta a la aplicación de las disposiciones del número 9.14 para la coordinación con estaciones de los servicios terrenales en las Regiones 1, 2 y 3. Para la utilización de la banda 12,2-12,7 GHz por el servicio de radiodifusión por satélite en la Región 2, véase el Apéndice 30. (CMR-03)
572	5.489	Atribución adicional: en Perú, la banda 12,1 - 12,2 GHz está también atribuida, a título primario, al servicio Fijo.
573	5.490	En la Región 2, en la banda 12,2 - 12,7 GHz, los servicios de radiocomunicación terrenal existentes y futuros no causarán interferencia perjudicial a los servicios de radiocomunicación espacial que funcionen de conformidad con el Plan de radiodifusión por satélite para la Región 2 que figura en el Apéndice 30.
574	5.491	SUP - CMR 2003.
592	5.504A	En la banda 14-14,5 GHz, las estaciones terrenas de aeronave del servicio móvil aeronáutico por satélite con categoría secundaria pueden funcionar con estaciones espaciales del servicio fijo por satélite. Las disposiciones de los números 5.29, 5.30 y 5.31 son aplicables. (CMR-03)
575	5.492	Las asignaciones a las estaciones del servicio de radiodifusión por satélite conformes al Plan regional pertinente o incluidas en la Lista de las Regiones 1 y 3 del apéndice 30 podrán ser utilizadas también para transmisiones del servicio fijo por satélite (espacio-Tierra), a condición de que dichas transmisiones no causen mayor interferencia ni requieran mayor protección contra la interferencias que las transmisiones del servicio de radiodifusión por satélite que funcionen de conformidad con este Plan o con la Lista, según sea el caso. (CMR-00)
576	5.493	En la Región 3, en la banda 12,5 - 12,75 GHz, el servicio de radiodifusión por satélite está limitado a una densidad de flujo de potencia que no rebase el valor de – 111 dB(W/m^2)/27 MHz para todas las condiciones y para todos los métodos de modulación en el borde de la zona de servicio. (CMR-97)
577	5.494	Atribución adicional: en Argelia, Angola, Arabia Saudita, Bahrein, Camerún, Centroafricana (Rep.), Congo, Côte d'Ivoire, Egipto, Emiratos Árabes Unidos, Eritrea, Etiopía, Gabón, Ghana, Guinea, Iraq, Israel, Jordania, Kuwait, Líbano, Jamahiriya Árabe Libia, Madagascar, Malí, Marruecos, Mongolia, Nigeria, Qatar, Rep. Dem. del Congo, República Árabe Siria, Somalia, Sudán, Chad, Togo y Yemen, la banda 12,5-12,75 GHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-03)
578	5.495	Atribución adicional: en Bosnia y Herzegovina, Francia, Grecia, Liechtenstein, Mónaco, Montenegro, Uganda, Rumania, Serbia, Suiza, Tanzanía y Túnez, la banda 12,5-12,75 GHz está también atribuida, a título secundario, a los servicios fijo y móvil, salvo móvil aeronáutico. (CMR-07)
579	5.496	Atribución adicional: en Austria, Azerbaiyán, Kirguistán y Turkmenistán, la banda 12,5- 12,75 GHz está también atribuida, a título primario, a los servicios fijo y móvil, salvo móvil aeronáutico. No obstante, las estaciones de estos servicios no deben causar interferencia perjudicial a las estaciones terrenas del servicio fijo por satélite de los países de la Región 1 distintos de los enumerados en esta nota. No se requiere ninguna coordinación de estas estaciones terrenas con las estaciones de los servicios fijo y móvil de los países enumerados en esta nota. En el territorio de los mismos, se aplicarán los límites de densidad de flujo de potencia en la superficie de la Tierra prescritos en el artículo 21, cuadro 21-4, para el servicio fijo por satélite. (CMR-00)
580	5.497	El servicio de radionavegación aeronáutica en la banda 13,25 - 13,4 GHz, se limitará a las ayudas a la navegación que utilizan el efecto Doppler.
581	5.498	SUP-CMR-97.
582	5.498A	Los servicios de exploración de la Tierra por satélite (activo) y de investigación espacial (activo) que funcionan en banda 13,25 – 13,4 GHz no ocasionarán interferencia perjudicial al servicio de radionavegación aeronáutica u obstaculizarán su utilización y desarrollo. (CMR-97)
583	5.499	Atribución adicional: en Bangladesh, India y Pakistán, la banda 13,25 - 14 GHz está también atribuida, a título primario, al servicio Fijo.
584	5.500	Atribución adicional: en Argelia, Angola, Arabia Saudita, Bahrein, Brunei Darussalam, Camerún, Egipto, Emiratos Árabes Unidos, Gabón, Indonesia, Irán (Rep. Islámica del), Iraq, Israel, Jordania, Kuwait, Líbano, Madagascar, Malasia, Malí, Malta, Marruecos, Mauritania, Nigeria, Pakistán, Qatar, República Árabe Siria, Singapur, Sudán, Chad y Túnez, la banda 13,4-14 GHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR- 03)
585	5.501	Atribución adicional: en Azerbaiyán, Hungría, Japón, Mongolia, Kirguistán, Rumania y Turkmenistán, la banda 13,4-14 GHz está también atribuida, a título primario, al servicio de radionavegación. (CMR-07)
586	5.501A	La atribución de la banda 13,4 – 13,75 GHz al servicio de investigación espacial a título primario está limitada a los sensores activos a bordo de vehículos espaciales. Otra utilización de la banda por el servicio de investigación espacial es a título secundario. (CMR-97)
587	5.501B	En la banda 13,4 – 13,75 GHz los servicios de exploración de la Tierra por satélite (activo) y de investigación espacial (activo) no causarán interferencia perjudicial al servicio de radiolocalización, ni limitarán su utilización y desarrollo. (CMR-97)
588	5.502	En la banda 13,75-14 GHz una estación terrena de una red geoestacionaria del servicio fijo por satélite tendrá un diámetro de antena mínimo de 1,2 m y una estación terrena de un sistema no geoestacionario del servicio fijo por satélite tendrá un diámetro de antena mínimo de 4,5 m. Además, el promedio en un segundo de la p.i.r.e. radiada por una estación de los servicios de radiolocalización o radionavegación no deberá rebasar el valor de 59 dBW por encima de 2º de elevación y 65 dBW por debajo. Antes de que una administración ponga en funcionamiento una estación terrena de una red de satélite geoestacionario del servicio fijo por satélite en esta banda con un diámetro de antena menor de 4,5 m, se asegurará que la densidad de flujo de potencia producida por esta estación terrena no rebase el valor de: – 115 dB(W/(m2 • 10 MHz)) para más del 1% del tiempo producido a 36 m sobre el nivel del mar en la línea de bajamar oficialmente reconocida por el Estado con litoral costero; – 115 dB(W/(m2 • 10 MHz)) para más del 1% del tiempo producido a 3 m de altura sobre el suelo en la frontera de una administración que esté instalando o tenga previsto instalar radares móviles terrestres en esta banda, a menos que se haya obtenido un acuerdo previamente. Para estaciones terrenas del servicio fijo por satélite que tengan un diámetro de antena igual o mayor que 4,5 m, la p.i.r.e. de cualquier emisión debería ser de al menos 68 dBW y no debería rebasar los 85 dBW. (CMR-03)
589	5.503	En la banda 13,75-14 GHz las estaciones espaciales geoestacionarias del servicio de investigación espacial, acerca de las cuales la Oficina ha recibido la información para publicación anticipada antes del 31 de enero de 1992, funcionarán en igualdad de condiciones que las estaciones del servicio fijo por satélite, fecha a partir de la cual las nuevas estaciones espaciales geoestacionarias del servicio de investigación espacial funcionarán con categoría secundaria. Hasta el momento en que las estaciones espaciales geoestacionarias del servicio de investigación espacial sobre las que la Oficina ha recibido información para publicación anticipada antes del 31 de enero de 1992 cesen su funcionamiento en esta banda: – en la banda 13,770 13,780 GHz la densidad de p.i.r.e. de las emisiones procedentes de cualquier estación terrena del servicio fijo por satélite que funcione con una estación espacial en la órbita de los satélites geoestacionarios no deberá ser superior a : i) 4,7D + 28 dB(W/40 kHz), donde D es el diámetro (m) de la antena de estación terrena del servicio fijo por satélite para diámetros de la antena de estación terrena iguales o mayores que 1,2 m y menores de 4,5 m; ii) 49,2 + 20 log(D/4,5) dB(W/40 kHz), donde D es el diámetro (m) de la antena de estación terrena del servicio fijo por satélite para diámetros de antena de estación terrena iguales o mayores de 4,5 m y menores de 31,9 m; iii) 66,2 dB(W/40 kHz) para cualquier estación terrena del servicio fijo por satélite para diámetros de antena iguales o mayores que 31,9 m; iv) 56,2 dB(W/4 kHz) para emisiones de banda estrecha (menos de 40 kHz de anchura de banda necesaria) de estaciones terrenas del servicio fijo por satélite y de cualquier estación terrena del servicio fijo por satélite con un diámetro de antena de 4,5 m o superior; – la densidad de p.i.r.e. de las emisiones procedentes de cualquier estación terrena del servicio fijo por satélite que funcione con una estación espacial no geoestacionaria no deberá ser superior a 51 dBW en una banda de 6 MHz entre 13,772 y 13,778 GHz. Puede utilizarse control automático de potencia para aumentar la densidad de p.i.r.e. en estas gamas de frecuencias a fin de compensar la atenuación debida a la lluvia, siempre que la densidad de flujo de potencia en la estación espacial del servicio fijo por satélite no rebase el valor resultante de la utilización por una estación terrena de una p.i.r.e. que cumpla los límites anteriores en condiciones de cielo despejado. (CMR-03)
590	5.503A	SUP - CMR 2003.
591	5.504	La utilización de la banda 14 - 14,3 GHz por el servicio de radionavegación deberá realizarse de tal manera que se asegure una protección suficiente a las estaciones espaciales del servicio Fijo por satélite.
640	5.531	Atribución adicional: en Japón, la banda 21,4 - 22 GHz está también atribuida, a título primario, al servicio de radiodifusión.
593	5.504B	Las estaciones terrenas a bordo de aeronaves que funcionen en el servicio móvil aeronáutico por satélite en la banda 14-14,5 GHz deben atender a las disposiciones del Anexo 1, Parte C de la Recomendación UIT-R M.1643, con respecto a cualquier estación de radioastronomía que realice observaciones en la banda 14,47-14,5 GHz y que esté situada en el territorio de España, Francia, India, Italia, Reino Unido y Sudafricana (Rep.). (CMR-03)
594	5.504C	En la banda 14-14,25 GHz, la densidad de flujo de potencia producida en el territorio de Arabia Saudita, Botswana, Côte d'Ivoire, Egipto, Guinea, India, Irán (República Islámica del), Kuwait, Lesotho, Nigeria, Omán, República Árabe Siria y Túnez por cualquier estación terrena a bordo de aeronave en el servicio móvil aeronáutico por satélite no debe rebasar los límites señalados en el Anexo 1, Parte B de la Recomendación UIT-R M.1643, a menos que acuerden específicamente otra cosa la administración o administraciones afectadas. Las disposiciones de esta nota no constituyen en modo alguno una derogación de las obligaciones del servicio móvil aeronáutico por satélite en el sentido de funcionar como servicio secundario de conformidad con el número 5.29. (CMR-03)
595	5.505	Atribución adicional: en Argelia, Angola, Arabia Saudita, Bahrein, Botswana, Brunei Darussalam, Camerún, China, Congo (Rep. del), Corea (Rep. de), Egipto, Emiratos Árabes Unidos, Gabón, Guinea, India, Indonesia, Irán (República Islámica del), Iraq, Israel, Japón, Jordania, Kuwait, Lesotho, Líbano, Malasia, Malí, Marruecos, Mauritania, Omán, Pakistán, Filipinas, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Singapur, Somalia, Sudán, Swazilandia, Tanzanía, Chad, Viet Nam y Yemen, la banda 14-14,3 GHz está también atribuida, a título primario, al servicio fijo. (CMR-07)
596	5.506	La banda 14 - 14,5 GHz puede ser utilizada, en el servicio Fijo por satélite (Tierra- espacio), para enlaces de conexión destinados al servicio de radiodifusión por satélite, a reserva de una coordinación con las otras redes del servicio Fijo por satélite. Tal utilización para los enlaces de conexión está reservada a los países exteriores a Europa.
597	5.506A	En la banda 14-14,5 GHz, las estaciones terrenas situadas a bordo de barcos cuya p.i.r.e. sea mayor que 21 dBW deberán funcionar en las mismas condiciones que las estaciones terrenas a bordo de buques de acuerdo con lo dispuesto en la Resolución 902 (CMR-03). Esta nota no se aplicará a las estaciones terrenas de barco sobre las que la Oficina haya recibido la información completa del Apéndice 4 antes del 5 de julio de 2003. (CMR-03)
598	5.506B	Las estaciones terrenas situadas a bordo de barcos que se comuniquen con estaciones espaciales del servicio fijo por satélite pueden funcionar en la banda de frecuencias 14-14,5 GHz sin necesidad de acuerdo previo con Chipre, Grecia y Malta, respetando la distancia mínima respecto de esos países, señalada en la Resolución 902 (CMR-03). (CMR-03)
599	5.507	No utilizado.
600	5.508	Atribución adicional: en Alemania, Bosnia y Herzegovina, Francia, Italia, Jamahiriya Árabe Libia, la ex República Yugoslava de Macedonia y Reino Unido, la banda 14,25-14,3 GHz está también atribuida, a título primario, al servicio fijo. (CMR-07)
601	5.508A	En la banda 14,25-14,3 GHz, la densidad de flujo de potencia producida en el territorio de Arabia Saudita, Botswana, China, Côte d'Ivoire, Egipto, Francia, Guinea, India, Irán (República Islámica del), Italia, Kuwait, Lesotho, Nigeria, Omán, República Árabe Siria, Reino Unido y Túnez por cualquier estación terrena a bordo de aeronave en el servicio móvil aeronáutico por satélite no rebasará los límites señalados en el Anexo 1, Parte B de la Recomendación UIT-R M.1643, a menos que acuerden específicamente otra cosa la administración o administraciones afectadas. Las disposiciones de esta nota no constituyen en modo alguno una derogación de las obligaciones del servicio móvil aeronáutico por satélite en el sentido de funcionar como servicio secundario de conformidad con el número 5.29. (CMR-03)
602	5.509	SUP – CMR 2007.
603	5.509A	En la banda 14,3-14,5 GHz, la densidad de flujo de potencia producida en el territorio de Arabia Saudita, Botswana, Camerún, China, Côte d'Ivoire, Egipto, Francia, Gabón, Guinea, India, Irán (República Islámica del), Italia, Kuwait, Lesotho, Marruecos, Nigeria, Omán, República Árabe Siria, Reino Unido, Sri Lanka, Túnez y Viet Nam por cualquier estación terrena a bordo de aeronave en el servicio móvil aeronáutico por satélite no rebasará los límites señalados en el Anexo 1, Parte B de la Recomendación UIT-R M.1643, a menos que acuerden específicamente otra cosa la administración o administraciones afectadas. Las disposiciones de esta nota no constituyen en modo alguno una derogación de las obligaciones del servicio móvil aeronáutico por satélite en el sentido de funcionar como servicio secundario de conformidad con el número 5.29. (CMR-03)
604	5.510	La utilización de la banda 14,5 - 14,8 GHz por el servicio Fijo por satélite (Tierra- espacio) está limitada a los enlaces de conexión para el servicio de radiodifusión por satélite. Esta utilización está reservada a los países exteriores a Europa.
605	5.511	Atribución adicional: en Arabia Saudita, Bahrein, Bosnia y Herzegovina, Camerún, Egipto, Emiratos Árabes Unidos, Guinea, Irán (República Islámica del), Iraq, Israel, Jamahiriya Árabe Libia, Kuwait, Líbano, Pakistán, Qatar, República Árabe Siria y Somalia, la banda 15,35-15,4 GHz está también atribuida, a título secundario, a los servicios fijo y móvil. (CMR-07)
606	5.511A	La banda 15,43-15,63 GHz se atribuye también al servicio fijo por satélite (espacio- Tierra) a título primario. La utilización de la banda 15,43-15,63 GHz por el servicio fijo por satélite (espacio-Tierra y Tierra-espacio) queda limitada a los enlaces de conexión de los sistemas de satélites no geoestacionarios del servicio móvil por satélite, a reserva de la coordinación con arreglo al número 9.11A. La utilización de la banda de frecuencias 15,43- 15,63 GHz por el servicio fijo por satélite (espacio-Tierra) queda limitada a los sistemas de enlace de conexión de los sistemas de satélites no geoestacionarios del servicio móvil por satélite con respecto a los cuales la Oficina haya recibido información para la publicación anticipada antes del 2 de junio de 2000. En el sentido espacio-Tierra, el ángulo mínimo de elevación de la estación terrena por encima del plano horizontal local y la ganancia en la dirección de dicho plano, así como las distancias mínimas de coordinación para proteger a una estación terrena contra la interferencia perjudicial, estarán en conformidad con lo dispuesto en la Recomendación UIT-R S.1341. Para proteger al servicio de radioastronomía en la banda 15,35-15,4 GHz, la densidad de flujo de potencia combinada radiada en la banda 15,35- 15,4 GHz por todas las estaciones espaciales de cualquier sistema de enlaces de conexión (espacio- Tierra) de un sistema de satélites no geoestacionarios del servicio móvil por satélite que funcione en la banda 15,43-15,63 GHz no deberá rebasar −156 dB(W/m 2 ) en una anchura de banda de 50 MHz, en el emplazamiento de cualquier observatorio de radioastronomía durante más del 2% del tiempo. (CMR-00)
607	5.511B	SUP-CMR-97.
608	5.511C	Las estaciones que funcionan en el servicio de radionavegación aeronáutica limitarán la p.i.r.e. efectiva, de conformidad con la Recomendación UIT-R S.1340. La distancia de coordinación mínima necesaria para proteger a las estaciones del servicio de radionavegación aeronáutica (se aplica el número 4.10) contra la interferencia perjudicial de las estaciones terrenas de enlace de conexión y la p.i.r.e. máxima transmitida hacia el plano horizontal local por una estación terrena de enlace de conexión estarán en conformidad con lo dispuesto en la Recomendación UIT-R S.1340. (CMR-97)
609	5.511D	Los sistemas del servicio fijo por satélite respecto de los cuales la Oficina haya recibido información completa para publicación anticipada hasta el 21 de noviembre de 1997 pueden funcionar en las bandas 15,4 – 15,43 GHz y 15,63 – 15,7 GHz en el sentido espacio-Tierra y 15,63 – 15,65 GHz en el sentido Tierra-espacio. En las bandas 15,4 – 15,43 GHz y 15,65 – 15,7 GHz, las emisiones de una estación espacial no geoestacionaria no rebasarán los límites de 2 densidad de flujo de potencia en la superficie de la Tierra de – 146 dB(W/m /MHz) para cualquier ángulo de llegada. En la banda 15,63 – 15,65 GHz cuando una administración proponga emisiones procedentes de una estación espacial no geoestacionaria, que rebasen el 2 valor de – 146 dB(W/m /MHz) para cualquier ángulo de llegada, deberá establecer coordinación con las administraciones afectadas conforme al número 9.11A. Las estaciones del servicio fijo por satélite que funcionen en la banda 15,63 – 15,65 GHz en el sentido espacio-Tierra no causarán interferencia perjudicial a las estaciones del servicio de radionavegación aeronáutica (se aplica el número 4.10). (CMR-97)
610	5.512	Atribución adicional: en Argelia, Angola, Arabia Saudita, Austria, Bahrein, Bangladesh, Brunei Darussalam, Camerún, Congo (Rep. del), Costa Rica, Egipto, El Salvador, Emiratos Árabes Unidos, Eritrea, Finlandia, Guatemala, India, Indonesia, Irán (República Islámica del), Jamahiriya Árabe Libia, Jordania, Kenya, Kuwait, Líbano, Malasia, Malí, Marruecos, Mauritania, Montenegro, Mozambique, Nepal, Nicaragua, Omán, Pakistán, Qatar, República Árabe Siria, Serbia, Singapur, Somalia, Sudán, Swazilandia, Tanzanía, Chad, Togo y Yemen, la banda 15,7-17,3 GHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-07)
611	5.513	Atribución adicional: en Israel, la banda 15,7 - 17,3 GHz está también atribuida, a título primario, a los servicios fijo y móvil. Estos servicios no gozarán de protección contra la interferencia perjudicial de los servicios que funcionan de conformidad con el Cuadro en los países no incluidos en el número 5.512, ni causarán interferencia a dichos servicios.
612	5.513A	Los sensores activos a bordo de vehículos que funcionan en la banda de frecuencias 17,2 – 17,3 GHz no causarán interferencia perjudicial ni obstaculizarán el desarrollo del servicio de radiolocalización y de otros servicios con atribución a título primario. (CMR-97)
613	5.514	Atribución adicional: en Argelia, Angola, Arabia Saudita, Bahrein, Bangladesh, Camerún, Costa Rica, El Salvador, Emiratos Árabes Unidos, Guatemala, India, Irán (República Islámica del), Iraq, Israel, Italia, Jamahiriya Árabe Libia, Japón, Jordania, Kuwait, Lituania, Nepal, Nicaragua, Nigeria, Omán, Uzbekistán, Pakistán, Qatar, Kirguistán y Sudán, la banda 17,3-17,7 GHz está también atribuida, a título secundario, a los servicios fijo y móvil. Se aplican los límites de potencia indicados en los números 21.3 y 21.5. (CMR-07)
614	5.515	En la banda 17,3 - 17,8 GHz la compartición entre el servicio Fijo por satélite (Tierra- espacio) y el servicio de radiodifusión por satélite deberá efectuarse también de acuerdo con lo dispuesto en el punto 1 del Anexo 4 al Apéndice 30A.
615	5.516	La utilización de la banda 17,3-18,1 GHz por los sistemas de satélites geoestacionarios del servicio fijo por satélite (Tierra-espacio) está limitada a los enlaces de conexión para el servicio de radiodifusión por satélite. La utilización de la banda 17,3-17,8 GHz en la Región 2 por sistemas del servicio fijo por satélite (Tierra-espacio) queda limitada a los satélites geoestacionarios. Para la utilización de la banda 17,3-17,8 GHz en la Región 2 por los enlaces de conexión del servicio de radiodifusión por satélite en la banda 12,2-12,7 GHz, véase el artículo 11. La utilización de las bandas 17,3-18,1 GHz (Tierra-espacio) en las Regiones 1 y 3 y 17,8-18,1 GHz (Tierra-espacio) en la Región 2 por los sistemas no OSG del servicio fijo por satélite está sujeta a la aplicación de lo dispuesto en el número 9.12 para la coordinación con otros sistemas no OSG del servicio fijo por satélite. Los sistemas no OSG del servicio fijo por satélite no reclamarán protección contra las redes OSG del servicio de radiodifusión por satélite que funcionen de conformidad con el Reglamento de Radiocomunicaciones, sea cual sea la fecha en que la Oficina reciba la información completa de coordinación o de notificación, según proceda, de los sistemas del SFS no OSG y la información completa de coordinación o de notificación, según proceda, de las redes OSG. El número 5.43A no se aplica. Los sistemas no OSG del SFS se explotarán en las bandas precitadas de forma ue cualquier interferencia inaceptable que pueda producirse durante su explotación se elimine rápidamente. (CMR-00)
616	5.516A	En la banda 17,3-17,7 GHz, las estaciones terrenas del servicio fijo por satélite (espacio-Tierra) en la Región 1 no solicitarán protección contra la interferencia que puedan ocasionar las estaciones terrenas de enlace de conexión del servicio de radiodifusión por satélite que funcionan con arreglo al Apéndice 30A ni impondrán limitación y/o restricción alguna a la ubicación de las estaciones terrenas de enlace de conexión del servicio de radiodifusión por satélite dentro de la zona de servicio del enlace de conexión. (CMR-03)
617	5.516B	Se han identificado las siguientes bandas para su utilización por las aplicaciones de alta densidad del servicio fijo por satélite: 17,3-17,7 GHz (espacio-Tierra) en la Región 1, 18,3-19,3 GHz (espacio-Tierra) en la Región 2, 19,7-20,2 GHz (espacio-Tierra), en todas las Regiones, 39,5-40 GHz (espacio-Tierra) en la Región 1, 40-40,5 GHz (espacio-Tierra), en todas las Regiones, 40,5-42 GHz (espacio-Tierra) en la Región 2, 47,5-47,9 GHz (espacio-Tierra) en la Región 1, 48,2-48,54 GHz (espacio-Tierra) en la Región 1, 49,44-50,2 GHz (espacio-Tierra) en la Región 1, y 27,5-27,82 GHz (Tierra-espacio) en la Región 1, 28,35-28,45 GHz (Tierra-espacio) en la Región 2, 28,45-28,94 GHz (Tierra-espacio), en todas las Regiones, 28,94-29,1 GHz (Tierra-espacio) en las Regiones 2 y 3, 29,25-29,46 GHz (Tierra-espacio) en la Región 2, 29,46-30 GHz (Tierra-espacio), en todas las Regiones, 48,2-50,2 GHz (Tierra-espacio), en la Región 2. Esta identificación no impide el empleo de tales bandas por otras aplicaciones del servicio fijo por satélite o por otros servicios a los cuales se encuentran atribuidas dichas bandas a título coprimario y no establece prioridad alguna entre los usuarios de las bandas estipuladas en el presente Reglamento de Radiocomunicaciones. Las administraciones deben tener esto presente a la hora de examinar las disposiciones reglamentarias referentes a dichas bandas. Véase la Resolución 143 (CMR-03). (CMR-03)
620	5.519	Atribución adicional: las bandas 18,0-18,3 en la Región 2 y 18,1-18,4 GHz en las Regiones 1 y 3 están también atribuidas, a título primario, al servicio de meteorología por satélite (espacio-Tierra). Su utilización está limitada solamente a los satélites geoestacionarios. (CMR-07)
621	5.520	La utilización de la banda 18,1-18,4 GHz por el servicio fijo por satélite (Tierra-espacio) se limita a los enlaces de conexión de los sistemas OSG del servicio de radiodifusión por satélite. (CMR-00)
622	5.521	Atribución sustitutiva: en Alemania, Dinamarca, Emiratos Árabes Unidos y Grecia, la banda 18,1-18,4 GHz está atribuida a los servicios fijo, fijo por satélite (espacio-Tierra) y móvil a título primario (véase el número 5.33). También se aplican las disposiciones del número 5.519. (CMR-03)
623	5.522	SUP - CMR 2000.
624	5.522A	Las emisiones del servicio fijo y del servicio fijo por satélite en la banda 18,6-18,8 GHz están limitadas a los valores indicados en los números 21.5A y 21.16.2, respectivamente. (CMR-00)
625	5.522B	La utilización de la banda 18,6-18,8 GHz por el servicio fijo por satélite se limita a los sistemas de satélites geoestacionarios y sistemas de satélites con una órbita cuyo apogeo sea superior a 20 000 km. (CMR-00)
626	5.522C	En la banda 18,6-18,8 GHz, en Argelia, Arabia Saudita, Bahrein, Egipto, los Emiratos Árabes Unidos, Jordania, Líbano, Libia, Marruecos, Omán, Qatar, Siria, Túnez y Yemen, los sistemas del servicio fijo que estén en funcionamiento en la fecha de entrada en vigor de las Actas Finales de la CMR-2000 no están sujetos a los límites del número 21.5A. (CMR-00)
627	5.523	SUP - CMR 2000.
628	5.523A	La utilización de las bandas 18,8 - 19,3 GHz (espacio-Tierra) y 28,6 - 29,1 GHz (Tierra- espacio) por las redes de los servicios fijos por satélite geoestacionario y no geoestacionario está sujeta a la aplicación de las disposiciones del número 9.11A y el número 22.2 no se aplica. Las administraciones que tengan redes de satélites geoestacionarias en proceso de coordinación antes del 18 de noviembre de 1995 cooperarán al máximo para concluir satisfactoriamente la coordinación, en cumplimiento al número 9.11A, con las redes de satélites no geoestacionarias cuya información de notificación se haya recibido en la Oficina antes de esa fecha, con el fin de llegar a resultados aceptables para todas las partes en cuestión. Las redes de satélite no geoestacionarias no causarán interferencia inaceptable a las redes del servicio fijo por satélite geoestacionario respecto de las cuales la Oficina considere que ha recibido una información completa de la notificación del apéndice 4 antes del 18 de noviembre de 1995. (CMR-97)
629	5.523B	La utilización de la banda 19,3 - 19,6 GHz (Tierra-espacio) por el servicio Fijo por satélite está limitada a los enlaces de conexión con sistemas de satélites no geoestacionarios del servicio móvil por satélite. Esta utilización no está sujeta a la coordinación a tenor del número 9.11A, y no se aplica el número 22.2.
630	5.523C	El número 22.2 del Reglamento de Radiocomunicaciones deberá continuar aplicándose en las bandas 19,3 - 19,6 GHz y 29,1 - 29,4 GHz entre los enlaces de conexión de las redes de satélites no geoestacionarios del servicio móvil por satélite y las redes del servicio fijo por satélite sobre las cuales la Oficina ha recibido antes del 18 de noviembre de 1995 la información de coordinación completa con arreglo al apéndice 4 o la información de notificación. (CMR-97)
631	5.523D	La utilización de la banda 19,3 - 19,7 GHz (espacio-Tierra) por sistemas del servicio fijo por satélite geoestacionario y por enlaces de conexión de sistemas de satélites no geoestacionarios del SMS está sujeta a la coordinación a tenor del número 9.11A, pero no está sujeta a las disposiciones del número 22.2. La utilización de esta banda por otros sistemas del servicio fijo por satélite no geoestacionario, o en los casos indicados en el número 5.523C y 5.523E no está sujeta a las disposiciones del número 9.11A y continuará sujeta a los procedimientos de los artículos 9 (excepto el número 9.11A) y 11 y a las disposiciones del número 22.2. (CMR-97)
632	5.523E	El número 22.2 del Reglamento de Radiocomunicaciones deberá continuar aplicándose en las bandas 19,6 - 19,7 GHz y 29,4 - 29,5 GHz entre los enlaces de conexión de las redes de satélites no geoestacionarios del servicio móvil por satélite y las redes del servicio fijo por satélite sobre las cuales la Oficina ha recibido hasta el 21 de noviembre de 1997 la información de coordinación completa con arreglo al apéndice 4 o la información de notificación. (CMR-97)
633	5.524	Atribución adicional: en Afganistán, Argelia, Angola, Arabia Saudita, Bahrein, Brunei Darussalam, Camerún, China, Congo (Rep. del), Costa Rica, Egipto, Emiratos Árabes Unidos, Gabón, Guatemala, Guinea, India, Irán (República Islámica del), Iraq, Israel, Japón, Jordania, Kuwait, Líbano, Malasia, Malí, Marruecos, Mauritania, Nepal, Nigeria, Omán, Pakistán, Filipinas, Qatar, República Árabe Siria, Rep. Dem. del Congo, Rep. Pop. Dem. de Corea, Singapur, Somalia, Sudán, Tanzanía, Chad, Togo y Túnez, la banda 19,7-21,2 GHz está también atribuida, a título primario, a los servicios fijo y móvil. Esta utilización adicional no debe imponer limitaciones de densidad de flujo de potencia a las estaciones espaciales del servicio fijo por satélite en la banda 19,7-21,2 GHz y a las estaciones espaciales del servicio móvil por satélite, en la banda 19,7-20,2 GHz cuando la atribución al servicio móvil por satélite es a título primario en esta última banda. (CMR-07)
634	5.525	A fin de facilitar la coordinación interregional entre redes de los servicios móvil por satélite y fijo por satélite, las portadoras del servicio móvil por satélite que son más susceptibles a la interferencia estarán situadas, en la medida prácticamente posible, en las partes superiores de las bandas 19,7 - 20,2 GHz y 29,5 - 30 GHz.
635	5.526	En las bandas 19,7 - 20,2 GHz y 29,5 - 30 GHz en la Región 2, y en las bandas 20,1 - 20,2 GHz y 29,9 - 30 GHz en las Regiones 1 y 3, las redes del servicio Fijo por satélite y del servicio móvil por satélite pueden comprender estaciones terrenas en puntos especificados o no especificados, o mientras están en movimiento, a través de uno o más satélites para comunicaciones punto a punto o comunicaciones punto a multipunto.
636	5.527	En las bandas 19,7 - 20,2 GHz y 29,5 - 30 GHz, las disposiciones del número 4.10 no se aplican al servicio móvil por satélite.
637	5.528	La atribución al servicio móvil por satélite está destinada a las redes que utilizan antenas de haz estrecho y otras tecnologías avanzadas en las estaciones espaciales. Las administraciones que explotan sistemas del servicio móvil por satélite en la banda 19,7 - 20,1 GHz en la Región 2, y en la banda 20,1 - 20,2 GHz, harán todo lo posible para garantizar que puedan continuar disponiendo de estas bandas a las administraciones que explotan sistemas fijos y móviles de conformidad con las disposiciones del número 5.524.
638	5.529	El uso de las bandas 19,7 - 20,1 GHz y 29,5 - 29,9 GHz por el servicio móvil por satélite en la Región 2 está limitado a redes de satélites que operan tanto en el servicio Fijo por satélite como en el servicio móvil por satélite como se describe en el número 5.526.
644	5.535	En la banda 24,75 - 25,25 GHz, los enlaces de conexión con estaciones del servicio de radiodifusión por satélite tendrán prioridad sobre otras utilizaciones del servicio Fijo por satélite (Tierra-espacio). Estas últimas utilizaciones deben proteger a las redes de enlaces de conexión de las estaciones de radiodifusión por satélite existentes y futuras, y no reclamarán protección alguna contra ellas.
645	5.535A	La utilización de la banda 29,1 - 29,5 GHz (Tierra-espacio) por el servicio fijo por satélite está limitada a los sistemas de satélites geoestacionarios y a los enlaces de conexión con sistemas de satélites no geoestacionarios del servicio móvil por satélite. Esta utilización está sujeta a las disposiciones del número 9.11A, pero no está sujeta a las disposiciones del número 22.2, salvo lo indicado en el número 5.523C y 5.523E donde dicha utilización no está sujeta a las disposiciones del número 9.11A y deberá continuar sujeta a los procedimientos de los artículos 9 (salvo el número 9.11A) y 11, y a las disposiciones del número 22.2. (CMR-97)
646	5.536	La utilización de la banda 25,25 - 27,5 GHz por el servicio entre satélites está limitada a aplicaciones de investigación espacial y de exploración de la Tierra por satélite, y también a transmisiones de datos procedentes de actividades industriales y médicas en el espacio.
647	5.536A	Las administraciones que exploten estaciones terrenas de los servicios de exploración de la Tierra por satélite o de investigación espacial no reclamarán protección respecto a las estaciones de los servicios fijo y móvil que explotan otras administraciones. Además, las estaciones terrenas que funcionan en los servicios de exploración de la Tierra por satélite o de investigación espacial tendrán en cuenta, respectivamente, las Recomendaciones UIT-R SA.1278 y UIT-R SA.1625. (CMR-03)
648	5.536B	Las estaciones terrenas de Alemania, Arabia Saudita, Austria, Bélgica, Brasil, Bulgaria, China, Corea (Rep. de), Dinamarca, Egipto, Emiratos Árabes Unidos, España, Estonia, Finlandia, Francia, Hungría, India, Irán (República Islámica del), Irlanda, Israel, Italia, Jamahiriya Árabe Libia, Jordania, Kenya, Kuwait, Líbano, Liechtenstein, Lituania, Moldova, Noruega, Omán, Uganda, Pakistán, Filipinas, Polonia, Portugal, República Árabe Siria, Rep. Pop. Dem. de Corea, Eslovaquia, Rep. Checa, Rumania, Reino Unido, Singapur, Suecia, Suiza, Tanzanía, Turquía, Viet Nam y Zimbabwe que funcionan en el servicio de exploración de la Tierra por satélite, en la banda 25,5-27 GHz, no reclamarán protección contra estaciones de los servicios fijo y móvil, ni obstaculizarán su utilización y desarrollo. (CMR-07)
649	5.536C	En Argelia, Arabia Saudita, Bahrein, Botswana, Brasil, Camerún, Comoras, Cuba, Djibouti, Egipto, Emiratos Árabes Unidos, Estonia, Finlandia, Irán (República Islámica del), Israel, Jordania, Kenya, Kuwait, Lituania, Malasia, Marruecos, Nigeria, Omán, Qatar, República Árabe Siria, Somalia, Sudán, Tanzanía, Túnez, Uruguay, Zambia y Zimbabwe, las estaciones terrenas del servicio de investigación espacial en la banda 25,5-27 GHz no reclamarán protección respecto a las estaciones de los servicios fijo y móvil, ni restringirán su utilización y despliegue. (CMR-03)
650	5.537	Los servicios espaciales que utilizan satélites no geoestacionarios del servicio entre satélites en la banda 27 - 27,5 GHz están exentos de cumplir las disposiciones del número 22.2.
651	5.537A	En Bhután, Camerún, Corea (Rep. de), Federación de Rusia, India, Indonesia, Irán (República Islámica del), Japón, Kazajstán, Lesotho, Malasia, Maldivas, Mongolia, Myanmar, Uzbekistán, Pakistán, Filipinas, Kirguistán, Rep. Pop. Dem. de Corea, Sri Lanka, Tailandia y Viet Nam, la atribución al servicio fijo en la banda 27,9-28,2 GHz puede ser utilizada también por las estaciones en plataformas de gran altitud (HAPS) en el territorio de estos países. Estos 300 MHz de la atribución al servicio fijo para las HAPS en los países antes mencionados se utilizarán exclusivamente en el sentido HAPS-Tierra sin causar interferencia perjudicial a los otros tipos de sistemas del servicio fijo o a los otros servicios coprimarios, ni reclamar protección contra los mismos. Además, el desarrollo de esos otros servicios no se verá limitado por las HAPS. Véase la Resolución 145 (Rev.CMR-07). (CMR-07)
652	5.538	Atribución adicional: las bandas 27,500-27,501 GHz y 29,999-30,000 GHz están atribuidas también a título primario al servicio fijo por satélite (espacio-Tierra) para las transmisiones de radiobalizas a efectos de control de potencia del enlace ascendente. Esas transmisiones espacio-Tierra no sobrepasarán una potencia isótropa radiada equivalente (p.i.r.e.) de +10 dBW en la dirección de los satélites adyacentes en la órbita de los satélites geoestacionarios. (CMR-07)
653	5.539	La banda 27,5 - 30 GHz puede ser utilizada por el servicio Fijo por satélite (Tierra- espacio) para el establecimiento de enlaces de conexión del servicio de radiodifusión por satélite.
654	5.540	Atribución adicional: la banda 27,501 - 29,999 GHz está atribuida también a título secundario al servicio Fijo por satélite (espacio-Tierra) para las transmisiones de radiobalizas a efectos de control de potencia del enlace ascendente.
655	5.541	En la banda 28,5 - 30 GHz, el servicio de exploración de la Tierra por satélite está limitado a la transferencia de datos entre estaciones y no está destinado a la recogida primaria de información mediante sensores activos o pasivos.
656	5.541A	Los enlaces de conexión de las redes de satélites no geoestacionarios del servicio móvil por satélite y las redes de satélites geoestacionarios del servicio fijo por satélite que funcionan en la banda 29,1-29,5 GHz (Tierra-espacio) deberán utilizar un control adaptable de la potencia para los enlaces ascendentes u otros métodos de compensación del desvanecimiento, con objeto de que las transmisiones de las estaciones terrenas se efectúen al nivel de potencia requerido para alcanzar la calidad de funcionamiento deseada del enlace a la vez que se reduce el nivel de interferencia mutua entre ambas redes. Estos métodos se aplicarán a las redes para las cuales se considera que la información del apéndice 4 sobre coordinación ha sido recibida por la Oficina después del 17 de mayo de 1996 y hasta que sean modificados por una futura conferencia mundial de radiocomunicaciones competente. Se insta a las administraciones que presenten la información de coordinación del apéndice 4 antes de esa fecha, a que utilicen estas técnicas en la medida de lo posible. (CMR-00)
657	5.542	Atribución adicional: en Argelia, Arabia Saudita, Bahrein, Brunei Darussalam, Camerún, China, Congo (Rep. del), Egipto, Emiratos Árabes Unidos, Eritrea, Etiopía, Guinea, India, Irán (República Islámica del), Iraq, Japón, Jordania, Kuwait, Líbano, Malasia, Malí, Marruecos, Mauritania, Nepal, Pakistán, Filipinas, Qatar, República Árabe Siria, Rep. Pop. Dem. de Corea, Somalia, Sudán, Sri Lanka y Chad, la banda 29,5-31 GHz está también atribuida, a título secundario, a los servicios fijo y móvil. Se aplicarán los límites de potencia indicados en los números 21.3 y 21.5. (CMR-07)
658	5.543	La banda 29,95 - 30 GHz se podrá utilizar, a título secundario, en los enlaces espacio- espacio del servicio de exploración de la Tierra por satélite con fines de telemedida, seguimiento y telemando.
659	5.543A	En Bhután, Camerún, Corea (Rep. de), Federación de Rusia, India, Indonesia, Irán (República Islámica del), Japón, Kazajstán, Lesotho, Malasia, Maldivas, Mongolia, Myanmar, Uzbekistán, Pakistán, Filipinas, Kirguistán, Rep. Pop. Dem. de Corea, Sri Lanka, Tailandia y Viet Nam, la atribución al servicio fijo en la banda 31-31,3 GHz puede ser utilizada también por los sistemas que utilizan estaciones en plataformas de gran altitud (HAPS) en el sentido tierra-HAPS. El empleo de esta banda por dichos sistemas está limitado a los territorios de los países antes enumerados y no deberá causar interferencia perjudicial a los otros tipos de sistemas del servicio fijo, a los sistemas del servicio móvil y a los sistemas que funcionan conforme a lo dispuesto en el número 5.545, ni reclamar protección con respecto a los mismos. Por otro lado, el desarrollo de estos servicios no se verá limitado por las HAPS. Los sistemas que utilizan las estaciones HAPS en la banda 31-31,3 GHz no causarán interferencia perjudicial al servicio de radioastronomía que tenga una atribución a título primario en la banda 31,3-31,8 GHz, teniendo en cuenta los criterios de protección indicados en la Recomendación UIT-R RA.769. Con miras a garantizar la protección de los servicios pasivos por satélite, el nivel de la densidad de potencia no deseada en la antena de una estación terrena HAPS en la banda 31,3- 31,8 GHz estará limitado a – 106 dB(W/MHz) en condiciones de cielo despejado y podría aumentarse hasta – 100 dB(W/MHz) en condiciones de pluviosidad para tener en cuenta el desvanecimiento debido a la lluvia, siempre y cuando su incidencia efectiva en el satélite pasivo no sea mayor que la correspondiente a las condiciones de cielo despejado. Véase la Resolución 145 (Rev.CMR-07). (CMR-07)
660	5.544	En la banda 3 1 - 31,3 GHz, los límites de densidad de flujo de potencia indicados en el artículo 21, cuadro 21-4 se aplican al servicio de investigación espacial.
661	5.545	Categoría de servicio diferente: en Armenia, Georgia, Mongolia, Kirguistán, Tayikistán y Turkmenistán, la atribución de la banda 31-31,3 GHz, al servicio de investigación espacial es a título primario (véase el número 5.33). (CMR-07)
662	5.546	Categoría de servicio diferente: en Arabia Saudita, Armenia, Azerbaiyán, Belarús, Egipto, Emiratos Árabes Unidos, España, Estonia, Federación de Rusia, Georgia, Hungría, Irán (República Islámica del), Israel, Jordania, Líbano, Moldova, Mongolia, Uzbekistán, Polonia, República Árabe Siria, Kirguistán, Rumania, Reino Unido, Sudafricana (Rep.), Tayikistán, Turkmenistán y Turquía, la banda 31,5-31,8 GHz, está atribuida al servicio fijo y al servicio móvil, salvo móvil aeronáutico, a título primario (véase el número 5.33). (CMR-07)
663	5.547	Las bandas 31,8-33,4 GHz, 37-40 GHz, 40,5-43,5 GHz, 51,4-52,6 GHz, 55,78-59 GHz y 64-66 GHz están disponibles para aplicaciones de alta densidad en el servicio fijo (véase la Resolución 75 (CMR-2000)). Las administraciones deben tener en cuenta esta circunstancia cuando consideren las disposiciones reglamentarias relativas a estas bandas. Debido a la posible instalación de aplicaciones de alta densidad en el servicio fijo por satélite en las bandas 39,5-40 GHz y 40,5-42 GHz, (véase el número 5.516B), las administraciones deben tener en cuenta además las posibles limitaciones a las aplicaciones de alta densidad en el servicio fijo, según el caso. (CMR-07)
664	5.547A	Las administraciones deberían tomar las medidas necesarias para reducir al mínimo la posible interferencia entre las estaciones del servicio fijo y las aerotransportadas del servicio de radionavegación en la banda 31,8-33,4 GHz, teniendo en cuenta las necesidades operacionales de los radares a bordo de aeronaves. (CMR-00)
665	5.547B	Atribución sustitutiva: en Estados Unidos la banda 31,8 - 32 GHz está atribuida a título primario a los servicios de radionavegación y de investigación espacial (espacio lejano) (espacio-Tierra). (CMR-97)
666	5.547C	Atribución sustitutiva : en Estados Unidos la banda 32-32,3 GHz está atribuida a título primario a los servicios de radionavegación y de investigación espacial (espacio lejano) (espacio-Tierra). (CMR-03)
667	5.547D	Atribución sustitutiva: en Estados Unidos la banda 32,3 - 33 GHz está atribuida a título primario a los servicios entre satélites y de radionavegación. (CMR-97)
668	5.547E	Atribución sustitutiva: en Estados Unidos la banda 33 – 33,4 GHz está atribuida a título primario al servicio de radionavegación. (CMR-97)
669	5.548	Al proyectar sistemas del servicio entre satélites en la banda 32,3-33 GHz, del servicio de radionavegación en la banda 32-33 GHz, así como del servicio de investigación espacial (espacio lejano) en la banda 31,8-32,3 GHz, las administraciones adoptarán todas las medidas necesarias para evitar la interferencia perjudicial entre estos servicios, teniendo en cuenta el aspecto de la seguridad del servicio de radionavegación (véase la Recomendación 707). (CMR- 03)
670	5.549	Atribución adicional: en Arabia Saudita, Bahrein, Bangladesh, Egipto, Emiratos Árabes Unidos, Gabón, Indonesia, Irán (Rep. Islámica del), Iraq, Israel, Jordania, Kuwait, Líbano, Jamahiriya Árabe Libia, Malasia, Malí, Malta, Marruecos, Mauritania, Nepal, Nigeria, Omán, Pakistán, Filipinas, Qatar, Rep. Dem. del Congo, República Árabe Siria, Singapur, Somalia, Sudán, Sri Lanka, Togo, Túnez y Yemen, la banda 33,4-36 GHz está también atribuida, a título primario, a los servicios fijo y móvil. (CMR-03)
671	5.549A	En la banda 35,5-36,0 GHz, la densidad de flujo de potencia media en la superficie de la Tierra radiada por cualquier sensor a bordo de un vehículo espacial del servicio de exploración de la Tierra por satélite (activo) o del servicio de investigación espacial (activo), para cualquier ángulo mayor que 0,8º, medido a partir del centro del haz, no rebasará el valor de –73,3 dB(W/m^2) en esta banda. (CMR-03)
672	5.550	Categoría de servicio diferente: en Armenia, Azerbaiyán, Belarús, Federación de Rusia, Georgia, Mongolia, Kirguistán, Tayikistán y Turkmenistán, la atribución de la banda 34,7-35,2 GHz, al servicio de investigación espacial es a título primario (véase el número 5.33). (CMR- 07)
673	5.550A	Para la compartición de la banda 36-37 GHz entre el servicio de exploración de la Tierra por satélite (pasivo) y los servicios fijo y móvil, se aplicará la Resolución 752 (CMR-07). (CMR-07)
674	5.551	SUP – CMR - 97.
675	5.551A	SUP - CMR 2003.
676	5.551AA	SUP - CMR 2003.
677	5.551B	SUP - CMR 2000.
678	5.551C	SUP - CMR 2000.
679	5.551D	SUP - CMR 2000.
680	5.551E	SUP - CMR 2000.
681	5.551F	Categoría de servicio diferente: en Japón, la atribución de la banda 41,5 - 42,5 GHz al servicio móvil es a título primario (véase el número 5.33). (CMR-97)
682	5.551G	SUP - CMR 2003.
700	5.559	En la banda 59-64 GHz podrán utilizarse radares a bordo de aeronaves en el servicio de radiolocalización, a reserva de no causar interferencias perjudiciales al servicio entre satélites (véase el número 5.43). (CMR-00)
701	5.559A	SUP – CMR – 07
683	5.551H	La densidad de flujo de potencia equivalente (dfpe) producida en la banda 42,5-43,5 GHz por todas las estaciones espaciales de cualquier sistema de satélites no geoestacionarios del servicio fijo por satélite (espacio-Tierra) o del servicio de radiodifusión por satélite (espacio-Tierra) en la banda 42-42,5 GHz, no superará los siguientes valores en el emplazamiento de cualquier estación de radioastronomía durante más del 2% del tiempo: −230 dB(W/m2) en 1 GHz y –246 dB(W/m2) en cualquier banda de 500 kHz de la banda 42,5-43,5 GHz en el emplazamiento de cualquier estación de radioastronomía registrada como telescopio de parábola única, y –209 dB(W/m2) en cualquier banda de 500 kHz de la banda 42,5-43,5 GHz en el emplazamiento de una estación de radioastronomía registrada como estación de interferometría con línea de base muy larga. Estos valores de dfpe deberán evaluarse mediante la metodología que figura en la Recomendación UIT-R S.1586-1 y el diagrama de antena de referencia y ganancia máxima de antena del servicio de radioastronomía consignados en la Recomendación UIT-R RA.1631, que deben aplicarse para todo el cielo y ángulos de elevación superiores al ángulo de funcionamiento mínimo θmín del radiotelescopio (para el que debe adoptarse un valor por defecto de 5° en ausencia de información notificada). Estos valores deberán aplicarse a cualquier estación de radioastronomía que: – esté en funcionamiento antes del 5 de julio de 2003 y se notifique a la Oficina antes del 4 de enero de 2004; o bien que – se haya notificado antes de la fecha de recepción de la información completa en materia de coordinación o notificación prevista en el Apéndice 4, según proceda, sobre la estación espacial a la que se aplican los límites. Las demás estaciones de radioastronomía notificadas tras estas fechas, pueden recabar el acuerdo de las administraciones que hayan autorizado las estaciones espaciales. En la Región 2 se aplicará la Resolución 743 (CMR-03). Los límites de esta nota pueden sobrepasarse en el emplazamiento de una estación de radioastronomía de cualquier país cuya administración lo admita. (CMR-07)
684	5.551I	La densidad de flujo de potencia producida en la banda 42,5-43,5 GHz por toda estación espacial geoestacionaria del servicio fijo por satélite (espacio-Tierra) o del servicio de radiodifusión por satélite en la banda 42-42,5 GHz no superará, en el emplazamiento de cualquier estación de radioastronomía, los siguientes valores: −137 dB(W/m^2) en 1 GHz y – 153 dB(W/m^2) en cualquier banda de 500 kHz de la banda 42,5- 43,5 GHz en el emplazamiento de una estación de radioastronomía registrada como telescopio de parábola única, y – 116 dB(W/m^2) en cualquier banda de 500 kHz de la banda 42,5-43,5 GHz en el emplazamiento de una estación de radioastronomía registrada como estación de interferometría con línea de base muy larga. Estos valores deberán aplicarse a cualquier estación de radioastronomía que: – esté en funcionamiento antes del 5 de julio de 2003 y se notifique a la Oficina antes del 4 de enero de 2004; o bien que – se haya notificado antes de la fecha de recepción de la información completa prevista en el Apéndice 4 para la coordinación o notificación, según proceda, sobre la estación espacial a la que se aplican los límites. Las demás estaciones de radioastronomía notificadas tras estas fechas, pueden recabar el acuerdo con las administraciones que hayan autorizado las estaciones espaciales. En la Región 2 se aplicará la Resolución 743 (CMR-03). Los límites de esta nota pueden sobrepasarse en el emplazamiento de una estación de radioastronomía de cualquier país cuya administración lo admita. (CMR-07)
685	5.552	En las bandas 42,5 - 43,5 GHz y 47,2 - 50,2 GHz se ha atribuido al servicio Fijo por satélite para las transmisiones Tierra-espacio mayor porción de espectro que la que figura en la banda 37,5 - 39,5 GHz para las transmisiones espacio-Tierra, con el fin de acomodar los enlaces de conexión de los satélites de radiodifusión. Se insta a las administraciones a tomar todas las medidas prácticamente posibles para reservar la banda 47,2 - 49,2 GHz para los enlaces de conexión para el servicio de radiodifusión por satélite que funciona en la banda 40,5 - 42,5 GHz.
686	5.552A	La atribución al servicio fijo en las bandas 47,2-47,5 GHz y 47,9-48,2 GHz está destinada para las estaciones en plataformas a gran altitud. Las bandas 47,2-47,5 GHz y 47,9- 48,2 GHz se utilizarán con arreglo a lo dispuesto en la Resolución 122 (Rev.CMR-07). (CMR- 07)
687	5.553	Las estaciones del servicio móvil terrestre pueden funcionar en las bandas 43,5-47 GHz y 66-71 GHz, a reserva de no causar interferencias perjudiciales a los servicios de radiocomunicación espacial a los que están atribuidas estas bandas (véase el número 5.43). (CMR-00)
688	5.554	En las bandas 43,5-47 GHz, 66-71 GHz, 95-100 GHz, 123-130 GHz, 191,8-200 GHz y 252-265 GHz se autorizan también los enlaces por satélite que conectan estaciones terrestres situadas en puntos fijos determinados, cuando se utilizan conjuntamente con el servicio móvil por satélite o el servicio de radionavegación por satélite. (CMR-00)
689	5.554A	La utilización de las bandas 47,5-47,9 GHz, 48,2-48,54 GHz y 49,44-50,2 GHz por el servicio fijo por satélite (espacio-Tierra) está limitada a los satélites geoestacionarios. (CMR-03)
690	5.555	Atribución adicional: la banda 48,94-49,04 GHz está también atribuida, a título primario, al servicio de radioastronomía. (CMR-00)
691	5.555A	SUP - CMR 2003.
692	5.555B	En la banda 48,94-49,04 GHz, la densidad de flujo de potencia producida por cualquier estación espacial geoestacionaria del servicio fijo por satélite (espacio-Tierra) que funcione en 2 las bandas 48,2-48,54 GHz y 49,44-50,2 GHz no debe exceder de – 151,8 dB(W/m ) en cualquier banda de 500 kHz en la ubicación de cualquier estación de radioastronomía. (CMR-03)
693	5.556	En virtud de disposiciones nacionales, pueden llevarse a cabo observaciones de radioastronomía en las banda51,4-54,25 GHz, 58,2-59 GHz y 64-65 GHz. (CMR-00)
694	5.556A	La utilización de las banda54,25 - 56,9 GHz, 57,0 - 58,2 GHz y 59,0 - 59,3 GHz por el servicio entre satélites se limita a los satélites geoestacionarios. La densidad de flujo de potencia de una sola fuente en altitudes de 1 000 km o inferiores sobre la superficie de la Tierra producida por las emisiones procedentes de una estación del servicio entre satélites, para todas las condiciones y todos los métodos de modulación, no deberá rebasar el valor de -147 dB(W/m^2/100MHz), en todos los ángulos de incidencia. (CMR-97)
695	5.556B	Atribución adicional: en Japón, la banda 54,25 - 55,78 GHz está también atribuida, a título primario, al servicio móvil para utilizaciones de baja densidad. (CMR-97)
696	5.557	Atribución adicional: en Japón, la banda 55,78 - 58,2 GHz está también atribuida, a título primario, al servicio de radiolocalización. (CMR-97)
697	5.557A	En la banda 55,78-56,26 GHz, para proteger las estaciones del servicio de exploración de la Tierra por satélite (pasivo), la máxima densidad de potencia entregada por un transmisor a la antena de una estación del servicio fijo está limitada a -26 dB(W/MHz). (CMR-00).
698	5.558	En las bandas 55,78-58,2 GHz, 59-64 GHz, 66-71 GHz, 122,25-123 GHz, 130-134 GHz, 167-174,8 GHz y 191,8-200 GHz podrán utilizarse estaciones del servicio móvil aeronáutico, a reserva de no causar interferencias perjudiciales al servicio entre satélites (véase el número 5.43). (CMR-00)
699	5.558A	La utilización de la banda 56,9-57 GHz por los sistemas entre satélites se limita a los enlaces entre satélites geoestacionarios y a las transmisiones procedentes de satélites no geoestacionarios en órbita terrestre alta dirigidas a satélites en órbita terrestre baja. Para los enlaces entre satélites geo-estacionarios, la densidad de flujo de potencia de una sola fuente en altitudes de 1 000 km o inferiores sobre la superficie de la Tierra, para todas las condiciones y 2 para todos los métodos de modulación, no deberá rebasar el valor de – 147 dB(W/ m /100 MHz), en todos los ángulos de incidencia. (CMR-97)
703	5.561	En la banda 74-76 GHz, las estaciones de los servicios fijo, móvil y de radiodifusión no causarán interferencias perjudiciales a las estaciones del servicio fijo por satélite o del servicio de radiodifusión por satélite que funcionen de conformidad con las decisiones de la conferencia encargada de elaborar un plan de adjudicación de frecuencias para el servicio de radiodifusión por satélite. (CMR-00)
704	5.561A	La banda 81-81,5 GHz también está atribuida a los servicios de aficionados y aficionados por satélite a título secundario. (CMR-00)
705	5.561B	En Japón, la utilización de la banda 84-86 GHz por el servicio fijo por satélite (Tierra- espacio) está limitada al enlace de conexión del servicio de radiodifusión por satélite que utiliza satélites geoestacionarios. (CMR-00)
706	5.562	La utilización de la banda 94 - 94,1 GHz por los servicios de exploración de la Tierra por satélite (activo) y de investigación espacial (activo) está limitada a los radares a bordo de vehículos espaciales para determinación de las nubes. (CMR-97)
707	5.562A	En las bandas 94-94,1 GHz y 130-134 GHz, las transmisiones de las estaciones espaciales del servicio de exploración de la Tierra por satélite (activo) dirigidas al haz principal de una antena de radioastronomía pueden afectar a algunos receptores de radioastronomía. Las agencias espaciales que explotan los transmisores y las estaciones de radioastronomía pertinentes deberían planificar de consenso sus operaciones a fin de evitar este problema en la mayor medida posible. (CMR-00)
708	5.562B	En las bandas 105-109,5 GHz, 111,8-114,25 GHz, 155,5-158,5 GHz y 217-226 GHz, el uso de esta atribución se limita estrictamente a las misiones espaciales de radioastronomía. (CMR-00)
709	5.562C	El uso de la banda 116-122,25 GHz por el servicio entre satélites está limitado a los satélites geoestacionarios. A todas las altitudes de 0 a 1 000 km por encima de la superficie de la Tierra y en la vecindad de todas las posiciones orbitales geoestacionarias ocupadas por sensores pasivos, la densidad de flujo de potencia de una sola fuente producida por una estación del servicio entre satélites, para todas las condiciones y todos los métodos de modulación, no deberá exceder de – 148 dB(W/(m 2 MHz)) cualquiera que sea el ángulo de llegada. (CMR-00)
710	5.562D	Atribución adicional : en Corea (Rep. de), las bandas 128-130 GHz, 171-171,6 GHz, 172,2-172,8 GHz y 173,3-174 GHz están atribuidas también al servicio de radioastronomía, a título primario, hasta 2015. (CMR-00)
711	5.562E	La atribución al servicio de exploración de la Tierra por satélite (activo) está limitada a la banda 133,5-134 GHz. (CMR-00)
712	5.562F	En la banda 155,5-158,5 GHz, la atribución a los servicios de exploración de la Tierra por satélite (pasivo) y de investigación espacial (pasivo) caducará el 1 de enero de 2018. (CMR- 00)
713	5.562G	La fecha de entrada en vigor de la atribución a los servicios fijo y móvil en la banda 155,5-158,5 GHz será el 1 de enero de 2018. (CMR-00)
714	5.562H	El uso de las bandas 174,8-182 GHz y 185-190 GHz por el servicio entre satélites está limitado a los satélites en órbita geoestacionaria. A todas las altitudes de 0 a 1 000 km por encima de la superficie de la Tierra y en la vecindad de todas las posiciones orbitales geoestacionarias ocupadas por sensores pasivos, la densidad de flujo de potencia de una sola fuente producida por una estación del servicio entre satélites, para todas las condiciones y todos los métodos de modulación, no deberá exceder de – 144 dB(W/(m 2 MHz)) cualquiera que sea el ángulo de llegada. (CMR-00)
715	5.563	SUP - CMR 2003.
716	5.563A	Las bandas 200-209 GHz, 235-238 GHz, 250-252 GHz y 265-275 GHz son utilizadas por sensores pasivos en tierra para efectuar mediciones atmosféricas destinadas al monitoreo de los constituyentes atmosféricos.
717	5.563B	La banda 237,9-238 GHz también está atribuida al servicio de exploración de la Tierra por satélite (activo) y al servicio de investigación espacial (activo) únicamente para los radares de nubes a bordo de vehículos espaciales. (CMR-00)
718	5.564	SUP - CMR 2000.
719	5.565	La banda de frecuencias 275-1 000 GHz puede ser utilizada por las administraciones para la experimentación y el desarrollo de distintos servicios activos y pasivos. Se ha reconocido que en esta banda es necesario efectuar las siguientes mediciones de rayas espectrales para los servicios pasivos: – servicio de radioastronomía: 275-323 GHz, 327-371 GHz, 388-424 GHz, 426-442 GHz, 453-510 GHz, 623-711 GHz, 795-909 GHz y 926-945 GHz; – servicio de exploración de la Tierra por satélite (pasivo) y servicio de investigación espacial (pasivo): 275-277 GHz, 294-306 GHz, 316-334 GHz, 342-349 GHz, 363-365 GHz, 371-389 GHz, 416-434 GHz, 442-444 GHz, 496-506 GHz, 546-568 GHz, 624-629 GHz, 634-654 GHz, 659-661 GHz, 684-692 GHz, 730-732 GHz, 851-853 GHz y 951- 956 GHz. En esta parte del espectro, todavía en gran parte inexplorada, los futuros trabajos de investigación podrían conducir al descubrimiento de nuevas rayas espectrales y bandas del continuum que interesan a los servicios pasivos. Se insta a las administraciones a que adopten todas las medidas prácticamente posibles para proteger los servicios pasivos contra las interferencias perjudiciales hasta la fecha en que se establezca el Cuadro de atribución en estas bandas. (CMR-00)
\.


--
-- TOC entry 2012 (class 0 OID 42637)
-- Dependencies: 1582
-- Data for Name: nota_internacional_banda; Type: TABLE DATA; Schema: public; Owner: -
--

COPY nota_internacional_banda (id_banda, id_nota_internacional) FROM stdin;
1	1
1	2
3	5
3	4
5	5
5	4
6	5
6	8
6	9
7	10
7	12
8	8
8	9
8	12
9	12
10	16
10	12
11	12
16	23
17	23
20	26
21	29
21	30
21	31
21	33
22	34
22	35
23	29
24	30
24	37
25	39
27	43
27	44
28	43
28	44
31	56
33	59
33	60
36	62
36	63
36	64
36	65
39	67
44	65
44	69
46	70
47	67
47	70
48	67
48	70
48	72
50	73
51	76
52	81
53	30
53	63
53	64
53	84
53	85
53	86
57	67
58	67
61	67
65	65
65	69
66	65
66	69
68	88
68	90
70	63
70	64
70	84
70	86
70	91
73	92
73	93
75	100
76	100
77	88
77	101
77	105
79	106
81	63
81	64
81	86
81	108
81	65
85	88
85	109
86	110
88	65
89	65
90	65
96	88
96	109
97	110
98	88
98	109
100	63
100	64
100	86
100	108
103	112
104	113
105	88
105	114
107	88
107	114
112	65
116	88
116	109
118	63
118	64
118	86
118	108
120	88
120	109
129	88
129	109
131	86
133	65
134	65
139	120
141	86
144	122
145	123
153	112
155	86
156	113
159	112
159	113
166	146
168	148
173	166
174	65
174	169
175	180
175	182
175	181
175	179
176	180
176	182
176	181
176	179
177	180
177	182
177	181
177	179
178	179
178	182
178	181
184	182
184	191
184	192
185	182
185	198
185	199
185	193
185	195
185	196
186	201
187	65
187	201
187	202
188	201
189	65
189	201
191	201
191	203
194	217
195	217
201	65
201	230
201	232
202	230
203	230
204	230
204	234
205	230
206	230
207	230
208	230
208	231
209	230
210	112
211	235
212	230
213	230
214	230
215	230
216	230
217	230
218	230
219	230
220	180
220	230
220	231
221	230
222	230
223	230
224	198
224	195
224	199
224	237
224	193
225	238
225	239
226	180
226	182
226	181
226	240
226	239
226	241
230	243
230	244
231	112
232	245
234	253
234	255
235	253
235	255
235	260
236	253
236	255
237	264
238	265
238	182
238	264
238	267
238	268
238	269
239	265
239	267
239	268
239	182
240	265
240	271
241	265
241	267
241	268
241	182
242	265
242	271
242	273
247	278
247	297
248	308
248	278
248	297
249	308
249	307
249	309
250	308
250	307
250	309
251	308
251	307
251	309
252	308
252	307
252	309
253	308
253	307
253	309
254	308
254	307
254	309
255	308
255	307
255	309
256	308
256	309
257	308
258	308
258	309
259	308
260	308
261	113
262	113
263	113
264	113
265	113
266	308
267	308
268	316
269	308
270	308
271	308
273	308
275	321
275	320
276	321
276	323
276	322
277	323
277	325
277	328
278	325
278	328
279	325
279	260
279	332
280	334
280	112
280	335
281	337
281	112
281	338
282	340
282	341
283	337
283	341
284	337
284	341
285	341
286	343
286	341
287	343
287	348
287	349
287	355
287	341
288	355
288	343
288	341
288	354
288	360
289	359
289	355
289	343
289	341
289	354
289	360
290	359
290	355
290	343
290	341
290	354
290	360
291	355
291	341
291	354
291	359
291	360
291	362
291	363
291	364
292	355
292	341
292	354
292	359
292	360
292	362
292	363
292	364
293	355
293	341
293	354
293	359
293	360
293	362
293	363
293	364
294	355
294	341
294	354
294	359
294	360
294	362
294	363
294	364
295	323
295	325
295	181
295	341
296	355
296	341
296	374
296	376
296	377
296	378
296	380
296	382
297	355
297	112
297	341
297	374
297	376
297	377
297	378
297	380
297	382
298	355
298	341
298	374
298	375
298	376
298	377
298	378
298	380
298	382
299	355
299	341
299	354
299	359
299	360
300	355
300	341
300	354
300	359
300	360
300	385
301	355
301	341
301	354
301	359
301	360
302	355
302	341
302	360
302	386
303	355
303	341
303	354
303	360
303	364
303	387
304	355
304	341
304	354
304	360
304	385
305	355
305	112
305	341
305	354
305	360
305	388
306	112
306	341
306	392
307	355
307	393
307	112
307	341
308	355
308	393
308	112
308	341
309	355
309	393
309	341
309	395
309	398
310	341
311	273
311	341
312	273
312	341
313	403
313	408
313	112
313	341
313	404
313	405
313	407
314	112
314	341
314	404
314	405
314	407
315	112
315	341
315	404
315	405
315	407
316	403
316	408
316	112
316	341
316	404
316	405
316	407
317	475
317	407
318	407
319	407
320	407
320	411
320	412
321	407
321	411
321	412
322	407
322	413
322	415
323	418
323	419
324	475
324	407
325	408
325	407
326	407
326	413
326	415
327	407
328	418
328	419
330	403
330	424
331	113
331	260
331	424
332	113
333	355
333	426
333	113
333	430
334	438
334	444
334	403
335	438
335	444
335	403
335	441
335	446
335	338
336	438
336	181
336	444
336	403
336	441
336	446
336	112
337	438
337	181
337	444
337	403
337	112
338	340
339	334
339	461
340	465
340	464
340	463
340	466
341	112
342	112
343	260
346	482
346	484
347	485
348	486
348	485
349	487
349	485
349	112
349	338
350	112
351	377
352	377
353	377
353	491
354	493
354	377
354	491
354	492
355	500
355	496
355	497
355	495
355	501
355	502
355	498
356	503
356	496
356	505
356	507
357	496
357	505
357	507
358	511
358	508
358	509
359	511
359	510
359	508
360	510
360	508
361	516
362	260
363	113
364	113
365	113
366	524
366	112
366	484
366	525
367	486
367	525
367	526
367	527
367	528
368	525
369	525
369	530
370	525
371	531
372	531
373	532
375	533
377	531
378	536
379	536
380	536
381	538
383	543
385	544
386	546
387	334
387	551
388	546
388	549
389	466
389	549
389	550
389	552
389	554
389	551
390	554
391	557
391	558
392	559
393	559
393	560
394	561
397	112
397	563
398	340
398	564
399	486
399	566
400	567
400	571
401	567
401	571
401	572
402	570
402	571
402	573
404	486
405	580
405	582
406	586
406	587
407	588
407	589
408	566
408	596
408	591
408	597
408	592
409	566
409	596
409	591
409	597
409	592
410	566
410	596
410	522
410	597
410	592
411	566
411	596
411	522
411	597
411	592
412	566
412	596
412	522
412	597
412	112
412	592
413	604
414	338
415	340
417	606
417	608
418	609
419	611
420	611
421	611
422	611
422	612
423	615
423	614
424	618
424	615
424	614
425	566
425	621
425	620
426	566
426	617
426	621
426	620
427	566
427	617
428	625
428	617
428	624
429	628
430	629
430	630
430	631
430	632
431	566
431	617
431	634
431	635
431	636
431	637
431	638
432	566
432	617
432	634
432	635
432	636
432	637
436	112
437	112
437	641
439	337
439	112
441	340
442	113
443	113
445	642
447	644
448	646
449	647
449	648
449	646
450	646
450	650
451	566
451	653
451	617
451	652
451	654
452	566
452	628
452	653
452	617
452	655
452	654
453	630
453	632
453	645
453	653
453	656
453	617
453	655
453	654
454	566
454	653
454	617
454	655
454	634
454	635
454	636
454	638
454	654
455	566
455	653
455	617
455	655
455	658
455	634
455	635
455	636
455	652
455	654
456	337
457	337
457	660
457	112
458	340
459	340
460	664
460	663
460	669
461	664
461	663
461	669
462	664
462	663
462	669
463	664
463	663
468	671
469	112
469	673
470	663
472	663
473	663
474	663
476	663
477	617
477	663
477	683
477	684
478	685
478	112
478	663
479	687
479	688
481	685
481	686
482	685
483	685
483	686
484	337
484	685
484	112
484	340
484	690
485	340
486	337
487	337
487	663
487	693
488	340
488	693
489	694
489	695
490	697
490	694
490	698
490	663
491	699
491	698
491	663
492	694
492	698
492	663
493	663
493	693
494	694
494	698
494	700
495	698
495	700
495	92
496	663
496	693
497	663
498	687
498	698
498	688
500	703
501	112
502	112
503	112
503	702
504	112
505	112
505	704
506	112
507	340
508	112
509	706
509	707
510	112
511	112
511	688
512	340
512	341
513	112
513	341
514	708
514	112
514	341
515	340
515	341
516	708
516	112
516	341
517	340
517	341
518	709
518	341
519	709
519	341
520	709
520	92
521	698
521	92
522	688
523	112
523	688
524	711
524	698
524	112
524	707
526	112
527	112
528	340
529	112
530	712
530	708
530	112
530	713
532	340
533	698
534	698
534	112
535	698
535	112
536	698
537	714
538	340
539	714
540	340
541	698
541	112
541	341
541	688
542	340
542	341
542	716
543	112
543	341
544	708
544	112
544	341
545	340
548	716
548	717
551	92
551	112
552	112
553	340
553	716
554	112
554	688
555	112
555	716
556	719
\.


--
-- TOC entry 2009 (class 0 OID 42602)
-- Dependencies: 1578
-- Data for Name: nota_nacional; Type: TABLE DATA; Schema: public; Owner: -
--

COPY nota_nacional (id_nota_nacional, nombre, descripcion) FROM stdin;
1	CLM 1	Se adoptan las Recomendaciones de la UIT-T Serie K sobre “Protección contra interferencias” y sobre el cumplimiento de los límites de exposición de las personas a los campos electromagnéticos". (6)
2	CLM 2	La cesión de los permisos para el uso del espectro radioeléctrico cuenta con reglamentación especial que puede consultarse en la sección Referencias. (7)
3	CLM 3	Se adoptan las bandas de frecuencias atribuidas internacionalmente a los servicios móvil marítimo, móvil marítimo por satélite y radionavegación marítima para su uso dentro del territorio nacional. (8)
4	CLM 4	Las estaciones que operan en las bandas asignadas para frecuencias patrón y señales horarias, gozan de protección contra interferencias perjudiciales.
5	CLM 5	Se atribuyen las bandas de frecuencias 0,045 – 0,490 MHz, 0,535 – 1,705 MHz, 26,957 – 27,283 MHz, 29,720 – 30,000 MHz, 36,000 – 36,600 MHz, 72,000 – 74,800 MHz, 174,000 – 216,000 MHz, 426,025 – 426,1375 MHz, 429,2375 – 429,250 MHz, 429,8125 – 429,925 MHz, 433,000 – 434,790 MHz, 449,8375 – 469,925 MHz, 894,000 – 896,000 MHz, 897,125 – 897,500 MHz, 905,000 – 908,000 MHz, 915,000 – 924,000 MHz, 924,000 – 928,000 MHz, 928,000 – 929,000 MHz, 932,000 – 935,000 MHz y 936,125 – 940,000 MHz; para ser utilizadas libremente por parte del público en general para aplicaciones de telemetría y telecontrol, con bajos niveles de potencia, siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones, que pueden ser consultados en la página web de la entidad. (9)
6	CLM 6	Se adoptan las bandas de frecuencias atribuidas internacionalmente a los servicios móvil aeronáutico (R), móvil aeronáutico por satélite (R) y radionavegación aeronáutica. El Plan de adjudicación de frecuencias del servicio móvil aeronáutico (R) está contenidoen el Apéndice 27 del Reglamento de Radiocomunicaciones de la UIT. Se adoptan las bandas de frecuencias atribuidas internacionalmente al servicio móvil aeronáutico (OR). El Ministerio de Tecnologías de la información y las Comunicaciones reglamentará y planificará el uso y explotación de estas bandas de frecuencias conforme a lo dispuesto en el Apéndice 26 del Reglamento de Radiocomunicaciones de la UIT, los desarrollos tecnológicos y las necesidades del País.
7	CLM 7	La banda 495 - 505 kHz está internacionalmente atribuida al servicio móvil (socorro y llamada) y en el ámbito nacional se atribuye para operación de los sistemas auxiliares de ayuda del servicio móvil marítimo.
8	CLM 8	Se prohíbe toda emisión que pueda causar interferencias perjudiciales a las comunicaciones de socorro, alarma, urgencia o seguridad transmitidas en las frecuencias 500 kHz, 2 174,5 kHz, 2 182 kHz, 2 187,5 kHz, 4 125 kHz, 4 177,5 kHz, 4 207,5 kHz, 6 215 kHz, 6 268 kHz, 6 312 kHz, 8 291 kHz, 8 376,5 kHz, 8 414,5 kHz, 12 290 kHz, 12 520 kHz, 12 577 kHz, 16 420 kHz, 16 695 kHz, 16 804,5 kHz, 121,5 MHz, 156,525 MHz, 156,8 MHz o en las bandas de frecuencias 406 - 406,1 MHz, 1 544 - 1 545 MHz y 1 645,5 - 1 646,5 MHz. Se prohibe toda emisión que cause interferencia perjudicial a las comunicaciones de socorro y seguridad en cualquiera de las demás frecuencias indicadas en el artículo 31 y en el apéndice 15 del Reglamento de Radiocomunicaciones de la UIT.
9	CLM 9	Se atribuye en el ámbito nacional la frecuencia 518 kHz para operación de los sistemas auxiliares de ayuda del servicio móvil marítimo. En el servicio móvil marítimo, la frecuencia 518 kHz se utiliza exclusivamente para la transmisión por estaciones costeras de avisos a los navegantes, boletines meteorológicos e información urgente con destino a los barcos, empleando la telegrafía de impresión directa de banda estrecha sistema NAVTEX internacional.
10	CLM 10	Se atribuyen las bandas de frecuencias 0,535 – 1,705 MHz, 27,500 – 28,000 MHz, 29,700 – 39,000 MHz, 72,000 – 74,800 MHz, 75,200 – 76,000 MHz, 88,000 – 108,000 MHz, 173,200 – 174,000 MHz, 216,0125 – 216,9875 MHz y las frecuencias 3,175, 3,225, 3,275 y 3,325 MHz; para ser utilizadas libremente por parte del público en general para aplicaciones en la transmisión de voz con bajos niveles de potencia, siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones, que pueden ser consultados en la página web de la entidad. (9)
11	CLM 11	Al Servicio de radiodifusión sonora en amplitud modulada (A.M.), le son aplicables los derechos, garantías y deberes previstos en la Constitución Política, la Ley 1341 de 2009, las normas que la reglamenten, y en el "Plan Técnico Nacional en Amplitud Modulada (A.M.)", el cual se entiende incorporado al presente Cuadro Nacional de Atribución de Bandas de Frecuencias y puede ser consultado en la página web del Ministerio de Tecnologías de la Información y las Comunicaciones. (10) (11) (12) (13) La banda de frecuencias 1 625 – 1 705 kHz se atribuye y reserva para futuros desarrollos de la radiodifusión sonora en ondas hectométricas.
12	CLM 12	El servicio de aficionados es un servicio de radiocomunicación que tiene por objeto la instrucción individual, la intercomunicación y los estudios técnicos efectuados por aficionados, debidamente autorizados, que se interesan en la radioexperimentación con fines exclusivamente personales y sin ánimo de lucro. Este servicio cuenta con reglamentación especial expedida por el Ministerio de Tecnologías de la Información y las Comunicaciones; dicha reglamentación puede consultarse en la sección Referencias. El plan de distribución de canales se encuentra relacionado en la Tabla 1; el plan nacional de frecuencias se puede consultar en la Tabla 2. (14)
13	CLM 13	La banda de frecuencias 2 173,5 - 2 190,5 kHz está internacionalmente atribuida al servicio MÓVIL (socorro y llamada) y nacionalmente se atribuye para la operación de los sistemas auxiliares de ayuda del servicio móvil marítimo.
14	CLM 14	Las frecuencias portadoras 2 182 kHz, 3 023 kHz, 5 680 kHz y 8 364 kHz, y las frecuencias 121,5 MHz, 156,8 MHz y 243 MHz pueden además utilizarse de conformidad con los procedimientos de vigor para los servicios de radiocomunicación terrenales, en operaciones de búsqueda y salvamento de vehículos espaciales tripulados. Las condiciones de utilización de estas frecuencias se fijan en el artículo 31 y en el apéndice 15 del Reglamento de Radiocomunicaciones y, en las disposiciones que para el efecto dicte el Ministerio de Tecnologías de la Información y las Comunicaciones. También pueden utilizarse las frecuencias 10 003 kHz, 14 993 kHz y 19 993 kHz, aunque en éste caso, las emisiones deben estar limitadas a un ancho de banda de 3 kHz en torno a dichas frecuencias.
41	CLM 41	Las bandas de frecuencias 343,050 – 345,150 MHz, 357,050 – 359,150 MHz, 380,025 – 382,000 MHz y 390,025 – 392,000 MHz; están atribuidas, a título primario, al servicio FIJO y para operación de sistemas de acceso fijo inalámbrico, y compartidas a título secundario con los servicios previstos en el presente Cuadro Nacional de Atribución de Bandas de Frecuencias. (27)
42	CLM 42	En virtud del número 5.262 del Reglamento de Radiocomunicaciones, se atribuye la banda de frecuencias 400,05 ‑401 MHz adicionalmente, para la prestación de los servicios FIJO y MÓVIL a título primario.
15	CLM 15	Se adoptan las bandas de frecuencias internacionalmente atribuidas de conformidad con los apéndices 17 y 18 del Reglamento de Radiocomunicaciones de la UIT. Los planes de distribución de canales para la banda de frecuencias entre 4 000 KHz y 27 500 KHz se encuentran relacionados a partir de la Tabla 3 y hasta la Tabla 18.<br /> Las bandas de frecuencias de transmisión para explotación símplex en banda lateral única (BLU), y frecuencias de transmisión para comunicaciones entre barcos en banda cruzada (dos frecuencias), entre 4 000 kHz y 27 500 kHz, se encuentran relacionadas en la Tabla 13.<br />Las frecuencias utilizables en las bandas atribuidas exclusivamente al servicio móvil marítimo entre 4 000 y 27 500 kHz, para ser utilizadas por las estaciones de barco, para la transmisión de datos oceanográficos, telefonía dúplex; para estaciones de barco y estaciones costeras para la telefonía en símplex, para telegrafía de banda ancha, facsímil y sistemas especiales de transmisión; para estaciones de barco, en sistemas de telegrafía de impresión directa de banda estrecha, y transmisión de datos a velocidades no superiores a 100 baudios para MDF y 200 baudios para MDP; para estaciones de barco para telegrafía Morse de clase A1A o A1B, se encuentran relacionadas en la Tabla 14.<br /> La disposición de canales para los sistemas de telegrafía de impresión directa de banda estrecha y de transmisión de datos en las bandas del servicio móvil marítimo comprendidas entre 4 000 kHz y 27 500 kHz (frecuencias asociadas por pares) se encuentra relacionada en la Tabla 15.<br /> La disposición de canales para los sistemas de telegrafía de impresión directa de banda estrecha y de transmisión de datos en las bandas del servicio móvil marítimo comprendidas entre 4 000 kHz y 27 500 kHz (frecuencias no asociadas por pares) se encuentra relacionada en la Tabla 16.<br />Las frecuencias de llamada asignables a las estaciones de barco para telegrafía morse de clase A1A o A1B, a velocidades no superiores a 40 baudios con anchura de banda en cada una de las bandas de 0,5 kHz, se encuentran relacionadas en la Tabla 17.
16	CLM 16	El uso de la banda de frecuencias 4 000 - 4 063 kHz, por el servicio móvil marítimo, está limitado a las estaciones de barco que funcionen en radiotelefonía con emisión J3E y no emplearán bajo ningún concepto una potencia de cresta superior a 1,5 kW por canal y le es aplicable el apéndice 17 del Reglamento de Radiocomunicaciones.
17	CLM 17	Se atribuyen en el ámbito nacional las frecuencias 4 125 kHz, 6 215 kHz, 6 268 kHz, 6 312 kHz, 6 313 kHz y 6 314 kHz, 8 291 kHz, 8 364 kHz, 8 376,5 kHz, 8 414,5 kHz y 8 416,5 kHz para operación de los sistemas auxiliares de ayuda del servicio móvil marítimo.
18	CLM 18	La operación de los sistemas de radiocomunicación de Banda Ciudadana cuenta con reglamentación especial que puede consultarse en la sección de Referencias. El plan nacional de distribución de canales para operación de estos sistemas se encuentra relacionado en la Tabla 19 y en la Tabla 20. Se atribuye dentro del territorio nacional, de manera exclusiva, los canales 7 (27.035 kHz), 8 (27.055 kHz), 9 (27.065 kHz) y 10 (27.075 kHz) para información y coordinación de atención de emergencias, desastres y seguridad ciudadana. (15) (16)
19	CLM 19	Se atribuyen las bandas de frecuencias 40,660 – 40,700 MHz, 70,000 – 108,000 MHz, 138,000 – 149,900 MHz, 150,500 – 156,500 MHz, 156,900 – 174,000 MHz, 174,000 – 260,000 MHz, 260,000 – 328,600 MHz, 335,400 – 399,900 MHz, 406,000 – 470,000 MHz, 470,000 – 960,000 MHz y frecuencias mayor a 1 427,000 MHz; para ser utilizadas libremente por parte del publico en general para aplicaciones de dispositivos de operación momentánea, , siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones. (9)
20	CLM 20	Se atribuyen las bandas de frecuencias 43,7 – 50,0 MHz, 902,0 – 928,0 MHz y 2 400,0 – 2 483,5 MHz; para ser utilizadas libremente para la operación de teléfonos inalámbricos que se conecten a la red telefónica pública conmutada (RTPC), siempre y cuando dichos aparatos operen en recintos cerrados, sean de baja potencia y corto alcance, siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones. (17)
21	CLM 21	La Ley 182 del 20 de enero de 1995, reglamentó el servicio de radiodifusión televisión y formuló políticas para su desarrollo, democratizó el acceso a éste, estableció normas para la prestación del servicio y conformó la Comisión Nacional de Televisión. La ley 335 del 20 de diciembre de 1996 modificó parcialmente la ley 14 de 1991 y la ley 182 de 1995, creó la televisión privada en Colombia y dictó otras disposiciones.<br />El Servicio de radiodifusión televisión en VHF se presta mediante la distribución de canales con un ancho de banda de 6 MHz cada uno, repartidos así: en la banda 54 - 66 MHz (canales 2 y 3), en la banda 66 - 72 MHz (canal 4), en la banda 76 - 88 MHz (canale5 y 6), en la banda 174 - 216 MHz (canales 7 al 13). El Plan de distribución de canales puede consultarse en la Tabla 21. El plan de frecuencias para la operación de los canales nacionales, regionales, locales y zonales de televisión hace parte integrante del presente Cuadro Nacional de Atribución de Bandas de Frecuencias. (18)
22	CLM 22	La banda de frecuencias 73 - 74,6 MHz, se atribuye a titulo secundario para los servicios fijo - móvil (ver el número 5.178 del R.R).
23	CLM 23	Al Servicio de radiodifusión sonora en frecuencia modulada (F.M.), le son aplicables los derechos, garantías y deberes previstos en la Constitución Política, la Ley 1341 de 2009, las normas que la reglamenten, y en el "Plan Técnico Nacional en Frecuencia Modulada (F.M.)", el cual se entiende incorporado al presente Cuadro Nacional de Atribución de Bandas de Frecuencias y pueden ser consultado en la página web del Ministerio de Tecnologías de la Información y las Comunicaciones. (10) (11) (12) (13)
24	CLM 24	La banda de frecuencias 108 - 112 MHz, se aplica para la operación de sistemas de aterrizaje por instrumentos (ILS) de las aeronaves en los aeropuertos nacionales.
25	CLM 25	La banda de frecuencias 112 - 117,975 MHz, se utiliza para la operación de radiofaros NDB de localización y radiofaros VOR, para la orientación efectiva de las aeronaves hacia el eje de rumbo deseado.
26	CLM 26	La banda de frecuencias 117,975 - 136 MHz se utiliza para establecer las coordinaciones entre la torre de control, las instalaciones del aeropuerto y las diversas aeronaves que convergen a dicho aeropuerto.
27	CLM 27	La frecuencia aeronáutica de emergencia 121,5 MHz, se utiliza con fines de socorro y urgencia en radiotelefonía. La frecuencia auxiliar 123,1 MHz podrá ser utilizada por estaciones que participen en operaciones de búsqueda y salvamento.
28	CLM 28	Se adopta una distribución de canales a 12,5 kHz entre frecuencias portadoras, para los sistemas convencionales o monocanales de voz que operen en las siguientes bandas de frecuencias: 138 - 144 MHz, 148 - 174 MHz, 225 - 245 MHz, 403 - 430 MHz y 440 - 470 MHz. (19)
29	CLM 29	Las bandas de frecuencias 138 -144 MHz, 148 – 156,4875 MHz, 156,5625 – 156,7625 MHz, 157, 45 – 174 MHz, 225 - 245 MHz y 406 - 430 MHz, 440 – 470 MHz están atribuidas al servicio móvil y podrán ser utilizadas en sistemas convencionales o monocanales de voz y repetidoras comunitarias. Los demás servicios a los que están atribuidas estas bandas de frecuencias conservan su categoría. (20)
43	CLM 43	Se adoptan los planes de distribución de canales radioeléctricos para los sistemas inalámbricos fijos digitales que funcionan en la banda de frecuencias 406,1-450 MHz, de acuerdo con la Recomendación UIT-R F. 1567, de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se pueden consultar a partir de la Tabla 26 y hasta la Tabla 39.
44	CLM 44	Se atribuyen las bandas de frecuencias 412,000 – 415,000 MHz, 422,000 – 425,000 MHz, 415,000 – 420,000 MHz y 425,000 – 430,000 MHz para operación de sistemas de acceso troncalizado, en áreas de servicio departamental y municipal. Los planes de distribución de canales radioeléctricos se pueden consultar a partir de la Tabla 40 y hasta la Tabla 41. (25)
30	CLM 30	Se atribuyen las siguientes frecuencias de los servicios Fijo y Móvil, para la operación exclusiva del Sistema Nacional de Radiocomunicación de Emergencia Ciudadana: 138,9125MHz, 140,0125MHz, 142,950MHz, 143,800MHz, 143,9125MHz, 148,812MHz, 154,1125MHz, 160,2625MHz, 164,0125MHz, 441,5500MHz, 441,8000MHz, 446,2500MHz, 453,0375MHz, 455,7125MHz, 457,0375MHz, 460,775MHz, 465,775MHz, 467,925MHz, 813,2375MHz, 813,4875MHz, 858,2375MHz, 858,4875MHz. Se atribuyen en forma particular las frecuencias 142,950MHz, 143,800MHz, 148,812MHz, 460,775MHz, 465,775MHz, 467,925MHz al Sistema Nacional de Radiocomunicación de Emergencia Ciudadana, para uso exclusivo por parte de la Policía Nacional y su red de apoyo, la cual se coordinará con los usuarios del Sistema Nacional de Radiocomunicación de Emergencia Ciudadana.<br />Los radios y equipos de Radiocomunicación para la operación del Sistema Nacional de Radiocomunicación de Emergencia Ciudadana deberán satisfacer las siguientes características técnicas: los radios o equipos de radiocomunicación deberán pertenecer a los sistemas monocanales de voz, denominados sistemas de radiocomunicación convencional o que utilicen tecnología de canales múltiples compartidos o sistemas de acceso troncalizado de voz, sintonizables, programables o ajustables a las frecuencias radioeléctricas atribuidas y deberán operar exclusivamente en dichas frecuencias; los radios o equipos de radiocomunicación, pertenecientes a los sistemas de radio convencional de voz o monocanales de voz, deberán operar con una anchura de banda necesaria de 11K0, o de 12,5 KHz de ancho de banda asignado y en la siguiente clase de emisión: F3E, un canal de voz con modulación en frecuencia; los radios o equipos de radiocomunicación, pertenecientes a los sistemas de acceso troncalizado, podrán operar con una anchura de banda necesaria de 16K0, o de 25 KHz de ancho de banda asignado.La potencia de transmisión de los radios o equipos de radiocomunicación fijos, móviles, transportables o portátiles, no deberá exceder de los 25 vatios nominales.<br />Los operadores del Sistema Nacional de Radiocomunicación de Emergencia Ciudadana deberán ajustar la potencia de transmisión de las estaciones de base y la ganancia de las antenas, de manera que se permita el cubrimiento geográfico del municipio respectivo y la coordinación de frecuencias con los municipios adyacentes.<br />Se asignan, de manera general, las frecuencias radioeléctricas 139,2375 MHz y 142,2375 MHz, 157,8375 MHz y 164,2375 MHz, con ancho de banda de 12,5 KHz, para ser utilizadas por los operadores del Sistema de Emergencia Ciudadana, libremente, dentro del territorio nacional, como frecuencias de enlace punto a punto.<br />Las frecuencias 138 MHz, 148,3 MHz y 149,275 MHz, están reservadas para el uso dentro de recintos cerrados, en las ciudades de Cali y Santa Fe de Bogotá, y con equipos portátiles con una potencia efectiva radiada menor a un vatio (1 W). (21) (22)
31	CLM 31	Las frecuencias radioeléctricas 138,9875 MHz, 143,1625 MHz, 157,8625 MHz, 160,5125 MHz, 440,1125 MHz, 445,1375 MHz, 451,1125 MHz y 454,5125 MHz podrán ser utilizadas libremente, en el ámbito local y municipal y las frecuencias radioeléctricas 139,2125 MHz, 143,2625 MHz, 159,7875 MHz, 164,6125 MHz, 441,5875 MHz, 446,8750 MHz, 453,4250 MHz, 458,5750 MHz podrán ser utilizadas libremente, en el ámbito departamental, sin lugar a pago de contraprestaciones, por las entidades territoriales para la operación de los Sistemas de Radiocomunicación Cívico Territorial, siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones.<br />Los radios y equipos de radiocomunicación del sistema de radiocomunicación cívico territorial, utilizados en la operación libre del espectro, deberán satisfacer las siguientes características técnicas: Operar en las frecuencias y bandas de frecuencias designadas para tal fin; los radios o equipos de radiocomunicaciones deberán pertenecer a los sistemas monocanales de voz denominados también sistemas de radiocomunicación convencional de voz, que operen dentro de los parámetros radioeléctricos autorizados; los radios o equipos de radiocomunicación deberán operar con una anchura de banda necesaria de 11K0, o de 12,5 KHz de ancho de banda asignado y con clase de emisión: F3E, un canal de voz con modulación en frecuencia; la potencia de transmisión de los radios o equipos de radiocomunicación de los usuarios no deberá exceder de los 25 vatios nominales.<br />La modalidad de explotación de los canales radioeléctricos podrá ser realizada en modo semidúplex, utilizando ambas frecuencias para la transmisión y recepción de la señal, respectivamente, o en modo de explotación simplex, para la operación radio a radio, entre los radios pertenecientes al sistema de radiocomunicación Cívico Territorial. Los operadores de sistemas de radiocomunicación Cívico Territorial deberán ajustar la potencia de transmisión de las estaciones de base y la ganancia de las antenas, de manera que se permita el cubrimiento geográfico del municipio respectivo y la coordinación de frecuencias con los municipios adyacentes. (23)
32	CLM 32	Se atribuyen las frecuencias 151,6125 MHz, 153,0125 MHz, 462,5625 MHz, 462,5875 MHz, 462,6125 MHz, 462,6375 MHz, 462,6625 MHz, 462,6875 MHz, 462,7125 MHz, 467,5625 MHz, 467,5875 MHz, 467,6125 MHz, 467,6375 MHz, 467,6625 MHz, 467,6875 MHz, 467,7125 MHz, 467,7625 MHz, 467,8125 MHz, 467,8375 MHz y 467,9125 MHz; para ser utilizadas libremente por parte del público en general en los radios portátiles de operación itinerante, siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones. (24)
33	CLM 33	Se atribuyen en el ámbito nacional las frecuencias 156,3 MHz, 156,525 MHz y 156,65 MHz para operación de los sistemas auxiliares de ayuda del servicio móvil marítimo.<br />Se puede utilizar la frecuencia 156,3 MHz, empleando emisiones de la clase G3E, para la comunicación entre las estaciones de barco y de aeronave que participen en operaciones coordinadas de búsqueda y salvamento. También puede ser utilizada por las estaciones de aeronave para comunicar con las estaciones de barco para otros fines de seguridad.<br />La frecuencia 156,525 MHz se utilizará exclusivamente para la llamada selectiva digital con fines de socorro, seguridad y llamada en el servicio móvil marítimo en ondas métricas (véase la Resolución 323 (Mob ‑87)). Las condiciones de utilización de esta frecuencia se hallan fijadas en los ar-tículos 31 y 52 y en los apéndices 13 y 18 del Reglamento de Radiocomunicaciones.<br />En las comunicaciones entre las estaciones de barco a barco, relativas a la seguridad de la navegación se utiliza la frecuencia 156,650 MHz conforme con la nota p) del apéndice 18 del Reglamento de Radiocomunicaciones.
34	CLM 34	Se puede utilizar la frecuencia 156,3 MHz, empleando emisiones de clase G3E, para la comunicación entre las estaciones de barco y aeronave que participan en operaciones coordinadas de búsqueda y salvamento. También puede ser utilizada por las estaciones de aeronave para comunicar las estaciones de barco para otros fines de seguridad (véase también la nota g) del apéndice 18 del Reglamento de Radiocomunicaciones).<br />Las bandas de frecuencias 156 - 157,45 MHz y 160,6 - 162,05 MHz se atribuyen a título primario para el servicio móvil marítimo en las zonas costeras colombianas, pero se autoriza su utilización en redes continentales de los servicios FIJO y MÓVIL a condición de no causar interferencia al servicio móvil marítimo. El cuadro de frecuencias de transmisión para estaciones de barco del servicio móvil marítimo en la banda 156 - 162,05 MHz, se encuentra relacionado en la Tabla 22.
35	CLM 35	Las bandas de frecuencias 227,500 - 228,250 MHz, 232,500 - 233,250 MHz y 245,450 - 246,950 MHz se atribuyen para la operación de los equipos transmóviles del servicio de radiodifusión sonora. El plan de distribución de canales para la operación de estos equipos aparece en la Tabla 23.
36	CLM 36	Se atribuyen las bandas de frecuencias 254,000 – 260,000 MHz y 262,000 – 268,000 MHz para operación de sistemas de acceso troncalizado, en áreas de servicio departamental y municipal. El Plan de distribución de canales radioeléctricos aparece en la Tabla 24. (25)
37	CLM 37	La banda de frecuencias 257 - 278 MHz, está prevista para operación de sistemas convencionales o monocanales de voz full duplex, con las siguientes características técnicas emisión y ancho de banda 11KF3E y separación entre frecuencias portadoras de 12,5 kHz, separación entre frecuencias de transmisión y recepción de 13 MHz.
38	CLM 38	Se atribuyen las bandas de frecuencias 285,000 – 322,000 MHz, 433,000 – 434,790 MHz, 902,000 – 928,000 MHz, 2 900,000 – 3 100,000 MHz, 3 267,000 – 3 332,000 MHz, 3 339,000 – 3 345,800 MHz, 3 358,000 – 3 400,000 MHz, 5 785,000 – 5 815,000 MHz, 13 400,000 – 13 750,000 MHz, 24 050,000 – 24 250,000 MHz, 76 000,000 – 77 000,000 MHz, y las frecuencias 0,1250, 0,1232 y 0,1342 MHz; para ser utilizadas libremente por parte del público en general para aplicaciones de telemetría, telealarmas y telecontrol vehicular con bajos niveles de potencia e intensidad de campo, siempre y cuando se respeten los límites de intensidad de campo establecidos por el Ministerio de Tecnologías de la Información y las Comunicaciones. (9)
39	CLM 39	La banda de frecuencias 300 - 328,6 MHz, se atribuye para operación de enlaces entre estudios y sistemas de transmisión de las estaciones de radiodifusión sonora, así: de 300,100 MHz a 328,500 MHz, 143 canales con una separación de 200 kHz. Los planes de distribución de canales para los enlaces entre estudios y sistemas de transmisión se encuentran en la Tabla 25. (26)
40	CLM 40	La banda de frecuencias 335,4 - 343 MHz, está prevista para operación de enlaces nacionales de las estaciones de radiodifusión sonora.
45	CLM 45	De acuerdo con la Resolución 2545 de 14 de octubre de 2009, se reserva en todo el territorio nacional la banda de frecuencias 470 - 512 MHz para dar cabida a nuevas tecnologías y aplicaciones de radiocomunicación. Conforme la citada resolución, en la banda de frecuencias 470 - 512 MHz no se otorgarán nuevos permisos para el uso del espectro radioeléctrico hasta tanto el Ministerio de Tecnologías de la Información y las Comunicaciones determine la atribución, canalización y distribución del espectro dentro del territorio nacional y el procedimiento administrativo para su otorgamiento y asignación. El Ministerio de Tecnologías de la Información y las Comunicaciones podrá reubicar en otras bandas de frecuencias a aquellos titulares que tengan asignadas frecuencias en la banda objeto de reglamentación. (28)
46	CLM 46	La ley 182 del 20 de enero de 1995, reglamentó el servicio de radiodifusión televisión y formuló políticas para su desarrollo, democratizó el acceso a éste, estableció normas para la prestación del servicio y conformó la Comisión Nacional de Televisión. La ley 335 del 20 de diciembre de 1996 modificó parcialmente la ley 14 de 1991 y la ley 182 de 1995, creó la televisión privada en Colombia y dictó otras disposiciones.<br />El servicio de radiodifusión televisión en UHF se presta mediante la distribución de canales con un ancho de banda de 6 MHz cada uno, repartidos así: en la banda 512 - 608 MHz (canales 21 al 36), en la banda 614 - 698 MHz (canales 38 al 51). El Plan de distribución de canales puede consultarse en la Tabla 21. El plan de frecuencias para la operación de los canales nacionales, regionales, locales y zonales de televisión, hace parte integrante del presente Cuadro Nacional de Atribución de Bandas de Frecuencias.(18)<br />El canal 37, no se utiliza para el servicio de radiodifusión televisión, toda vez que, corresponde a la banda 608 - 614 MHz, que está atribuida a titulo primario al servicio de radioastronomía.<br />En virtud de la Resolución 2623 de 2009, el Ministerio de Tecnologías de la Información y las Comunicaciones atribuye y reserva la banda de frecuencias 698 - 806 MHz de conformidad con la resolución 224 (CMR-07) de la UIT. De igual forma atribuye y reserva la banda de frecuencias 470 - 512 MHz. La citada resolución incluye un plan de implementación en la banda de frecuencias 698 - 806 MHz y un plan de migración en la banda de frecuencias 470 - 512 MHz. (29)
47	CLM 47	Se atribuyen las bandas de frecuencias 806 - 821 MHz, 821 – 824 MHz, 851 - 866 MHz, 866 – 869 MHz, 896 – 897,125 MHz y 935 – 936,125 para operación de sistemas de acceso troncalizado (Trunking). Los planes de distribución de canales para la instalación, operación y explotación de estos sistemas se pueden consultar a partir de la Tabla 42 y hasta la Tabla 44.<br />El Ministerio de Tecnologías de la Información y las Comunicaciones, podrá reglamentar y planificar otras bandas de frecuencias, teniendo en cuenta los desarrollos tecnológicos de los sistemas de acceso troncalizado y las necesidades en materia de telecomunicaciones dentro del territorio nacional. (30)
48	CLM 48	Se atribuyen las bandas de frecuencias 824 - 849 MHz y 869 - 894 MHz para operación de redes de telefonía móvil celular. El plan de distribución de canales para operación de estas redes se relaciona en la Tabla 45.<br />El Ministerio de Tecnologías de la Información y las Comunicaciones, podrá reglamentar y planificar otras bandas de frecuencias para operación de este tipo de redes, teniendo en cuenta los avances tecnológicos en telecomunicaciones móviles y las necesidades en la materia dentro del territorio nacional. (31) (32)
49	CLM 49	Se atribuyen, a título primario al servicio FIJO, y compartidas a título secundario con las aplicaciones y servicios previstos en el Cuadro Nacional de Atribución de Bandas de Frecuencias, las bandas de frecuencias 894,000 - 895,225 MHz, 895,525 - 896,000 MHz, 897,125 - 901,225 MHz, 901,525 - 902,225 MHz, 908,000 - 911,500 MHz, 911,500 - 915,000 MHz y MHz, 939,000 - 940,225 MHz, 940,525 - 941,000 MHz, 942,125 - 946,225 MHz, 946,525 - 947,225 MHz, 953,000 - 956,500 MHz, 956,500 - 960,000 MHz para la operación de sistemas de Acceso Fijo Inalámbrico, como elemento de la Red Telefónica Pública Básica Conmutada (RTPBC). Para la operación por Separación Dúplex por división de frecuencia, las bandas o rangos de frecuencias se planifican de manera pareada con separación de 45 MHz entre frecuencias de transmisión y recepción. (33)
50	CLM 50	En las bandas de frecuencias 894,6750 - 894,9925 MHz, 897,1375 - 897,5000 MHz, 897,5000 - 901,0000 MHz, 902,0000 - 905,0000 MHz, 942,5000 - 950,0000 MHz, 3425,0000 - 3450,0000 MHz, 3475,0000 - 3500,0000 MHz, 3525,0000 - 3550,0000 MHz y 3575,0000 - 3600,0000 MHz no se otorgarán nuevos permisos para su uso, hasta tanto el Ministerio de Tecnologías de la Información y las Comunicaciones adelante los estudios que permitan planificar y reordenar el uso de dichas bandas de frecuencias conforme a los desarrollos tecnológicos. (34)
51	CLM 51	Las bandas de frecuencias 894,6750 – 894,9925 MHz, 897,1375 – 897,500 MHz, 897,500 – 901,000 MHz, 901,200 – 901,500 MHz, 902,000 – 905,000 MHz, 908,000 – 915,000 MHz, 931,500 – 932,000 MHz, 940,200 – 940,500 MHz, 942,500 – 950,000 MHz y 953,000 – 960,000 MHz están atribuidas, a título primario, al servicio FIJO y para operación de sistemas de acceso fijo inalámbrico, y compartidas a título secundario con los servicios previstos en el presente Cuadro Nacional de Atribución de Bandas de Frecuencias. (27)
52	CLM 52	De acuerdo con la Resolución 2544 de 14 de octubre de 2009, se atribuyen a título secundario, para operación sobre una base de no-interferencia y no protección de interferencia, las bandas de frecuencias 902 – 928 MHz, 2 400 – 2 483,5 MHz, 5 150 – 5 250 MHz, 5 250 – 5 350 MHz, 5 470 – 5 725 MHz y 5 725 – 5 850 MHz; para ser utilizadas libremente por sistemas de acceso inalámbrico y redes inalámbricas de área local, que empleen tecnologías de espectro ensanchado y modulación digital, de banda ancha y baja potencia en las condiciones de operación establecidas en la citada Resolución. (35) (36) (37)
53	CLM 53	En la banda de frecuencias 924 - 928 MHz, el Ministerio de Tecnologías de la Información y las Comunicaciones podrá autorizar la operación de sistemas de radiolocalización, utilizando sistemas convencionales y/o de espectro ensanchado.
54	CLM 54	Se reservan las bandas de frecuencias 929,000 – 931,500 MHz, 901,000 – 901,200 MHz, 940,000 – 940,200 MHz y 940,500 – 941,000 MHz para los servicios radioeléctricos fijo y móvil. (38)
55	CLM 55	Los enlaces, entre estudios y el sistema de transmisión de las estaciones de radiodifusión sonora, que operan en la banda de frecuencias 947,000 – 954,2 MHz, fueron reubicados en la banda de frecuencias 300,000 – 328,600 MHz de acuerdo con lo dispuesto en el “Plan de Reubicación de Emisoras”. (26)
56	CLM 56	En las bandas de frecuencias 1 400 - 1 427 MHz, 2 690 - 2 700 MHz, excepto las indicadas en los números 5.421 y 5.422; 10,68 - 10,7 GHz, excepto las indicadas en el número 5.483; 15,35 - 15,4 GHz, excepto las indicadas en el número 5.511, 23,6 - 24 GHz; 31,3 - 31,5 GHz; 31,5 - 31,8 GHz; 48,94 - 49,04 GHz por estaciones a bordo de aeronaves; 50,2 - 50,4 GHz, excepto las indicadas en el número 5.555A, 52,6 – 54,25 GHz; 86 - 92 GHz; 100-102 GHz; 109,5-111,8 GHz; 114,25-116 GHz; 148,5-151,5 GHz; 164-167 GHz; 182-185 GHz, excepto las indicadas en el número 5.563, 190-191,8 GHz; 200-209 GHz; 226-231,5 GHz y 250-252 GHz, están prohibidas todas las emisiones.
57	CLM 57	Las bandas de frecuencias 1 427 – 1 525 MHz y 2 300 – 2 500 MHz están atribuidas, a título primario, al servicio FIJO y para operación de sistemas de acceso fijo inalámbrico, y compartidas a título secundario con los servicios previstos en el presente Cuadro Nacional de Atribución de Bandas de Frecuencias, excepto en la banda de frecuencias 1 518 – 1 525 MHz, la cual se atribuye para ser compartida, a titulo primario, entre el servicio FIJO, los sistemas de acceso fijo inalámbrico y el servicio MÓVIL POR SATÉLITE. (27)
58	CLM 58	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 1,5 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 46 hasta la Tabla 49.
59	CLM 59	Se prohíbe la utilización libre, por parte del público en general, de las bandas de frecuencias 1 660 – 1 710 MHz, 2 655 – 3 400 MHz, 4 200 – 4 400 MHz, 5 000 – 5 220 MHz, 5 350 – 5 470 MHz, 7 450 – 7 550 MHz, 8 025 – 8 500 MHz, 9 000 – 9 200 MHz, 9 300 – 9 500 MHz y 10 600 – 12 700 MHz, 13 250 – 13 400, MHz, 14 470 – 14 500 MHz, 15 350 – 16 200 MHz, 17 700 – 21 400 MHz, 22 010 – 23 120 MHz, 23 600 – 24 000 MHz, 31 200 – 31 800 MHz, 36 430 – 36 500 MHz, y superiores a la frecuencia 38 600 MHz, para dispositivos de operación momentánea. (9)
60	CLM 60	Se atribuyen, a título primario, las bandas de frecuencias 1710 a 1755 MHz, 1850 a 1865 MHz, 1930 a 1945 MHz y 2110 a 2155 MHz, para la operación de servicios radioeléctricos móviles terrestres. Se reservan las bandas de frecuencias 1710 a 2025 MHz y 2100 a 2200 MHz, para la futura operación de servicios radioeléctricos móviles terrestres, excluyendo las bandas atribuidas. El Ministerio de Tecnologías de la Información y las Comunicaciones podrá reubicar en otras bandas de frecuencias a aquellos operadores autorizados que tengan asignadas frecuencias o bandas de frecuencias radioeléctricas en las bandas atribuidas. (39)
61	CLM 61	Las bandas de frecuencias 1 710 - 1 885 MHz, 1 885 - 2 025 MHz, 2 110 - 2 200 MHz, 2 300 - 2 400 MHz y 2 500 - 2 690 MHz han sido identificadas por la Unión Internacional de Telecomunicaciones para su utilización en las Telecomunicaciones Móviles Internacionales (IMT). Dicha identificación no excluye su uso por ninguna aplicación de los servicios a los cuales están atribuidas y no implica prioridad alguna en el presente Cuadro Nacional de Atribución de Bandas de Frecuencias. El Ministerio de Tecnologías de la Información y las Comunicaciones no otorgará nuevos permisos para uso del espectro radioeléctrico en las bandas de frecuencias 1 710 – 2 025 MHz, 1 910 – 1 920 MHz y 2 100 – 2 200 MHz hasta que se defina el espectro radioeléctrico necesario para la introducción de las Telecomunicaciones Móviles Internacionales, IMT – 2000. (40)
62	CLM 62	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 2 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 50 y hasta la Tabla 53, y en la Tabla 55.
63	CLM 63	Se atribuyen las bandas de frecuencias 1 850 - 1 910 MHz y 1 930 - 1 990 MHz para el servicio MÓVIL terrestre. El Ministerio de Tecnologías de la Información y las Comunicaciones, podrá reglamentar y planificar otras bandas de frecuencias para estos propósitos teniendo en cuenta los avances tecnológicos en telecomunicaciones móviles y las necesidades en la materia dentro del territorio nacional. (41) (42)
64	CLM 64	En virtud del Decreto 4722 de 2009, el Ministerio de Tecnologías de la Información y las Comunicaciones estableció los criterios para el otorgamiento de espectro radioeléctrico en la banda de frecuencias 1850 - 1990 MHz para el servicio móvil terrestre. De acuerdo con el citado Decreto, el tope máximo de espectro radioeléctrico asignado por operador para la prestación de servicios móviles terrestres será de 55 MHz; este ancho de banda incluye tanto el espectro asignado inicialmente en las respectivas concesiones o títulos habilitantes, así como sus adiciones mediante permisos de espectro otorgados por el Ministerio de Tecnologías de la Información y las Comunicaciones. Para efectos de la contabilización del tope de espectro no se tendrán en cuenta los permisos otorgados para enlaces punto a punto de la red soporte del proveedor. (43)
65	CLM 65	Las bandas de frecuencias 1 890 – 1910 MHz y 1 970 – 1 990 MHz están atribuidas en forma exclusiva para ser utilizadas durante la vigencia de las concesiones para la operación de redes de PCS. (40) (44)
66	CLM 66	La banda de frecuencias 1 910 – 1 920 MHz está atribuida, en forma exclusiva, para operación de sistemas de acceso fijo inalámbrico como elemento de la red telefónica pública básica conmutada (RTPBC). (27)
67	CLM 67	Se atribuye, a titulo primario, la banda de frecuencias 2 025 MHz - 2 110 MHz para ser utilizada por los sistemas o estaciones transmisoras móviles del servicio de Radiodifusión Televisión. El Plan de distribución de canales radioeléctricos aparecen en la Tabla 54. La banda 2 025 MHz - 2 110 MHz, con 85 MHz de ancho de banda total, estará compartida, a título primario, con los servicios fijo y móvil radioeléctrico, los servicios de operación e investigación espacial y exploración de la tierra por satélite. Las estaciones autorizadas para operar según el plan de distribución de canales de la banda 2 025 MHz - 2 110 MHz, podrán utilizar también los siguientes 40 canales de conexión de datos (CCD), en las bandas 2 025,0 - 2 025,5 MHz y 2 109,5 - 2 110,0 MHz, para facilitar sus operaciones en la banda atribuida.<br />Los sistemas o estaciones transmisoras móviles del servicio de Radiodifusión Televisión operarán en los canales radioeléctricos de la banda atribuida, dentro del área geográfica de servicio autorizada, de manera compartida y coordinada, con otros operadores autorizados de sistemas o estaciones transmisoras móviles del servicio de Radiodifusión de Televisión. Para la operación en un área de servicio municipal determinada, donde se encuentren ubicados los estudios de televisión y la estación transmisora principal del operador, el Ministerio de Tecnologías de la Información y las Comunicaciones podrá asignar un canal radioeléctrico específico, a título transitorio, el cual, en tanto sea necesario, deberá entrar a compartirse y coordinarse con otros operadores autorizados. En las transmisiones fuera del área de servicio municipal autorizada, los sistemas o estaciones transmisoras móviles del servicio de radiodifusión de televisión podrán operar en cualesquiera de los canales libres de la banda atribuida, no asignados de manera transitoria; mediante la operación compartida y coordinada con otros operadores autorizados de sistemas o estaciones transmisoras móviles del servicio de radiodifusión de televisión. En tal caso, la transmisión del operador visitante deberá realizarse a título secundario, sin causar interferencia perjudicial a los operadores autorizados. La potencia máxima permitida para la operación de los sistemas o estaciones transmisoras móviles del servicio de Radiodifusión Televisión en la banda de 2 025 MHz a 2 100 MHz es de 35 dBW EIPR (potencia isotrópica radiada efectiva). Los sistemas o estaciones transmisoras móviles del servicio de Radiodifusión Televisión autorizados para operar en la banda de 2 025 MHz a 2 110 MHz, operarán con un ancho de banda máximo de 12 MHz y con la emisión correspondiente a las comunicaciones análogas o digitales del servicio de televisión. Podrán utilizarse por los operadores autorizados los sub-canales A y B, de un canal de 12 MHz de la banda de 2 025 MHz a 2 110 MHz, con ancho de banda de 6 MHz cada uno, para su operación en modo digital. (45) (46)
68	CLM 68	Se atribuye, a título secundario, la banda de frecuencias 2 025 - 2 400 MHz para operar sistemas de espectro ensanchado. (47)
69	CLM 69	Se atribuye, a título secundario, la banda de frecuencias 2 300 – 2 400 MHz para operación de sistemas de acceso inalámbrico y redes inalámbricas de área local, que empleen tecnologías de espectro ensanchado y modulación digital, de banda ancha y baja potencia, previo registro ante el Ministerio de Tecnologías de la Información y las Comunicaciones. (36)
70	CLM 70	Se atribuye, a título primario, y se reserva en todo el territorio nacional la banda de frecuencias 2 500 – 2 690 MHz, para la operación de servicios de radiocomunicación fijo y móvil terrestre. En la banda de frecuencias atribuida no se otorgarán nuevos permisos para el uso del espectro radioeléctrico hasta tanto el Ministerio de Tecnologías de la Información y las Comunicaciones determine la atribución, canalización y distribución del espectro dentro del territorio nacional y el procedimiento administrativo para su otorgamiento y asignación. Los concesionarios y titulares de permisos que se encuentren operando redes y sistemas de telecomunicaciones deberán suspender todas las emisiones y operaciones, en la banda de frecuencias objeto de reglamentación. El Ministerio de Tecnologías de la Información y las Comunicaciones podrá reubicar en otras bandas de frecuencias a aquellos operadores autorizados que tengan asignadas frecuencias en la banda de frecuencias atribuida. (48)
71	CLM 71	Se atribuye, a título primario, al servicio FIJO para operación de sistemas de Distribución Punto a Punto y Punto Multipunto para Acceso de Banda Ancha Inalámbrica, la banda de frecuencias 3 400 MHz - 3 600 MHz. El plan de distribución de canales radioeléctricos se puede consultar en la Tabla 56. Para establecer el correcto y racional uso del espectro radioeléctrico en la banda de frecuencias objeto de reglamentación, el Ministerio de Tecnologías de la Información y las Comunicaciones expidió la reglamentación respectiva, que puede ser consultada en la sección de Referencias. De todas formas, los titulares de permisos para el uso del espectro radioeléctrico en la banda de frecuencias atribuida no podrán tener asignado más de 42 MHz de ancho de banda. El Ministerio de Tecnologías de la Información y las Comunicaciones podrá reubicar en otras bandas de frecuencias a aquellos operadores que tengan asignadas frecuencias o bandas de frecuencias radioeléctricas en cualquiera de las bandas atribuidas. (49) (50)
72	CLM 72	Para establecer el correcto, eficiente y racional uso del espectro radioeléctrico en las bandas de frecuencias 3 421 - 3 435 MHz, 3 435 - 3 449 MHz, 3 521 - 3 535 MHz y 3 535 - 3 549 MHz, para la prestación de servicios de telecomunicaciones que utilicen sistemas de distribución Punto a Punto y Punto Multipunto para Acceso de Banda Ancha Inalámbrica, en el Área de Servicio Departamental y establecer los requisitos y el procedimiento para el otorgamiento de los permisos correspondientes en las bandas de frecuencias en mención, el Ministerio de Tecnologías de la Información y las Comunicaciones expidió la reglamentación respectiva, que puede ser consultada en la sección de Referencias. (49) (50)(51)
73	CLM 73	Las bandas de frecuencias 3 425 – 3 450 MHz, 3 475 – 3 500 MHz, 3 525 – 3 550 MHz y 3 575 – 3 600 MHz están atribuidas, en forma exclusiva, para la operación de sistemas de acceso fijo inalámbrico como elemento de la red telefónica pública básica conmutada (RTPBC). (27)
74	CLM 74	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 4 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 57 y hasta la Tabla 58.
75	CLM 75	Se atribuye, a título primario, a los servicios FIJO y MÓVIL la banda de frecuencias 4 940 - 4 990 MHz, para ser utilizada para Acceso de Banda Ancha Inalámbrica en radiocomunicaciones para protección pública, operaciones de socorro y salvaguarda de la vida humana. La banda de frecuencias atribuida se agrupa y distribuye para conformar un plan general de compartición de canales, que facilite su utilización por parte de entidades autorizadas dentro de una misma área de servicio sin que se requiera coordinación previa entre las mismas, de la siguiente forma:<br /><IMG SRC="tabla_CLM75.png"><br />La potencia de transmisión de las estaciones que operan en la banda de frecuencias 4 940 MHz - 4 990 MHz no debe exceder los límites máximos establecidos; a los transmisores bajo la categoría de "alta potencia"que requieran transmitir con ganancias de antena superiores a los 9 dBi se les permite usar antenas de transmisión con una ganancia direccional de hasta 26 dBi a la máxima potencia de salida de transmisión usando la máscara de emisión X; si se utilizan antenas de transmisión de ganancia direccional mayor a 26 dBi, la potencia pico de transmisión y la densidad de potencia espectral pico deberán reducirse en la cantidad de dB que la ganancia direccional de la antena exceda los 26 dBi; en estos casos igualmente se aplica la máscara de emisión X; se permite el uso de antenas direccionales u omnidireccionales con ganancia de hasta 9 dBi con transmisores que están en la categoría de "baja potencia"en transmisiones punto a punto y punto a multipunto, usando la máscara de la emisión Y; si se utilizan antenas de transmisión de ganancia direccional mayor a 9 dBi, la potencia pico de transmisión y la densidad de potencia espectral pico deberán reducirse en la cantidad de dB que la ganancia direccional de la antena exceda los 9 dBi; en estos casos se aplica la máscara de emisión X. Máscara de emisión X: Para transmisores de alta potencia que operen en la banda de frecuencia 4 940 MHz - 4 990 MHz, la densidad de potencia espectral de las emisiones deberá atenuarse por debajo de la potencia de salida del transmisor como sigue: En cualquier frecuencia que se aleje de la frecuencia central entre 0 - 45 % del ancho de banda autorizado (BW): 0 dB. En cualquier frecuencia que se aleje de la frecuencia central entre 45 – 50 % del ancho de banda autorizado 568 Log (el % de (BW) / 45) dB. En cualquier frecuencia que se aleje de la frecuencia central entre 50 - 55 % del ancho de banda autorizado: 26 + 145 Log (el % de BW 50) dB. En cualquier frecuencia que se aleje de la frecuencia central entre 55 – 100 % del ancho de banda autorizado 32 + 31 Log (el % de (BW) / 55) dB. En cualquier frecuencia que se aleje de la frecuencia central entre 100 – 150 % del ancho de banda autorizado: 40 + 57 Log (el % de (BW) / 100) dB. En cualquier frecuencia que se aleje de la frecuencia central por encima de 150 % del ancho de banda autorizado: 50 dB o 55 + 10 Log (P) dB, cualquiera sea la atenuación inferior. Máscara de emisión Y: Para transmisores de baja potencia que operen en la banda de frecuencia 4 940 MHz a 4 990 MHz, la densidad de potencia espectral de las emisiones debe ser atenuada por debajo de la potencia de salida del transmisor como sigue: En cualquier frecuencia que se aleje de la frecuencia central entre 0 - 45 % del ancho de banda autorizado (BW): 0 dB. En cualquier frecuencia que se aleje de la frecuencia central entre 45 – 50 % del ancho de banda autorizado 219 Log (el % de (BW) / 45) dB. En cualquier frecuencia que se aleje de la frecuencia central entre 50 - 55 % del ancho de banda autorizado: 10 + 242log (el % de (BW) / 50) dB. En cualquier frecuencia que se aleje de la frecuencia central entre 55 – 100 % del ancho de banda autorizado: 20 + 31log (el % de (BW) / 55) atenuación dB. En cualquier frecuencia que se aleje de la frecuencia central entre 100 – 150 % del ancho de banda autorizado: 28 + 68 Log (el % de (BW) / 100) atenuación dB. En cualquier frecuencia que se aleje de la frecuencia central por encima de 150 % del ancho de banda autorizado: 40 dB. El Ministerio de Tecnologías de la Información y las Comunicaciones podrá permitir el uso de los canales del espectro radioeléctrico atribuido, en aquellas áreas geográficas donde dicho espectro se encuentre disponible y en todo caso, podrá modificar los permisos que hubieren sido otorgados con antelación a la expedición de esta norma o reubicar en otras bandas de frecuencias a aquellos operadores que tengan asignadas frecuencias o bandas de frecuencias radioeléctricas en las bandas atribuidas. En caso de estar ocupada la banda atribuida de 4 940 MHz a 4 990 MHz, en una determinada área geográfica, por un concesionario previamente habilitado, al momento de la ocurrencia de una emergencia, situación de seguridad o calamidad pública o evento catastrófico, el Ministerio de Tecnologías de la Información y las Comunicaciones dispondrá del espectro atribuido. En tal caso, los operadores autorizados, en el área incidental, deberán cesar sus emisiones para permitir el uso apropiado de la banda atribuida, durante el periodo de la emergencia, por las radiocomunicaciones para protección pública, operaciones de socorro y salvaguarda de la vida humana. En cualquier caso se dará prelación absoluta a las transmisiones relacionadas con la protección de la vida humana. (52)
76	CLM 76	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 5 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 59 y hasta la Tabla 63.
77	CLM 77	Las bandas de frecuencias 5,15 – 5,25 GHz, 5,25 – 5,255 GHz, 5,255 – 5,3 GHz y 5,725 – 5,825 GHz están atribuidas, a título primario, en el ámbito nacional para operación de redes inalámbricas privadas de banda ancha, baja potencia y corto alcance conocidas en el ámbito internacional como Hiperlan o U-NII. (53)
78	CLM 78	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 6 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 64 y hasta la Tabla 65.
79	CLM 79	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 7 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 66 y hasta la Tabla 68.
80	CLM 80	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 8 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 69 y hasta la Tabla 72.
81	CLM 81	En la Región 2, no se permite a las estaciones de aeronave transmitir en la banda de frecuencias 8 025 - 8 400 MHz.
82	CLM 82	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 10 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados en la Tabla 73.
83	CLM 83	El Ministerio de Tecnologías de la Información y las Comunicaciones adelantará los estudios que permitan planificar y reordenar el uso de la banda de frecuencias 10,5 - 10,68 GHz conforme a los desarrollos tecnológicos y las necesidades del País.
84	CLM 84	En Colombia, la banda de frecuencias 10,68 - 10,7 GHz está también atribuida, a título primario, a los servicios FIJO y MÓVIL, salvo móvil aeronáutico, conforme con el numeral 5.483 del Reglamento de Radiocomunicaciones. Este uso está limitado a los equipos en funcionamiento el 1 de enero de 1985.
85	CLM 85	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 1 1 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 74 y hasta la Tabla 76.
86	CLM 86	Se autoriza de manera general en todo el territorio nacional y en el espacio aéreo del mismo, la operación para el acceso de banda ancha a Internet, de estaciones terrenas de aeronave y de los sistemas móviles aeronáuticos por satélite que se instalen y operen en aeronaves, tanto colombianas como de bandera extranjera, que transiten legalmente por el territorio nacional. Se atribuye, a título secundario, en todo el territorio nacional, así como en el espacio aéreo colombiano, la banda de frecuencias 14.0 - 14.5 GHz para el servicio móvil por satélite, incluidos los enlaces Tierra-espacio que se precisen para la operación de sistemas móviles aeronáuticos por satélite. Se autoriza de manera general, dentro del territorio nacional, el uso a título secundario de las frecuencias comprendidas en la banda de frecuencias 14.0 - 14.5 GHz para enlaces Tierra–espacio y 11.7 - 12.2 GHz para enlaces espacio–Tierra, requeridos en la operación de sistemas móviles aeronáuticos por satélite para el acceso de banda ancha a Internet, instalados en aeronaves nacionales y extranjeras.<br />El Ministerio de Tecnologías de la Información y las Comunicaciones no otorgará protección contra interferencias a los sistemas instalados en aeronaves que operen en dichas bandas, por lo tanto, los titulares de tales estaciones no podrán reclamar protección respecto de asignaciones efectuadas a título primario, ni causarles interferencia. Los sistemas móviles por satélite que operen en aeronaves nacionales o extranjeras, cuyo uso se autoriza de manera general, dentro del territorio nacional, deberán cumplir las normas y recomendaciones señaladas por la Unión Internacional de Telecomunicaciones, en especial las previstas en la Recomendación UIT-R M.1643, de manera que no interfieran a los servicios atribuidos a título primario en el País, ni afecten el normal funcionamiento de los equipos y sistemas dispuestos en las aeronaves, en particular los requeridos para la seguridad del vuelo, así como las demás instalaciones de tipo aeronáutico, terrestres o espaciales que se empleen para este propósito.<br />La autorización general dispuesta para los sistemas radioeléctricos móviles por satélite instalados en aeronaves para acceso en banda ancha a Internet, no faculta a su titular para exigir o brindar interconexión a las redes de telecomunicaciones públicas o privadas instaladas dentro del territorio colombiano, ni confiere el derecho para prestar al público servicios de telecomunicaciones en Colombia por fuera de las aeronaves donde se instalen dichos sistemas. El sistema debe hacer uso de segmentos espaciales debidamente registrados en Colombia; comprometerse expresamente a que el sistema cumple con las recomendaciones establecidas por la Unión Internacional de Telecomunicaciones, no causa interferencias a servicios atribuidos a título primario y no interfiere con el correcto funcionamiento de los equipos dispuestos, tanto en tierra como en la aeronave, para la seguridad del vuelo; abstenerse de llevar a cabo prácticas comerciales restrictivas o discriminatorias entre las personas que utilicen el sistema; comprometerse a permitir, en condiciones no discriminatorias, el acceso al sistema a empresas aéreas matriculadas y autorizadas en Colombia que adquieran las estaciones de aeronave técnicamente compatibles con el sistema que se registra. (54)
87	CLM 87	Se autoriza de manera general en todo el territorio nacional, la operación para el acceso de banda ancha a Internet, de estaciones terrenas de barco y de los sistemas móviles marítimos por satélite que se instalen y operen en naves, tanto colombianas como de bandera extranjera, que transiten legalmente por el territorio nacional. Se atribuye, a título secundario, en todo el territorio nacional, la banda de frecuencias 14.0 - 14.5 GHz para el servicio móvil por satélite, incluidos los enlaces Tierra-espacio que se precisen para la operación de sistemas móviles marítimos por satélite. Se autoriza de manera general, dentro del territorio nacional, el uso a título secundario de las frecuencias comprendidas en la banda de frecuencias 14.0 - 14.5 GHz para enlaces Tierra–espacio y 11.7 - 12.2 GHz para enlaces espacio–Tierra, requeridos en la operación de sistemas móviles marítimos por satélite para el acceso de banda ancha a Internet, instalados en naves nacionales y extranjeras.<br />El Ministerio de Tecnologías de la Información y las Comunicaciones no otorgará protección contra interferencias a los sistemas instalados en naves que operen en dichas bandas, por lo tanto, los titulares de tales estaciones no podrán reclamar protección respecto de asignaciones efectuadas a título primario, ni causarles interferencia a aquellas ni tampoco a los demás sistemas que operen a título secundario en las mismas frecuencias. Los sistemas móviles marítimos por satélite que operen en naves nacionales o extranjeras, cuyo uso se autoriza de manera general, dentro del territorio nacional, deberán cumplir las normas y recomendaciones señaladas por la Unión Internacional de Telecomunicaciones, de manera que no interfieran a los servicios atribuidos a título primario en el País, ni afecten el normal funcionamiento de los equipos y sistemas dispuestos en las naves, en particular los requeridos para la seguridad, así como las demás instalaciones de tipo aeronáutico, marítimo, terrestres o espaciales que se empleen para este propósito.<br />La autorización general dispuesta para los sistemas radioeléctricos móviles por satélite instalados en naves marítimas para acceso en banda ancha a Internet, no faculta a su titular para exigir o brindar interconexión a las redes de telecomunicaciones públicas o privadas instaladas dentro del territorio colombiano, ni confiere el derecho para prestar al público servicios de telecomunicaciones en Colombia por fuera de las naves donde se instalen dichos sistemas. El Ministerio de Tecnologías de la Información y las Comunicaciones expedirá las licencias para los sistemas radioeléctricos móviles por satélite que se instalen y utilicen en naves marítimas con matrícula colombiana. En cumplimiento de las normas y tratados internacionales, Colombia otorgará en su territorio a los sistemas radioeléctricos móviles por satélite instalados en las naves marítimas extranjeras que transiten el espacio marítimo colombiano los mismos reconocimientos e igual tratamiento que confiere el país de matrícula de la nave extranjera a los sistemas instalados y autorizados en naves de matrícula colombiana.<br />Las empresas responsables de la administración y gestión mundial o regional de un sistema radioeléctrico móvil por satélite dispuesto para uso en naves marítimas están en la obligación de registrar su sistema ante el Ministerio de Tecnologías de la Información y las Comunicaciones. Las naves marítimas de matrícula colombiana o extranjeras que transiten el espacio marítimo colombiano están obligadas a utilizar únicamente los sistemas radioeléctricos móviles por satélite que se encuentren debidamente registrados en el país. (55)
88	CLM 88	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 13 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 77 y hasta la Tabla 79.
89	CLM 89	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 15 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados en Tabla 80.
90	CLM 90	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 18 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 81 y hasta la Tabla 84.
91	CLM 91	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 23 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados en la Tabla 85 y en la Tabla 86.
92	CLM 92	Se adoptan los planes de distribución de canales radioeléctricos para la banda de frecuencias de 26 GHz de acuerdo con las Recomendaciones de la Unión Internacional de Telecomunicaciones. Estos planes de distribución de canales se encuentran relacionados a partir de la Tabla 87 y hasta la Tabla 92.
93	CLM 93	Se atribuye la banda de frecuencias 27 500 – 28 350 MHz para el establecimiento, dentro del territorio nacional, de redes radioeléctricas de distribución Punto Multipunto de banda ancha con tecnología LMDS. El plan de distribución de canales para esta banda puede consultarse en la Tabla 93. (56)
94	CLM 94	Se adoptan los planes de distribución de canales radioeléctricos en las bandas de frecuencias 37 058 – 37 338 MHz y 38 318 – 38 600 MHz de acuerdo con la Recomendación de la Unión Internacional de Telecomunicaciones UIT-R F.749-1 Anexo 1. Se adoptan los planes de distribución de canales radioeléctricos en las bandas de frecuencias 38 600 – 39 950 MHz de acuerdo con la Recomendación de la Unión Internacional de Telecomunicaciones UIT- R F.749-1 Anexo 2. Los planes de distribución de canales para las bandas de frecuencias en mención se pueden consultar a partir de la Tabla 94 y hasta la Tabla 95. (57)
95	CLM 95	Se adoptan los planes de distribución de canales radioeléctricos para los sistemas inalámbricos fijos que funcionan en la banda de frecuencias 51,4-52,6 GHz de acuerdo con la Recomendación UIT-R F. 1496-1, de la Unión Internacional de Telecomunicaciones. Los planes de distribución de canales para las bandas de frecuencias en mención se pueden consultar a partir de la Tabla 96 y hasta la Tabla 100.
96	CLM 96	Se adoptan los planes de distribución de canales radioeléctricos para los sistemas inalámbricos fijos que funcionan en la banda de frecuencias 55,78-57 GHz de acuerdo con la Recomendación UIT-R F. 1497-1, de la Unión Internacional de Telecomunicaciones. Los planes de distribución de canales para las bandas de frecuencias en mención se pueden consultar a partir de la Tabla 101 y hasta la Tabla 110.
97	CLM 97	Se adoptan los planes de distribución de canales radioeléctricos para los sistemas inalámbricos fijos que funcionan en la banda de frecuencias 57-59 GHz de acuerdo con la Recomendación UIT-R F. 1497-1, de la Unión Internacional de Telecomunicaciones. Los planes de distribución de canales para las bandas de frecuencias en mención se pueden consultar a partir de la Tabla 111 y hasta la Tabla 112.
\.


--
-- TOC entry 2011 (class 0 OID 42622)
-- Dependencies: 1581
-- Data for Name: nota_nacional_banda; Type: TABLE DATA; Schema: public; Owner: -
--

COPY nota_nacional_banda (id_banda, id_nota_nacional) FROM stdin;
3	3
4	4
5	3
5	5
6	3
6	5
7	5
8	3
8	5
8	38
9	3
9	5
9	38
10	3
10	5
10	12
10	38
11	3
11	5
11	38
12	5
13	5
14	4
14	5
15	3
15	6
15	5
16	3
16	6
16	5
17	3
17	5
18	3
18	6
18	5
19	6
19	5
20	5
21	3
21	6
21	5
22	7
22	8
23	3
24	6
24	9
25	6
26	5
26	10
26	11
26	38
27	5
27	10
27	11
28	5
28	10
28	11
29	6
30	12
31	12
33	3
35	3
36	8
36	13
36	14
37	3
39	11
40	4
41	4
42	4
44	6
44	14
45	6
46	10
47	10
47	11
48	10
48	11
49	6
50	12
51	12
52	3
52	15
52	16
53	3
53	8
53	15
53	17
55	6
56	6
57	11
58	11
59	4
60	4
61	11
64	6
65	6
66	6
66	14
68	11
69	11
70	3
70	8
70	15
70	17
71	6
72	6
74	12
75	12
76	12
77	11
80	3
80	15
81	3
81	8
81	14
81	15
81	17
82	6
83	6
85	11
86	11
88	4
89	4
89	14
90	6
91	12
93	6
94	6
96	11
97	11
98	11
100	3
100	8
100	15
101	6
102	6
105	11
106	11
107	11
109	12
110	12
112	4
112	14
113	4
114	6
115	11
116	11
118	3
118	8
118	15
120	11
121	11
122	6
123	6
126	12
128	3
128	15
129	11
131	3
131	15
133	4
133	14
134	4
136	12
137	11
140	6
141	3
141	15
144	6
147	12
148	4
149	4
151	3
151	15
154	11
155	3
155	15
156	5
156	18
157	10
158	12
159	5
159	10
159	19
159	20
160	20
161	20
162	12
163	21
164	19
164	21
165	5
165	10
165	19
166	5
166	10
166	19
166	22
167	5
167	10
167	19
168	6
168	19
169	10
169	19
170	10
170	19
171	19
171	21
172	10
172	19
172	23
173	6
173	24
173	25
174	6
174	8
174	14
174	26
174	27
179	19
179	28
179	29
179	30
179	31
180	19
180	28
180	29
181	19
181	28
181	29
182	12
182	19
183	12
183	19
184	19
184	28
184	29
185	28
185	29
186	19
186	28
186	29
186	30
186	32
186	33
187	3
187	8
187	19
187	28
187	30
187	33
188	28
188	29
188	30
188	33
189	3
189	8
189	14
189	19
189	28
189	34
190	19
190	28
190	29
190	31
191	3
191	19
191	28
191	29
191	34
192	10
192	19
192	28
192	29
192	31
193	5
193	19
193	21
194	3
194	10
194	19
195	12
195	19
196	19
196	28
196	29
197	19
197	28
197	29
197	35
198	19
198	28
198	29
199	19
199	28
199	29
199	35
200	28
200	29
200	19
201	14
201	19
201	28
201	29
202	19
202	35
203	19
203	36
203	37
204	19
204	36
204	37
205	19
205	37
206	19
206	37
206	38
207	19
207	38
207	39
207	55
208	19
208	38
208	39
208	55
209	19
209	38
209	39
209	55
210	19
210	39
210	55
211	6
212	19
212	40
213	19
214	19
214	41
215	19
216	19
216	41
217	19
218	19
218	41
219	19
220	19
221	19
222	19
222	41
223	19
225	4
225	42
226	42
229	28
230	8
230	19
230	28
230	29
231	19
231	28
231	29
231	43
232	19
232	28
232	29
232	43
232	44
233	5
233	19
233	28
233	29
233	43
233	44
234	12
234	19
234	29
234	43
235	5
235	12
235	19
235	29
235	38
235	43
236	12
236	19
236	29
236	38
236	43
237	5
237	19
237	28
237	29
237	31
237	43
238	5
238	19
238	28
238	29
238	31
239	5
239	19
239	28
239	29
240	5
240	19
240	28
240	29
240	31
241	5
241	19
241	28
241	29
242	5
242	19
242	28
242	29
242	32
243	19
243	45
244	19
244	45
245	19
245	46
246	19
246	46
247	19
247	46
249	19
249	47
250	19
250	47
251	19
251	48
252	19
253	19
253	47
254	19
254	47
255	19
255	48
256	19
256	48
257	5
257	19
257	49
257	50
257	51
258	19
258	47
259	5
259	19
259	49
259	50
259	51
260	19
260	49
260	50
260	51
261	12
261	19
261	20
261	38
261	49
261	50
261	51
261	52
262	5
262	12
262	19
262	20
262	38
262	52
263	12
263	19
263	20
263	38
263	49
263	51
263	52
264	5
264	12
264	19
264	20
264	38
264	52
265	5
265	12
265	19
265	20
265	38
265	52
265	53
266	5
266	19
266	51
266	54
267	5
267	19
268	19
268	47
269	5
269	19
269	49
270	19
270	49
270	51
270	54
271	19
271	49
272	19
272	49
272	50
272	51
272	55
273	19
273	55
274	19
274	49
274	51
274	55
275	6
276	6
278	12
279	12
280	6
282	56
283	19
283	57
283	58
284	19
284	57
284	58
285	19
285	57
285	58
286	19
286	57
286	58
287	19
287	57
288	19
288	58
289	19
290	3
290	19
291	19
292	8
292	19
293	19
294	19
295	6
295	19
296	6
296	19
297	6
297	19
298	6
298	19
299	19
300	19
301	19
302	8
302	19
303	19
304	19
305	19
305	59
306	19
306	59
307	19
307	59
308	19
308	59
309	19
309	59
310	19
310	59
311	19
311	59
312	19
312	59
313	19
313	60
313	61
313	62
313	63
313	64
314	19
314	60
314	61
314	63
314	64
314	65
315	19
315	60
315	61
315	63
315	64
315	65
316	19
316	54
316	60
316	61
316	62
316	63
316	64
316	66
317	19
317	60
317	61
317	62
317	63
317	64
318	19
318	60
318	61
318	63
318	64
318	65
319	19
319	60
319	61
319	63
319	64
319	65
320	19
320	60
320	61
320	63
320	64
320	65
321	19
321	60
321	61
321	62
321	63
322	19
322	60
322	61
322	62
323	19
323	60
323	61
323	62
323	67
323	68
324	19
324	60
324	61
324	62
324	68
325	19
325	60
325	61
325	62
325	68
326	19
326	60
326	61
326	62
326	68
327	19
327	60
327	61
327	62
327	68
328	19
328	62
328	68
329	19
329	62
329	68
330	12
330	19
330	57
330	62
330	68
330	69
331	12
331	19
331	20
331	52
331	57
331	62
332	19
332	20
332	52
332	57
332	62
333	19
333	57
333	62
334	19
334	62
334	70
335	19
335	62
335	70
336	19
336	59
336	62
336	70
337	19
337	59
337	62
337	70
338	19
338	56
338	59
339	6
339	19
339	59
340	19
340	38
340	59
341	19
341	38
341	59
342	12
342	19
342	38
342	59
343	12
343	19
343	50
343	59
343	71
343	72
343	73
344	19
344	50
344	59
344	71
344	72
344	73
345	19
345	74
346	6
346	19
346	59
347	19
347	76
348	19
348	76
349	19
349	75
349	76
350	19
351	6
351	19
351	59
352	6
352	19
352	59
353	6
353	19
353	59
354	6
354	19
354	59
355	6
355	19
355	52
355	59
355	76
355	77
356	19
356	52
356	77
357	19
357	52
357	77
358	6
358	19
358	59
359	19
359	59
360	3
360	19
360	52
361	3
361	19
361	52
362	12
362	19
362	52
363	12
363	19
363	38
363	52
363	77
364	12
364	19
364	52
365	12
365	19
366	19
366	78
367	19
367	78
368	19
368	54
368	78
368	79
369	19
369	79
370	19
370	79
371	19
371	54
371	79
372	19
372	54
372	79
373	19
373	54
373	59
373	79
374	19
374	54
374	79
374	80
375	19
375	80
376	19
376	80
377	19
377	80
378	19
378	59
378	80
378	81
379	19
379	59
379	80
379	81
380	19
380	59
380	80
380	81
381	19
381	59
381	80
382	19
383	19
384	19
385	6
385	19
386	3
386	19
387	6
387	19
387	59
388	3
388	19
389	19
389	59
390	19
391	19
392	19
393	12
393	19
394	12
394	19
395	19
395	82
395	83
396	19
396	82
396	83
397	19
397	59
397	82
397	83
398	19
398	56
398	59
398	84
399	19
399	59
399	85
400	19
400	59
400	86
400	87
401	19
401	59
401	86
401	87
402	19
402	59
403	19
404	19
404	88
405	6
405	19
405	59
406	4
406	19
406	38
407	4
407	19
408	19
408	86
408	87
409	19
409	86
409	87
410	19
410	86
410	87
411	19
411	86
411	87
411	89
412	19
412	59
412	86
412	87
412	89
413	19
413	89
414	19
414	89
415	19
415	56
415	59
416	6
416	19
416	59
417	6
417	19
417	59
418	6
418	19
418	59
419	19
419	59
420	19
421	19
422	19
423	19
424	19
424	59
424	90
425	19
425	59
425	90
426	19
426	59
426	90
427	19
427	59
427	90
428	19
428	59
428	90
429	19
429	59
429	90
430	19
430	59
430	90
431	19
431	59
432	19
432	59
433	4
433	19
433	59
434	19
434	59
434	91
435	19
435	91
436	19
436	59
436	91
437	19
437	59
437	91
438	19
438	59
438	91
439	19
439	59
439	91
440	19
440	91
441	19
441	56
441	59
442	12
442	19
443	12
443	19
443	38
444	19
445	19
446	19
447	19
448	4
448	19
448	92
449	4
449	19
449	92
450	19
450	92
451	19
451	93
452	19
453	19
454	19
455	19
456	4
456	19
457	4
457	19
457	59
458	19
458	56
458	59
459	19
459	56
459	59
460	19
461	19
462	19
463	19
464	19
465	19
466	19
467	19
468	19
469	19
469	59
470	19
470	94
471	19
472	19
473	19
473	59
473	94
474	19
474	59
474	94
475	19
475	59
476	19
476	59
477	19
477	59
478	19
478	59
479	19
479	59
480	12
480	19
480	59
481	19
481	59
482	19
482	59
483	19
483	59
484	56
484	19
484	59
485	19
485	59
486	19
486	59
487	19
487	59
487	95
488	19
488	56
488	59
489	19
489	59
490	19
490	59
490	96
491	19
491	59
491	96
492	19
492	59
492	97
493	19
493	59
493	97
494	19
494	59
495	19
495	59
496	19
496	59
497	19
497	59
498	19
498	59
499	19
499	59
500	19
500	59
501	12
501	19
501	38
501	59
502	12
502	19
502	59
503	12
503	19
503	59
504	12
504	19
504	59
505	19
505	59
506	19
506	59
507	19
507	56
507	59
508	19
508	59
509	19
509	59
510	19
510	59
511	19
511	59
512	19
512	57
512	59
513	19
513	59
514	19
514	59
515	19
515	56
515	59
516	19
516	59
517	19
517	56
517	59
518	19
518	59
519	19
519	59
520	19
520	59
521	12
521	19
521	59
522	19
522	59
523	19
523	59
524	19
524	59
525	12
525	19
525	59
526	12
526	19
526	59
527	19
527	59
528	19
528	57
528	59
529	19
529	59
530	19
530	59
531	19
531	59
532	19
532	57
532	59
533	19
533	59
534	19
534	59
535	19
535	59
536	19
536	59
537	19
537	59
538	19
538	56
538	59
539	19
539	59
540	19
540	57
540	59
541	19
541	59
542	19
542	57
542	59
543	19
543	59
544	19
544	59
545	19
545	56
545	59
546	19
546	59
547	19
547	59
548	19
548	59
549	19
549	59
550	19
550	59
551	12
551	19
551	59
552	12
552	19
552	59
553	19
553	57
553	59
554	19
554	59
555	19
555	59
556	19
556	59
\.


--
-- TOC entry 2020 (class 0 OID 42742)
-- Dependencies: 1596
-- Data for Name: operador; Type: TABLE DATA; Schema: public; Owner: -
--

COPY operador (id_operador, nombre, descripcion) FROM stdin;
\.


--
-- TOC entry 2007 (class 0 OID 42581)
-- Dependencies: 1574
-- Data for Name: rango_frecuencias; Type: TABLE DATA; Schema: public; Owner: -
--

COPY rango_frecuencias (id_frecuencias, nombre, frecuencia_inicial, frecuencia_final, longitud_onda_inicial, longitud_onda_final) FROM stdin;
VLF	Very Low Frecuency	3	30	10000000	100000000
LF	Low Frecuency	30	300	1000000	10000000
MF	Médium Frecuency	300	3000	100000	1000000
HF	High Frecuency HF	3000	30000	10000	100000
VHF	Very High Frecuency	30000	300000	1000	10000
UHF	Ultra High Frecuency	300000	3000000	100	1000
SHF	Super High Frecuency	3000000	30000000	10	100
EHF	Extremely High Frecuency	30000000	300000000	1	10
\.


--
-- TOC entry 2015 (class 0 OID 42679)
-- Dependencies: 1587
-- Data for Name: region; Type: TABLE DATA; Schema: public; Owner: -
--

COPY region (id_region, nombre) FROM stdin;
1	Default
\.


--
-- TOC entry 2013 (class 0 OID 42654)
-- Dependencies: 1584
-- Data for Name: servicio; Type: TABLE DATA; Schema: public; Owner: -
--

COPY servicio (id_servicio, nombre, tipo) FROM stdin;
1	(No atribuida)	secundario
2	AFICIONADOS	primario
3	Aficionados	secundario
4	AFICIONADOS POR SATÉLITE	primario
5	Aficionados por satélite	secundario
6	AYUDAS A LA METEOROLOGÍA	primario
7	AYUDAS A LA METEOROLOGÍA EXPLORACIÓN DE LA TIERRA POR SATÉLITE (Tierra-espacio)	primario
8	AYUDAS A LA METEOROLOGÍA METEOROLOGÍA POR SATÉLITE (espacio-Tierra)	primario
9	AYUDAS A LA METEOROLOGÍA OPERACIONES ESPACIALES (espacio-Tierra)	primario
10	ENLACES ESTUDIOS Y SISTEMAS DE TRANSMISIÓN (Radiodifusión Sonora)	primario
11	ENLACES NACIONALES (Estaciones de Radiodifusión Sonora)	primario
12	ENTRE SATÉLITES	primario
13	Exploración de la Tierra por satélite	secundario
14	EXPLORACIÓN DE LA TIERRA POR SATÉLITE	primario
15	EXPLORACIÓN DE LA TIERRA POR SATÉLITE (activo)	primario
16	Exploración de la Tierra por satélite (activo)	secundario
17	EXPLORACIÓN DE LA TIERRA POR SATÉLITE (espacio-Tierra)	primario
18	Exploración de la Tierra por satélite (espacio-Tierra)	secundario
19	EXPLORACIÓN DE LA TIERRA POR SATÉLITE (espacio-Tierra) (espacio-espacio)	primario
20	EXPLORACIÓN DE LA TIERRA POR SATÉLITE (pasivo)	primario
21	Exploración de la Tierra por satélite (pasivo)	secundario
22	EXPLORACIÓN DE LA TIERRA POR SATÉLITE (Tierra-espacio)	primario
23	Exploración de la Tierra por satélite (Tierra-espacio)	secundario
24	EXPLORACIÓN DE LA TIERRA POR SATÉLITE (Tierra-espacio) (espacio-espacio)	primario
25	EXPLORACIÓN DE LATIERRA POR SATÉLITE (activo)	primario
26	FIJO	primario
27	Fijo	secundario
28	FIJO (Espectro Ensanchado)	primario
29	Fijo (Espectro Ensanchado)	secundario
30	FIJO (LMDS)	primario
31	FIJO POR SATÉLITE (espacio-Tierra)	primario
32	FIJO POR SATÉLITE (Tierra-espacio)	primario
33	FIJO POR SATÉLITE (Tierra-espacio) (espacio-Tierra)	primario
34	FIJO POR SATÉLITE (espacio-Tierra) (Tierra-espacio)	primario
35	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS	primario
36	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (10 000 kHz)	primario
37	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (15 000 kHz)	primario
38	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (2 500 kHz)	primario
39	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (20 000 kHz)	primario
40	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (25 000 kHz)	primario
41	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (20 kHz)	primario
42	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS (5 000 kHz)	primario
43	FRECUENCIAS PATRÓN Y SEÑALES HORARIAS POR SATÉLITE (400.1 MHz)	primario
44	Frecuencias patrón y señales horarias por satélite (espacio-Tierra)	secundario
45	Frecuencias patrón y señales horarias por satélite (Tierra-espacio)	secundario
46	INVESTIGACIÓN ESPACIAL	primario
47	Investigación espacial	secundario
48	Investigación espacial (activo)	secundario
49	INVESTIGACIÓN ESPACIAL (activo)	primario
50	Investigación espacial (espacio lejano)	secundario
51	INVESTIGACIÓN ESPACIAL (espacio lejano) (espacio-Tierra)	primario
52	Investigación espacial (espacio lejano) (espacio-Tierra)	secundario
53	Investigación espacial (espacio lejano) (Tierra-espacio)	secundario
54	INVESTIGACIÓN ESPACIAL (espacio lejano) (Tierra-espacio)	primario
55	INVESTIGACIÓN ESPACIAL (espacio-espacio)	primario
56	INVESTIGACIÓN ESPACIAL (espacio-Tierra)	primario
57	Investigación espacial (espacio-Tierra)	secundario
58	INVESTIGACIÓN ESPACIAL (espacio-Tierra) (espacio-espacio)	primario
59	INVESTIGACIÓN ESPACIAL (pasivo)	primario
60	Investigación espacial (pasivo)	secundario
61	INVESTIGACIÓN ESPACIAL (Tierra-espacio)	primario
62	INVESTIGACIÓN ESPACIAL (Tierra-espacio) (espacio-espacio)	primario
63	METEOROLOGÍA POR SATÉLITE	primario
64	METEOROLOGÍA POR SATÉLITE (espacio-Tierra)	primario
65	Meteorología por satélite (espacio-Tierra)	secundario
66	METEOROLOGÍA POR SATÉLITE (Tierra-espacio)	primario
67	MÓVIL	primario
68	Móvil	secundario
69	MÓVIL (ACCESO TRONCALIZADO)	primario
70	MÓVIL (EQUIPOS TRANSMÓVILES)	primario
71	MÓVIL (PCS)	primario
72	MÓVIL (socorro y llamada)	primario
73	MÓVIL (TELEFONÍA MÓVIL CELULAR)	primario
74	MÓVIL AERONÁUTICO	primario
75	Móvil aeronáutico	secundario
76	MÓVIL AERONÁUTICO (OR)	primario
77	MÓVIL AERONÁUTICO (R)	primario
78	MÓVIL MARÍTIMO	primario
79	MÓVIL MARÍTIMO (socorro y llamada por LLSD)	primario
80	MÓVIL POR SATÉLITE	primario
81	MÓVIL POR SATÉLITE (espacio-Tierra)	primario
82	MÓVIL POR SATÉLITE (Tierra-espacio)	primario
83	Móvil por satélite (Tierra-espacio)	secundario
84	Móvil por satélite (espacio-Tierra)	secundario
85	Móvil por satélite (Tierra-espacio) salvo móvil aeronáutico por satélite	secundario
86	Móvil por satélite salvo móvil aeronáutico por satélite (Tierra-espacio)	secundario
87	Móvil salvo móvil aeronáutico	secundario
88	MÓVIL salvo móvil aeronáutico	primario
89	Móvil salvo móvil aeronáutico (R)	secundario
90	MÓVIL salvo móvil aeronáutico (R)	primario
91	MÓVIL TERRESTRE	primario
92	Móvil terrestre	secundario
93	OPERACIONES ESPACIALES (espacio-Tierra)	primario
94	Operaciones espaciales (espacio-Tierra)	secundario
95	OPERACIONES ESPACIALES (espacio-Tierra) (espacio-espacio)	primario
96	OPERACIONES ESPACIALES (Tierra-espacio) (espacio-espacio)	primario
97	RADIOASTRONOMÍA	primario
98	Radioastronomía	secundario
99	Radiodeterminación Por Satélite (espacio-Tierra)	secundario
100	RADIODETERMINACIÓN POR SATÉLITE (Tierra-espacio)	primario
101	RADIODIFUSIÓN	primario
102	RADIODIFUSIÓN POR SATÉLITE	primario
103	RADIODIFUSIÓN SONORA	primario
104	RADIODIFUSIÓN TELEVISIÓN	primario
105	RADIOLOCALIZACIÓN	primario
106	Radiolocalización	secundario
107	RADIOLOCALIZACIÓN POR SATÉLITE (Tierra-espacio)	primario
108	RADIONAVEGACIÓN	primario
109	RADIONAVEGACIÓN AERONÁUTICA	primario
110	Radionavegación aeronáutica	secundario
111	RADIONAVEGACIÓN MARÍTIMA	primario
112	Radionavegación marítima (radiofaros)	secundario
113	RADIONAVEGACIÓN MARÍTIMA (radiofaros)	primario
114	RADIONAVEGACIÓN POR SATÉLITE	primario
115	Radionavegación por satélite	secundario
116	RADIONAVEGACIÓN POR SATÉLITE (espacio-Tierra) (espacio-espacio)	primario
117	RADIONAVEGACIÓN POR SATÉLITE (Tierra-espacio)	primario
\.


--
-- TOC entry 2014 (class 0 OID 42662)
-- Dependencies: 1585
-- Data for Name: servicio_banda; Type: TABLE DATA; Schema: public; Owner: -
--

COPY servicio_banda (id_banda, id_servicio) FROM stdin;
1	1
2	108
3	26
3	78
4	41
5	26
5	78
6	26
6	78
6	111
6	106
7	108
7	27
8	26
8	78
8	111
8	106
9	26
9	78
10	26
10	78
10	3
11	26
11	78
12	26
13	109
14	109
14	75
15	109
15	75
15	112
16	109
16	113
17	113
17	110
18	109
18	75
18	112
19	109
19	75
20	108
20	75
21	78
21	110
22	67
23	78
24	67
24	109
25	109
26	103
27	103
28	103
29	26
29	67
29	105
29	109
30	2
31	2
31	26
31	88
31	105
31	108
32	26
32	67
33	78
34	26
34	67
35	78
36	72
37	78
38	26
38	67
39	26
39	67
39	103
40	38
41	35
41	47
42	35
43	26
43	67
44	77
45	76
46	26
46	90
47	26
47	90
47	103
48	26
48	88
48	103
49	77
50	2
51	2
51	26
51	90
52	26
52	78
53	78
54	26
54	90
55	77
56	76
57	26
57	90
57	103
58	26
58	91
58	103
59	42
60	35
60	47
61	26
61	103
62	26
62	87
63	26
63	88
64	77
65	77
66	76
67	26
67	90
68	103
69	103
70	78
71	77
72	76
73	26
73	92
74	2
74	4
75	2
76	2
77	103
78	26
78	88
79	26
79	88
80	26
80	78
81	78
82	77
83	76
84	26
85	101
86	103
87	26
88	36
89	35
89	47
90	77
91	26
91	3
92	26
92	89
93	76
94	77
95	26
96	103
97	103
98	103
99	26
100	78
101	76
102	77
103	26
103	97
104	26
104	89
105	101
106	103
107	103
108	26
108	89
109	2
109	4
110	2
111	26
111	89
112	37
113	35
113	47
114	76
115	103
116	103
117	26
118	78
119	26
120	103
121	103
122	77
123	76
124	26
125	26
125	47
126	2
126	4
127	26
127	87
128	78
129	103
130	26
131	78
132	26
133	35
133	47
134	39
135	26
135	68
136	2
136	4
137	103
138	26
139	26
140	77
141	78
142	26
143	26
143	89
144	26
144	76
145	26
145	88
146	26
146	91
147	2
147	4
148	40
149	35
149	47
150	26
150	88
151	78
152	26
152	88
153	97
154	103
155	78
156	26
156	88
157	6
157	26
157	67
158	2
158	4
159	26
159	67
160	26
160	67
161	26
161	67
162	2
163	104
164	104
165	26
165	67
166	97
166	27
166	68
167	26
167	67
168	109
169	26
169	67
170	26
170	67
171	104
172	103
173	109
174	77
175	93
175	64
175	81
175	56
175	27
175	89
176	93
176	64
176	56
176	27
176	84
176	89
177	93
177	64
177	81
177	56
177	27
177	89
178	93
178	64
178	56
178	27
178	84
178	89
179	26
179	67
179	105
179	57
180	26
180	67
180	105
180	56
181	26
181	67
181	105
181	57
182	2
182	4
183	2
184	26
184	67
184	82
185	82
185	114
186	26
186	67
187	79
188	26
188	67
189	78
190	26
190	67
191	78
191	26
191	67
192	26
192	67
193	104
194	26
194	78
194	106
195	2
195	26
195	67
195	106
196	26
196	67
197	70
198	26
198	67
199	70
200	26
200	67
201	26
201	67
202	70
203	26
203	67
204	26
204	67
204	94
205	93
205	26
205	67
206	26
206	67
207	26
207	10
208	26
208	10
208	83
209	26
209	10
210	26
210	10
210	97
211	109
212	26
212	11
213	26
213	67
214	26
215	26
215	67
216	26
217	26
217	67
218	26
219	26
219	67
220	26
220	67
220	84
221	26
221	67
222	26
223	26
223	67
224	82
224	114
225	43
225	26
225	67
226	26
226	67
226	8
226	81
226	56
226	94
227	9
227	22
227	66
227	27
227	87
228	7
228	66
228	27
228	87
229	6
229	27
229	87
230	82
231	26
231	87
231	97
232	26
232	87
232	55
233	26
233	87
233	106
234	105
234	2
235	105
235	2
235	16
236	105
236	2
237	26
237	87
237	106
238	26
238	67
239	26
239	67
239	82
240	26
240	67
241	26
241	67
241	82
242	26
242	67
242	65
243	101
244	101
245	104
246	97
246	86
247	104
248	67
248	27
249	69
250	69
251	73
252	26
252	67
253	69
254	69
255	73
256	73
257	26
257	87
257	106
258	69
259	26
259	87
259	106
260	26
260	87
260	106
261	28
261	3
262	28
262	3
262	87
262	106
263	28
263	3
264	28
264	3
264	87
264	106
265	26
265	3
265	87
265	106
266	26
266	87
266	106
267	26
267	87
267	106
268	69
269	26
269	87
269	106
270	26
270	87
270	106
271	26
271	67
272	26
273	26
273	67
274	26
275	109
275	77
276	109
276	116
277	105
277	116
277	15
277	49
278	105
278	116
278	15
278	49
278	3
279	105
279	15
279	116
279	49
279	3
280	109
280	105
280	117
281	105
282	20
282	97
282	59
283	26
284	26
285	26
286	26
286	67
287	26
287	67
287	81
288	93
288	81
288	13
288	27
288	68
289	93
289	81
289	13
289	27
289	68
290	93
290	81
290	13
290	27
290	68
291	81
292	81
293	81
294	81
295	109
295	116
296	82
296	109
296	100
297	82
297	97
297	109
297	100
298	82
298	109
298	100
298	84
299	82
300	82
301	82
302	82
303	82
304	82
305	82
305	97
306	97
306	59
306	27
306	87
307	82
307	97
307	59
307	27
307	87
308	6
308	26
308	87
308	82
308	97
309	6
309	26
309	64
309	67
309	82
310	6
310	26
310	64
310	87
310	82
311	6
311	64
311	82
312	26
312	64
312	87
312	82
313	26
313	67
314	71
315	71
316	26
316	67
317	26
317	67
317	83
318	71
319	71
320	71
321	26
321	67
321	82
322	26
322	67
322	82
323	96
323	24
323	28
323	67
323	62
324	28
324	67
324	54
325	28
325	67
325	84
326	28
326	67
326	81
327	28
327	67
327	81
328	95
328	19
328	28
328	67
328	58
329	28
329	87
329	51
330	28
330	3
331	28
331	3
332	28
333	26
333	68
333	84
333	106
333	99
334	26
334	31
334	87
335	26
335	31
335	87
335	102
336	26
336	33
336	87
336	102
336	21
336	98
336	60
337	26
337	32
337	87
337	21
337	98
337	60
338	20
338	97
338	59
339	109
339	106
340	108
340	106
341	105
341	16
341	48
342	105
342	3
342	27
342	68
343	26
343	3
344	26
345	26
345	31
345	87
346	109
347	26
347	67
348	26
348	31
348	67
349	26
349	67
349	98
350	26
350	87
350	97
350	60
351	109
351	117
352	109
352	117
353	109
354	109
354	74
355	109
355	32
355	87
356	15
356	105
356	46
356	87
357	25
357	105
357	49
357	87
358	109
358	15
358	105
358	49
359	108
359	15
359	49
359	105
360	111
360	87
360	14
360	46
360	105
361	111
361	106
362	105
362	3
362	50
363	105
363	3
363	29
364	105
364	3
364	29
365	26
365	32
365	67
365	3
365	106
366	26
366	32
366	67
367	26
367	33
367	67
368	26
368	67
369	26
369	67
369	46
370	26
370	67
371	26
371	31
371	67
372	26
372	31
372	87
373	26
373	31
373	64
373	87
374	26
374	31
374	87
375	26
375	87
375	64
376	26
376	87
377	26
377	32
377	67
378	17
378	26
378	32
378	67
379	17
379	26
379	32
379	63
379	67
380	17
380	26
380	32
380	67
381	26
381	87
381	56
382	105
383	105
383	49
383	15
384	105
385	105
385	109
386	105
386	111
387	109
387	105
388	105
388	111
389	108
389	15
389	49
389	105
390	15
390	105
390	108
390	49
391	105
391	16
391	48
391	27
392	105
392	27
393	105
393	3
394	105
394	3
394	5
395	26
395	67
395	105
396	26
396	88
396	106
397	20
397	26
397	88
397	97
397	59
397	106
398	20
398	97
398	59
398	26
398	88
399	26
399	31
399	88
400	26
400	31
400	88
401	31
402	26
402	88
402	101
402	102
403	26
403	32
403	88
404	26
404	32
404	67
404	52
405	109
405	15
405	49
406	105
406	15
406	49
406	45
407	32
407	105
407	45
407	47
408	32
408	108
408	85
408	47
409	32
409	108
409	85
409	47
410	32
410	85
410	115
411	26
411	32
411	88
411	85
411	57
412	26
412	32
412	88
412	85
412	98
413	26
413	32
413	67
413	47
414	26
414	67
414	47
415	20
415	97
415	59
416	109
417	32
417	109
418	109
419	105
420	105
420	53
421	105
422	105
422	15
422	49
423	32
423	102
423	106
424	26
424	34
424	102
424	67
425	26
425	34
425	67
426	26
426	34
426	67
427	26
427	31
427	67
428	20
428	26
428	31
428	88
428	59
429	26
429	31
429	67
430	26
430	34
430	67
431	31
431	81
432	31
432	81
433	31
433	81
433	44
434	20
434	26
434	67
434	59
435	26
435	67
436	26
436	88
437	20
437	26
437	88
437	97
437	59
438	26
438	67
439	26
439	12
439	67
440	26
440	67
441	20
441	97
441	59
442	2
442	4
443	105
443	3
443	16
444	108
445	12
445	108
446	12
446	107
447	32
448	26
448	12
448	67
448	45
449	17
449	26
449	12
449	67
449	45
450	26
450	32
450	12
450	67
451	30
451	32
451	67
452	26
452	32
452	67
452	23
453	26
453	32
453	67
453	23
454	32
454	82
454	23
455	32
455	82
455	23
456	32
456	82
456	44
457	26
457	67
457	44
457	47
458	20
458	97
458	59
459	20
459	97
459	59
460	26
460	108
460	51
461	26
461	12
461	108
461	51
462	26
462	12
462	108
463	26
463	108
464	105
465	105
465	54
466	105
466	47
467	6
467	105
468	15
468	6
468	105
468	49
469	20
469	26
469	67
469	59
470	26
470	67
471	26
471	67
472	31
472	56
472	18
473	26
473	67
473	18
474	26
474	67
474	18
475	22
475	26
475	31
475	67
475	81
475	61
475	18
476	101
476	102
476	26
476	31
476	67
476	84
477	26
477	31
477	101
477	102
477	67
478	26
478	32
478	88
478	97
479	67
479	80
479	108
479	114
480	2
480	4
481	26
481	32
481	67
482	26
482	32
482	67
483	26
483	32
483	67
484	26
484	32
484	67
485	20
485	59
486	26
486	32
486	67
486	83
487	26
487	67
488	20
488	59
489	20
489	12
489	59
490	20
490	26
490	12
490	67
490	59
491	20
491	26
491	12
491	67
491	59
492	20
492	26
492	12
492	67
492	59
493	20
493	26
493	67
493	59
494	20
494	59
494	26
494	12
494	67
494	105
495	26
495	12
495	67
495	105
496	26
496	12
496	88
497	14
497	46
497	12
497	26
497	88
498	67
498	80
498	108
498	114
498	12
499	26
499	31
499	67
499	81
500	26
500	31
500	67
500	101
500	102
500	57
501	97
501	105
501	3
501	5
501	57
502	2
502	4
502	98
502	57
503	105
503	3
503	5
503	98
503	57
504	97
504	105
504	3
504	5
504	57
505	26
505	32
505	67
505	82
505	97
505	57
506	26
506	32
506	67
506	97
507	20
507	97
507	59
508	26
508	67
508	97
508	105
509	15
509	105
509	49
509	98
510	26
510	67
510	97
510	105
511	26
511	67
511	97
511	105
511	108
511	114
512	20
512	97
512	59
513	26
513	67
513	97
514	26
514	67
514	97
514	59
515	20
515	97
515	59
516	26
516	67
516	97
516	59
517	20
517	97
517	59
518	20
518	12
518	59
519	20
519	12
519	59
520	20
520	12
520	59
521	26
521	12
521	67
521	3
522	31
522	81
522	108
522	114
522	98
523	31
523	81
523	108
523	114
523	98
524	15
524	26
524	12
524	67
524	97
525	2
525	4
525	98
526	97
526	105
526	3
526	5
527	26
527	67
527	97
527	105
528	20
528	97
528	59
529	26
529	67
529	97
529	105
530	20
530	26
530	67
530	97
530	59
531	26
531	31
531	67
531	81
532	20
532	97
532	59
533	26
533	31
533	12
533	67
534	26
534	31
534	12
534	67
535	26
535	31
535	12
535	67
536	26
536	12
536	67
537	20
537	12
537	59
538	20
538	97
538	59
539	20
539	12
539	59
540	20
540	59
541	26
541	12
541	67
541	80
541	108
541	114
542	20
542	97
542	59
543	26
543	32
543	67
543	97
544	26
544	32
544	67
544	97
544	59
545	20
545	97
545	59
546	26
546	67
546	106
547	26
547	31
547	67
547	106
548	20
548	31
548	59
549	26
549	31
549	67
549	105
549	108
549	114
550	26
550	67
550	105
551	97
551	105
551	3
551	5
552	2
552	4
552	98
553	20
553	97
553	59
554	26
554	67
554	82
554	97
554	108
554	114
555	26
555	32
555	67
555	97
556	1
\.


--
-- TOC entry 2024 (class 0 OID 42790)
-- Dependencies: 1602
-- Data for Name: servicio_licencia; Type: TABLE DATA; Schema: public; Owner: -
--

COPY servicio_licencia (id_servicio, id_licencia, id_horario) FROM stdin;
\.


--
-- TOC entry 2021 (class 0 OID 42751)
-- Dependencies: 1597
-- Data for Name: servicio_operador; Type: TABLE DATA; Schema: public; Owner: -
--

COPY servicio_operador (id_operador, id_servicio) FROM stdin;
\.


--
-- TOC entry 1918 (class 2606 OID 42621)
-- Dependencies: 1580 1580
-- Name: banda_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY banda
    ADD CONSTRAINT banda_pk PRIMARY KEY (id_banda);


--
-- TOC entry 1954 (class 2606 OID 42830)
-- Dependencies: 1605 1605 1605
-- Name: canal_licencia_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY canal_licencia
    ADD CONSTRAINT canal_licencia_pk PRIMARY KEY (id_canal, id_licencia);


--
-- TOC entry 1952 (class 2606 OID 42820)
-- Dependencies: 1604 1604
-- Name: canal_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY canal
    ADD CONSTRAINT canal_pk PRIMARY KEY (id_canal);


--
-- TOC entry 1958 (class 2606 OID 42856)
-- Dependencies: 1608 1608 1608
-- Name: comentario_canal_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY comentario_canal
    ADD CONSTRAINT comentario_canal_pk PRIMARY KEY (id_comentario, id_canal);


--
-- TOC entry 1956 (class 2606 OID 42851)
-- Dependencies: 1607 1607
-- Name: comentario_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT comentario_pk PRIMARY KEY (id_comentario);


--
-- TOC entry 1934 (class 2606 OID 42695)
-- Dependencies: 1589 1589
-- Name: departamento_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT departamento_pk PRIMARY KEY (id_departamento);


--
-- TOC entry 1960 (class 2606 OID 42877)
-- Dependencies: 1610 1610
-- Name: evento_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT evento_pk PRIMARY KEY (id_evento);


--
-- TOC entry 1972 (class 2606 OID 42955)
-- Dependencies: 1617 1617 1617 1617
-- Name: excepcion_municipio_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY excepcion_municipio
    ADD CONSTRAINT excepcion_municipio_pk PRIMARY KEY (id_excepcion, id_municipio, id_canal);


--
-- TOC entry 1970 (class 2606 OID 42945)
-- Dependencies: 1616 1616
-- Name: excepcion_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY excepcion
    ADD CONSTRAINT excepcion_pk PRIMARY KEY (id_excepcion);


--
-- TOC entry 1938 (class 2606 OID 42724)
-- Dependencies: 1593 1593
-- Name: formula_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY formula
    ADD CONSTRAINT formula_pk PRIMARY KEY (id_formula);


--
-- TOC entry 1940 (class 2606 OID 42729)
-- Dependencies: 1594 1594 1594
-- Name: formula_servicio_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY formula_servicio
    ADD CONSTRAINT formula_servicio_pk PRIMARY KEY (id_formula, id_servicio);


--
-- TOC entry 1946 (class 2606 OID 42773)
-- Dependencies: 1599 1599
-- Name: horario_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY horario
    ADD CONSTRAINT horario_pk PRIMARY KEY (id_horario);


--
-- TOC entry 1966 (class 2606 OID 42912)
-- Dependencies: 1613 1613 1613
-- Name: licencia_departamental_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY licencia_departamental
    ADD CONSTRAINT licencia_departamental_pk PRIMARY KEY (id_licencia, id_departamento);


--
-- TOC entry 1968 (class 2606 OID 42927)
-- Dependencies: 1614 1614 1614
-- Name: licencia_municipal_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY licencia_municipal
    ADD CONSTRAINT licencia_municipal_pk PRIMARY KEY (id_licencia, id_municipio);


--
-- TOC entry 1964 (class 2606 OID 42902)
-- Dependencies: 1612 1612
-- Name: licencia_nacional_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY licencia_nacional
    ADD CONSTRAINT licencia_nacional_pk PRIMARY KEY (id_licencia);


--
-- TOC entry 1948 (class 2606 OID 42784)
-- Dependencies: 1601 1601
-- Name: licencia_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY licencia
    ADD CONSTRAINT licencia_pk PRIMARY KEY (id_licencia);


--
-- TOC entry 1962 (class 2606 OID 42887)
-- Dependencies: 1611 1611 1611
-- Name: licencia_regional_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY licencia_regional
    ADD CONSTRAINT licencia_regional_pk PRIMARY KEY (id_licencia, id_region);


--
-- TOC entry 1936 (class 2606 OID 42708)
-- Dependencies: 1591 1591
-- Name: municipio_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_pk PRIMARY KEY (id_municipio);


--
-- TOC entry 1922 (class 2606 OID 42641)
-- Dependencies: 1582 1582 1582
-- Name: nota_internacional_banda_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nota_internacional_banda
    ADD CONSTRAINT nota_internacional_banda_pk PRIMARY KEY (id_banda, id_nota_internacional);


--
-- TOC entry 1914 (class 2606 OID 42599)
-- Dependencies: 1576 1576
-- Name: nota_internacional_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nota_internacional
    ADD CONSTRAINT nota_internacional_pk PRIMARY KEY (id_nota_internacional);


--
-- TOC entry 1920 (class 2606 OID 42626)
-- Dependencies: 1581 1581 1581
-- Name: nota_nacional_banda_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nota_nacional_banda
    ADD CONSTRAINT nota_nacional_banda_pk PRIMARY KEY (id_banda, id_nota_nacional);


--
-- TOC entry 1916 (class 2606 OID 42610)
-- Dependencies: 1578 1578
-- Name: nota_nacional_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY nota_nacional
    ADD CONSTRAINT nota_nacional_pk PRIMARY KEY (id_nota_nacional);


--
-- TOC entry 1942 (class 2606 OID 42750)
-- Dependencies: 1596 1596
-- Name: operador_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY operador
    ADD CONSTRAINT operador_pk PRIMARY KEY (id_operador);


--
-- TOC entry 1912 (class 2606 OID 42588)
-- Dependencies: 1574 1574
-- Name: rango_frecuencias_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rango_frecuencias
    ADD CONSTRAINT rango_frecuencias_pk PRIMARY KEY (id_frecuencias);


--
-- TOC entry 1930 (class 2606 OID 42685)
-- Dependencies: 1587 1587
-- Name: region_nombre_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_nombre_key UNIQUE (nombre);


--
-- TOC entry 1932 (class 2606 OID 42687)
-- Dependencies: 1587 1587
-- Name: region_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY region
    ADD CONSTRAINT region_pk PRIMARY KEY (id_region);


--
-- TOC entry 1928 (class 2606 OID 42666)
-- Dependencies: 1585 1585 1585
-- Name: servicio_banda_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servicio_banda
    ADD CONSTRAINT servicio_banda_pk PRIMARY KEY (id_banda, id_servicio);


--
-- TOC entry 1950 (class 2606 OID 42794)
-- Dependencies: 1602 1602 1602
-- Name: servicio_licencia_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servicio_licencia
    ADD CONSTRAINT servicio_licencia_pk PRIMARY KEY (id_servicio, id_licencia);


--
-- TOC entry 1924 (class 2606 OID 42659)
-- Dependencies: 1584 1584
-- Name: servicio_nombre_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servicio
    ADD CONSTRAINT servicio_nombre_key UNIQUE (nombre);


--
-- TOC entry 1944 (class 2606 OID 42755)
-- Dependencies: 1597 1597 1597
-- Name: servicio_operador_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servicio_operador
    ADD CONSTRAINT servicio_operador_pk PRIMARY KEY (id_operador, id_servicio);


--
-- TOC entry 1926 (class 2606 OID 42661)
-- Dependencies: 1584 1584
-- Name: servicio_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servicio
    ADD CONSTRAINT servicio_pk PRIMARY KEY (id_servicio);


--
-- TOC entry 1989 (class 2606 OID 42821)
-- Dependencies: 1604 1584 1925
-- Name: canal_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY canal
    ADD CONSTRAINT canal_fk FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);


--
-- TOC entry 1991 (class 2606 OID 42831)
-- Dependencies: 1951 1605 1604
-- Name: canal_licencia_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY canal_licencia
    ADD CONSTRAINT canal_licencia_fk_1 FOREIGN KEY (id_canal) REFERENCES canal(id_canal);


--
-- TOC entry 1990 (class 2606 OID 42836)
-- Dependencies: 1601 1947 1605
-- Name: canal_licencia_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY canal_licencia
    ADD CONSTRAINT canal_licencia_fk_2 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 1992 (class 2606 OID 42857)
-- Dependencies: 1607 1608 1955
-- Name: comentario_canal_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comentario_canal
    ADD CONSTRAINT comentario_canal_fk_1 FOREIGN KEY (id_comentario) REFERENCES comentario(id_comentario);


--
-- TOC entry 1993 (class 2606 OID 42862)
-- Dependencies: 1604 1608 1951
-- Name: comentario_canal_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comentario_canal
    ADD CONSTRAINT comentario_canal_fk_2 FOREIGN KEY (id_canal) REFERENCES canal(id_canal);


--
-- TOC entry 1979 (class 2606 OID 42696)
-- Dependencies: 1931 1587 1589
-- Name: departamento_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT departamento_fk FOREIGN KEY (id_region) REFERENCES region(id_region);


--
-- TOC entry 1994 (class 2606 OID 42878)
-- Dependencies: 1610 1947 1601
-- Name: evento_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT evento_fk FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 2002 (class 2606 OID 42946)
-- Dependencies: 1947 1601 1616
-- Name: excepcion_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY excepcion
    ADD CONSTRAINT excepcion_fk FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 2003 (class 2606 OID 42956)
-- Dependencies: 1617 1969 1616
-- Name: excepcion_municipio_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY excepcion_municipio
    ADD CONSTRAINT excepcion_municipio_fk_1 FOREIGN KEY (id_excepcion) REFERENCES excepcion(id_excepcion);


--
-- TOC entry 2004 (class 2606 OID 42961)
-- Dependencies: 1591 1935 1617
-- Name: excepcion_municipio_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY excepcion_municipio
    ADD CONSTRAINT excepcion_municipio_fk_2 FOREIGN KEY (id_municipio) REFERENCES municipio(id_municipio);


--
-- TOC entry 2005 (class 2606 OID 42966)
-- Dependencies: 1617 1604 1951
-- Name: excepcion_municipio_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY excepcion_municipio
    ADD CONSTRAINT excepcion_municipio_fk_3 FOREIGN KEY (id_canal) REFERENCES canal(id_canal);


--
-- TOC entry 1981 (class 2606 OID 42730)
-- Dependencies: 1593 1937 1594
-- Name: formula_servicio_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY formula_servicio
    ADD CONSTRAINT formula_servicio_fk_1 FOREIGN KEY (id_formula) REFERENCES formula(id_formula);


--
-- TOC entry 1982 (class 2606 OID 42735)
-- Dependencies: 1584 1594 1925
-- Name: formula_servicio_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY formula_servicio
    ADD CONSTRAINT formula_servicio_fk_2 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);


--
-- TOC entry 1998 (class 2606 OID 42913)
-- Dependencies: 1947 1601 1613
-- Name: licencia_departamental_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_departamental
    ADD CONSTRAINT licencia_departamental_fk_1 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 1999 (class 2606 OID 42918)
-- Dependencies: 1933 1589 1613
-- Name: licencia_departamental_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_departamental
    ADD CONSTRAINT licencia_departamental_fk_2 FOREIGN KEY (id_departamento) REFERENCES departamento(id_departamento);


--
-- TOC entry 1985 (class 2606 OID 42785)
-- Dependencies: 1601 1941 1596
-- Name: licencia_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia
    ADD CONSTRAINT licencia_fk FOREIGN KEY (id_operador) REFERENCES operador(id_operador);


--
-- TOC entry 2000 (class 2606 OID 42928)
-- Dependencies: 1947 1601 1614
-- Name: licencia_municipal_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_municipal
    ADD CONSTRAINT licencia_municipal_fk_1 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 2001 (class 2606 OID 42933)
-- Dependencies: 1935 1614 1591
-- Name: licencia_municipal_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_municipal
    ADD CONSTRAINT licencia_municipal_fk_2 FOREIGN KEY (id_municipio) REFERENCES municipio(id_municipio);


--
-- TOC entry 1995 (class 2606 OID 42888)
-- Dependencies: 1611 1947 1601
-- Name: licencia_regional_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_regional
    ADD CONSTRAINT licencia_regional_fk_1 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 1996 (class 2606 OID 42893)
-- Dependencies: 1931 1587 1611
-- Name: licencia_regional_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_regional
    ADD CONSTRAINT licencia_regional_fk_2 FOREIGN KEY (id_region) REFERENCES region(id_region);


--
-- TOC entry 1997 (class 2606 OID 42903)
-- Dependencies: 1947 1601 1612
-- Name: licenica_nacional_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY licencia_nacional
    ADD CONSTRAINT licenica_nacional_fk FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 1980 (class 2606 OID 42709)
-- Dependencies: 1589 1933 1591
-- Name: municipio_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_fk FOREIGN KEY (id_departamento) REFERENCES departamento(id_departamento);


--
-- TOC entry 1975 (class 2606 OID 42642)
-- Dependencies: 1913 1582 1576
-- Name: nota_internacional_banda_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY nota_internacional_banda
    ADD CONSTRAINT nota_internacional_banda_fk_1 FOREIGN KEY (id_nota_internacional) REFERENCES nota_internacional(id_nota_internacional);


--
-- TOC entry 1976 (class 2606 OID 42647)
-- Dependencies: 1917 1580 1582
-- Name: nota_internacional_banda_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY nota_internacional_banda
    ADD CONSTRAINT nota_internacional_banda_fk_2 FOREIGN KEY (id_banda) REFERENCES banda(id_banda);


--
-- TOC entry 1973 (class 2606 OID 42627)
-- Dependencies: 1915 1581 1578
-- Name: nota_nacional_banda_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY nota_nacional_banda
    ADD CONSTRAINT nota_nacional_banda_fk_1 FOREIGN KEY (id_nota_nacional) REFERENCES nota_nacional(id_nota_nacional);


--
-- TOC entry 1974 (class 2606 OID 42632)
-- Dependencies: 1581 1580 1917
-- Name: nota_nacional_banda_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY nota_nacional_banda
    ADD CONSTRAINT nota_nacional_banda_fk_2 FOREIGN KEY (id_banda) REFERENCES banda(id_banda);


--
-- TOC entry 1977 (class 2606 OID 42667)
-- Dependencies: 1585 1925 1584
-- Name: servicio_banda_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_banda
    ADD CONSTRAINT servicio_banda_fk_1 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);


--
-- TOC entry 1978 (class 2606 OID 42672)
-- Dependencies: 1580 1917 1585
-- Name: servicio_banda_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_banda
    ADD CONSTRAINT servicio_banda_fk_2 FOREIGN KEY (id_banda) REFERENCES banda(id_banda);


--
-- TOC entry 1986 (class 2606 OID 42795)
-- Dependencies: 1925 1602 1584
-- Name: servicio_licencia_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_licencia
    ADD CONSTRAINT servicio_licencia_fk_1 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);


--
-- TOC entry 1987 (class 2606 OID 42800)
-- Dependencies: 1602 1601 1947
-- Name: servicio_licencia_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_licencia
    ADD CONSTRAINT servicio_licencia_fk_2 FOREIGN KEY (id_licencia) REFERENCES licencia(id_licencia);


--
-- TOC entry 1988 (class 2606 OID 42805)
-- Dependencies: 1945 1599 1602
-- Name: servicio_licencia_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_licencia
    ADD CONSTRAINT servicio_licencia_fk_3 FOREIGN KEY (id_horario) REFERENCES horario(id_horario);


--
-- TOC entry 1983 (class 2606 OID 42756)
-- Dependencies: 1597 1596 1941
-- Name: servicio_operador_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_operador
    ADD CONSTRAINT servicio_operador_fk_1 FOREIGN KEY (id_operador) REFERENCES operador(id_operador);


--
-- TOC entry 1984 (class 2606 OID 42761)
-- Dependencies: 1584 1925 1597
-- Name: servicio_operador_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY servicio_operador
    ADD CONSTRAINT servicio_operador_fk_2 FOREIGN KEY (id_servicio) REFERENCES servicio(id_servicio);


--
-- TOC entry 2040 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-01-27 22:17:07 COT

--
-- PostgreSQL database dump complete
--

